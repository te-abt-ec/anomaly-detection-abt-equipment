# Anomaly detection LBDS manual

Previous readme [here](documents/old_documents/README2019.md), with possibly still relevant information.

Actual list of required fixes and features (2019-2020): [notes-2020.md](documents/old_documents/notes-2020.md)
For a snapshot of the repo with old (2019-2020) files, switch to the 'before-cleanup' branch (commit 7e5c0da0)

The new list of notes (2020-2021) can be found here: [notes-2021.md](notes-2021.md)

## Instructions

1) Query and preprocess the features using Spark with the [get_data_wrapper](src/spark/get_data_wrapper) script, see [src/spark/README.md](src/spark/README.md) for detailed instructions. 

2) Once generated, download the (csv) file manually from your CernBox (EOS) to the correct `./data-cern/ml-data/raw/<machine>/<year>/` folder while renaming it as stated in your config file (`local_preprocessing<raw_data_filename`).
If a wildcard (```{}```) is provided inside the filename, it will be filled in with the beam number by default.
If you want to import multiple input files, copy them in the correct year-subfolder in `./data-cern/ml-data/` (e.g. features from May to June `./data-cern/ml-data/2017/month_5-6`) and enable `multi_input` in the config.
Each input csv file should be put into a seperate "month" subfolder, they will get concatenated and sorted automatically. 
Make sure all input files have the same number of columns and column names (= same beam).
There is an example, pre-generated LBDS 2017 feature-set which can be downloaded as follows: `cd data-cern && ./download_features.sh`

3) With the features on disk, run as follows (pass the right config file) Note that a second preprocess stage is called locally, since some steps can be executed more efficiently locally than on Spark.
:
```bash
pipenv shell
python src/scripts/preprocess_features.py -c [config.yaml]
# run one of the below
python src/scripts/pipeline_ad.py -c [config.yaml]
python src/scripts/grid_search_ad.py -c [config.yaml]
```

To start the webapp:
```bash
python src/webapp/wsgi.py -c [config.yaml] <--debug>
```
and browse to http://127.0.0.1:8050/  
The --debug option launches the Dash app in debug mode which automatically updates any change in the code and has a less cluttered output.  
Click [here](src/webapp/README.md) a detailed webapp manual

## CERN lxplus instructions
Note that these cluster machines are not very performant for heavy jobs, on purpose, as explained at https://cern.service-now.com/service-portal/article.do?n=KB0003547.

SSH into lxplus, use tmux to keep your session. Best to ensure not losing your Kerberos token, cause of AFS dependencies:

```bash
ssh <user>@lxplus.cern.ch
k5reauth -x -i 3600 -f -- tmux
virtualenv -p `which python3` venv
source venv/bin/activate
pip install pipenv
pipenv install
```

### Using Spark for feature queries and preprocessing
See [src/spark/README.md](src/spark/README.md)
