CONFIG USER GUIDE

The configuration file is split up in different (chronological) parts:
- [machine](#machine)
- [spark](#spark)
- [local_preprocessing](#local_preprocessing)
- [grid_search](#grid_search)
- [pipeline](#pipeline)
- [webapp](#webapp)
- [live_evaluation](#live-evaluation)
- [analysis](#analysis)
- [feature selection](#feature-selection)

## machine
The global machine type to use <LBDS | MKI>

## spark
Contains all configurations related to the data querying and preprocessing done on the remote Spark pipeline.
Used when launching the [get_data_wrapper script](src/spark/get_data_wrapper)


- **time_interval**: <list of strings> List of the raw data time intervals to be queried and preprocessed. Format is ```'%Y-%m-%d %H:%M:%S.%f'```
This list can contain multiple intervals (even indices are start timestamps, uneven indices are end timestamps)
- **write_features_data**: <boolean> Whether to query/preprocess/write the features (raw data)
- **write_statemode_data**: <boolean> Whether to query/preprocess/write the STATE:MODE data.
This is only applicable to MKI. The STATE:MODE data needs to be dumped seperately before resampling
- **index**: <string> The raw data timestamps index
- **dataformat**: <string> The data format to use when writing the output to HDFS <'csv'>
- **beam_filter**: <boolean> Whether to filter the features on selected beam below
- **beam**: <int> The beam number <1 | 2>
- **beam_identification**: The keywords (string) that occur in the feature names to filter on beam
- **verbose**: <int> Write output logs verbose <0 | 1>
- **segmentation**: Segmentation information for the postprocessing
    - **type**: <string> <timestamp_based | statemode_based> Timestamp based creates a segment for each timestamp in the data (LBDS).
    Statemode based creates segments based on the STATE:MODE feature (MKI) obtained with below information.
    - **statemode_query_data**: <list of strings> The query for the STATE:MODE data
    - **columns_to_write_regex**: <string> The columns from this data to write to the output statemode file
    - **local_file**: <string> The filename of the statemode file to use locally
- **querying**: All information related to raw data querying
    - **timestamps**: <list of strings> The queries to get the IPOC timestamps to resample to (for both beams)
    - **systems**: <list of strings> The queries to get the raw features data for each
      - **<BEAM|BUNCH|IPOC|TSU|BEI|SCSS|BEIL|BEM>**
      	- combination of a valid CMW **deviceLike** and **propertyLike** pair; or a **variableLike** (ref. NXCALS manual)
        - multiple entries are allowed per system
- **preprocessing**: All information related to the part of the preprocessing that is done on Spark
(These are the operations that reduce the large dataset to a manageable sized dataset that can be used locally)
    - **drop_columns**: <list of strings> The features to drop for each system
    - **allowed_proc_types**: <list of strings> The allowed preprocessor types (DataProcessor.py class)
    - **array_handling**: <string> The method to use for arrays in the dataset. 'fst' transforms an array to the first value in that array
    - **data_processor_config**: <list of dicts> The preprocessing operations for each feature set in a (key, value) pair
    with key the function name of the operation and value the arguments to be passed to that function.

## local_preprocessing
All configuration for the local preprocessing. Used when launching the [local preprocessing](src/scripts/preprocess_features.py)

- **time_interval**: <list of strings> The time interval to preprocess features on (only 1 interval allowed). Format is ```'%Y-%m-%d %H:%M:%S.%f'```
- **raw_data_filename**: <string> The filename of the raw data located at ```/data-cern/ml-data/raw/[machine]/[year]/```. This is the output of the Spark pipeline.
- **train_data_filename**: <string> The filename of the training data/preprocessed data. This is the output of the local preprocessing step of the pipeline.
Written to ```/data-cern/ml-data/preprocessed/[machine]/[year]/```
- **multi_input**: <boolean> Whether to concatenate all features in submaps of ```/data-cern/ml-data/raw/[machine]/[year]/```. Crossing years is currently not supported.
Used when multiple intervals were used in the Spark script and multiple raw output files were generated (e.g. for each month).
- **db_type**: <string> The data type of the raw input data (old)
- **index**: <string> The index of the preprocessed output data. The index of the raw input data gets renamed to this index.
- **beam_filter**: <boolean> Whether to filter the features on beam
- **beam**: <int> The beam number to filter the features on <1 | 2>
- **parallel**: <boolean> Whether to preprocess multiprocessed (WIP) (old)
- **kind**: <string> The kind of input data (old)
- **logbook**: <string> The logbook filename
- **dump_colnames**: <boolean> Whether to write the column names inbetween the different preprocessing steps to a file (```data-cern/analysis/```).
Used for analysis purposes.
- **drop_columns**: <list of strings> The columns to drop for each feature set.
- **drop_columns_regex**: <list of strings> All columns in the mask of the regexes listed get dropped on top of the columns above.
- **keep_columns**: <boolean> Whether to only keep below columns.
- **keep_columns_regex**: <list of strings> The regexes to keep columns. After dropping columns, filtering on a certain subset can be done by for example
passing the regex 'SCSS_(\w|\d)*', which will only keep features of the SCSS system
- **filters**: The extreme value filters. Rows can be dropped/replaced by a min/max value in a certain interval when necessary (for e.g. unrealistic data)
    - **use_filters**: <boolean> Whether to use the filters
    - **filter_info**:
        - **regex**: <string> The mask of the features that will be affected by this filter
        - **value**: <int/float> The threshold value of this filter
        - **operator**: <string> The operator of this filter <NE | GE | LE | GT> These are the standard Python functions
        - **type**: <string> <'drop' | 'nan'> What to to when a value to filter is found. 'drop' drops the whole row. 'nan' replaces the value by 'nan' which will be forwardfilled/backfilled afterwards by values surrounding it.
    - **queries**: <dict of string:string> Regexes per system to do certain preprocess operations on (MKI) (old)
- **feature_engineering**:
    - **sw**: <boolean> Whether to construct sliding window features (all features)
    - **sw_size**: <int> The size of the sliding window
    - **fourier**: <boolean> Whether to construct Fourier features
    - **fourier_columns**: <dict of string:list of strings> The columns to construct Fourier features from

## grid_search
All information related to the parameter grid search. Used when launching a [grid search](src/scripts/grid_search_ad.py)

- **time_interval**: <list of strings> The date range to do the grid search over. Format is ```'%Y-%m-%d %H:%M:%S.%f'```
- **index**: <string> The input feature set (preprocessed) index column name
- **logbook**: <string> The logbook filename for postprocessing
- **logbook_type**: <string> The logbook type. 'anomaly' represents a logbook where every entry is an anomaly
'all' represents a logbook where not every entry is an anomaly (filtering necessary)
- **beam_filter**: <boolean> Whether to filter the features on beam
- **beam**: <int> The beam number to filter input features on <1 | 2>
- **output_filename**: <string> The filename suffix for the predictions csv file for the best result of the gridsearch (in ```/results/<ad_type>_results/[machine]/[year]/best/```)
- **max_runtime**: <int> The maximum runtime in seconds for a single pipeline run after which it is killed (and the next one is started)
- **preprocessing**: These preprocessing steps can only be done here because the chosen time_interval is necessary
    - **drop_low_variance_columns**: <boolean> Whether to drop features with a variance lower than the threshold
    - **variance_filtered_filename**: <string> The filename of the preprocessed input features after the variance filter.
    - **variance_threshold**: <float> All features with variance under this threshold get dropped
    - **features_to_keep_during_variance_regex**: <list of string> Regexes that are applied to determine the columns to keep through the variance filter
- **anomaly_detection**: All information related to the anomaly detection of the grid search
    - **anomaly_detector**: <string> The anomaly detector type <iforest | gmm | hbos>
    - next are the possible anomaly detector parameter configurations for each anomaly detector type
        - **scale_data**: <'standard' | 'none'>
        - **segment_score**: <'max' | 'top_k' | 'top_percentage'>
        - **feature_selection**: <string> Name of the feature set
        - **scoring**: <'auc' | 'auprg' | 'rank'> How the best performing pipeline is selected. 'auc' is the regular
            area under the precision-recall curve. 'auprg' is the area under the precision-recall-gain curve.
        - **threshold**: <float> the anomaly score threshold used to classify an instance as anomalous or normal behaviour
        - Hyperparameters of the anomaly detector
                **params**:
                    {
                    param1: val1,
                    param2: val2,
                    ...
                    paramN: valN
                    }
## pipeline
All information related to the standard pipeline. Used when launching the anomaly detection [pipeline](src/scripts/pipeline_ad.py)

- **time_interval**: <list of strings> The date range for the pipeline. Format is '%Y-%m-%d %H:%M:%S.%f'
- **index**: <string> The input feature set (preprocessed) index column name
- **logbook**: <string> The logbook filename for postprocessing
- **logbook_type**: <string> The logbook type. 'anomaly' represents a logbook where every entry is an anomaly
'all' represents a logbook where not every entry is an anomaly (filtering necessary)
- **beam_filter**: <boolean> Whether to filter the features on beam
- **beam**: <int> The beam number to filter input features on <1 | 2>
- **output_filename**: <string> The filename suffix for the predictions csv file for the result of the pipeline (in ```/results/<ad_type>_results/[machine]/[year]/[start_month]-[end_month]/```)
- **dump_colnames**: <boolean> Whether to write the feature names to a txt file inbetween pipeline steps (analysis purposes)
- **preprocessing**: These preprocessing steps can only be done here because the chosen time_interval is necessary
    - **use_cached_file**: <boolean> Whether to use the already existing variance filtered input features file in ```/data-cern/ml-data/pipeline/[machine]/[year]/```
    - **drop_low_variance_columns**: <boolean> Whether to drop features with a variance lower than the threshold
    - **variance_filtered_filename**: <string> The filename of the preprocessed input features after the variance filter.
    - **variance_threshold**: <float> All features with variance under this threshold get dropped
    - **features_to_keep_during_variance_regex**: <list of string> Regexes that are applied to determine the columns to keep through the variance filter
- **anomaly_detection**: All information related to the anomaly detection of the pipeline
    - **anomaly_detector**: <string> The anomaly detector type <iforest | gmm | hbos>
    - next are the anomaly detector parameters for each anomaly detector type with important parameters:
        - **scale_data**: <'standard' | 'none'>
        - **segment_score**: <'max' | 'top_k' | 'top_percentage'>
        - **threshold**: <float> the anomaly score threshold used to classify an instance as anomalous or normal behaviour
        - Hyperparameters of the anomaly detector
                **params**:
                    {
                    param1: val1,
                    param2: val2,
                    ...
                    paramN: valN
                    }
- **postprocessing**:
    - These are the logbook columnnames that are necessary for the postprocessing (segmentation)

## webapp
All configurations and defaults of the webapp. The [webapp](src/webapp/wsgi.py) can be run after at least running the pipeline or grid search (to create the predictions file-

- **input_features_type**: <string> <"raw" | "preprocessed" | "variance_filtered"> Default is (and should be) variance_filtered because these are the training data of the model.
"raw" is currently not supported because the raw data contains non-numerical data which can not be plotted.
- **show_fft_features**: <boolean> Whether to include fourier features.
- **show_SW_features**: <boolean> Whether to include sliding window features.
- **long_table_calc_disabled**: <boolean> To prevent the webapp from loading too long, computational intensive operations are disabled by default.
These are the histogram ranking calculations for 'all FP', 'all TN' and 'all'. Enabling them can lead to long waiting times (track progress in terminal output).
- **write_rankings_aggregated**: <boolean> Whether to write the calculated ```'all [prediction type]'``` histogram rankings to a csv file in ```/results/webapp_generated/rankings/histogram/[machine]/[year]/[start_month]-[end_month]/```
- **write_rankings_single**: <boolean> Whether to write the calculated ```'[prediction type]'``` histogram rankings for a single prediction to a csv in
```/results/webapp_generated/rankings/histogram/[machine]/[year]/[start_month]-[end_month]/single_predictions/```
- **defaults**:
    - **default_beam**: <int> The default selected beam <1 | 2>
    - **default_score_method**: <string> The default selected score method <'max' | 'top_k' | 'top_percentage'>
    - **shap_aggregator_method**: <string> The default SHAP value aggregator method when calculating SHAP values for multiple predictions. <'sum' | 'max' | 'min' | 'mean'>
    - **default_nb_bins**: <int> The default number of histogram bins
    - **default_nb_hours**: <int> The default number of time margin around the prediction timestamp when constructing the local histogram.
    - **default_min_freq**: <int> The default minimal frequency percentage. <0-100>
    Selected features must have a ranking with frequency less than (default_min_freq / 100)*nb_features_in_dataset
    - **default_data_1**: <string> The default first plotted feature
    - **default_data_2**: <string> The default second plotted feature
- **threshold_slider:
    - **min**: <float> the minimal threshold value <0-1>
    - **max**: <float> the maximal threshold value <0-1>
    - **step_size**: <float> the discrete step size for choosing a threshold value between min and max <0-1>
    - **initial_threshold_values_caching**: <boolean> Whether to cache the calculations done when selecting a certain threshold before launching the webapp (for a smoother webapp experience)

## live evaluation
Used when launching the [live evaluation](src/scripts/live_evaluation.py) script.

- **type**: <string> The type of live evaluation <'whole_year_before | 'same_period_year_before' | 'manual_date_range'>
- **time_interval_to_predict**: <list of strings> The time interval of the test dataset. Format is ```'%Y-%m-%d %H:%M:%S.%f'```
- **time_interval_to_train_manual**: <list of strings> The time interval of the train dataset (when doing manual live evaluation)


## analysis
Certain configurations for analysis parts (should be removed)

- **shap**:
    - **mode**: <string> <'pipeline' | 'grid_search'> The config to use when calculating SHAP values with /src/webapp/shap_values.py
    Select 'pipeline' if you last ran a pipeline and 'grid_search' if you last ran a grid search
    - **predictions**: <dict of string:boolean> Select what types of predictions to calculate SHAP values of.

## feature selection
These parameters are used in the [feature selection](src/scripts/feature_selection.py) script.
More information and a feature selection guide can be found in the feature selection part of the [scripts README](src/scripts/README.md).

- **generated_config_filename**: <string> The selected features get filled in the ```keep_columns_list``` attribute of the local_preprocessing.
To not confuse the original configuration file with the feature selection one, a new one is created in the same directory with this name.
- **type**: <string> <'SHAP' | 'histogram'> 
- **prediction_types**: <list of string> The prediction types <'TP' | 'FN' | 'FP' | 'TN'> that are plotted if ```plot_result``` is enabled.
- **method**: <string> The method of feature selection. More information in [feature selection readme](src/scripts/README.md)
- **nb_features_to_select**:
    - **type**: <string> <'absolute' | 'percentage'>
    - **value**: <int | float> depending on <'absolute' | 'percentage'>
- **add_other_magnets**: Whether to keep magnets grouped
- **plot_result**: Enable for a feature selection visualisation. Turn on/off prediction types with ```prediction_types```.
