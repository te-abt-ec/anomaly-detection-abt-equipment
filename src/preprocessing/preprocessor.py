from abc import ABC, abstractmethod
from preprocessing.preprocess_modules import *
from analysis.dump_cols import dump_cols_to_txt
from util import CERN_DATA_DIR
from datetime import datetime
from IO.IO import IO


class AbstractPreprocessor(ABC):
    def __init__(self, config):
        self.config = config
        # TODO add parameters from config as self parameters (like preprocessor type etc...)
        self.type = config["machine"].lower()
        self.io = IO(config, 'local_preprocessing')


    def get_args_from_config(self, args):
        """
        Get parameter values from config file
        :param config: yaml config file:
        :param args: list<String> of argument names
        """
        arg_conf = self.config['local_preprocessing']
        arg_vals = []
        for arg in args:
            try:
                arg_vals.append(arg_conf[arg])
            except KeyError as e:
                print("Argument {} not found, assigning value: None".format(e))
        return arg_vals

    @abstractmethod
    def preprocess(self):
        pass


class MkiPreprocessor(AbstractPreprocessor):
    def preprocess(self):
        args = ['raw_data_filename', 'train_data_filename', 'multi_input', 'beam', 'feature_engineering', 'time_interval', 'db_type', 'parallel', 'kind', 'host', 'logbook', 'dump_colnames']
        [raw_data_filename, train_data_filename, multi_input, beam, feature_engineering, time_interval, db_type, parallel, kind, host, logbook, dump_colnames] = self.get_args_from_config(args)
        start, end = time_interval

        # input and preparation
        concat_features(self.io, multi_input, raw_data_filename, db_type)
        db = get_db(self.io, raw_data_filename, host, db_type, logbook) #cant just use read_features_from_csv since mki seems to need slightly different handling

        # TODO: dump can become a class variable if the functions are moved into this class
        dump = None
        if dump_colnames:
            dump_path = CERN_DATA_DIR + '/analysis/dumps/preprocessing/'
            dump_cols_to_txt(db.db, "mki_1_raw_cols.txt", dump_path)
            dump = {"dump_colnames": dump_colnames, "path": dump_path}

        # operations
        all_regex = self.config['local_preprocessing']["filters"]['queries']['all'] #what does this do? Used to select all columns to filter object columns, regex can be modified in the future
        kits_regex = self.config['local_preprocessing']["filters"]['queries']['kits']
        # Drop any object columns
        method_ipoc = (lambda df, col: df.drop(col, axis=1), False)
        # Dictionaries are set as strings due to spark, take first element
        method_kits = (lambda df, col: df[col].apply(lambda x: x.split(',')[2]), True)
        db = handle_object_columns(db, [all_regex, kits_regex], [method_ipoc, method_kits])
        features = build_features_multiprocessed(db, beam, feature_engineering["sw_size"], start, end, parallel, self.config)
        features = apply_global_filters(features, self.config, dump) #removed filtering from get_db so put it here to load from config
        print(features.shape)
        # output
        write_features_to_file_simple(features, self.io, beam=beam, kind=kind, filename=train_data_filename)
        seg_info = self.config["spark"]['segmentation']
        build_state_mode_to_file(db, beam, start, end, seg_info['type'], filename=seg_info['local_file'])


class LbdsPreprocessor(AbstractPreprocessor):
    def preprocess(self):
        args = ['time_interval', 'raw_data_filename', 'train_data_filename', 'multi_input', 'db_type', 'dump_colnames', 'drop_columns', 'drop_columns_regex', 'keep_columns', 'keep_columns_regex', 'keep_columns_list', 'feature_engineering']
        [time_interval, raw_data_filename, train_data_filename, multi_input, db_type, dump_colnames, cols_to_drop, cols_to_drop_regexes, keep_columns, cols_to_keep_regexes, cols_to_keep, feature_engineering] = self.get_args_from_config(args)
        # input and preparation
        concat_features(self.io, multi_input, raw_data_filename, db_type)
        features = read_features_from_csv(self.io, raw_data_filename)
        features = set_features_index(features, index=util.INDEX_COL)
        features = sort_on_index(features, ascending=True)

        # TODO: dump can become a class variable if the functions are moved into this class
        dump = None
        if dump_colnames:
            dump_path = CERN_DATA_DIR + '/analysis/dumps/preprocessing/'
            dump_cols_to_txt(features, "lbds_1_raw_cols.txt", dump_path)
            dump = {"dump_colnames": dump_colnames, "path": dump_path}

        # operations
        features = features[time_interval[0]:time_interval[1]]
        features = drop_columns(features, cols_to_drop)
        features = drop_columns_regex(features, cols_to_drop_regexes)
        if keep_columns:
            features = keep_columns_list(features, cols_to_keep)
            features = keep_columns_regex(features, cols_to_keep_regexes)
        features = drop_nan_cols(features, dump)
        features = drop_object_and_ts_cols(features, dump)
        features = fill_missing_vals(features, dump)
        features = apply_global_filters(features, self.config, dump)

        # feature engineering
        if feature_engineering['sw']:
            features = add_sw_features(features,
                                       feature_engineering['sw_size'],
                                       feature_engineering['sw_columns'],
                                       float(self.config['pipeline']['preprocessing']['variance_threshold']),
                                       self.config["pipeline"]["preprocessing"]["features_to_keep_during_variance_regex"])

        # output
        features = rename_index(features, name='timestamps')
        features = rename_axis(features, name="series", axis="columns")
        if '{}' in train_data_filename:
            train_data_filename = train_data_filename.format(self.config['local_preprocessing']['beam'])
        backup_and_write(features, self.io, raw_data_filename, new_f_name=train_data_filename)
        assert safety_check(features), 'Columns with type object still exist after preprocessing'

        # no state_mode printing for LBDS (each entry is a valid "segment")
