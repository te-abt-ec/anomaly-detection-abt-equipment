import time


def variance_preprocessing(features, eps=0, cols_to_keep=None):
    print("Starting variance preprocessing of feature vectors")
    start_time = time.time()
    # Dropping nan columns already done in preprocessing of full sized feature vectors
    print('Columns before variance filter: {}'.format(features.shape[1]))
    print('Filtering columns with variance <= {}, keeping {} columns through the filter'.format(eps, len(cols_to_keep)))
    # Variance filtering has to be done in pipeline run because it is dependent on the timeframe
    # Features that are constant for the given timespan are not useful for the anomaly detector
    dropped_cols = []
    for col in features:
        if features[col].dtype == 'object': # Should all be gone already from the local preprocessing step
            print('Deleting column: \'{}\' with type \'object\''.format(col))
            features = features.drop(col, axis=1)
            dropped_cols.append(col)
        elif features[col].var() <= eps:
            if col not in cols_to_keep:
                features = features.drop(col, axis=1)
                dropped_cols.append(col)

    print('Dropped {} columns with variance <= {}'.format(len(dropped_cols), eps))
    #print('Dropped columns: {}'.format(dropped_cols))
    print('Remaining columns: {}'.format(features.shape[1]))
    print("Variance preprocessing done in {:.2f} seconds".format(time.time() - start_time))
    return features
