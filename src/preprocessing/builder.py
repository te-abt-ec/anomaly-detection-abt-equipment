import os
import multiprocessing
import numpy as np
import pandas as pd
import re
import time

#from preprocessing import filter_extremes  # doesn't exist anymore
from preprocessing.fourier_transform import _intensity_max_var, _intensity_max_all, _intensity_median, _frequency_max, \
    _frequency_percentage_intensity5, _frequency_percentage_intensity10, _frequency_percentage_intensity20, fft_ft
from preprocessing.sliding_window import sliding_window_mean_diff, sliding_window_sum
from util import log_and_print
import util


def intermediary_filename(month):
    return "features-month_{0}.csv".format(month)


def build_features(db, beam, sw_size, start, end, parallel, config=None):
    """
    Builds a set of features ready for the anomaly detection method of choice.
    :param db: CERNClient which is used to fetch the data for the features (if parallel is False),
               host for db (if parallel is True)
    :param beam: Specifies which LHC beam's data to use.
    :param sw_size: int, window size in seconds for the sliding window features.
    :param start: Start datetime for the DB queries.
    :param end: End datetime for the DB queries.
    :param parallel: bool, False means single-core and True means all cores
    :param config: config file for pipeline run
    :return: A dataframe containing the built features with a DatetimeIndex as the index.
    """
    assert beam in util.BEAMS
    start = pd.to_datetime(start)
    end = pd.to_datetime(end)

    apply_filters = True
    end_month = start
    months = int((end - start) / np.timedelta64(1, 'M')) + 1

    intervals = []
    for i in range(0, months):
        start_month = start + pd.DateOffset(months=i)
        end_month = end_month + pd.DateOffset(months=1)
        if not db.db.loc[start_month:end_month].shape[0]:
            continue

        # Subtract one day before start_month date for sliding window calculations
        start_month_sw = start_month - pd.DateOffset(days=1)

        # IPOC indices go out of sync at the end, don't go beyond this point
        if end_month > end:
            end_month = end

        intervals.append([apply_filters, db, end_month, i, beam, start, start_month, start_month_sw, sw_size, parallel, config])

    log_and_print("Building features for beam {} from {} to {}".format(beam, start, end))
    start_time = time.time()
    if parallel:
        pool = multiprocessing.Pool()
        res = pool.map(build_features_month, intervals)
        pool.close()
        pool.join()
    else:
        res = map(build_features_month, intervals)

    for original_length, feature_length in res:
        log_and_print('|| Original length: {}, current length {}'.format(original_length, feature_length))

    log_and_print("|| Concatenate data of all months")
    feature_filenames = [os.path.join(util.CERN_DATA_DIR, intermediary_filename(i)) for i in range(0, months)]
    all_months = [util.load_df_from_csv(filename) for filename in feature_filenames]
    features = pd.concat(all_months, sort=True)  # type: pd.DataFrame
    features = features.fillna(method='ffill').fillna(method='backfill')

    log_and_print('\n| Build features execution time: {:.1f} seconds\n'.format(time.time() - start_time))

    #if parallel:
    #    db = CERNMongoClient(host=db)
    safety_check_features(apply_filters, db, end, features, beam, start)

    return features


def build_features_month(params):
    """
    Builds a set of features for a certain period to a CSV file.
    This method should be used for e.g. 1 month. When using longer periods, a reasonable memory usage is not guaranteed.
    :param params: a list of parameters as built by build_features
    """
    apply_filters = params[0]
    db = params[1]
    end_month = params[2]
    month_i = params[3]
    beam = params[4]
    start = params[5]
    start_month = params[6]
    start_month_sw = params[7]
    sw_size = params[8]
    parallel = params[9]
    config = params[10]

    #if parallel:
    #    db = CERNMongoClient(host=params[1])

    if config['preprocessing']['db_type'] == 'json':
        queries = config['preprocessing']['queries']
        query_ipoc = queries['ipoc']
        query_state = queries['state']
        query_controller = queries['controller']
        query_continuous = queries['continuous']
        query_beam_intensity = queries['beam_intensity']
        query_bunch_length_mean = queries['bunch_length']

    else:
        query_ipoc = r"^MKI\.{ua_num}\.IPOC\.{magnets}B{beam}:.*".format(ua_num=util.beam_to_num(beam),
                                                                         magnets=util.MAGNETS, beam=beam)
        query_state = r"^MKI\.{ua_num}\.STATE:.*".format(ua_num=util.beam_to_num(beam))
        query_controller = r"^MKI\.{ua_num}\.F3\.CONTROLLER:.*".format(ua_num=util.beam_to_num(beam))
        query_continuous = r"^MKI\.{magnets}.*\.B{beam}:.*".format(magnets=util.MAGNETS, beam=beam)
        query_beam_intensity = r"^LHC\.BCTFR\.A6R4\.B{beam}:BEAM_INTENSITY".format(beam=beam)
        query_bunch_length_mean = r"^LHC\.BQM\.B{beam}:BUNCH_LENGTH_MEAN".format(beam=beam)

    print("| MONTH {}".format(month_i))
    features, ipoc_cols, original_length = ipoc_and_state_data(db, start_month, start_month_sw, end_month,
                                                               apply_filters, query_ipoc, query_state)
    features = controller_data(db, start_month_sw, end_month, features, ipoc_cols, query_controller)
    features = continuous_data(db, start, start_month_sw, apply_filters, end_month, features, query_continuous,
                               query_ipoc, sw_size)
    features = beam_data(db, start_month_sw, end_month, features, query_beam_intensity, sw_size)
    features = bunch_length_data(db, start_month_sw, end_month, features, query_bunch_length_mean, sw_size)
    #features = filter_extremes.filter_extremes(features) # doesn't exist anymore
    features.to_csv(os.path.join(util.CERN_DATA_DIR, intermediary_filename(month_i)))
    return original_length, len(features)


def safety_check_features(apply_filters, db, end, features, beam, start):
    """
    Apply some basic checks to verify that the built features are legal.
    """
    query_ipoc = r"^MKI\.{ua_num}\.IPOC\.{magnets}B{beam}:.*".format(ua_num=util.beam_to_num(beam),
                                                                     magnets=util.MAGNETS, beam=beam)

    # There should be no NaNs in the features
    assert features.equals(features.dropna()), "Unexpected NaN in features:\n {}\n{}" \
        .format(features[features.isnull().any(1)], features[features.isnull().any(1)].describe())

    # There should be no other timestamps than those of the IPOC data
    ipoc = db.query(query_ipoc, start, end, apply_filters)
    ipoc.index = ipoc.index.round("s")
    features_index = features.index.to_series()
    ipoc_index = ipoc.index.to_series()
    assert features_index.isin(ipoc_index).all(), "Features index is not a subset of IPOC index. New dates:\n {}" \
        .format(features[~features.index.isin(ipoc.index)])


def ipoc_and_state_data(db, start_month, start_month_sw, end_month, apply_filters, query_ipoc, query_state):
    print("|| IPOC and STATE data")
    month_ipoc = db.query(query_ipoc, start_month, end_month, filters=apply_filters)
    month_ipoc.index = month_ipoc.index.round("s")
    original_length = len(month_ipoc)

    month_state = db.query(query_state, start_month_sw, end_month)
    features = month_ipoc.join(month_state, how='outer')
    state_cols = [c for c in features.columns if re.match(query_state, c)]
    features[state_cols] = features[state_cols].ffill()
    ipoc_cols = [c for c in features.columns if re.match(query_ipoc, c)]
    features = features.dropna(axis=0, how='any', subset=ipoc_cols)
    return features, ipoc_cols, original_length


def controller_data(db, start_month_sw, end_month, features, ipoc_cols, query_controller):
    print("|| Controller data")
    month_controller = db.query(query_controller, start_month_sw, end_month)
    features = features.join(month_controller, how='outer')
    controller_cols = [c for c in features.columns if re.match(query_controller, c)]
    features[controller_cols] = features[controller_cols].ffill()
    features = features.dropna(axis=0, how='any', subset=ipoc_cols)
    return features


def continuous_data(db, start, start_month_sw, apply_filters, end_month, features, query_continuous, query_ipoc,
                    sw_size):
    print("|| Continuous data")
    month_cont = db.query(query_continuous, start_month_sw, end_month, filters=apply_filters, resample_to="1s")
    # Merge used because .join errors since there are no common values and now indexes can be specified
    features = pd.merge(features, month_cont, left_index=True, right_index=True)
    features = features.join(sliding_window_mean_diff(month_cont, "s", sw_size))
    features = features.join(sliding_window_sum(month_cont, "s", sw_size))

    print("|| Continuous data: FFT")
    fs = 1 / 60
    minute = 1
    month_cont_min = db.query(query_continuous, start_month_sw, end_month, filters=apply_filters,
                              resample_to=str(minute) + "min")
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_int_max_var', _intensity_max_var),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_int_max_all', _intensity_max_all),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_int_median', _intensity_median),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_max', _frequency_max),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_percentage_intensity5',
                                    _frequency_percentage_intensity5), how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_percentage_intensity10',
                                    _frequency_percentage_intensity10), how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_percentage_intensity20',
                                    _frequency_percentage_intensity20), how='outer')
    fft_cols = [c for c in features.columns if 'fft' in c]
    features[fft_cols] = features[fft_cols].ffill()
    ipoc_cols = [c for c in features.columns if re.match(query_ipoc, c)]
    features = features.dropna(axis=0, how='any', subset=ipoc_cols)
    return features


def beam_data(db, start_month_sw, end_month, features, query_beam_intensity, sw_size):
    print("|| Beam data")
    month_beam_intensity = db.query(query_beam_intensity, start_month_sw, end_month, resample_to="1s")
    features = features.join(month_beam_intensity)
    features = features.join(sliding_window_mean_diff(month_beam_intensity, "s", sw_size))
    return features


def bunch_length_data(db, start_month_sw, end_month, features, query_bunch_length_mean, sw_size):
    print("|| Bunch length data")
    month_bunch_length = db.query(query_bunch_length_mean, start_month_sw, end_month, resample_to="1s")
    features = features.join(month_bunch_length)
    features = features.join(sliding_window_mean_diff(month_bunch_length, "s", sw_size))
    return features


def build_features_to_file(db, beam, sliding_window_size, start, end, parallel=False, filename=None, kind='train', config=None):
    """
    Builds a set of features ready for the anomaly detection method of choice and stores them in a file.
    :param db: CERNClient which is used to fetch the data for the features.
    :param beam: Specifies which LHC beam's data to use.
    :param kind: Specifies whether built features are used for training or testing. Only influences the file name.
    :param sliding_window_size: int, window size in seconds for the sliding window features.
    :param start: Start datetime for the DB queries.
    :param end: End datetime for the DB queries.
    :return: The filename used for the new file.
    """
    if filename is None:
        filename = "features_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    features = build_features(db, beam, sliding_window_size, start, end, parallel, config)
    features.to_csv(os.path.join(util.CERN_DATA_DIR, filename))
    return filename


def load_features_from_file(beam, kind='train', filename=None, start=None, end=None):
    """
    Loads features from a file. If no filename is given, loads the latest features file for a given beam.
    A file of features can be created by `build_features_to_file`.
    :param filename: Filename of the file that contains the features
    :return: Dataframe of features.
    """
    if filename is None:
        filename = "features_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    df = util.load_df_from_csv(os.path.join(util.CERN_DATA_DIR, filename))
    if start or end is not None:
        df = df[start:end]
    return df


def build_state_mode(db, beam, start, end):
    state_mode = db.query('MKI.' + util.beam_to_num(beam) + '.STATE:MODE', start=start, end=end)
    return state_mode


def build_state_mode_to_file(db, beam, start, end, kind='test', filename=None):
    """
    Builds a set of features ready for the anomaly detection method of choice and stores them in a file.
    :param beam: Specifies which LHC beam's data to use.
    :param start: Start datetime for the DB queries.
    :param end: End datetime for the DB queries.
    :return: The filename used for the new file.
    """
    if filename is None:
        filename = "state_mode_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    state_mode = build_state_mode(db, beam, start, end)
    state_mode.to_csv(os.path.join(util.CERN_DATA_DIR, filename))
    return filename


def load_state_mode_from_file(beam, kind='test', filename=None, start=None, end=None):
    """
    Loads the STATE:MODE data from a file.
    :param filename: Filename of the file that contains the features
    :return: Dataframe of STATE:MODE data.
    """
    if filename is None:
        filename = "state_mode_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    df = util.load_df_from_csv(os.path.join(util.CERN_DATA_DIR, filename))
    if start or end is not None:
        df = df[start:end]
    return df


def build_logbook_to_file(db, beam, filename=None):
    if filename is None:
        filename = "logbook_" + util.beam_to_str(beam) + ".csv"
    logbook = db.query_elogbook(beam=beam)
    logbook.to_csv(os.path.join(util.CERN_DATA_DIR, filename))
    return filename


def load_logbook_from_file(beam, filename=None):
    if filename is None:
        filename = "logbook_" + util.beam_to_str(beam) + ".csv"
        filename = os.path.join(util.CERN_DATA_DIR, filename)
    return util.load_df_from_csv(filename)


def info(logbook):
    return logbook[logbook["TAG"] == "info"]


def intervention(logbook):
    return logbook[logbook["TAG"] == "intervention"]


def research(logbook):
    return logbook[logbook["TAG"] == "research"]


def fault(logbook):
    return logbook[logbook["TAG"] == "fault"]


def anomalies(logbook):
    return logbook[logbook["TAG"] == "anomaly"]
