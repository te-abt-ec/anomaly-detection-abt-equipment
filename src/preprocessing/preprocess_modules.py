import operator
import os
import multiprocessing
import re
import time
import math
from shutil import copyfile
from datetime import datetime

from database.CERNCsvClient import CERNCsvClient
from preprocessing.fourier_transform import _intensity_max_var, _intensity_max_all, _intensity_median, _frequency_max, \
    _frequency_percentage_intensity5, _frequency_percentage_intensity10, _frequency_percentage_intensity20, fft_ft
from preprocessing.sliding_window import sliding_window_mean_diff, sliding_window_sum
from util import log_and_print
import util
from preprocessing import variance_preprocessing as vprep
from analysis.dump_cols import dump_cols_to_txt, dump_cols_delta

import numpy as np
import pandas as pd


def intermediary_filename(month):
    return "features-month_{0}.csv".format(month)


def build_features_multiprocessed(db, beam, sw_size, start, end, parallel, config=None):
    """
    Builds a set of features ready for the anomaly detection method of choice.
    :param db: CERNClient which is used to fetch the data for the features (if parallel is False),
               host for db (if parallel is True)
    :param beam: Specifies which LHC beam's data to use.
    :param sw_size: int, window size in seconds for the sliding window features.
    :param start: Start datetime for the DB queries.
    :param end: End datetime for the DB queries.
    :param parallel: bool, False means single-core and True means all cores
    :param config: config file for pipeline run
    :return: A dataframe containing the built features with a DatetimeIndex as the index.
    """
    assert beam in util.BEAMS

    start = pd.to_datetime(start)
    end = pd.to_datetime(end)

    apply_filters = True
    end_month = start
    # no.timedelta64(1, 'M') is 30 days, so months with 31 days are seen as 2 months
    months = int((end - start) / np.timedelta64(1, 'M')) + 1

    # Keep track of what months to skip (if data is empty), initialization:
    months_to_skip = {}
    for i in range(0, months):
        months_to_skip[i] = False

    # Create an interval per month (that has features)
    intervals = []
    for i in range(0, months):
        start_month = start + pd.DateOffset(months=i)
        end_month = end_month + pd.DateOffset(months=1)

        # If no rows in month, continue
        if not db.db.loc[start_month:end_month].shape[0]:
            months_to_skip[i] = True
            continue

        # Subtract one day before start_month date for sliding window calculations
        start_month_sw = start_month - pd.DateOffset(days=1)

        # IPOC indices go out of sync at the end, don't go beyond this point
        if end_month > end:
            end_month = end

        intervals.append(
            [apply_filters, db, end_month, i, beam, start, start_month, start_month_sw, sw_size, parallel, config])

    if not (False in months_to_skip.values()):
        print("Skipping all months because they didn't contain any data. Did you pass the config dates correctly? Exiting...")
        exit()

    # Preprocess and write every interval to a seperate "month" file
    # TODO: parallelize this
    log_and_print("Building features for beam {} from {} to {}".format(beam, start, end))
    start_time = time.time()
    if parallel:
        pool = multiprocessing.Pool()
        res = pool.map(build_features_month, intervals)
        pool.close()
        pool.join()
    else:
        res = map(build_features_month, intervals)

    for original_length, feature_length in res:
        log_and_print('|| Original length: {}, current length {}'.format(original_length, feature_length))

    # Read the seperate "month" features files and concatenate them back to 1 file
    log_and_print("|| Concatenate data of all months")
    feature_filenames = [os.path.join(util.CERN_DATA_DIR, intermediary_filename(i)) for i in range(0, months) if not months_to_skip[i]]
    all_months = [util.load_df_from_csv(filename) for filename in feature_filenames]
    features = pd.concat(all_months, sort=True)  # type: pd.DataFrame
    features = features.fillna(method='ffill').fillna(method='backfill')

    log_and_print('\n| Build features execution time: {:.1f} seconds\n'.format(time.time() - start_time))

    safety_check_features(apply_filters, db, end, features, beam, start, config) #?????
    return features


def write_features_to_file_simple(features, io, filename, beam, kind):
    if filename is None:
        filename = "features_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    print("Writing features with dimensions {} to {}...".format(features.shape, filename), end='')
    io.write_to_csv(features, filename, io.preprocessed_data_path)
    print("done!")


def build_features_month(params):
    """
    Builds a set of features for a certain period to a CSV file.
    This method should be used for e.g. 1 month. When using longer periods, a reasonable memory usage is not guaranteed.
    :param params: a list of parameters as built by build_features
    """
    apply_filters = params[0]
    db = params[1]
    end_month = params[2]
    month_i = params[3]
    beam = params[4]
    start = params[5]
    start_month = params[6]
    start_month_sw = params[7]
    sw_size = params[8]
    parallel = params[9]
    config = params[10]

    l_prep = config["local_preprocessing"]

    queries = l_prep['filters']['queries']
    query_ipoc = queries['ipoc']
    query_state = queries['state']
    query_controller = queries['controller']
    query_continuous = queries['continuous']
    query_beam_intensity = queries['beam_intensity']
    query_bunch_length_mean = queries['bunch_length']


    print("| MONTH {}".format(month_i))
    features, ipoc_cols, original_length = ipoc_and_state_data(db, start_month, start_month_sw, end_month,
                                                               apply_filters, query_ipoc, query_state)
    features = controller_data(db, start_month_sw, end_month, features, ipoc_cols, query_controller)
    features = continuous_data(db, start, start_month_sw, apply_filters, end_month, features, query_continuous,
                               query_ipoc, sw_size)
    features = beam_data(db, start_month_sw, end_month, features, query_beam_intensity, sw_size)
    features = bunch_length_data(db, start_month_sw, end_month, features, query_bunch_length_mean, sw_size)
    features.to_csv(os.path.join(util.CERN_DATA_DIR, intermediary_filename(month_i)))
    return original_length, len(features)


def safety_check_features(apply_filters, db, end, features, beam, start, config):
    """
    Apply some basic checks to verify that the built features are legal.
    """
    l_prep = config["local_preprocessing"]
    if l_prep['db_type'] == 'json' or l_prep['db_type'] == 'csv':
        queries = l_prep["filters"]['queries']
        query_ipoc = queries['ipoc']
    else:
        query_ipoc = r"^MKI\.{ua_num}\.IPOC\.{magnets}B{beam}:.*".format(ua_num=util.beam_to_num(beam),
                                                                     magnets=util.MAGNETS, beam=beam)

    # There should be no NaNs in the features
    assert features.equals(features.dropna()), "Unexpected NaN in features:\n {}\n{}" \
        .format(features[features.isnull().any(1)], features[features.isnull().any(1)].describe())

    # There should be no other timestamps than those of the IPOC data
    ipoc = db.query(query_ipoc, start, end, apply_filters)
    ipoc.index = ipoc.index.round("s")
    features_index = features.index.to_series()
    ipoc_index = ipoc.index.to_series()
    assert features_index.isin(ipoc_index).all(), "Features index is not a subset of IPOC index. New dates:\n {}" \
        .format(features[~features.index.isin(ipoc.index)])



def ipoc_and_state_data(db, start_month, start_month_sw, end_month, apply_filters, query_ipoc, query_state):
    print("|| IPOC and STATE data")
    month_ipoc = db.query(query_ipoc, start_month, end_month, filters=apply_filters)
    month_ipoc.index = month_ipoc.index.round("s")
    original_length = len(month_ipoc)

    month_state = db.query(query_state, start_month_sw, end_month)  # 1 column
    features = month_ipoc.join(month_state, how='outer')
    state_cols = [c for c in features.columns if re.match(query_state, c)]
    features[state_cols] = features[state_cols].ffill()
    ipoc_cols = [c for c in features.columns if re.match(query_ipoc, c)]
    features = features.dropna(axis=0, how='any', subset=ipoc_cols)
    return features, ipoc_cols, original_length


def controller_data(db, start_month_sw, end_month, features, ipoc_cols, query_controller):
    print("|| Controller data")
    month_controller = db.query(query_controller, start_month_sw, end_month)
    features = features.join(month_controller, how='outer')
    controller_cols = [c for c in features.columns if re.match(query_controller, c)]
    features[controller_cols] = features[controller_cols].ffill()
    features = features.dropna(axis=0, how='any', subset=ipoc_cols)
    return features


def continuous_data(db, start, start_month_sw, apply_filters, end_month, features, query_continuous, query_ipoc,
                    sw_size):
    print("|| Continuous data")
    month_cont = db.query(query_continuous, start_month_sw, end_month, filters=apply_filters, resample_to="1s")
    # Merge used because .join errors since there are no common values and now indexes can be specified
    features = pd.merge(features, month_cont, left_index=True, right_index=True)
    features = features.join(sliding_window_mean_diff(month_cont, "s", sw_size))
    features = features.join(sliding_window_sum(month_cont, "s", sw_size))

    print("|| Continuous data: FFT")
    fs = 1 / 60
    minute = 1
    month_cont_min = db.query(query_continuous, start_month_sw, end_month, filters=apply_filters,
                              resample_to=str(minute) + "min")
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_int_max_var', _intensity_max_var),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_int_max_all', _intensity_max_all),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_int_median', _intensity_median),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_max', _frequency_max),
                             how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_percentage_intensity5',
                                    _frequency_percentage_intensity5), how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_percentage_intensity10',
                                    _frequency_percentage_intensity10), how='outer')
    features = features.join(fft_ft(month_cont_min, start, fs, '_fft_freq_percentage_intensity20',
                                    _frequency_percentage_intensity20), how='outer')
    fft_cols = [c for c in features.columns if 'fft' in c]
    features[fft_cols] = features[fft_cols].ffill()
    ipoc_cols = [c for c in features.columns if re.match(query_ipoc, c)]
    features = features.dropna(axis=0, how='any', subset=ipoc_cols)
    return features


def beam_data(db, start_month_sw, end_month, features, query_beam_intensity, sw_size):
    print("|| Beam data")
    month_beam_intensity = db.query(query_beam_intensity, start_month_sw, end_month, resample_to="1s")
    features = features.join(month_beam_intensity)
    features = features.join(sliding_window_mean_diff(month_beam_intensity, "s", sw_size))
    return features


def bunch_length_data(db, start_month_sw, end_month, features, query_bunch_length_mean, sw_size):
    print("|| Bunch length data")
    month_bunch_length = db.query(query_bunch_length_mean, start_month_sw, end_month, resample_to="1s")
    if month_bunch_length.empty:
        return features
    features = features.join(month_bunch_length)
    features = features.join(sliding_window_mean_diff(month_bunch_length, "s", sw_size))
    return features


def load_features_from_file(beam, kind='train', filename=None, start=None, end=None):
    """
    Loads features from a file. If no filename is given, loads the latest features file for a given beam.
    A file of features can be created by `build_features_to_file`.
    :param filename: Filename of the file that contains the features
    :return: Dataframe of features.
    """
    if filename is None:
        filename = "features_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    df = util.load_df_from_csv(os.path.join(util.CERN_DATA_DIR, filename))
    if start or end is not None:
        df = df[start:end]
    return df


def build_state_mode(db, beam, start, end):
    state_mode = db.query('SCSS_MKI\.' + util.beam_to_num(beam) + '\.GEN_first\(mode,false\)$', start=start, end=end)
    return state_mode


def build_state_mode_to_file(db, beam, start, end, seg_type, filename=None, kind='test'):
    """
    Builds a set of features with the timestamps and the STATE:MODE column and stores them in a file.
    :param beam: Specifies which LHC beam's data to use.
    :param start: Start datetime for the DB queries.
    :param end: End datetime for the DB queries.
    :return: The filename used for the new file.
    """
    if filename is None:
        filename = "state_mode_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    if "{}" in filename:
        filename = filename.format(util.beam_to_str(beam))
    if seg_type == 'statemode_based':
        # STATE:MODE file already exists, but some operations are still needed
        state_mode = get_statemode_data(filename, beam)
    else:
        # This is the old way, write statemode data to a file after spark resampling + local preprocessing
        # Generate timestamps based segmentation file from locally preprocessed data
        state_mode = build_state_mode(db, beam, start, end)
    print("Writing STATE_MODE with dimensions {} to {}...".format(state_mode.shape, filename), end='')
    state_mode.to_csv(os.path.join(util.CERN_DATA_DIR, filename))
    print("done!")
    return filename

def get_statemode_data(filename, beam):
    print("Reading STATE_MODE data from {}".format(filename))
    path = util.CERN_DATA_DIR
    data = path + '/' + filename
    statemode_db = pd.read_csv(data)

    print("Preprocessing STATE_MODE data")
    if 'acqStamp' in list(statemode_db.columns):
        statemode_db.set_index('acqStamp', inplace=True)
        statemode_db.index = pd.to_datetime(statemode_db.index, unit='s')
    else:
        statemode_db.set_index('timestamps', inplace=True)
    statemode_db.sort_index(inplace=True)
    # Rename the acqStamp column to timestamps and mode to the longer "Spark-type" column for consistency
    statemode_db.rename_axis('timestamps', inplace=True)
    statemode_db = statemode_db.rename(columns={'mode': 'SCSS_MKI.{}.GEN_first(mode,false)'.format(util.beam_to_num(beam))})
    # Forwardfill and backfill STATE:MODE data
    statemode_db = statemode_db.fillna(method='ffill').fillna(method='backfill')
    return statemode_db

def load_state_mode_from_file(beam, kind='test', filename=None, start=None, end=None):
    """
    Loads the STATE:MODE data from a file.
    :param filename: Filename of the file that contains the features
    :return: Dataframe of STATE:MODE data.
    """
    if filename is None:
        filename = "state_mode_" + util.beam_to_str(beam) + "_" + kind + ".csv"
    df = util.load_df_from_csv(os.path.join(util.CERN_DATA_DIR, filename))
    if start or end is not None:
        df = df[start:end]
    return df


def build_logbook_to_file(db, beam, filename=None):
    if filename is None:
        filename = "logbook_" + util.beam_to_str(beam) + ".csv"
    logbook = db.query_elogbook(beam=beam)
    logbook.to_csv(os.path.join(util.CERN_DATA_DIR, filename))
    return filename


def load_logbook_from_file(beam, filename=None):
    if filename is None:
        filename = "logbook_" + util.beam_to_str(beam) + ".csv"
        filename = os.path.join(util.CERN_DATA_DIR, filename)
    return util.load_df_from_csv(filename)


def info(logbook):
    return logbook[logbook["TAG"] == "info"]


def intervention(logbook):
    return logbook[logbook["TAG"] == "intervention"]


def research(logbook):
    return logbook[logbook["TAG"] == "research"]


def fault(logbook):
    return logbook[logbook["TAG"] == "fault"]


def anomalies(logbook):
    return logbook[logbook["TAG"] == "anomaly"]


def drop_nan_cols(df, dump):
    print('Dropping columns containing only missing values (NaN etc.)')
    cnames = df.columns.values.tolist()
    df = df.dropna(axis=1, how='all')
    dropped_cols = [c for c in cnames if not c in df.columns.values.tolist()]
    #print('\nDropped columns are {}'.format(
    #    [c for c in cnames if not c in df.columns.values.tolist()]))
    print("Dropped {} columns containing only missing values".format(len(dropped_cols)))
    print('Remaining columns: {}'.format(df.shape[1]))
    if dump and dump['dump_colnames']:
        dump_cols_to_txt(df, "lbds_2_nandropped_cols.txt", dump['path'])
        dump_cols_delta("lbds_1_raw_cols.txt", "lbds_2_nandropped_cols.txt", dump['path'])
    return df


def drop_object_and_ts_cols(df, dump):
    print('Filtering columns with non-numerical data')
    dropped_cols = []
    for col in df:
        if df[col].dtype == 'object':
            print('Deleting column: \'{}\' with type \'object\''.format(col))
            df = df.drop(col, axis=1)
            dropped_cols.append(col)
        elif 'timestamp' in col:
            # Any column except for acqStamp should be removed if it contains a timestamp (duplicate)
            print('Deleting column: \'{}\' with duplicate timestamp information'.format(col))
            df = df.drop(col, axis=1)
            dropped_cols.append(col)
    print('Dropped {} columns with non-numerical data'.format(len(dropped_cols)))
    #print('Dropped columns are {}'.format(dropped_cols))
    print('Remaining columns: {}'.format(df.shape[1]))
    if dump and dump['dump_colnames']:
        dump_cols_to_txt(df, "lbds_3_objectdropped_cols.txt", dump['path'])
        dump_cols_delta("lbds_2_nandropped_cols.txt", "lbds_3_objectdropped_cols.txt", dump['path'])
    return df


def fill_missing_vals(df, dump):
    """
    Forward fill columns that contain 'null' values.
    Should already be done on Spark, so no cols with missing values should be found.
    """
    cols_with_missing_values = []
    nans = df.isna()
    for col in nans:
        if True in nans[col].values:
            cols_with_missing_values.append(col)
    print('Forward-filling {} columns'.format(len(cols_with_missing_values)))
    #print('Forward-filling columns {}'.format(cols_with_missing_values))
    df = df.fillna(method='ffill')
    # additional backfill as a dirty fix for remaining first rows missing values
    df = df.fillna(method='bfill')
    print('Forward-filled {} columns'.format(len(cols_with_missing_values)))
    #print('\nNot filled columns are {}'.format(
    #    [c for c in df.columns.values.tolist() if c not in missing_values]))
    if dump and dump['dump_colnames']:
        dump_cols_to_txt(df, "lbds_4_missingfilled_cols.txt", dump['path'])
        dump_cols_delta("lbds_3_objectdropped_cols.txt", "lbds_4_missingfilled_cols.txt", dump['path'])
    return df

def apply_global_filters(df, cfg, dump):
    """
    Applies filters to the given dataset, gets necessary filter data from given config
    Applies extreme value filtering on the whole features dataset
    Args:
        df: the features data
        cfg: the parsed config
    """

    # Filter extreme values
    df = filter_extremes(df, cfg)

    if dump and dump['dump_colnames']:
        dump_cols_to_txt(df, "lbds_5_globalfiltered_cols.txt", dump['path'])
        dump_cols_delta("lbds_4_missingfilled_cols.txt", "lbds_5_globalfiltered_cols.txt", dump['path'])
    return df


def filter_extremes(df, cfg):
    """
    Filter incorrect/impossible measurements out of a dataframe. Values provided by CERN.
    For continuous data, cells that exceed allowed values are set to NaN. Then, those cells are filled (ffill,
    then backfill). The same cannot be done for IPOC data, so the rows are removed as a whole.

    NOTE: If IPOC data is being filtered, it is important that I_STRENGTH and T_DELAY measurements are in the DataFrame,
    as other wrong IPOC data measurements will be filtered out by the filter for I_STRENGTH and T_DELAY. This happens
    because all IPOC data is sampled at the same timestamps within one beam, and if data is incorrect for some of the
    measurements, it will be incorrect for I_STRENGTH and T_DELAY as well.

    :param data: DataFrame as given by query
    :type data: pd.DataFrame
    :return: filtered dataframe
    :rtype: pd.DataFrame
    """
    dimBefore = df.shape
    print("Filtering extreme values...")
    try:
        use_filters = cfg['local_preprocessing']['filters']['use_filters']
        filter_info = cfg['local_preprocessing']['filters']['filter_info']
    except KeyError as e:
        print("Config file is missing parameter: {}, skipping IPOC filters".format(e))
        return df
    if not use_filters:
        return df

    # deep copy because changing data by indexing can change the original DataFrame
    df = df.copy(deep=True)  # type: pd.DataFrame

    # Added before for-loop because columns don't change (change if this condition is violated)
    cols = list(df)
    for i, data_filter in enumerate(filter_info):
        regex = data_filter['regex']
        f_val = eval(str(data_filter['value']))
        method = data_filter['operator']
        f_type = data_filter['type']
        print("Rows before filter [{}]: {}".format(regex, df.shape[0]))

        # Set the function to the operator specified in config
        if method == 'GE':
            f = operator.ge
        elif method == 'GT':
            f = operator.gt
        elif method == 'LE':
            f = operator.le
        elif method == 'NE':
            f = operator.ne
        else:
            f = operator.lt

        # Filter affected columns on regex
        mask = util.get_mask_for_regex(cols, regex)
        for col in mask:
            if f_type == 'nan':
                # nans are used when working with intervals
                # Set all values of column 'col' where ~f (not f) applies to NaN
                # The inverse of f must be applied because f selects the interval itself and everything outside must be set to NaN
                df.loc[~f(df[col], f_val), col] = np.NaN
            elif f_type == 'drop':
                # If the operator is not an interval, we can just select the part of df where f applies, the rest will be dropped
                df = df.loc[f(df[col], f_val)]
            else: # default is 'drop'
                df = df.loc[f(df[col], f_val)]
        print("Rows after filter [{}]: {}".format(regex, df.shape[0]))

    # Drop columns that have no values at all (on which ffill and backfill doesn't work)
    df = df.dropna(axis=1, how='all')
    # ffill and backfill NaN's created by interval filtering
    df = df.fillna(method='ffill').fillna(method='backfill')

    print("Filtering extreme values: done!")
    print("Extreme values dropped: {} rows and {} columns".format(dimBefore[0] - df.shape[0],
                                                                   dimBefore[1] - df.shape[1]))
    return df


def backup_and_write(df, io, f_name, new_f_name=None):
    filename = f_name
    # setting index for dataframe to avoid writing default pandas index to file
    if util.INDEX_COL in df:
        print("Setting df index to {}".format(util.INDEX_COL))
        df = df.set_index(util.INDEX_COL)
    if not new_f_name:
        fp = io.preprocessed_data_path + '/' + f_name
        print("Creating a {} backup file".format(f_name))
        copyfile(fp, fp + '.backup')
    else:
        filename = new_f_name
    print('(Over)writing preprocessed feature vectors to: ' + io.preprocessed_data_path)
    io.write_to_csv(df, filename, io.preprocessed_data_path)


def get_db(io, fntrn, host, db_type, logbook):
    """
    Reads a csv and creates a local database from it to query data
    Args:
        fntrn: filename (without path)
        host: host
        db_type: type of the file
        logbook: filename of the logbook (without path)
    """
    if db_type == 'parallel':
        db = host
    elif db_type == "csv":
        path = io.raw_data_path
        data = path + fntrn
        logbook = util.CERN_DATA_DIR + '/' + logbook
        db = CERNCsvClient(data, logbook)
    else:
        print("Invalid db_type in config, use csv")
    return db

def read_features_from_csv(io, fn):
    try:
        path = io.raw_data_path
        if "{}" in fn:
            fn = fn.format(io.beam)
        fn = path + fn
        print("Reading in features from {}".format(fn))
        features = pd.read_csv(fn)
        print('Columns in feature vectors: {}'.format(features.shape[1]))
        print('Rows in feature vectors: {}'.format(features.shape[0]))
    except FileNotFoundError as file_not_found:
        print("Input features {} not found, exiting...".format(file_not_found.filename))
        exit()
    return features

def set_features_index(df, index='acqStamp'):
    """
    Sets the index of the dataframe to the given index name
    Converts the index to a datetime format if the index name supports this
    """
    df.set_index(index, inplace=True)
    if index is not ('acqStamp' or 'timestamps'):
        print("WARN: Cannot set the given index column to a datetime format, continuing without formatting...")
    else:
        df.index = pd.to_datetime(df.index, unit='s')
    return df

def sort_on_index(df, ascending=True):
    """
    Sorts the index of given dataframe
    """
    df.sort_index(inplace=True, ascending=ascending)
    return df

def rename_index(df, name='timestamps'):
    df.rename_axis('timestamps', inplace=True)
    return df

def rename_axis(df, name, axis):
    df.rename_axis(name, axis=axis, inplace=True)
    return df

def handle_object_columns(db, regexps, methods):
    print("Handling object columns...")
    features = db.db
    for regex, method in zip(regexps, methods):
        cols = list(features)
        search = lambda x: re.search(regex, x)
        mask = list(filter(search, cols))
        print(regex)
        for col in mask:
            #print(col)
            if features[col].dtype == 'object':
                m, col_type = method
                if col_type:
                    features[col] = m(features, col)
                else:
                    features = m(features, col)
    db.db = features
    print('done!')
    return db


def drop_columns(df, cols_dict):
    """
    Args:
        df: The dataframe
        cols_dict: Dict of [DATASET_TYPE]:[List<String>]
    """
    cols_to_drop = []
    if cols_dict is None:
        print("drop columns option not set, continuing without dropping...")
        return df
    for dataset_type, cols in cols_dict.items():
        if cols is not None:
            cols_to_drop.extend(cols)
    print("Current # columns: {}".format(len(df.columns)))
    cols_before = len(df.columns)
    cols_to_keep = [i for i in df.columns if i not in cols_to_drop]
    df = df[cols_to_keep]
    print("Dropped {} columns".format(cols_before - len(df.columns)))
    print("Columns left: {}".format(len(df.columns)))
    return df


def drop_columns_regex(df, regexes):
    if regexes is None or len(regexes) == 0:
        print("drop columns regexes not set, continuing without dropping...")
        return df
    cols_to_drop = util.get_columns_from_regexes(df.columns, regexes)
    cols_to_keep = [i for i in df.columns if i not in cols_to_drop]
    cols_before = len(df.columns)
    print("Current # columns: {}".format(cols_before))
    df = df[cols_to_keep]
    print("Dropping {} columns from regexes".format(cols_before - len(df.columns)))
    print("Columns left: {}".format(len(df.columns)))
    return df


def keep_columns_list(df, cols_to_keep):
    if cols_to_keep is None or len(cols_to_keep) == 0:
        return df
    nb_cols_before = len(df.columns)
    print("Current # columns: {}".format(nb_cols_before))
    df = df[cols_to_keep]
    print("Dropped {} columns when keeping columns".format(nb_cols_before - len(df.columns)))
    print("Columns left: {}".format(len(df.columns)))
    return df


def keep_columns_regex(df, regexes):
    cols_to_keep = util.get_columns_from_regexes(df.columns, regexes)
    cols_before = len(df.columns)
    print("Current # columns: {}".format(cols_before))
    df = df[cols_to_keep]
    print("Dropping {} columns from keep regexes".format(cols_before - len(df.columns)))
    print("Columns left: {}".format(len(df.columns)))
    return df


def map_to_float(x):
    if type(x) == str:
        x = x[1: -1]
        if x == 'false':
            return 0
        elif x == 'true':
            return 1
        else:
            return float(x)
    else:
        return x


def safety_check(df):
    print("Running safety check on data")
    for col in df:
        try:
            if df[col].type == 'object':
                return False
        except AttributeError:
            # ignore 'Series' object has no attribute 'type' , not fully understood (Pieter)
            pass
    return True


def add_sw_features(features, sw_size, sw_columns, eps, features_to_keep_regexes):
    print("Creating sliding window features...")
    if sw_columns["all"]:
        # remove columns that have low variance, no use in calculating SW for these features
        features_to_keep_variance = util.get_columns_from_regexes(features.columns, features_to_keep_regexes)
        df = vprep.variance_preprocessing(features, eps, cols_to_keep=features_to_keep_variance)
        features_to_sw = df.columns
        cols_before = len(df.columns)
    else:
        features_to_sw = []
        for i, (system_type, colns) in enumerate(sw_columns["systems"].items()):
            if colns is not None:
                 for sw_coln in colns:
                    for coln in features.columns:
                        if system_type in coln and sw_coln in coln:
                            features_to_sw.append(coln)
        cols_before = len(features.columns)
    print("Calculating SW for {} features".format(len(features_to_sw)))
    print("Columns before: {}".format(cols_before))
    start_time = datetime.now()
    # maximum amount of columns at the same time to avoid memory issues
    max_features = 10
    nb_iterations = math.ceil(len(features_to_sw) / max_features)
    for i in range(nb_iterations):
        print("Calculated features: {}/{}".format(i * max_features, len(features_to_sw)))
        if i == nb_iterations - 1:
            features_iter = features_to_sw[i * max_features:]
        else:
            features_iter = features_to_sw[i*max_features:(i+1)*max_features]
        if len(features_iter) != 0:
            to_sw_df = features[features_iter]
            features = features.join(sliding_window_mean_diff(to_sw_df, "s", sw_size))
        if i == nb_iterations - 1:
            print("Calculated features: {}/{}".format(len(features_to_sw), len(features_to_sw)))
    print("Columns after: {}".format(len(features.columns)))
    print("Time taken for SW calculation: {}".format(datetime.now() - start_time))
    return features


def concat_features(io, multi_input, filename, db_type):
    """
    Concatenates the features from each folder in /data-cern/ml-data/ and writes output to /data-cern/filename
    Currently only csv files are supported.
    Source: https://stackoverflow.com/questions/29206384/python-folder-names-in-the-directory
    Args:
        multi_input: Whether to concatenate or skip this function
        filename: The filename to write the output to
        db_type: The database type
    TODO: Date check/cutoff with start and end args
    """
    if multi_input and db_type == 'csv':
        # Fill in filename with beam
        if "{}" in filename:
            filename = filename.format(io.beam)

        # Get all paths to files to concat
        file_list = list()
        for f in get_filelist(io, io.start_year):
            file_list.append(f)

        f_path = io.raw_data_path
        if len(file_list) < 1:
            print("No data in {} to concatenate, turn off multi-input if all data is already in {}, Exiting...".format(f_path, f_path + filename))
            exit()

        # Concat data
        print("Concatenating the multi data input from {} into {}, this can take a while...".format(f_path, f_path + filename))
        all_data = [util.load_df_from_csv(filename) for filename in file_list]
        features = pd.concat(all_data, sort=True)
        features = features.fillna(method='ffill').fillna(method='backfill')

        # Write output
        output_path = os.path.join(f_path, filename)
        features.to_csv(os.path.join(output_path))
        print("Wrote {} features to {}".format(features.shape, output_path))

def get_filelist(io, year):
    file_list = list()
    for root, dirs, files in os.walk(io.raw_data_path):
        for name in dirs:
            r, d, filenames = list(os.walk(os.path.join(root, name)))[0]
            for n in filenames:
                if n != '_SUCCESS':
                    file_list.append(r + '/' + n)
    return file_list