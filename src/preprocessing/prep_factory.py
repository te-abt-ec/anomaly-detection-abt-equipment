from preprocessing.preprocessor import *


class PreprocessorFactory:
    @staticmethod
    def create(preprocessor, config):
        """
        :param preprocessor: The name of the preprocessor
        :param config: Config file containing the parameters for the preprocessing
        :return: An instance of the requested preprocessor
        """
        preprocessor = preprocessor.lower()

        if preprocessor == "mki":
            return MkiPreprocessor(config)
        elif preprocessor == "lbds":
            return LbdsPreprocessor(config)
        else:
            raise Exception("Unknown preprocessor: {}".format(preprocessor))
