import copy

import numpy as np
import pandas as pd

import util
from postprocessing.label import Label
from postprocessing.segment import Segment


def df_to_segments(df, state_mode):
    """
    Takes a DataFrame and splits it into a list of Segments. Splitting is based on the state of the MKI magnets.
    A segment represents a time interval in which the MKI magnets change from on (mode=1) to standby (mode=3).
    An LBDS segment is defined as the time interval between two consecutive XPocData logs
    :param df: DataFrame
    :param state_mode: The STATE:MODE data
    :return list of Segment
    """
    segments = _create_empty_segments(state_mode)
    segments = assign_tuples_to_segments(df, segments)
    segments = prune_empty_segments(segments)
    return segments


def _create_empty_segments(state_mode):
    """
    Takes a DataFrame of states and returns a List of segments. 
    A segment is created for each "on" state, followed by the first "standby", "off" or "faulty" state.
    Each segment will contain a minimum and maximum timestamp, but no tuples are associated with it.
    :param df: DataFrame
    :type df: pd.DataFrame
    :return list of Segment
    """
    segments = []
    started_segment = False
    if not len(state_mode.columns):
        # Timestamp based segmentation
        for index in state_mode.index:
            segments.append(Segment(ts_min=index, ts_max=index))
    else:
        # statemode based segmentation
        for index, row in state_mode.itertuples():
            if not started_segment:
                if row == 1:  # on
                    start = index
                    started_segment = True
            else:
                if row == 3 or row == 2 or row == 4:  # standby or off or faulty
                    segments.append(Segment(ts_min=start, ts_max=index))
                    started_segment = False
    return segments


def assign_tuples_to_segments(tuples, segments):
    """
    Takes a DataFrame of IPOC tuples (sorted order) and a List of empty segments.
    IPOC tuples will be assigned to a segment, which corresponds to their timestamp.
    """
    for s in segments:
        tuples_for_seg = tuples.loc[s.ts_min(): s.ts_max()]
        s.add_tuples(tuples_for_seg)
    return segments


def prune_empty_segments(segments):
    """
    Prune segments containing no tuples.
    """
    return [s for s in segments if len(s) != 0]


def set_ground_truth(segments, labels, id_field, date_field, fields):
    """
    Annotates segments with the ground truth using labels. If a segment lies near one of the given labels, it's
    is_anomalous flag is set to true.
    :param segments: list of Segments
    :param labels: pd.DataFrame of labels
    :return: list of annotated Segments
    """
    new_segments = copy.deepcopy(segments)
    for s in new_segments:
        # first() returns the first entry of a list of anomalous logbook timestamps which the current segment s is near to
        # The first label will be the representative anomaly for all logbook entries in this 12h range
        ts_near_label = util.first((ts for ts in labels.index if s.is_near(ts, "start")), None)

        # Set the segment anomalous label accordingly
        if ts_near_label:
            s.set_label(Label.create_from_series_row(id_field, date_field, labels[fields].loc[ts_near_label]))
        else:
            s.set_label(None)

    return new_segments


def print_segment_stats(segments):
    lengths = [len(s) for s in segments]
    anomalous_segments = [s for s in segments if s.is_labeled()]
    time_lengths = [s.length_time() for s in segments]

    print("IPOC SEGMENT STATS:")
    print("Number of segments:     {}".format(len(segments)))
    print("Anomalous segments:     {}".format(len(anomalous_segments)))
    print("Min segment length:     {}, {} points".format(min(time_lengths), min(lengths)))
    print("Mean segment length:    {}, {} points".format(np.mean(time_lengths).round('s'), np.mean(lengths, dtype=int)))
    print("Max segment length:     {}, {} points".format(max(time_lengths), max(lengths)))
    print()
