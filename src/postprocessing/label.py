class Label:
    def __init__(self, id, date, attributes):
        """
        :param attributes: Dictionary with key: attribute name, value: attribute value
        """
        self.id = id
        self.date = date
        self.attributes = attributes

    def __str__(self):
        return 'Label with attributes: {}'.format(self.attributes)

    def __eq__(self, other):
        return other == self.attributes

    @staticmethod
    def create_from_series_row(id, date, series_row):
        """
        Create a label from a Pandas Series
        :param series_row: Pandas Series containing label information
        """
        return Label(series_row[id], series_row[date], dict((n, val) for n, val in series_row.items()))
