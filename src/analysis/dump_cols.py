import os
import sys
from os import path
from pathlib import Path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import util


def dump_cols_to_txt(df, f_name, f_path):
    dump_list_to_txt(list(df.columns), f_name, f_path)


def dump_list_to_txt(lst, f_name, f_path):
    if not path.exists(f_path):
        Path(f_path).mkdir(parents=True, exist_ok=True)
    with open(f_path + f_name, 'w') as f:
        for item in lst:
            f.write("%s\n" % item)


def read_cols_from_txt(f_name, f_path):
    if not path.exists(f_path):
        Path(f_path).mkdir(parents=True, exist_ok=True)
    opened_file = open(f_path + f_name, "r")
    read_lines = opened_file.readlines()
    return [c.split("\n")[0] for c in read_lines]


def calc_cols_delta(f_name1, f_name2, f_path):
    """
    Calculates the cols in 1 that are not in 2
    """
    cols1 = read_cols_from_txt(f_name1, f_path)
    cols2 = read_cols_from_txt(f_name2, f_path)
    return [c for c in cols1 if c not in cols2]


def dump_cols_delta(f_name1, f_name2, f_path):
    delta = calc_cols_delta(f_name1, f_name2, f_path)
    dump_list_to_txt(delta, "{} without {}".format(f_name1, f_name2), f_path + "deltas/")


def generate_single_file(f_path):
    global_list = []
    f_name = "global_column_list.txt"
    for root, dirs, files in os.walk(f_path):
        for f in sorted(files):
            if f != f_name:
                global_list += [f + ":", "\n"]
                global_list = global_list + read_cols_from_txt(f, f_path)
                global_list += ["\n"] * 2
        break
    dump_list_to_txt(global_list, f_name, f_path)


if __name__ == '__main__':
    base_f_path = util.CERN_DATA_DIR + '/analysis/dumps/'
    dump_cols_delta("LBDS_1_pipeline_input.txt", "LBDS_2_after_beam_filter.txt", base_f_path + 'pipeline/')
    dump_cols_delta("LBDS_2_after_beam_filter.txt", "LBDS_3_after_variance_filter.txt", base_f_path + 'pipeline/')

    generate_single_file(base_f_path + 'pipeline/deltas/')
