import sys
import os
import argparse

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import util
from IO.IO import IO


def update_drop_cols_config(io, colns):
    """
    Updates the local_preprocessing:drop_columns attribute of the config yaml file
    Args:
         io: The dataframe
         colns: The column names
    """
    try:
        print("Updating the dropped columns in config")
        for dataset_type in util.DATASET_TYPES[io.machine]:
            io.config["local_preprocessing"]["drop_columns"][dataset_type] = []
            for coln in colns:
                if dataset_type in coln:
                    try:
                        curr_cols_list = io.config["local_preprocessing"]["drop_columns"][dataset_type]
                        if curr_cols_list is None:
                            io.config["local_preprocessing"]["drop_columns"][dataset_type] = [coln]
                        else:
                            io.config["local_preprocessing"]["drop_columns"][dataset_type].append(coln)
                    except KeyError:
                        io.config["local_preprocessing"]["drop_columns"][dataset_type] = [coln]
        io.update_config_file()
        return 1
    except:
        print("Error updating dropped column names in config")
        return 0

