#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from util import get_config, RESULTS_DIR
from IO.IO import IO

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to combine 2 csv files with different columns (non-overlapping)")
    parser.add_argument('-c', dest='configfn', default='config_lbds_variance_gridsearch.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    f_path = io.raw_data_path
    fn_1 = "data_lbds_raw_B{}_no_xpoc.csv".format(io.beam)
    fn_2 = "xpoc2_B{}.csv".format(io.beam)
    output_fn = "data_lbds_raw_B{}.csv".format(io.beam)

    df_1 = io.read_csv(fn_1, f_path)
    df_2 = io.read_csv(fn_2, f_path)

    # check if both files have the same number of rows
    if df_1.shape[0] != df_2.shape[0]:
        print("Imported csvs do not have the same amount of rows ({}) and ({}), exiting...".format(df_1.shape[0], df_2.shape[0]))
        exit()

    # combine dfs
    print("Combining {} {} and {} {} columns".format(fn_1, df_1.shape, fn_2, df_2.shape))
    combined_df = pd.concat([df_1, df_2], axis=1)
    assert df_1.index.all() == combined_df.index.all() and df_2.index.all() == combined_df.index.all()
    print("Final dimensions: {}".format(combined_df.shape))

    io.write_to_csv(combined_df, output_fn, f_path)