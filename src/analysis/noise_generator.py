import argparse
import os
import sys

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import util
from preprocessing.preprocess_modules import *
from IO.IO import IO


def generate_noise(io, nb_cols=None, nb_rows=None, write=False):
    filename = io.config['pipeline']['preprocessing']['variance_filtered_filename']
    filename = filename.format(io.beam) if "{}" in filename else filename
    df = pd.read_csv(io.pipeline_data_path + filename)
    df = set_features_index(df, index='timestamps')
    df = sort_on_index(df, ascending=True)

    if nb_rows is None:
        nb_rows = df.shape[0]
    if nb_cols is None:
        nb_cols = df.shape[1]

    # generate noise
    mu, sigma = 0, 1
    print("Generating random data with dimensions ({}x{})".format(nb_rows, nb_cols))
    noise = np.random.normal(mu, sigma, [nb_rows, nb_cols])
    noise_df = pd.DataFrame(data=noise, index=df.index)
    noise_df.columns = df.columns

    if write:
        f_path = io.preprocessed_data_path
        f_name = "random_noise_B{}.csv".format(io.beam)
        print("Writing noise data to {}".format(f_path + f_name))
        io.write_to_csv(noise_df, f_name, f_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Python script to generate noisy data as a baseline for the results")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = util.get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    io = IO(cfg, 'pipeline')
    seed = 456
    np.random.seed(seed)

    generate_noise(io, write=True)
