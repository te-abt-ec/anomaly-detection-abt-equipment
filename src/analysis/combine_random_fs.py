#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from util import get_config, RESULTS_DIR
from IO.IO import IO

import matplotlib.pyplot as plt

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    nb_iterations = 20
    ad = "iforest"
    beam = io.beam
    beam = 1
    histogram_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/experiments/random feature selection/".format(ad,
                                                                                                                     io.start_year,
                                                                                                                     io.start_month,
                                                                                                                     io.end_month,
                                                                                                                     beam)
    import glob

    filenames_means = []
    filenames_means_best_auc = []
    filenames_means_best_rank = []
    for i in range(nb_iterations):
        f_path = histogram_path
        for filename in glob.glob(f_path + '*.csv'):
            if "means_result_iter={}".format(i) in filename:
                filenames_means.append(filename)
            if "means_best_auc_result_iter={}".format(i) in filename:
                filenames_means_best_auc.append(filename)
            if "means_best_rank_result_iter={}".format(i) in filename:
                filenames_means_best_rank.append(filename)

    dfs = {
        'means': [],
        'best_auc': [],
        'best_rank': []
    }
    for i in range(nb_iterations):
        dfs['means'].append(pd.read_csv(filenames_means[i], index_col=0, parse_dates=True).sort_values(by='max_nb_features'))
        dfs['best_auc'].append(pd.read_csv(filenames_means_best_auc[i], index_col=0, parse_dates=True).sort_values(by='max_nb_features'))
        dfs['best_rank'].append(pd.read_csv(filenames_means_best_rank[i], index_col=0, parse_dates=True).sort_values(by='max_nb_features'))

    means = []
    for i in range(nb_iterations):
        means.append(dfs['means'][i]['auc'].iloc[0])

    #method = "concat" # wierd mistake in mean calculation, use lists instead
    method = "lists"
    averaged_dfs = {}
    for k in list(dfs.keys()):
        if method == "concat":
            df_concat = pd.concat(tuple([dfs[k][j] for j in range(nb_iterations - 1)]))
            by_row_index = df_concat.groupby('max_nb_features')
            averaged_dfs[k] = by_row_index.mean()
            averaged_dfs[k] = averaged_dfs[k].sort_values(by='max_nb_features')
            print(averaged_dfs[k])
        elif method == "lists":
            means_auc = np.zeros(len(dfs[k][0].index))
            means_rank = np.zeros(len(dfs[k][0].index))
            means_nb_cols = np.zeros(len(dfs[k][0].index))
            for j in range(nb_iterations):
                for l in range(len(dfs[k][0]['auc'])):
                    means_auc[l] += (dfs[k][j]['auc'].iloc[l] / nb_iterations)
                    means_rank[l] += (dfs[k][j]['rank'].iloc[l] / nb_iterations)
                    means_nb_cols[l] += (dfs[k][j]['nb_cols'].iloc[l] / nb_iterations)
            averaged_dfs[k] = pd.DataFrame(data={'max_nb_features': dfs[k][0]['max_nb_features'],
                                                 'nb_cols': means_nb_cols,
                                                 'auc': means_auc,
                                                 'rank': means_rank}, index=dfs[k][0].index)
            print(averaged_dfs[k])

    for k in list(dfs.keys()):
        io.write_to_csv(averaged_dfs[k], "averaged_{}.csv".format(k), histogram_path)