import argparse
import os
import sys

import numpy as np
import pandas as pd

from datetime import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import util
from preprocessing.preprocess_modules import *
from IO.IO import IO
from evaluation import evaluation
import dump_cols


def print_col(io, regex):
    filename = io.config['local_preprocessing']['raw_data_filename']
    filename = io.config['pipeline']['preprocessing']['variance_filtered_filename']
    df = read_features_from_csv(io, filename)
    df = set_features_index(df, index='timestamps')
    df = sort_on_index(df, ascending=True)

    cols_to_print = []
    mask = get_mask_for_regex(df.columns, regex)
    for c in mask:
        if c not in cols_to_print:
            cols_to_print.append(c)

    print("LBDS control column and #rows where \"value=6\" :")
    c_df = df[cols_to_print]
    for i, col in enumerate(c_df.columns):
        df2 = c_df.loc[c_df[col] == 6]
        io.write_to_csv(df2, str(regex) + "_" + str(i) + ".csv", util.CERN_DATA_DIR + '/analysis/feature_dumps/')

        print("{}: {}".format(col, str(len(df2))))

    features = df[cols_to_print]
    for i, col in enumerate(features.columns):
        features = features.loc[features[col] == 6]
    io.write_to_csv(features, str(regex) + ".csv", util.CERN_DATA_DIR + '/analysis/feature_dumps/temp/')
    print("# rows where all control columns \"value=6\" : {}".format(len(features)))

    features = df[cols_to_print]
    print("rows before nan filter: {}".format(len(features)))
    features = features[~features[features.columns].isna().all(axis=1)]
    print("{}".format(str(len(features))))




    print("Printing {}:".format(regex))
    to_file = True
    if to_file:
        io.write_to_csv(df[cols_to_print], str(regex) + ".csv", util.CERN_DATA_DIR + '/analysis/feature_dumps/')



if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Python script")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = util.get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    io = IO(cfg, 'pipeline')
    # example
    cols_to_print_regexes = ["SCSS_MKD\.UA63\.SCSS\.\w\w\w_first\(control,false\)"]
    for regex in cols_to_print_regexes:
        print_col(io, regex)
