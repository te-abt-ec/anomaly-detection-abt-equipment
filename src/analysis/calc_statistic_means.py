#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, DATASET_TYPES
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from itertools import chain, combinations
from datetime import datetime

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds_variance_gridsearch.yaml',
                        help='config file for pipeline run')
    parser.add_argument('--short', dest='short', action='store_true',
                        help='True if only 1 configuration has to run (for CI code check)')
    parser.set_defaults(short=False)

    args = parser.parse_args()
    short = args.short

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()


    def powerset(iterable):
        s = list(iterable)
        return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))

    feature_sets = powerset(DATASET_TYPES["LBDS"])
    feature_sets_list = []
    for i in feature_sets:
        feature_sets_list.append(list(i))
    feature_sets_list = feature_sets_list[1:]
    io = IO(cfg, 'pipeline')

    def get_filelist():
        file_list = list()
        for root, dirs, files in os.walk("."):
            for n in files:
                if "grid_search_statistics_B2_iforestres_" in n:
                    file_list.append(root + '/' + n)
        return file_list

    dfs = []
    file_list = list()
    read_files = []
    for f in get_filelist():
        file_list.append(f)
    for fs in feature_sets_list:
        suffix = ""
        for feature_set in fs:
            suffix = suffix + "_{}".format(feature_set)
        for filename in file_list:
            if suffix in filename and filename not in read_files:
                dfs.append(pd.read_csv(filename, index_col=0))
                read_files.append(filename)

    # Calculate means of df
    exp_means = {}
    print(len(feature_sets_list))
    print(len(dfs))
    for i, stats in enumerate(dfs):
        exp_means[str(feature_sets_list[i])] = stats.mean(axis=0, skipna=True)

    # print and save the result
    series_results = []
    print("Experiment result:")
    for i, (k, v) in enumerate(exp_means.items()):
        print("{} | {}".format(i, k))
        print(v)
        series_results.append(v)

    experiment_result = pd.concat(series_results, axis=1)
    experiment_result.columns = [str(i) for i in feature_sets_list][1:]
    experiment_result = experiment_result.transpose()
    experiment_result.to_csv(
        'results/experiments/feature_set_combination_experiment_result_' + str(datetime.now()) + '.csv')