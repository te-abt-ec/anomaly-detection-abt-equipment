import math

import numpy as np
import time
import os.path
from os import path
from pathlib import Path
from threading import Lock

import preprocessing.scaler
from anomaly_detection.factory import AnomalyDetectorFactory
from evaluation import clustering, evaluation
from postprocessing.segmentation import df_to_segments, set_ground_truth, print_segment_stats
from util import beam_to_str, CERN_DATA_DIR, RESULTS_DIR, get_parameters_string, set_seed, POSITIVES_LIMIT


def pipeline(beam, scale_data, anomaly_detector, detector_params, labels, segment_score, x_train, state_mode, io, scoring='auc',
             x_test=None, config=None, write_output=True, task='pipeline', lock=None, id=None, seed=None, live_evaluation=None):
    """
    Executes the anomaly detection pipeline for a set of features and labels and other parameters that can be set.
    :param beam: 1 or 2, beam for which training and testing data is provided
    :param scale_data: 'standard' or False (no scaling)
    :param anomaly_detector: string, AnomalyDetector to use
    :param detector_params: dictionary of parameters for the chosen AnomalyDetector
    :param labels: DataFrame of labels
    :param segment_score: string or list of strings, the function to use for segment anomaly scores
    :param x_train: DataFrame of features
    :param state_mode: DataFrame with STATE:MODE to create segments
    :param x_test: DataFrame of features to be predicted
    :param config: config file (yaml file)
    :param write_output: Write the predictions output of this pipeline to /results/ folder
    :param task: The task that called this pipeline
    :param lock: Used when writing the metadata (same file) in a multiprocessed grid search
    :param id: The unique id of this pipeline run
    :param live_evaluation: contains parameters of the live evaluation (pretrained model)
    """
    # CHECK ARGUMENTS
    assert not x_train.empty, 'x_train should be non-empty'
    if x_test is not None:
        assert not x_test.empty, 'x_test should be non-empty'
        assert set(x_train.columns) == set(x_test.columns), 'x_train and x_test should have same features.'
    assert config is not None, 'config file should be provided'

    # set the seed
    set_seed(seed)

    # TIMING
    start = time.time()

    # PREPROCESSING: scale train and test data
    x_train = x_train.replace(np.inf, np.nan)
    x_train = x_train.dropna(how='any')
    if not x_train.shape[0]:
        print("All rows have been removed because they all contain nan values. Please run preprocess_features.py before running this script")
    x_train = scale(x_train, scale_data)
    x_test = scale(x_test, scale_data) if x_test is not None else x_train

    # ANOMALY DETECTION: create detector, train on training data, predict on test data
    detector = AnomalyDetectorFactory.create(anomaly_detector, **detector_params)
    train(detector=detector, features=x_train)
    if task == 'live_evaluation':
        # Only the trained detector is necessary when doing live evaluation
        return detector
    if live_evaluation is not None:
        # A pretrained detector is passed through the live_evaluation argument
        print("\nPredicting with pretrained model...")
        detector = live_evaluation
    features_scored = predict(detector, x_test)

    prep_start = time.time()
    # POSTPROCESSING: transform tuples into segments
    print("\n|| POSTPROCESSING")
    truth_and_pred_df = postprocess(
        features_scored=features_scored,
        state_mode=state_mode,
        labels=labels,
        segment_score_method=segment_score,
        config=config
    )

    print("|| Postprocessed results: {:.1f} seconds".format(time.time() - prep_start))

    # calculate confusion matrix
    filename = io.get_pipeline_output_filename(anomaly_detector, "clustering", seed)
    try:
        if scoring == 'practical_recall':
            threshold = get_practical_recall_threshold(truth_and_pred_df, POSITIVES_LIMIT)
        else:
            threshold = config["pipeline"]["anomaly_detection"][anomaly_detector]["threshold"]
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    # TOTAL TIME
    total_time = time.time() - start

    _, confusion_matrix_stats = clustering.cluster_segments(truth_and_pred_df, segment_score, beam, threshold, filename)

    # EVALUATION: Area Under PR-Curve and PR-Gain-Curve
    auc, precision, recall = evaluate_pr(truth_and_pred_df)
    auprg, precision_gain, recall_gain = evaluate_prg(truth_and_pred_df)
    # EVALUATION: Ranking
    rank = evaluate_rank(truth_and_pred_df)
    # EVALUATION: Recall for set number of positives
    practical_recall = get_recall(confusion_matrix_stats)
    print("|| Practical recall for {} positives: {}".format(POSITIVES_LIMIT, practical_recall))

    result = {
        'truth_and_pred_df': truth_and_pred_df,
        'auc': auc,
        'precision': precision,
        'recall': recall,
        'auprg': auprg,
        'precision_gain': precision_gain,
        'recall_gain': recall_gain,
        'rank': rank,
        'anomaly_detector': anomaly_detector,
        'params': detector_params,
        'scale': scale_data,
        'segment_score': segment_score,
        'time': total_time,
        'model': detector,
        "id": id,
        'seed': seed,
        'anomaly_scores': features_scored,
        'confusion_matrix_stats': confusion_matrix_stats,
        'practical_recall': practical_recall
    }

    # FEEDBACK: improve results using COBRAS
    if write_output:
        if task == 'grid_search':
            with lock:
                io.write_pipeline_result(result, id, seed)
        elif task == 'pipeline':
            io.write_pipeline_result(result)
        if task == 'pipeline':
            # Dump the trained model and input features (+ predicted score) for later analysis
            io.dump_scored_training_data(features_scored)
            io.dump_pipeline_model(anomaly_detector, detector)
        if math.isnan(result["auc"]):
            print("Note: The resulting AUC value has NaN as output, consider using a bigger time interval to include anomalies")
    return result


def scale(features, method):
    """
    Scale the given features using the given method.
    :param features: DataFrame of features to scale
    :param method: 'standard' or False (no scaling)
    :return: DataFrame of features transformed according to the given method
    """
    if method == 'standard':
        features = preprocessing.scaler.scale(features)
    elif method == 'none':
        print('No scaling performed')
    else:
        raise Exception("Unsupported scale method.")

    return features


def train(detector, features):
    """
    Fits an AnomalyDetector on the given features
    :param detector: AnomalyDetector, the detector
    :param features: DataFrame of features
    """
    start_time = time.time()
    detector.fit(features)
    print("|| Trained detector: {:.1f} seconds".format(time.time() - start_time))


def predict(detector, features):
    """
    Takes a trained anomaly detector and assigns scores to the given features
    :param detector: AnomalyDetector, the detector
    :param features: DataFrame of features
    :return:
    """
    # TODO assert that detector has been trained before
    start_time = time.time()
    scores = detector.anomaly_scores(features)
    features = features.assign(score=scores)
    print("|| Scored features: {:.1f} seconds".format(time.time() - start_time))
    return features


def postprocess(features_scored, state_mode, labels, segment_score_method, config):
    """
    Takes a DataFrame of scored features, transforms them into Segments, runs the evaluation procedure using
    the anomaly scores of the Segments, and returns the evaluation results.
    :param features_scored: DataFrame of features
    :param state_mode: DataFrame with STATE:MODE to create segments
    :param labels: DataFrame of labels
    :param segment_score_method: string or list of strings, the function to use for segment anomaly scores
    """

    try:
        post = config["pipeline"]["postprocessing"]
        fields_to_use = post['labels_to_use']
        id_col = post['label_id']
        date_col = post['label_date']
        seg_type = config["spark"]['segmentation']['type']
    except KeyError as e:
        print("Key {} not found in config file, using all fields from logbook for label".format(e))
        fields_to_use = list(labels)
        id_col = list(labels)[0]
        date_col = list(labels)[1]

    print("starting {} segmentation".format(seg_type))

    # transform scored tuples into scored segments
    segments = df_to_segments(features_scored, state_mode)

    # add ground truth to segments
    segments = set_ground_truth(segments, labels, id_col, date_col, fields_to_use)

    # create DataFrame that holds ground truth (0 or 1 for each segment) and anomaly score predictions
    truth_and_pred_df = evaluation.segments_scored_to_truth_and_pred_df(segments, segment_score_method)

    # sanity check: check that the amount of rows in truth_and_pred_df is correct
    check_truth_and_pred_df_length(segments, truth_and_pred_df)

    # Print some segment information
    print_segment_stats(segments)
    return truth_and_pred_df


def evaluate_pr(truth_and_pred_df):
    # compute area under the precision recall curve
    auc, precision, recall = evaluation.pr_curve(
        y_true=truth_and_pred_df["y_true"].astype(float),
        y_pred=truth_and_pred_df["y_pred"].astype(float),
        fig_filename=None
    )
    print("|| Area under PR curve = {}".format(auc))
    return auc, precision, recall

def evaluate_prg(truth_and_pred_df):
    # compute area under the precision recall gain curve
    auprg, precision_gain, recall_gain = evaluation.prg_curve(
        y_true=truth_and_pred_df["y_true"].astype(int),
        y_pred=truth_and_pred_df["y_pred"].astype(float),
        fig_filename=None
    )
    print("|| Area under PRG curve = {}".format(auprg))
    return auprg, precision_gain, recall_gain

def evaluate_rank(df):
    """
    Computes the rank of a truth_and_pred_df. Lower rank means better prediction. Perfect ranking is 1.
    The DataFrame is sorted on decreasing anomaly score, then the rank of anomalies is summed
    and divided by the length of the dataframe.
    """
    df_sorted = df.sort_values(by=['y_pred'], ascending=False)[['y_true', 'y_pred']]
    ranking = df_sorted.reset_index(drop=True)
    is_anomaly = ranking['y_true'] == 1
    anomaly_indices = ranking[is_anomaly].index.tolist()
    if not len(anomaly_indices):  # no anomalies, so makes no sense to compute ranking
        print("|| Rank = {}".format(np.NaN))
        return np.NaN
    rank = sum(anomaly_indices) / len(anomaly_indices)
    print("|| Rank = {}".format(rank))
    return rank


def get_recall(confusion_matrix_stats):
    return confusion_matrix_stats['TP'] / (confusion_matrix_stats['TP'] + confusion_matrix_stats['FN'])


def get_practical_recall_threshold(truth_and_pred_df, nb_max_positives):
    anomaly_scores = truth_and_pred_df['y_pred']
    threshold = (len(anomaly_scores) - nb_max_positives) / len(anomaly_scores)
    print("Setting threshold to {}".format(threshold))
    return threshold

def check_truth_and_pred_df_length(segments, truth_and_pred_df):
    """
    Checks that the amount of rows in truth_and_pred_df is equal to
    amount_of_segments - amount_of_labels_that_occur_too_much

    e.g. when there are 3 segments for 1 label, 2 of them occur too much
    => those 3 segments should be represented by the one with the worst anomaly score

    For more details about this approach, see notes.md: 23/04, IPOC segments, ground truth annotation, and evaluation

    :param segments: list of segments
    :param truth_and_pred_df: DataFrame of y_true values and y_pred anomaly scores
    """
    segments_labeled = [s for s in segments if s.get_label() is not None]
    unique_labels = set([s.get_label().id for s in segments_labeled])
    segments_to_remove = len(segments_labeled) - len(unique_labels)

    if len(truth_and_pred_df) != len(segments) - segments_to_remove:
        raise Exception(
            "Wrong amount of labels ({}) in truth_and_pred_df: segments = {}, segments_labeled = {}, unique_labels = {}"
                .format(len(truth_and_pred_df), len(segments), len(segments_labeled), len(unique_labels))
        )
