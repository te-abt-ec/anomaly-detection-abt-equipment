import multiprocessing
import pandas as pd
import numpy as np
import time
from uuid import uuid4

import os
from pathlib import Path

import sklearn.model_selection
from pipeline.pipeline import pipeline
from util import beam_to_str, RESULTS_DIR, set_seed
from IO.IO import IO
from random import randint

def pipeline_with_selection(params):
    beam = params[0]
    feature_set_name, feature_set = params[1]
    scale_data = params[2]
    anomaly_detector = params[3]
    detector_parameters = params[4]
    labels = params[5]
    segment_score = params[6]
    scoring = params[7]
    x_train = params[8]
    state_mode = params[9]
    x_test = params[10]
    config = params[11]
    lock = params[12]
    id = params[13]
    seed = params[14]
    io = params[15]

    print("\nFeatures before gs selection: {}".format(len(x_train.columns)))
    x_train_selected = x_train[feature_set].copy()
    x_test_selected = x_test[feature_set].copy() if x_test is not None else None
    print("\nFeatures after gs selection: {}".format(len(x_train_selected.columns)))

    try:
        pipeline_res = pipeline(beam=beam,
                                scale_data=scale_data,
                                anomaly_detector=anomaly_detector,
                                detector_params=detector_parameters,
                                labels=labels,
                                segment_score=segment_score,
                                x_train=x_train_selected,
                                state_mode=state_mode,
                                x_test=x_test_selected,
                                io=io,
                                scoring=scoring,
                                write_output=True,
                                config=config,
                                task='grid_search',
                                lock=lock,
                                seed=seed,
                                id=id)
    except ValueError as v:
        print(v)
        pipeline_res = {'auc': -1., 'auprg': -1, 'rank': -1, 'practical_recall': -1,
                        'confusion_matrix_stats': {'TP': -1, 'FN':-1, 'FP':-1, 'TN':-1}, 'scale': scale_data,
                        'segment_score': segment_score, 'params': detector_parameters,
                        'truth_and_pred_df': pd.DataFrame(), 'time': -1, 'id': -1, 'seed': -1}

    pipeline_res['feature'] = feature_set_name
    return pipeline_res


def grid_search(feature_selection, beam, scale_data, anomaly_detector, detector_params, labels, segment_score, x_train,
                state_mode, io, x_test=None, scoring='auc', max_runtime=None, config=None, filename='', short=False, nb_runs=None, seed=None):
    """
    Performs grid search for a given anomaly detector and the given parameters.
    :param beam: 1 or 2, beam for which training and testing data is provided
    :param state_mode: DataFrame with STATE:MODE to create segments
    :param feature_selection: List of list of features to be used in grid search
    :param labels: DataFrame of labels
    :param anomaly_detector: string, AnomalyDetector to use
    :param detector_params: dictionary of parameters for the chosen AnomalyDetector
    :param scale_data: bool, whether or not to scale the data before fitting and scoring
    :param segment_score: string or list of strings, the function to use for segment anomaly scores
    :param filename: Suffix of the filename of the best result
    :param scoring: Parameter to evaluate the grid search result
    :param max_runtime: maximum time a single anomaly detector instance can be trained for
    """
    # Check that parameters have type list so they can be used in a grid
    for param in (feature_selection, scale_data, segment_score):
        assert (isinstance(param, list))

    set_seed(seed)
    if nb_runs:
        # x1000 should be enough for a multiple pipeline run (configs is cutted to nb_runs later)
        scale_data = [scale_data[0] for i in range(1000)]

    # Create grids
    detector_grid = sklearn.model_selection.ParameterGrid(detector_params)
    evaluation_grid = sklearn.model_selection.ParameterGrid({
        'segment_score': segment_score
    })

    m = multiprocessing.Manager()
    l = m.Lock()
    # Create configurations
    configs = []
    for scale_method in scale_data:
        for fs in feature_selection:
            for detector_params in detector_grid:
                for evaluation_params in evaluation_grid:
                    configs.append([beam, fs, scale_method, anomaly_detector, detector_params, labels,
                                    evaluation_params['segment_score'], scoring, x_train, state_mode, x_test, config,
                                    l, str(uuid4()), randint(0, 10000), io])
    if short:
        configs = [configs[0]]
    if nb_runs is not None:
        configs = configs[0:int(nb_runs)]
    print("## Starting grid search with {} configurations".format(len(configs)))


    pool = multiprocessing.Pool()
    batches = [(i, pool.apply_async(pipeline_with_selection, (config,))) for i, config in enumerate(configs)]
    results = []
    start_time = time.time()
    start_times = {}
    if not max_runtime:
        print("Traning anomaly detectors without an upper bound on training time (max_runtime can be set in config.yaml)")
    while batches:
        try:
            x, result = batches.pop(0)
            start_times[x] = time.time()
            print("{} configurations left to try".format(len(batches)))
            results.append(result.get(timeout=max_runtime))
        except multiprocessing.context.TimeoutError as e:
            print(str(e))
            print("Timeout Exception for process {} in {} seconds".format(x, time.time() - start_times[x]))
            del start_times[x]
            for i in reversed(range(len(pool._pool))):
                p = pool._pool[i]
                if p.exitcode is None:
                    p.terminate()
                del pool._pool[i]
    pool.terminate()
    pool.join()

    total_time = time.time() - start_time
    print('\n| Grid search execution time: {:.1f} seconds\n'.format(total_time))

    results_sorted = sorted(results, key=lambda res: res[scoring], reverse=True)
    for r in results_sorted:
        print(
            '| AUC = {:6.3f}, AUPRG = {:6.3f}, rank = {:.2f} practical_recall = {:6.3f} in for '
            'feature={}, scale={:1}, a_score_method={}, params={}, time={:.1f}s '.format(
                r['auc'], r['auprg'], r['rank'], r['practical_recall'], r['feature'], r['scale'],
                r['segment_score'], r['params'], r['time']))
    if -1 in [r['auc'] for r in results_sorted]:
        print("Some pipeline runs encountered an error in execution (timeout or ValueError). Their results have been set to -1")
    machine = config['machine']
    # Write the best performing truth_and_pred_df to a csv file so it can be reused
    if len(results_sorted):
        best = results_sorted[0]
        filename_infix = machine + '_' + beam_to_str(beam) + '_' + anomaly_detector + '_' + scoring + '_' + filename
        best_results_filename = 'grid_search_' + filename_infix + '-best.csv'
        try:
            io.dump_scored_training_data(best["anomaly_scores"])
            # dump best model and id of best prediction file
            io.dump_pipeline_model(anomaly_detector, best['model'])
            io.write_best_id(anomaly_detector, {'params': best['params'], 'scale_data': best['scale'],
                                                'segment_score': best['segment_score']})
        except KeyError:
            print("No anomaly scores data found in returned result, possibly because all runs were skipped (timeout or ValueError in pipeline). continuing...")

        path = io.get_grid_search_output_path(anomaly_detector)
        # store best results
        if not os.path.exists(path):
            Path(path).mkdir(parents=True, exist_ok=True)
        best['truth_and_pred_df'].to_csv(path + best_results_filename)
        print('| Wrote best results to file {}'.format(path + best_results_filename))
        print('| Best pipeline id: {}'.format(best["id"]))
        print('| Best pipeline seed: {}'.format(best["seed"]))

        # store statistics
        exec_times = [res['time'] for res in results]
        scores = [res[scoring] for res in results]
        ids = [res["id"] for res in results]
        seeds = [res["seed"] for res in results]
        statistics = pd.DataFrame(data={'scores': scores, 'time': exec_times, 'id': ids, 'seed': seeds})
        statistics_filename = 'grid_search_statistics_' + filename_infix + '.csv'
        statistics.to_csv(path + statistics_filename)
    return results
