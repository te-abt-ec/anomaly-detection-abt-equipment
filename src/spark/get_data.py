import os
import argparse
import getpass
from datetime import datetime
from threading import Lock
from threading import Thread, current_thread
import re

import yaml
from cern.nxcals.api.extraction.data.builders import DevicePropertyDataQuery, DataQuery
from pyspark import SparkContext
from pyspark.conf import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import floor, col, asc, regexp_replace

from DataProcessor import DataProcessor, _dfindex, _dfdev

preprocess_lock = Lock()
max_nr_of_threads = 2

def cmw_query(spark, startt, endt, devicel=None, propertyl=None, variablel=None):
    """
    Constructs a CMW query with given start and endtime,
    device id and property id (features>timestamps in config) OR variable
    For query properties, consult http://nxcals-docs.web.cern.ch/current/user-guide/extraction-api/
    """
    if variablel is not None:
        df = DataQuery \
            .builder(spark).byVariables() \
            .system("CMW") \
            .startTime(startt) \
            .endTime(endt) \
            .variableLike(variablel) \
            .buildDataset()
        source = variablel
        # rename columns cause filter and pivot later on assume a device column
        df = df.withColumnRenamed("nxcals_variable_name", _dfdev).withColumnRenamed("nxcals_timestamp", _dfindex)
        numpart = df.rdd.getNumPartitions()
        if numpart > 10:
            thread_print("Reducing number of partions from {} to 10".format(numpart))
            # decrease to avoid later heap OutOfMemoryError
            df = df.repartition(10)
        # remove special chars from device values, would stall beam filter later on
        df = df.withColumn(_dfdev, regexp_replace(col(_dfdev), "[:_ ]+", "."))

    elif devicel is not None and propertyl is not None:
        df = DevicePropertyDataQuery \
            .builder(spark) \
            .system("CMW") \
            .startTime(startt) \
            .endTime(endt) \
            .entity() \
            .deviceLike(devicel) \
            .propertyLike(propertyl) \
            .buildDataset()
        source = devicel

    else:
        thread_print("Error: invalid set of deviceLike, propertyLike or variableLike passed: {}, {}, {}".format(devicel, propertyl, variablel))
        return None
    thread_print("{} - Fetched {} data, row count is {}".format(datetime.now(), source, df.count()))
    # Does these df.count() give a lot of overhead? Seemed so in the SWAN notebook
    return df


def get_segment_timestamps(time_interval, timestamp_query, spark):
    """
    Get IPOC segment timestamps
    TODO: Check doc of timestamp_query
    Args:
        time_interval: The time interval of the query
        timestamp_query: The device and property from which the final featurevector timestamps will be fetched
        spark: The SparkSession
    Returns:
        An unordered list containing the timestamps in seconds
    """
    start_time, end_time = str(time_interval[0]), str(time_interval[1])
    thread_print("{} - Reading segment timestamps: from {} to {}".format(datetime.now(), start_time, end_time))
    # Segment timestamp query
    df = cmw_query(spark, start_time, end_time, timestamp_query[0], timestamp_query[1])
    if df:
        df_seg = df.select(_dfindex)
        # round timestamps to seconds
        df_seg = df_seg.withColumn(_dfindex, floor(col(_dfindex) / 1000000000))
        return df_seg.rdd.map(lambda x: x[0]).collect()
    return None


def write_to_hdfs(df, path, dformat='csv', repartition=True):
    """
    Write a file to hdfs
    Args:
        df: dataframe to write away
        path: subfolder to write to (can be nested)
        dformat: data-format
        repartition: repartition data to 1 large file or not
    """
    if df is None:
        return None

    f_path = "/user/" + getpass.getuser() + path

    thread_print("{} - Writing out a total of {} columns/features and {} entries to {}".format(datetime.now(),
                                                                                               len(df.columns) - 1,
                                                                                               df.count(),
                                                                                               f_path))
    if repartition:
        # df.repartition(1) does a "coalesce" and "shuffle", use df.coalesce(1) to keep the sort
        df = df.coalesce(1)
        thread_print("{} - Repartition finished, starting write..".format(datetime.now()))
    if dformat == 'csv':
        df.write.csv(f_path, mode='overwrite', header=True)
    elif dformat == 'json':
        df.write.json(f_path, mode='overwrite')
    else:
        thread_print("{} - Warning - unsupported data-format, not writing out".format(datetime.now()))
        return None
    thread_print("{} - Write finished".format(datetime.now()))
    return f_path


def update_features(features, new_data):
    """
    Update existing features with new data
    """
    if features is not None:
        # join on index which is _dfindex
        features = features.join(new_data, on=_dfindex, how='left')
    else:
        features = new_data
    return features


def preprocess(df, subs, filter_list, time_interval, timestamps, config=None, store_data=False):
    """
    Preprocess the given data of the given system type:
        - Create DataProcessor object with given data and system type (drops columns in filter_list)
        - Preprocess
        - Resample with given timestamps
        - Optionally store the data of this system
    Args:
        df: The data
        subs: The system type
        filter_list: The columns to be dropped
        time_interval: The data time interval
        timestamps: The timestamps to resample the data on
        config: The parsed config file
        store_data: Indicates whether to store the data of this system seperately or not
    Returns:
        The preprocessed and resampled data
    """
    if subs not in config["preprocessing"]["allowed_proc_types"]:
        thread_print("{} - Invalid proc type, not building processor".format(datetime.now()))
        return None

    thread_print("{} - Building {} processor".format(datetime.now(), subs))
    proc = DataProcessor(filter_list, time_interval, df, config=config, proc_type=subs)

    thread_print('{} - Preprocessing {}'.format(datetime.now(), subs))
    if not proc.data:
        thread_print("{} - Empty dataframe, aborting preprocessing".format(datetime.now()))
        return None
    proc.preprocess_data()

    thread_print('{} - Preprocessing {} finished, start resampling..'.format(datetime.now(), subs))
    if timestamps:
        # not a real resample, more a filter, data has to be filled first (happens in preprocess above)
        proc.resample(timestamps)
        thread_print('{} - Resampled {}, {} entries remain'.format(datetime.now(), subs, proc.data.count()))
    if store_data:
        thread_print("{} - Preprocessing done for {}, writing...".format(datetime.now(), subs))
        path = "/ml-data/" + subs
        write_to_hdfs(proc.data, path)
    return proc.data

def read_from_hdfs(path, schema):
    """
    Reads the given path from hdfs
    Args:
        path: the path to read form
        schema: the schema to use while reading in the data
    """
    thread_print('{} - Reading {} from HDFS'.format(datetime.now(), path))

    df = spark.read.format('csv').load(path, schema=schema, header=True)

    return df

def filter_nan_columns(df):
    """
    Filter columns containing only nan
    """
    thread_print('{} - Filtering nan columns'.format(datetime.now()))
    nr_of_rows = df.count()
    for coln in df.schema.names:
        coln = "`" + coln + "`"
        if df.select(count(when(isnull(coln), coln)).alias(coln)).rdd.map(lambda x: x[0]).collect()[0] == nr_of_rows:
            thread_print("{} - Deleting column: {}".format(datetime.now(), coln))
            df = df.drop(coln)
    return df


def drop_non_variance_cols(df, eps=0):
    """
    Drop columns which have variance <= eps
    """
    thread_print('{} - Filtering columns with variance <= {}'.format(datetime.now(), eps))
    for coln in df.schema.names:
        coln = "`" + coln + "`"
        var = df.agg({coln: 'variance'}).rdd.map(lambda x: x[0]).collect()[0]
        if var is None or var < eps:
            thread_print("{} - Deleting column: {}".format(datetime.now(), coln))
            df = df.drop(coln)
    return df


def load_preprocess_write(params):
    """
    Writes the requested features of the given time interval to HDFS.
        - Gets the timestamps to resample on (IPOC timestamps for MKI)
        - For each system, data gets queried, preprocessed, columns renamed to "<system>_column" except _dfindex and joined
        - Orders the final list of features on _dfindex and writes to HDFS
    :param params: list of arguments (for multithreading purposes) containing:
        part (int): index of time interval (for writing data in separate directories when using multiple time frames)
        start_time (str): start time for data querying
        end_time (str): end time for data querying
        systems (list<str>): list of systems to query data from
        filter_list (map<str : list<str>>): map with key the system and value the list of columns to remove for that system
        spark (SparkSession): spark session
        sc (SparkContext): the spark context to use
        dformat ('csv' | 'json'): file format for writing data to HDFS
        tsq: The timestamp query data
        n_timestamps: The number of timestamps
        machine: The machine that is being processed (MKI/LBDS)
        cfg_spark: The part of the parsed config about Spark
    """
    part = params[0]
    start_time = params[1]
    end_time = params[2]
    systems = params[3]
    filter_list = params[4]
    spark = params[5]
    sc = params[6]
    dformat = params[7]
    tsq = params[8]
    n_timestamps = params[9]
    machine = params[10]
    cfg_spark = params[11]

    time_interval = (start_time, end_time)

    with preprocess_lock:
        thread_id = current_thread().name
        thread_print("{} - Starting preprocessing for part {}".format(datetime.now(), thread_id))
        thread_print("{} - Getting unique timestamps from {} device".format(datetime.now(), tsq[0]))
        timestamps = get_segment_timestamps(time_interval, tsq, spark)
        # If no timestamps to resample to, then no preprocessing can be done
        if not timestamps:
            thread_print("{} - No timestamps to resample to, skipping data retrieval for interval {} - {}".format(datetime.now(), start_time, end_time))
            return

        # Set pool in spark scheduler to get equal computing resources for each thread
        sc.setLocalProperty("spark.scheduler.pool", "pool" + str(thread_id))

        thread_print("{} - Reading data from {} to {}".format(datetime.now(), start_time, end_time))
        features = None
        # subs is the system name (eg. IPOC, SCSS, ...), q is the device_id and property_id for in the query
        for i, (subs, query_list) in enumerate(systems.items()):
            for q in query_list:
                dev = q["device"] if "device" in q else None
                prop = q["property"] if "property" in q else None
                var = q["variable"] if "variable" in q else None
                df = cmw_query(spark, start_time, end_time, dev, prop, var)

                filterl = None
                if subs in filter_list:
                    filterl = filter_list[subs]
                # Preprocess the data for this system, the last argument decides whether the data of the systems should be saved on HDFS seperately (default=False)
                dfp = preprocess(df, subs, filterl, time_interval, timestamps, cfg_spark, store_data=False)

                if not dfp:
                    thread_print("{} - Warning - no Preprocessor could be created for {}".format(datetime.now(), subs))
                else:
                    thread_print("{} - Updating {} column names and adding these to the common features dataset".format(
                        datetime.now(), subs))
                    # add prefix cause unique column names required by hdfs;
                    # backticks needed cause of spaces in column names, replacing these for the new alias
                    # _dfindex has to be kept cause it's used for joining
                    dfp = dfp.select([col("`" + c + "`").alias(subs + "_" + c.replace(" ", "")) for c in dfp.columns]).withColumnRenamed(subs + "_" + _dfindex, _dfindex)
                    features = update_features(features, dfp)

    print("{} - Preprocessing done for all systems".format(datetime.now()))

    # this program seems to love to mess with row order, so add another orderBy to fix that,
    # I assume this requires the timestamp column to be of some numeric type to enable ordering that makes sense,
    # not sure though since this was not a requirement in an earlier version of the program, but it seems to be needed now
    # (sorting strings returns unexpected results, e.g. 11 comes before 2).
    # Timestamp column to numeric type conversion is done in forward fill
    # Add suffix to support data retrieval in multiple parts and concurrently from multiple years
    dt_start = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S.%f')
    dt_end = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S.%f')
    suffix = str(dt_start.year) + '_part_' + str(part) if n_timestamps > 2 else str(dt_start.year)
    path = "/ml-data/" + str(machine) + '/' + suffix + '/month_' + str(dt_start.month) + '-' + str(dt_end.month)
    write_to_hdfs(features.orderBy(asc(_dfindex)), path, dformat)

    # Unset pool in spark scheduler because thread has finished running
    sc.setLocalProperty("spark.scheduler.pool", None)


def thread_print(s):
    """
    Wrapper for print statements within a thread
    """
    name = current_thread().name
    print("Thread {}: {}".format(name, s))


def write_features_data(sc, spark, machine, fn, cfg_spark):
    """
    Writes the requested features to the HDFS (Hadoop File System) according to the passed configuration file using
    PySpark.
        - Split the task up in multiple threads if multiple time intervals are given in the config
    Args:
        sc: SparkContext
        spark: SparkSession
        fn: Path to config
        cfg_spark: Parsed config (Spark part)
    """
    try:
        beam = cfg_spark["beam"]
        time_intervals = cfg_spark['time_interval']
        filter_list = cfg_spark["preprocessing"]["drop_columns"]
        systems = cfg_spark["querying"]["systems"]
        tsq = cfg_spark["querying"]["timestamps"][beam]
        dformat = cfg_spark["dataformat"]
        # Turn off prints if wanted
        if not cfg_spark["verbose"]:
            thread_print = lambda x: None
    except KeyError as e:
        print("Config file {} is missing required keys: {}".format(fn, str(e)))
        exit()

    n_timestamps = len(time_intervals)
    intervals = [(time_intervals[i], time_intervals[i + 1]) for i in range(0, n_timestamps, 2)]

    configs = []
    for i, time_interval in enumerate(intervals):
        start_time, end_time = time_interval
        configs.append([i, start_time, end_time, systems, filter_list, spark, sc, dformat, tsq, n_timestamps, machine, cfg_spark])

    print("Starting multithreaded preprocessing for {} time periods".format(str(len(configs))))
    preprocess_start = datetime.now()

    # Thread pool was not used because Spark DataFrame and SparkContext cannot be pickled
    # Manually reusing a thread with different arguments is not possible outside of threadpool (I think)
    threads = []
    while configs:
        for th in threads:
            if not th.is_alive():
                threads.remove(th)
        if len(threads) < max_nr_of_threads:
            t = Thread(target=load_preprocess_write, args=(configs.pop(0),))
            t.name = len(threads)
            t.start()
            threads.append(t)
        else:
            print("Maximum amount of threads running, waiting for first one to finish")
            # TODO: not neccesarily the first has to finish in order to start a new thread?
            threads[0].join()

    print("All configs assigned to a thread, config queue empty")

    # Finish main thread only once all other threads are finished
    while threads:
        th = threads[0]
        if not th.is_alive():
            threads.pop(0)
        else:
            th.join()
            threads.remove(th)
    print("ALL PREPROCESSING OPERATIONS COMPLETED")
    preprocess_end = datetime.now()
    total_time = preprocess_end - preprocess_start
    print("Total elapsed time: ", total_time)


def write_statemode_data(spark, machine, fn, cfg_spark):
    """
    Writes the statemode data to the HDFS (Hadoop File System) according to the passed configuration file using
    PySpark.
    Note: doesn't split up the task in intervals, but does the whole interval at once
    Args:
        spark: SparkSession
        fn: Path to config
        cfg_spark: Parsed config (Spark part)
    """
    try:
        time_intervals = cfg_spark['time_interval']
        query_data = cfg_spark['segmentation']['statemode_query_data']
        regex = cfg_spark['segmentation']['columns_to_write_regex']
    except KeyError as e:
        print("Config file {} is missing required keys: {}".format(fn, str(e)))
        exit()

    time_interval = (time_intervals[0], time_intervals[-1])
    start_time, end_time = str(time_intervals[0]), str(time_intervals[-1])
    print("{} - Reading STATE:MODE data: from {} to {}".format(datetime.now(), start_time, end_time))

    df = cmw_query(spark, start_time, end_time, query_data[0], query_data[1])

    # Drop every column except regex
    filter_list = []
    for col in df.schema.names:
        if not re.search(regex, col):
            filter_list.append(col)

    print("{} - Building STATE_MODE processor".format(datetime.now()))
    proc = DataProcessor(filter_list, time_interval, df, config=cfg_spark, proc_type="STATE_MODE")

    # Print state_mode data dimensions
    print("STATEMODE COLUMNS before prep:")
    print(list(proc.data.schema.names))
    print("STATEMODE ROWS before prep:")
    print(proc.data.count())

    print('{} - Preprocessing STATE_MODE'.format(datetime.now()))
    if not proc.data:
        print("{} - Empty dataframe, aborting STATE_MODE preprocessing".format(datetime.now()))
        exit()
    proc.preprocess_data()

    # Print state_mode data dimensions
    print("STATEMODE COLUMNS after prep:")
    print(list(proc.data.schema.names))
    print("STATEMODE ROWS after prep:")
    print(proc.data.count())

    # Write state_mode data
    if proc.data:
        dt_start = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S.%f')
        dt_end = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S.%f')
        path = "/ml-data/" + str(machine) + '/' + str(dt_start.year) + '/STATE_MODE_month_' + str(dt_start.month) + '-' + str(dt_end.month)
        write_to_hdfs(proc.data.orderBy(asc(_dfindex)), path, 'csv')
        print("{} - Succesfully wrote STATE:MODE data!".format(datetime.now()))
    else:
        print("{} - Failed to write STATE:MODE data, no data was given".format(datetime.now()))


if __name__ == "__main__":
    """
    Setups the necessary variables for PySpark and executes the asked jobs from config.
        - Initializes the SparkConf, SparkContext and SparkSession
        - Parse the configuration file
        - Execute the asked jobs
    Args:
        -c : configuration (yaml) file
    """
    # note: the below SparkConf parameters cannot always be taken into account, since several are set upon launch and cannot be changed afterwards (ref. spark-submit)
    conf = (SparkConf()
            .setAppName("anomalydetection_fetch_data")
            .setMaster('yarn')
            .set('spark.executor.instances', '10')
            .set('spark.executor.cores', '10')
            .set('spark.executor.memory', '20G')
            .set('spark.driver.memory', '100G')
            .set('spark.sql.autoBroadcastJoinThreshold', '-1')
            .set('spark.dynamicAllocation.enabled', 'false')
            .set('spark.shuffle.service.enabled', 'true')
            .set('spark.dynamicAllocation.minExecutors', '2')
            .set('spark.dynamicAllocation.maxExecutors', '10')
            )
    sc = SparkContext(conf=conf)
    sc.setSystemProperty("dfs.client.slow.io.warning.threshold.ms", "600000")
    spark = SparkSession(sc)

    parser = argparse.ArgumentParser(description="Pyspark script to fetch data from NXCALS; start with spark-submit")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for NXCALS queries and preprocessing')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        print("Config file {} is being read".format(fn))
        with open(fn, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)
    else:
        print("Config file {} couldn't be found, exiting".format(fn))
        exit()

    # Extract jobs to execute from config
    try:
        cfg_spark = cfg["spark"]
        machine = cfg["machine"]
        write_features = cfg_spark['write_features_data']
        write_statemode = cfg_spark['write_statemode_data']
        seg_type = cfg_spark['segmentation']['type']
    except KeyError as e:
        print("Config file {} is missing required keys: {}".format(fn, str(e)))
        exit()

    # Execute jobs
    if write_statemode and seg_type == 'statemode_based':
        write_statemode_data(spark, machine, fn, cfg_spark)

    if write_features:
        write_features_data(sc, spark, machine, fn, cfg_spark)
