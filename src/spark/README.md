# Spark at CERN
User manual at http://nxcals-docs.web.cern.ch/current/user-guide/data-access/access-methods/#pyspark-tool

## Login
To make an SSH connection with a LXPLUS machine:
```
ssh <user>@lxplus.cern.ch
k5reauth -x -i 3600 -f -- tmux
```
After the connection has been established, you will be placed in a tmux session and see the LXPLUS machine number (```[<user>@lxplusXXX]$```).

Remember or write down this number if you want to run a large job and close your connection (using ```tmux attach```)
because you have to connect to the same machine again (on which your script is running).

Tmux is a tool to easily work with multiple terminals at the same time within 1 SSH connection. [Tmux cheat sheet](https://gist.github.com/henrik/1967800)

### Keytab generation and activation
To generate a 24h keytab:
```
cern-get-keytab --user --keytab <keytab.filename>
```
Activate it using:
```
kdestroy && kinit -f -r 5d -kt /<path_to_keytab_file>/<user>.keytab <user>
```
The active keytab can be checked using:
```
klist
```

## Spark bundle installation
You only need to do this once.

To use Spark on a LXPLUS machine, you first have to install the spark bundle as described [here](http://nxcals-docs.web.cern.ch/current/user-guide/data-access/access-methods/#nxcals-spark-bundle)
(follow the guide until the example query).
Do this in the EOS (CernBox) filesystem:
```
cd /eos/home-<first-letter-of-username>/<username>/nxcals/
# For example: cd /eos/home-t/tclaesse/nxcals/
curl -s -k -O http://photons-resources.cern.ch/downloads/nxcals_pro/spark/spark-nxcals.zip && unzip spark-nxcals.zip && rm spark-nxcals.zip && cd spark-*-bin-hadoop*
./source-me.sh
```

## Running the application

### Using a wrapper script
After cloning the repo at the location of your NXCALS bundle, [```get_data_wrapper```](get_data_wrapper) can be executed to launch the application and write the output to EOS (or not).

You should pass arguments as follows:
```bash
# Make sure your Python environment is activated:
source nxcals-python3-env/bin/activate

# First time only, clone the repo and install dependencies
git clone ssh://git@gitlab.cern.ch:7999/te-abt-ec/anomaly-detection-abt-equipment.git
pip install pyyaml scipy

# Run the script with or without write to EOS:
anomaly-detection-abt-equipment/src/spark/get_data_wrapper <config> <CERN-username> <--write>

#example
anomaly-detection-abt-equipment/src/spark/get_data_wrapper config_mki.yaml
```

### Using spark-submit directly
Within the NXCALS bundle, renew your keytab and activate the venv as explained in [the manual](http://nxcals-docs.web.cern.ch/current/user-guide/data-access/access-methods/#pyspark-tool).
Make sure you have installed the venv by executing the source-me file as explained in the guide.
Then:
```
source nxcals-python3-env/bin/activate
git clone https://gitlab.cern.ch/te-abt-ec/anomaly-detection-abt-equipment.git
./bin/spark-submit --master yarn --num-executors 10 --executor-cores 10 --executor-memory 20G --conf spark.driver.memory=100G --conf spark.hadoop.fs.hdfs.impl.disable.cache=true --conf spark.security.credentials.renewalRatio=0.1 anomaly-detection-abt-equipment/src/spark/get_data.py -c <config.yaml>

```

If you receive ModuleNotFoundErrors, please make sure the following packages are installed correctly:
```
pip install numpy==1.14.2 scipy==1.4.0 pyyaml
```

## Miscellaneous info

### View Spark job logs
To view the logs of your Spark jobs you first have to connect to the lxtunnel using ssh SOCKS:
```
ssh -D 5000 <username>@lxtunnel.cern.ch
```
When you are connected, you must change your Firefox settings>Network settings: 
you can either set the SOCKS host to "localhost" on port "5000" or upload a configuration file that handles all CERN traffic in Firefox
through the tunnel. Now you can browse to http://ithdp1005.cern.ch:18080/ where you can search on your username to get a list of all ran jobs.


### Spark 3
Since the 2021 migration of Spark2 to Spark3 at CERN, including the NXCALS cluster, an increase of driver memory has been observed. Ensure that your machine has about 8 GB of RAM before executing the script. Note that lxplus generally does **not** have enough memory; a dedicated VM will have to be created.

### HDFS to EOS
To copy files from hdfs to eos, from lxplus:
```
source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh hadoop-nxcals
source /cvmfs/sft.cern.ch/lcg/views/LCG_96python3/x86_64-centos7-gcc8-opt/setup.sh
hdfs dfs -ls -h hdfs://nxcals/user/<username>/ml-data/
hdfs dfs -ls -h hdfs://nxcals/user/<username>/ml-data/featuresXXX/
hdfs dfs -cp hdfs://nxcals/user/<username>/ml-data/featuresXXX/partYYY.csv root://eosuser/eos/user/<first letter of username>/<username>/
# the following error can be ignored: syncFile: status [ERROR] Invalid operation
```
(See also https://cern.service-now.com/service-portal/article.do?n=KB0004426 and https://hadoop-user-guide.web.cern.ch/hadoop-user-guide/getstart/client_edge_machine.html)


### Data retrieval for specific years:
- 2017 (LBDS): all data can be queried with a single time interval spanning the full year (2017-01-01 00:00:00.000 - 2018-01-01 00:00:00.000)
- 2018 (LBDS): 
    + split up per month (faster due to lots of SCSS data (60M+/month) + safer because queries are sometimes aborted which causes loss of all progress)
    + last date has to be 2018-12-31 23:59:59.000 instead of 2019-01-01 00:00:00.000 because the second one errors
    + IMPORTANT: split april in 3 parts:\
                                         2018-04-01 00:00:00.000 - 2018-04-10 00:00:00.000\
                                         2018-04-10 00:00:00.000 - 2018-04-20 23:40:00.000\
                                         2018-04-21 00:00:00.000 - 2018-05-01 00:00:00.000
        - First split to avoid following error: 
        -  `ERROR AsyncEventQueue: Dropping event from queue appStatus. This likely means one of the listeners is too slow and cannot keep up with the rate at which tasks are being started by the scheduler.`
        - Second split for the non-promotable field error because the 'effectiveToleranceCh1Buffer' column changed type during 2018
