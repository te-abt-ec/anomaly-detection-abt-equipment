from datetime import datetime

import numpy as np
from scipy import signal

# TODO: fix use of f.col
import pyspark.sql.functions as f
from pyspark.sql import Window
from pyspark.sql.functions import udf, collect_list, first, last, asc, lit
from pyspark.sql.types import DateType, LongType, ArrayType

# Column names:
#   _dfindex: timestamps
#   _dfdev: device name
_dfindex = 'acqStamp'
_dfdev = 'device'

class DataProcessor:
    def __init__(self, excess_columns, time_interval=None, data=None, index=_dfindex, config=None, proc_type=None):
        """
        Args:
            excess_columns: The columns to drop
            time_interval: The time interval of the data
            data: The data
            index: The index of the data (in case of MKI, the timestamps of the IPOC data)
            config: The parsed config file (Spark part)
            proc_type: The preprocessor type which (currently) corresponds to the system (IPOC, SCSS, ...)
        """
        if not proc_type:
            print("No processor type provided, creating uninitialized processor")
            return
        self.proc_type = proc_type
        self.config = config
        if not data:
            self.data = None
        elif excess_columns:
            # remove excess columns with unnecessary data
            self.data = self.filter_columns(data, excess_columns)
        else:
            self.data = data
        if proc_type == "XPOC":
            # for XPOC data, the property really contains the device-info
            self.data = self.data.drop(_dfdev)
            self.data = self.data.withColumnRenamed("property", _dfdev)
        # remove columns with low variance commented out, done at local preprocessing to vary threshold
#        self.drop_non_variance_cols()
        self.beam = self.config["beam"]
        self.beam_filter_keywords = self.config["beam_identification"][self.beam]
        # filter on beam
        if self.config["beam_filter"]:
            self.filter_on_beam()
        self.date = time_interval
        if self.data and self.data.count() == 0:
            self.data = None
        if self.data:
            self.round_ts()
            self.data.orderBy(asc(index))
            # Maybe try some other ways of ordering? https://sparkbyexamples.com/pyspark/pyspark-orderby-and-sort-explained/
        self.sorted = False
        # only gets used once..., how to check if data gets unordered again?/quickly check ordering before every operation where ordering is needed?
        # this is maybe too time consuming...
        # or maybe not if we assume that operations that unorder the data unorder uniformly, so the unordering is noticed quickly in the beginning of the check sweep over the data

    def preprocess_data(self):
        """
        Call preprocessing functions based on function names and args in config
        """
        proc_funcs = self.config['preprocessing']['data_processor_config'][self.proc_type]
        for f_call in proc_funcs:
            (func, args), = f_call.items()
            # Check if arguments were passed
            if not args:
                # Is the function static or not?
                if 'static_' in func:
                    func = func.replace('static_', '')
                    getattr(DataProcessor, func)()
                else:
                    getattr(DataProcessor, func)(self)
            else:
                if 'static_' in func:
                    func = func.replace('static_', '')
                    getattr(DataProcessor, func)(*args)
                else:
                    if isinstance(args, list):
                        args = [self] + args
                    args = [self] + [args]
                    getattr(DataProcessor, func)(*args)

    def round_ts(self):
        """
        Round timestamps to seconds
        """
        self.data = self.data.withColumn(_dfindex, f.floor(f.col(_dfindex) / 1000000000))

    @staticmethod
    def filter_columns(df, filter_list):
        """
        Filter unnecessary columns from data using info in config yaml file
        (self.data is not used because it does not exist at time of method call)
        Args:
            df: DataFrame to filter columns from
            filter_list: Iterable of columns to filter
        """
        ll = df.columns
        for col in ll:
            if col in filter_list:
                df = df.drop(col)
        print("Filtered out {} from {} columns from the config".
              format(len([e for e in filter_list if e in ll]), len(ll)))
        return df

    def filter_on_beam(self):
        """
        Filter columns on the selected beam
        This function is called before combine_sensor_data, so it operates on the device column.
        """
        rows_before = self.data.count()
        print("Filtering data on beam {}, rows: {} with {}".
              format(str(self.beam), rows_before, self.get_beam_filter_function_string()))
        self.data = self.data.filter(eval(self.get_beam_filter_function_string()))
        rows_after = self.data.count()
        print("Filtered out {} rows, remaining rows: {}".
              format(str(rows_before - rows_after), rows_after))

    def get_beam_filter_function_string(self):
        """
        Construct a piece of code that represents the conditional for the beam filter
        """
        fs = "f.col(\"" + _dfdev + "\").like(\"%{}%\")"
        ff = fs.format(self.beam_filter_keywords[0])
        for i in range(len(self.beam_filter_keywords) - 1):
            ff = ff + " | " + fs.format(self.beam_filter_keywords[i + 1])
        return ff

    def drop_non_variance_cols(self, eps=0):
        """
        Drop columns that have a variance under a lower-bound
        Args:
            eps: lower bound for accepted variance
        """
        for col in self.data.schema.names:
            # rdd map needed because variance is returned as a DataFrame containing a Float
            var = self.data.agg({col: 'variance'}).rdd.map(lambda x: x[0]).collect()[0]
            if var is None or var <= eps:
                self.data = self.data.drop(col)

    def treshold(self, col_name, low, high):
        """
        Filter out extreme values outside of range [low, high]
        Args:
            col_name: name of column to treshold
            low: lower-bound
            high: upper-bound
        """
        # split into separate filters becuase the '&' operator gives errors
        self.data = self.data.filter(self.data[col_name] > low).filter(self.data[col_name] < high)

    def fft_ft(self, columns, fs, col_name_suffix, func, spark, start_df=None):
        for col in columns:
            freq, t, Sxx = signal.spectrogram(np.array(self.data.select(col).rdd.map(lambda x: x[0]).collect()), fs)
            fft_data = func(freq, t, Sxx)
            time = [0]
            time.extend(t[:-1])
            # numpy types currently not supported by spark
            time = [int(x) for x in time]
            fft_data = [float(x) for x in fft_data]
            if start_df is None:
                start_df = spark.createDataFrame(list(zip(time, fft_data)), ['int', 'float']).toDF('t',
                                                                                                   col + col_name_suffix)
                start_df = start_df.withColumn('t', f.from_unixtime(start_df['t']).cast(DateType()))
            else:
                temp_df = spark.createDataFrame(list(zip(time, fft_data)), ['int', 'float']).toDF('t2',
                                                                                                  col + col_name_suffix)
                temp_df = temp_df.withColumn('t2', f.from_unixtime(temp_df['t2']).cast(DateType()))
                # TODO fix assert
                # assert (start_df.select('t') == temp_df.select('t'))
                start_df = start_df.join(temp_df, start_df.t == temp_df.t2)
                start_df = start_df.drop('t2')
        start_df = start_df.drop('t')
        return start_df

    def resample(self, timestamps, step=1):
        """
        Resample data to certain timestamps (IPOC timestamps for MKI).
            - If there are any timestamps before the smallest data timestamp, these get saved to add again later
            - Creates a new column "next_ts" in the dataset that contains the value of _dfindex of the next entry
            - Replaces the _dfindex column with the new, final timestamps list created by data_range()
            - Remove the "next_ts" column and remove potential duplicate timestamps
        Args:
            timestamps: timestamps which will be kept in DataFrame
            step: in seconds, default=1, The step size to sample on
        """
        def date_range(t1, t2, ts, min_dftime, preceding_rows, step=1):
            """
            Creates a list of timestamps that lie between 2 given timestamps and are a subset of the IPOC timestamps.
            Removes the last timestamp of this created list if it is in the IPOC timestamps list.
            Adds the preceding timestamps of the IPOC data to this created list.
            Args:
                t1: first timestamp of the interval
                t2: last timestamp of the interval
                ts: the IPOC timestamps list
                min_dftime: The smallest timestamp of the IPOC dataset
                preceding_rows: List of IPOC timestamps that precede the smallest IPOC dataset timestamp
                step: Step size to generate timestamps (default = 1s)
            Returns:
                The created list with timestamps
            """
            t1 = int(t1)
            t2 = int(t2)
            # resample to seconds and generate all timestamps in seconds between t1 and t2
            seconds = [t1 + step * x for x in range(int((t2 - t1) / step) + 1)]
            # only keep IPOC timestamps
            #TODO: Maybe this can be sped up by instead checking in the previous statement if the entry is between min_ts and max_ts
            # so that it doesn't has to go over the whole list again
            time = [t for t in seconds if t in ts]
            # Only forwardfill until next timestamp if it is not in the IPOC timestamps (IPOC timestamps are generated by calling get_segment_timestamps from load_preprocess_write)
            # otherwise the following timestamp will be wrongly removed
            if t2 in ts and t1 != t2:
                time.remove(t2)

            # if there are extra IPOC timestamps before the first dataset timestamp, add them in front
            if t1 == min_dftime:
                if preceding_rows:
                    time = preceding_rows + time
                    preceding_rows.clear() # so this if does not get entered again, not sure if any time is saved by doing this but intuitively it might

            return time

        if sorted(timestamps) == sorted(self.data.select(_dfindex).rdd.map(lambda x: x[0]).collect()):
            print("No resampling needed, dataframe timestamps are equal to IPOC ones")
            return

        # map timestamps to int because otherwise data_range() resamples to []
        # because no timestamp from 'seconds' of type int is in a list of strings
        timestamps = [int(t) for t in timestamps]

        # get smallest timestamp in dataframe to check if there are any IPOC timestamps prior to the first timestamp of this dataset
        # these would otherwise be missed by the algorithm since in general only IPOC timestamps between two concurrent dataset timestamps are considered
        # could also be omitted by backward filling again after resampling, but this seems like a way less expensive option
        print("{} - Finding min timestamp".format(datetime.now()))
        # change type of dfindex column to long to enable sorting
        self.data = self.data.withColumn(_dfindex, self.data[_dfindex].cast("long"))
        min_dftime = self.data.agg({_dfindex: 'min'}).collect()[0][0]
        # we also need the smallest IPOC timestamp to be able to compare
        min_ts = min(timestamps)

        preceding_rows = []
        # if the smallest IPOC timestamp is smaller than the smallest dataset timestamp
        if min_dftime > min_ts:
            # go through all IPOC timestamps
            for stamp in timestamps:
                # add those that are smaller than the smallest dataset timestamp to a list
                if stamp < min_dftime:
                    preceding_rows.append(stamp)

        # udf to generate upsampled timestamps
        # will effectively generate a column of arrays containing IPOC timestamps in the range dictated by the start and end of _dfindex (see call a few lines down)
        date_range_udf = udf(lambda x, y: date_range(x, y, timestamps, min_dftime, preceding_rows, step), ArrayType(LongType()))

        # get upper limit of timestamps for final timestamp in dataframe (handles edge case for last timestamp)
        print("{} - Finding max timestamp".format(datetime.now()))
        #max_time = self.data.agg({_dfindex: 'max'}).collect()[0][0]
        # instead of taking the max timestamp of the dataset, we can take the max timestamp of the IPOC timestamps
        # by doing this, we solve the issue of missing IPOC timestamps if the max dataset timestamp is lower than the max IPOC timestamp
        # the + 1 is necessary because otherwise the timestamp itself will not be added (see date_range definition)
        max_time = max(timestamps) + 1

        # get next timestamp using sliding window
        # define window over entire timestamp column
        window = Window.orderBy(_dfindex)
        # create new column 'next_ts' that contains the next timestamp from _dfindex (default 'next row' offset for f.lead is 1), and this for every timestamp
        print("{} - Creating new column with next timestamp".format(datetime.now()))
        self.data = self.data.withColumn('next_ts', f.lead(f.col(_dfindex), default=max_time).over(window))

        # resample data to 'step'
        # for each pair of _dfindex and next_ts entries, the _dfindex entry is replaced by the sequence of IPOC timestamps between those entries
        # explode automatically forward fills these new rows with whatever values were present in the original _dfindex entry,
        # but for this to actually produce meaningful results all original timestamps must be filled in already -> RUN FORWARD FILL FIRST! (put it in config file)
        print("{} - Starting resample (=explode) operation".format(datetime.now()))
        self.data = self.data.withColumn(_dfindex, f.explode(date_range_udf(f.col(_dfindex), f.col('next_ts'))))

        # remove column with following timestamps
        print("{} - Removing temp column".format(datetime.now()))
        self.data = self.data.drop('next_ts')

        # join dataframes on timestamp index (rounded to nearest second)
        print("{} - Dropping duplicate timestamps".format(datetime.now()))
        self.data = self.data.dropDuplicates([_dfindex])

        # Remove old timestamps if they did not align with ipoc timestamps (doesn't explode do this already?)
        #print("{} - Removing non-IPOC timestamps".format(datetime.now()))
        #self.data = self.data.where(f.col(_dfindex).isin(timestamps))

        # Sort data (if not done locally)
        #self.data = self.data.orderBy(asc(_dfindex))

    def fill_all(self, cols, kind):
        """
        Fill all columns with method according to 'kind'
        Args:
            kind: 'forward' or 'back'"
        """

        print("{} - Starting {} fill".format(datetime.now(), kind))

        filled_cols = []

        # New column with constant value for window partitionBy later on, to suppress WARN WindowExec: No Partition Defined for Window operation
        cstcn = "groupid"
        self.data = self.data.withColumn(cstcn, lit("id1"))

        # Different windows depending on forward or back fill, starting at respectively the first and last row until the current one
        if kind == "forward":
            window = Window.partitionBy(cstcn).orderBy(_dfindex).rowsBetween(Window.unboundedPreceding, Window.currentRow)
        else:
            window = Window.partitionBy(cstcn).orderBy(_dfindex).rowsBetween(Window.currentRow, Window.unboundedFollowing)

        # for each column in the dataframe
        for col in cols:
            if kind == "forward":
                # if forward fill, get the last known non-null value from this column before the current row
                filled_col = last(self.data["`" + col + "`"], ignorenulls=True).over(window).alias(col)
            else:
                # if back fill, get the first known non-null value from this column after the current row
                filled_col = first(self.data["`" + col + "`"], ignorenulls=True).over(window).alias(col)

            # make a list of all filled columns
            filled_cols.append(filled_col)

        # return the dataframe in which all columns specified in 'cols' are 'kind'-filled, preceded by the untouched _dfindex column
        return self.data.select([_dfindex] + filled_cols)

    def forward_fill(self):
        """
        Forward and backfill all columns
        """

        # order dataframe by ascending dfindex, time-consuming
        print("{} - Ordering timestamps".format(datetime.now()))
        # change type of dfindex column to long to enable sorting
        self.data = self.data.withColumn(_dfindex, self.data[_dfindex].cast("long"))
        if not self.sorted:
            self.data = self.data.orderBy(asc(_dfindex))
            self.sorted = True

        # no need to fill _dfindex column
        cols = self.data.schema.names
        if _dfindex in cols:
            cols.remove(_dfindex)

        self.data = self.fill_all(cols, 'forward')
        # backfill is only actually needed to fill nulls before the first non-null value, disabled
        # cause these few values don't matter much and important runtime improvement
        #self.data = self.fill_all(cols, 'back')

    @staticmethod
    def combine(df, name):
        """
        Returns a DataFrame with dfindex and an array of the non-unique values from the grouped aggregation on _dfindex
        """
        return df.groupby(_dfindex).agg(collect_list(name).alias(name + '_agg'))

    def combine_sensor_data(self):
        """
        Combine sensor data needed because:
        1/ several devices published data at the same _dfindex (i.e. acqStamp)
        2/ same device can publish multiple time for the same _dfindex, often with different data
        As a solution:
        1/ Using pivot to transpose the data per device; yielding a single row with additional columns per grouped aggregate
        2/ FIXME: for now simply dropping the others, results in data loss; strategy to be defined
        """
        print(
            '{} - {} combine_sensor_data starting, {} entries with {} columns'.format(datetime.now(), self.proc_type,
                                                                                      self.data.count(),
                                                                                      len(self.data.columns)))
        df = self.data.dropDuplicates()
        cols = df.schema.names
        if _dfdev not in cols:
            print(
                "{} - Info - no device column found in {} data, sensor data won't be transposed".format(datetime.now(), self.proc_type))
        elif df.groupBy(_dfdev).count().count() < 2:
            print(
                "{} - Info - single device found in {} data, sensor data won't be transposed".format(datetime.now(), self.proc_type))

            # comment out below code if you don't want column renaming if there is only one device, it's a quick fix really and not functionally needed
            renamed_cols = []
            devicename = df.select(_dfdev).first()[0]
            for col in cols:
                # if col is timestamp or device column, just add it
                if col == _dfindex:
                    renamed_cols.append(_dfindex)
                elif col == _dfdev:
                    renamed_cols.append(_dfdev)
                else:
                    # else, add it in the same way as the result one would get from the .groupBy.pivot.agg line below
                    renamed_cols.append(devicename + "_first(" + col + ", false)")
            df = df.toDF(*renamed_cols)
            df = df.drop(_dfdev)
        else:
            cols.remove(_dfindex)
            cols.remove(_dfdev)
            firsts = {}
            for e in cols:
                if e not in firsts:
                    firsts[e] = 'first'
            # generate new columns based on the different devices for this dataset (e.g. B1 and B2)
            df = df.groupBy(_dfindex).pivot(_dfdev).agg(firsts)

        print("{} - Info - removing remaining {} duplicates".format(datetime.now(), _dfindex))
        # using dropDuplicates here causes information to be lost in some cases, it is now done after resampling
        # sometimes the timestamp duplicate that contains actual information is removed instead of the null one
        # if somehow dropduplicates could be conditioned to only keep the duplicate timestamp that has useful other columns. this might speed things up
        # I suspect the speedup would be marginal, since there are not that many duplicate stamps (at least not for SCSS_MKI.UA23.GEN)
        #self.data = df.dropDuplicates([_dfindex])
        self.data = df
        print(
            '{} - {} combine_sensor_data done, {} entries remain with {} columns'.format(datetime.now(), self.proc_type,
                                                                                         self.data.count(),
                                                                                         len(self.data.columns)))

    def handle_arrays(self):
        print("{} - Start array handling".format(datetime.now()))
        def safe_mean(lst):
            lst = [x for x in lst if x is not None]
            return sum(lst) / len(lst)

        def fst_struct(arr):
            d = None
            try:
                if arr[0]:
                    d = arr[0][0]
            except IndexError:
                print("{} - [0][0] IndexError for data element {}".format(datetime.now(), *arr))
            return d

        try:
            method = self.config['preprocessing']['array_handling']
        except KeyError as e:
            print("{} - Missing {} parameter in config, dropping ArrayType columns".format(datetime.now(), e))
            method = 'drop'
        # UDF to only calculate mean when list is not None and if there are None values in the list, remove them
        mean_udf = udf(lambda r: safe_mean(r) if r is not None else None)
        # UDF to only try and take first element when there is a list
        fst_udf = udf(lambda r: r[0] if r is not None else None)
        # columns with arrays from the NXCALS query appear to be of the 'StructType' type instead of 'ArrayType'
        # in this case the value we want is one level deeper than it would be for a regular array
        fst_udf_struct = udf(lambda r: fst_struct(r) if r is not None else None)
        cols = []
        for col, dtype in self.data.dtypes:
            if 'array' in dtype:
                cols.append(col)
                if method == 'drop':
                    self.data = self.data.drop(col)
                elif method == 'mean':
                    self.data = self.data.withColumn(col, mean_udf(f.col(col)))
                elif method == 'fst':
                    if 'struct' in dtype:
                        self.data = self.data.withColumn(col, fst_udf_struct(f.col(col)))
                    else:
                        self.data = self.data.withColumn(col, fst_udf(f.col(col)))
                else:
                    print("Warning: invalid array_handling parameter in config, dropping array instead")
                    self.data = self.data.drop(col)
        print("{} - Array handling completed. Columns where arrays have been found were {}".format(datetime.now(), cols))

    def sliding_window_avg_diff(self, col_name, window_size=3):
        """
        Sliding window feature which calculates the difference between the sliding mean
        Args:
            col_name: column to apply operation on
            window_size: size of sliding window
        """
        window = Window.partitionBy(_dfdev).orderBy(_dfindex).rowsBetween(-window_size // 2, window_size // 2)
        filled_col = f.avg(self.data[col_name]).over(window)
        self.data = self.data.withColumn(col_name + '_avg_diff', self.data[col_name] - filled_col)

    def sliding_window_sum(self, col_name, window_size=3):
        """
        Sliding window feature which calculates the sliding sum
        Args:
            col_name: column to apply operation on
            window_size: size of sliding window
        """
        window = Window.partitionBy(_dfdev).orderBy(_dfindex).rowsBetween(-window_size // 2, window_size // 2)
        filled_col = f.sum(self.data[col_name]).over(window)
        self.data = self.data.withColumn(col_name + '_sw', filled_col)

    def apply_sw(self, window_size):
        print("{} - Calculating sliding window features".format(datetime.now()))
        # added call before for-loop to avoid iterating over newly added columns due to sw
        cols = self.data.schema.names
        print("{} - Nr of columns before sw: {}".format(datetime.now(), len(cols)))
        for col in cols:
            if col == _dfindex:
                continue
            self.sliding_window_sum(col, window_size)
            self.sliding_window_avg_diff(col, window_size)
        print("{} - Nr of columns after sw: {}".format(datetime.now(), len(self.data.schema.names)))

    @staticmethod
    def handle_strings(data):
        """
        Cast columns of type string to float (unused)
        """
        for col_name in data.schema.names:
            data = data.withColumn(col_name, f.col(col_name).cast('float'))
        return data

    def handle_bool(self):
        """
        Cast columns of type bool to integer
        """
        for col, dtype in self.data.dtypes:
            if dtype == 'bool':
                self.data = self.data.withColumn(col, f.col(col).cast('integer'))

    def drop_buffer_cols(self):
        """
        Remove buffer arrays from data
        """
        for col in self.data.schema.names:
            if 'Buffer' in col:
                self.data = self.data.drop(col)
