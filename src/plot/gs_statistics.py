import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import numpy as np


from util import get_config
import util
from pipeline import pipeline
import evaluation.evaluation

# for AD parameters and 3d plots
#import plot.pyplot as plt

# for regular statistics plot
import matplotlib.pyplot as plt

import pandas as pd
from datetime import datetime, timedelta
import math
import plotly.express as px

from preprocessing.preprocess_modules import *
from IO.IO import IO

import plotly.graph_objects as go
from plotly.subplots import make_subplots


def plot_gs_statistics(cfg):
    io = IO(cfg, 'grid_search')
    df = io.read_csv("grid_search_statistics_B1_iforestres_gooddaterange_filter1.csv", util.RESULTS_DIR + '/statistics/')
    print(df)
    y_to_plot = ["auc", "rank", "TP", "FN", "FP", "TN"]

    nb_rows = 3
    nb_cols = 2
    fig = make_subplots(rows=nb_rows, cols=nb_cols)
    for i in range(6):
        fig.add_trace(go.Scatter(x=np.array(range(len(df))), y=df[y_to_plot[i]],
                                 mode='lines+markers',
                                 name=y_to_plot[i]), row=math.floor(i / nb_cols) + 1, col=i % nb_cols + 1)
    fig.update_layout(title_text="Grid search {}".format(y_to_plot[i]))
    fig.show()


def plot_gs_statistic(cfg):
    io = IO(cfg, 'grid_search')
    filenames = ["grid_search_statistics_B2_iforestres.csv",
                 "grid_search_statistics_B2_gmmres.csv"]
    y_to_plot = ["auc"]
    legends = [
        "iforest",
        "gmm"
    ]

    dfs = []
    for fn in filenames:
        dfs.append(io.read_csv(fn, io.grid_search_statistics_path))
    print(dfs[0].columns)

    def get_plot_index(row, col, index):
        return row * 100 + col * 10 + index

    for i, df in enumerate(dfs):
        plt.subplot(get_plot_index(1, len(dfs), i+1))
        plt.plot(np.array(range(len(df))), df[y_to_plot[0]])
        plt.xlabel('grid search result')
        plt.ylabel(y_to_plot[0])
        plt.legend([legends[i]])
    plt.suptitle("{} for gs results".format(y_to_plot[0]))
    plt.show()


def plot_3d_statistics(cfg):
    io = IO(cfg, 'grid_search')
    #filename = "grid_search_statistics_B2_gmmres.csv"
    filename = "grid_search_statistics_B2_iforestres.csv"
    #y_to_plot = "n_components"
    #possible_y_values = [2, 4, 6, 8, 10]
    #y_to_plot = "init_params"
    #possible_y_values = ['random', 'kmeans']
    y_to_plot = "max_features"
    #possible_y_values = ['standard', 'none']
    #possible_y_values = [10, 25, 50, 100]
    #possible_y_values = [32, 64]
    #possible_y_values = [0.001, 0.005, 0.01]
    possible_y_values = [.4, .5, .6, .8]
    y_colors = ['red', 'blue', 'green', 'purple', 'yellow']
    z_to_plot = "auc"

    df = io.read_csv(filename, io.grid_search_statistics_path)
    print(df.columns)
    print(len(df))

    colors = []
    for i in df[y_to_plot]:
        colors.append(y_colors[possible_y_values.index(float(i))])

    fig = go.Figure(data=[go.Scatter3d(x=np.array(range(len(df))), y=df[y_to_plot], z=df[z_to_plot], mode='markers',
                                       marker=dict(
                                           size=12,
                                           color=colors,  # set color to an array/list of desired values
                                       ))])
    fig.show()


def plot_multi_pipeline_run_stats(cfg, statistics_dfs, dfs_to_plot = ['auc', 'rank'], save_figures=True):
    io = IO(cfg, 'pipeline')
    ad = cfg["pipeline"]["anomaly_detection"]["anomaly_detector"]
    machine = cfg["machine"]

    plot_data = {type: {'x': [], 'y': [], 'err_min': [], 'err_max': []} for type in dfs_to_plot}
    # means
    for type in dfs_to_plot:
        for experiment_name, stats_df in statistics_dfs.items():
            plot_data[type]['x'].append(experiment_name)
            plot_data[type]['y'].append(stats_df[type].mean())
            plot_data[type]['err_min'].append(stats_df[type].mean() - stats_df[type].min())
            plot_data[type]['err_max'].append(stats_df[type].max() - stats_df[type].mean())
    # plot statistics
    threshold = cfg["pipeline"]["anomaly_detection"][ad]["threshold"]

    #plot
    colors = ['bo']
    e_colors = ["#045EDB"]

    # seperate plots
    """
    for i, type in enumerate(dfs_to_plot):
        plt.figure(figsize=(7.5, 5))
        plt.errorbar(plot_data[type]['x'], plot_data[type]['y'], yerr=[plot_data[type]['err_min'], plot_data[type]['err_max']],
                     ecolor=e_colors[0],
                     fmt=colors[0])
        plt.title(
            " {}: {} for different experiments | {} | {} Beam {}".format(machine, type, ad, io.start_year, io.beam),
            fontsize=10)
        plt.xlabel("Experiment")
        plt.ylabel(type)
        if save_figures:
            prefix = "{}_{} for different experiments".format(machine, type)
            filename = '{}_{}.png'.format(prefix, ad)
            fig_path = io.get_figures_path(ad)
            io.save_figure(plt, fig_path, filename)
    plt.show()
    """

    cols = 2
    rows = math.ceil(len(dfs_to_plot) / cols)
    fig, axs = plt.subplots(nrows=rows, ncols=cols, sharex=True)
    for i, type in enumerate(dfs_to_plot):
        if len(dfs_to_plot) <= 2:
            ax = axs[i]
        else:
            curr_col = i % cols
            curr_row = math.floor(i / cols)
            ax = axs[curr_row, curr_col]
        ax.errorbar(plot_data[type]['x'],
                                         plot_data[type]['y'],
                                         yerr=[plot_data[type]['err_min'], plot_data[type]['err_max']],
                           ecolor=e_colors[0],
                           fmt=colors[0])
        ax.set(xlabel='Experiment', ylabel=type)
    fig.suptitle(" {}: {} mean, min and max | {} | {} Beam {} | threshold: {}".format(machine, dfs_to_plot, ad, io.start_year, io.beam, threshold),
                           fontsize=10)
    if save_figures:
        prefix = "{}_{} mean, min and max".format(machine, dfs_to_plot)
        filename = '{}_{}.png'.format(prefix, ad)
        fig_path = io.get_figures_path(ad)
        io.save_figure(plt, fig_path, filename)
    fig.tight_layout()
    return fig


if __name__ == '__main__':
    # config
    parser = argparse.ArgumentParser(description="Python script plot useful graphs")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()



    # code
    machine = cfg['machine'].lower()
    logbook_fn = cfg['pipeline']['logbook']

    # Anomaly vs detectors parameters
    detector_params = {}
    ad = cfg['pipeline']['anomaly_detection']
    for d in ['gmm', 'iforest', 'hbos']:
        detector_params[d] = ad[d]

    #plot_gs_statistics(cfg)
    #plot_gs_statistic(cfg)
    #plot_3d_statistics(cfg)

    io = IO(cfg, 'pipeline')
    # get filenames
    nb_runs = 100
    detector = ad = cfg['pipeline']['anomaly_detection']['anomaly_detector']
    statistics_filename = io.get_multiple_pipeline_statistics_filename(nb_runs, detector)
    statistics_filename = 'grid_search_statistics_multiple_pipelines_100_(-12h,12h).csv'
    f_path = io.get_multiple_pipeline_statistics_path(detector)
    statistics_df = io.read_csv(statistics_filename, f_path)
    statistics_dfs = {'Experiment 1': statistics_df}

    dfs_to_plot = ['auc', 'rank', 'TP', 'FN', 'FP', 'TN']
    fig = plot_multi_pipeline_run_stats(cfg, statistics_dfs, dfs_to_plot=dfs_to_plot)
    plt.show()