import util
import plot.pyplot as plt


def anomaly_scores(index, scores, anomaly_detector, show=True, filename=None):
    """
    Plots anomaly scores generated by the given AnomalyDetector.
    :param index: array_like, the index for the scores
    :param scores: array_like, the scores to plot
    :param anomaly_detector: string, the name of the AnomalyDetector that was used
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    fig = plt.figure()
    plt.scatter(index, scores, s=2, label="score")
    # plt.scatter(index, sorted(scores), s=1, label="score sorted")

    plt.title("Anomaly scores generated by {}".format(anomaly_detector))
    plt.xlabel("Time")
    plt.ylabel("Anomaly Score")
    plt.tight_layout()

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    plt.close(fig)
