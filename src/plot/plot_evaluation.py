import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import numpy as np


from util import get_config
import util
from pipeline import pipeline
import evaluation.evaluation
import plot.pyplot as plt
import pandas as pd
from datetime import datetime, timedelta
import math

from preprocessing.preprocess_modules import *
from IO.IO import IO

import plotly.graph_objects as go
from plotly.subplots import make_subplots

def plot_precision_recall_curve(precision, recall, auc=None, show=True, filename=None):
    fig = plt.figure()
    plt.plot(precision, label="precision")
    plt.plot(recall, label="recall")

    plt.xlabel("threshold index")
    plt.ylim([0.0, 1.05])
    plt.xlim([0, len(precision)])
    plt.tight_layout(rect=(0, 0, 1, 0.98))
    plt.title('Precision and recall')
    plt.legend(loc="upper center")
    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, "pr-" + fname + util.FILE_EXTENSION))
    if show:
        plt.show()
    plt.close(fig)

    fig = plt.figure()
    plt.plot(recall, precision)
    # plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.00])
    plt.tight_layout(rect=(0, 0, 1, 0.98))
    if auc is not None:
        plt.title('2-class Precision-Recall curve, area = {0:0.3f}'.format(auc))
    else:
        plt.title('2-class Precision-Recall curve')
    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, "pr-curve-" + fname + util.FILE_EXTENSION))
    if show:
        plt.show()
    plt.close(fig)


def plot_pr_curves_named(pr_dict, show=True, filename=None):
    fig = plt.figure()

    for name in pr_dict:
        recall = pr_dict[name]["recall"]
        precision = pr_dict[name]["precision"]
        label = "{} (area = {:.3f})".format(name, pr_dict[name]["auc"])

        plt.plot(recall, precision, label=label)

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.00])
    plt.tight_layout(rect=(0, 0, 1, 0.98))
    plt.title('2-class Precision-Recall curves')
    plt.legend(loc="upper right")

    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, "pr-curve-" + fname + util.FILE_EXTENSION))
    if show:
        plt.show()

    plt.close(fig)


def labels_vs_predictions_for_different_thresholds(truth_and_pred_df, features, feature_regexes, show=True,
                                                   filename=None):
    """
    Takes a DataFrame of ground truth and anomaly score predictions and plots the anomalies that were predicted for
    different thresholds. The labels are also plotted so that the predictions can be checked.
    :param truth_and_pred_df: DataFrame of ground truth and predictions as given by `segments_scored_to_truth_and_pred_df`
    :param features: DataFrame of features
    :param feature_regexes: list of measurement regexes to plot the labels and anomalies over
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    threshold_percentiles = [0.99, 0.98, 0.95]  # [0.99, 0.95, 0.85, 0.75]
    thresholds = truth_and_pred_df.y_pred.quantile(threshold_percentiles)

    labels = truth_and_pred_df[truth_and_pred_df.y_true > 0]
    ts_labels = labels.timestamp_max.tolist()

    for percentile, threshold in zip(threshold_percentiles, thresholds):
        fig, axes = plt.subplots(nrows=len(feature_regexes) + 1, ncols=1, sharex="col")
        ax0 = axes[0] if len(feature_regexes) > 0 else axes

        # make predictions for the current threshold
        predictions_df, p, r, _, _, _, _ = evaluation.evaluation.make_predictions_for_threshold(truth_and_pred_df,
                                                                                                threshold,
                                                                                                'y_pred')
        predictions = predictions_df[predictions_df.y_pred == True]
        ts_predictions = predictions.timestamp_max.tolist()

        # in the first subplot, just show dots for labels and dots for predictions
        # ax0.set_xlim(features.index.min(), features.index.max())
        ax0.scatter(x=ts_labels, y=labels.y_true, c="red", label="Anomalies", s=200, zorder=2, edgecolors="black")
        ax0.scatter(x=ts_predictions, y=predictions.y_pred, c="blue", label="Predictions", s=25, zorder=3)
        legend = ax0.legend(loc="upper right", bbox_to_anchor=(1.1, 1))
        legend.legendHandles[0]._sizes = [35]
        legend.legendHandles[1]._sizes = [35]

        # for each given column regex, plot the data of the columns and plot the labels and predictions over the data.
        if len(feature_regexes):
            for axis, regex in zip(axes[1:], feature_regexes):
                df = features.filter(regex=regex).sort_index(axis=1)

                axis.scatter(x=ts_labels, y=df.mean(axis=1)[ts_labels], c="red", label="Anomalies", s=200, zorder=2,
                             edgecolors="black")
                axis.scatter(x=ts_predictions, y=df.mean(axis=1)[ts_predictions], c="blue", label="Predictions", s=25,
                             zorder=3)

                for col in df:
                    axis.scatter(x=df.index, y=df[col], s=2)

                axis.set_xlim(df.index.min(), df.index.max())
                yrange_offset = 0.05 * (df.max().max() - df.min().min())
                axis.set_ylim(df.min().min() - yrange_offset, df.max().max() + yrange_offset)

                axis.set_title(regex)
                legend = axis.legend(loc="upper right", bbox_to_anchor=(1.15, 1))
                for i in range(len(df.columns)):
                    legend.legendHandles[i]._sizes = [35]

        plt.xlabel("Time")
        plt.suptitle(
            "Labels vs. predictions for threshold at {}-th percentile (= {:.2E}, precision = {:.2f}, recall = {:.2f})"
            .format(int(percentile*100), threshold, p, r), fontsize=15, fontweight="bold"
        )
        plt.tight_layout(rect=(0, 0, 1, 0.98))

        if filename is not None:
            plt.savefig(filename + "-threshold_" + str(percentile) + util.FILE_EXTENSION)
        if show:
            plt.show()

        plt.close(fig)

def anomalies_vs_different_detectors(io, machine, detectors_data ,beam, time_interval, labels, thresholds):
    """
    Creates a table plot showing if the anomaly is detected as a TP for the given detector.
    Args:
        detectors_data: Dictionary with keys the detector names (string) that have to be compared
                   (gmm, iforest, hbos, admercs) and values another dict listing the parameters of this AD.
        time_interval: All anomalies in this interval will be compared
        labels: The logbook anomaly data
    """
    remove_duplicate_anomalies_12h_range = False
    y = []
    header_values = ["Anomaly"]
    annotations = []

    for ad, metadata in detectors_data.items():
        id, parameters = metadata
        header_values.append("<b>{}</b> (t={})".format(ad.upper(), thresholds[ad]))
        print("===Calculating {}:".format(ad))
        f_path = io.get_pipeline_output_path(ad)
        truth_and_pred_df = io.read_csv_with_id(id, f_path)

        # EVALUATION: Area Under PR-Curve
        auc, precision, recall = pipeline.evaluate_pr(truth_and_pred_df)
        # EVALUATION: Ranking
        rank = pipeline.evaluate_rank(truth_and_pred_df)

        # TP FN FP TN
        anomalys_plot_data = truth_and_pred_df[truth_and_pred_df.y_true == 1].sort_values(by=['timestamp_min'], ascending=True)
        nb_of_true_anomalies = anomalys_plot_data.shape[0]
        #print(anomalys_plot_data[['y_true', 'y_pred', 'timestamp_min', 'timestamp_max', 'nb']])
        print("number of true anomalies: ", nb_of_true_anomalies)

        threshold = thresholds[ad]
        threshold_score = truth_and_pred_df['y_pred'].quantile(threshold)
        print("treshold value: {}".format(str(threshold_score)))
        nb_TP = anomalys_plot_data[anomalys_plot_data.y_pred >= threshold_score].shape[0]
        nb_FN = nb_of_true_anomalies - nb_TP
        fp_truth_and_pred_df = truth_and_pred_df[truth_and_pred_df.y_true == 0][truth_and_pred_df.y_pred >= threshold_score].sort_values(by=['y_pred'], ascending=False)
        nb_FP = len(fp_truth_and_pred_df)
        nb_TN = len(truth_and_pred_df) - nb_TP - nb_FN - nb_FP
        print("TP: ", nb_TP)
        print("FN: ", nb_FN)
        print("FP: ", nb_FP)
        print("TN: ", nb_TN)
        annotations.append("{}: TP: {} | FP: {} | FN: {} | TN: {} | AUC: {:.4f} | rank: {:.3f} | threshold: {}".format(ad.upper(), str(nb_TP), str(nb_FP), str(nb_FN), str(nb_TN), auc, rank, str(threshold)))

        # find biggest and smallest anomaly dates
        start_an_tot = datetime.strptime(anomalys_plot_data.iloc[0]['timestamp_min'], '%Y-%m-%d %H:%M:%S')
        end_an_tot = datetime.strptime(anomalys_plot_data.iloc[-1]['timestamp_max'], '%Y-%m-%d %H:%M:%S')

        # x
        labels = labels.loc[start_an_tot - timedelta(hours=12):end_an_tot + timedelta(hours=12)]

        if machine == "mki":
            anomaly_dates = labels["EVENTDATE"].tolist()
            anomaly_dates = [np.datetime64(datetime.strptime(a, '%d/%m/%Y %H:%M:%S')) for a in anomaly_dates]
        elif machine == "lbds":
            anomaly_dates = labels["Timestamp"].tolist()
            anomaly_dates = [np.datetime64(datetime.strptime(a, '%Y-%m-%d %H:%M:%S')) for a in anomaly_dates]
        anomaly_dates.sort(reverse=False)
        x = anomaly_dates
        if len(y) == 0:
            # add the anomalies as first column
            y.append([])
            for i in range(len(x)):
                split = np.datetime_as_string(x[i]).split('T')
                y[0].append("{} {}".format(split[0], split[1].split('.')[0]))

        if remove_duplicate_anomalies_12h_range:
            # remove "duplicate" anomalies within 12 hours
            anomaly_dates_new = []
            for i in range(len(anomaly_dates)):
                if i == len(anomaly_dates) - 1 or anomaly_dates[i] < anomaly_dates[i + 1] - np.timedelta64(12, 'h'):
                    anomaly_dates_new.append(anomaly_dates[i])
            x = anomaly_dates_new

        # y
        curr_y = [0 for i in x]
        true_positives = anomalys_plot_data[anomalys_plot_data.y_pred >= threshold_score].sort_values(by=['timestamp_min'])
        #print(true_positives[['y_true', 'y_pred', 'timestamp_min', 'timestamp_max', 'nb']])
        # for each anomaly in x, check if the EVENTDATE lies inbetween the min and max of a detected entry in true_positives
        # If yes, set that index to 1
        for i in range(len(x)):
            for j in range(nb_TP):
                a_min = np.datetime64(true_positives.iloc[j].timestamp_min)
                a_max = np.datetime64(true_positives.iloc[j].timestamp_max)
                if a_min <= x[i] <= a_max:
                    curr_y[i] = 1
                    break
        y.append(curr_y)


    # plot
    red = 'rgb(200, 0, 0)'
    green = 'rgb(0,150,0)'
    black = 'rgb(0,0,0)'
    white = 'rgb(255,255,255)'
    colors = [red, green]

    fill_color = [white]
    text_color = [black]
    text_align = ['right']
    column_widths = [120]
    for i in range(len(detectors_data)):
        fill_color.append(np.array(colors)[y[i+1]])
        text_color.append(white)
        text_align.append('center')
        column_widths.append(10)

    layout_annotations = [go.layout.Annotation(
                showarrow=False,
                text=annotations[i],
                x=0.5,
                y=0.1*(len(detectors_data)+1) - (0.1 * (i+1))
            ) for i in range(len(annotations))]

    layout = go.Layout(
        annotations=layout_annotations)

    table = go.Table(
        columnwidth=column_widths,
        header=dict(
            values=header_values,
            line_color='white', fill_color='white',
            align=text_align, font=dict(color='black', size=12)
        ),
        cells=dict(
            values=y,
            line_color=fill_color,
            fill_color=fill_color,
            align=text_align, font=dict(color=text_color, size=11)
        ))

    fig = go.FigureWidget(data=[table], layout=layout) # to include annotations

    #pr curve?
    #plot_precision_recall_curve(precision, recall, auc, show=False, filename=fig_filename)
    return table


def parameter_vs_confusion_matrix(machine, detector, beam, time_interval, labels):
    """
        Creates a plot of the confusion matrix parameters vs the parameter varying.
        Args:
            detector: The detector that is used (gmm, iforest, hbos, admercs)
            parameter: The parameter that will be put on the x-axis
            values: The values to plot on the x-axis of parameter
            beam: The beam number
            time_interval: The time interval to calculate the confusion matrix over
            labels: The logbook anomaly data
        """

    scale_data = 'standard'
    segment_score = 'max'
    parameter = "n_bins"
    values = [4, 8, 16, 32, 64, 283]

    # y will be TP, FN, FP, TN, auc or rank
    y = np.zeros((6, len(values)))

    for k in range(len(values)):
        print("{} value: {}".format(parameter, str(values[k])))
        parameters = {}
        # CHANGE HERE WHICH PARAMETER TO PLOT
        if detector == "hbos":
            parameters = get_hbos_parameters(n_bins=values[k])
        if detector == "iforest":
            parameters = get_iforest_parameters()
        if detector == "gmm":
            parameters = get_gmm_parameters()

        truth_and_pred_df = get_truth_and_pred_df(machine, time_interval, beam, detector, parameters["scale_data"],
                                                  parameters["segment_score"], parameters["params"])

        # EVALUATION: Area Under PR-Curve
        auc, precision, recall = pipeline.evaluate_pr(truth_and_pred_df)
        # EVALUATION: Ranking
        rank = pipeline.evaluate_rank(truth_and_pred_df)

        # TP FN FP TN
        anomalys_plot_data = truth_and_pred_df[truth_and_pred_df.y_true == 1].sort_values(by=['timestamp_min'],
                                                                                          ascending=True)
        nb_of_true_anomalies = anomalys_plot_data.shape[0]

        threshold = .99
        if detector == 'gmm':
            threshold = .99
        elif detector == 'iforest':
            threshold = .95
        elif detector == 'hbos':
            threshold = .97  # untested

        threshold_score = truth_and_pred_df['y_pred'].quantile(threshold)
        nb_TP = anomalys_plot_data[anomalys_plot_data.y_pred >= threshold_score].shape[0]
        nb_FN = nb_of_true_anomalies - nb_TP
        fp_truth_and_pred_df = truth_and_pred_df[truth_and_pred_df.y_true == 0][
            truth_and_pred_df.y_pred >= threshold_score].sort_values(by=['y_pred'], ascending=False)
        nb_FP = len(fp_truth_and_pred_df)
        nb_TN = len(truth_and_pred_df) - nb_TP - nb_FN - nb_FP
        y[0][k] = nb_TP
        y[1][k] = nb_FN
        y[2][k] = nb_FP
        y[3][k] = nb_TN
        y[4][k] = auc
        y[5][k] = rank

    y_names = ["TP", "FN", "FP", "TN", "AUC", "rank"]
    names_to_plot = [True, True, True, True, True, True]
    nb_rows = 3
    nb_cols = 2
    fig = make_subplots(rows=nb_rows, cols=nb_cols)
    for i in range(len(y)):
        if names_to_plot[i]:
            fig.add_trace(go.Scatter(x=values, y=y[i],
                                     mode='lines+markers',
                                     name=y_names[i]), row=math.floor(i/nb_cols)+1, col=i%nb_cols + 1)
    fig.update_layout(title_text="Confusion matrix metrics vs {}".format(parameter))
    fig.show()


def get_truth_and_pred_df(machine, time_interval, beam, ad, scale_data, segment_score, parameters):
    splitted_time_interval = (time_interval[0].split("-"), time_interval[1].split("-"))
    start_year = splitted_time_interval[0][0]
    start_month = splitted_time_interval[0][1]
    end_month = splitted_time_interval[1][1]

    filename = machine.upper() +"_pipeline_" + str(start_year) + "_" + util.beam_to_str(beam) + "_" + ad
    s = "_scale_data={}_segment_score={}".format(scale_data, segment_score)
    filename = filename + s
    print(filename)
    # add parameters to filename
    for p_name, v in parameters.items():
        filename = filename + "_{}={}".format(p_name, v)

    fn = util.CERN_DATA_DIR + '/' + ad + '_results/' + machine + '/' + str(start_year) + '/' + str(start_month) + '-' + str(end_month) + '/' + filename + ".csv"
    truth_and_pred_df = pd.read_csv(fn)
    return truth_and_pred_df

def get_logbook(machine, logbook_fn, beam):
    try:
        # LOAD LOGBOOK
        f_path = util.CERN_DATA_DIR + '/'
        print("Loading logbook data from {} ...".format(logbook_fn), end='')
        logbook = pd.read_csv(f_path + logbook_fn)
        # Set start time as logbook index
        logbook = logbook.set_index(list(logbook)[0])
        logbook.index = pd.to_datetime(logbook.index)
        # Filter on anomalies
        if machine == "mki":
            logbook = logbook[logbook["TAG"] == "anomaly"]
        # Only keep anomalies for current beam (if this column exists)
        if 'BEAM' in logbook:
            logbook = logbook[logbook['BEAM'] == beam]
        elif 'Beam' in logbook:
            logbook = logbook[logbook['Beam'] == beam]
        else:
            print("No beam specified for logbook anomalies, assuming all labeled anomalies are for current beam")
        print("done!")
    except FileNotFoundError:
        print("ERROR - missing {}, continuing without labels".format(logbook_fn))
        logbook = None
    return logbook


def plot_anomalies_vs_detectors(io, cfg_gs, machine, logbook_fn, detector_params, detectors_to_plot, thresholds):
    time_interval = cfg_gs['time_interval']
    beam = cfg_gs['beam']

    detectors_data = {}
    for detector in detectors_to_plot:
        id = io.get_id_with_params(detector, detector_params[detector])
        pred_df = io.get_pred_with_params(detector, detector_params[detector])
        if pred_df is not None:
            detectors_data[detector] = id, pred_df
        else:
            print("Predictions file with params not found for {}, continuing...".format(detector))
    labels = get_logbook(machine, logbook_fn, beam)
    fig = anomalies_vs_different_detectors(io, machine, detectors_data, beam, time_interval, labels, thresholds)
    return fig


def plot_confusion_matrix_vs_parameter(machine, logbook_fn):
    detector = "hbos"
    time_interval = ('2017-03-01 00:00:00.000', '2017-12-01 00:00:00.000')
    beam = 1
    labels = get_logbook(machine, logbook_fn, beam)

    parameter_vs_confusion_matrix(machine, detector, beam, time_interval, labels)

def plot_feature_statistics(machine, cfg):
    io = IO(cfg, 'pipeline')

    # configuration
    overwrite_time_interval = False  # default is min and max timestamp of input features
    time_interval = ('2017-05-01 00:00:00.000', '2017-11-01 00:00:00.000')
    beam = "1"
    # supported statistics : "min", "max", "mean", "variance", "median", "mode", "skew", "std"
    statistics_to_show = ["min", "max", "mean", "std", "variance", "median", "mode", "skew"]
    sort_on = "variance"  # None for no sort
    ascending = True
    raw_prepped_variance = "variance"  #TODO: raw data contains strings and NaN, null values, drop it?
    filter_with_regexes_show = True  # False for all columns
    filter_with_regexes_drop = False
    #columns_to_show_regexes = ['IPOC_(\w|\d){3}\.(\w|\d){4}\.IPOC\.CTC1A\.\wB\w_first\(maxCurrent,false\)']

    columns_to_show_regexes = [ # mode features
                                #'(\w|\d)*Mode(\w|\d)*',
                                # current features
                                #'(\w|\d)*Current(\w|\d)*'
                                #'(\w|\d)*bagk(\w|\d)*'
        'BEI(\w|\d)*'
    ]

    columns_to_drop_regexes = ['(\w|\d)*Current(\w|\d)*',
                               '(\w|\d)*Mode(\w|\d)*',
                               '(\w|\d)*triggerTimeStamp(\w|\d)*']

    """
    columns_to_drop_regexes = columns_to_drop_regexes + [
        '(\w|\d)*triggerTimeStamp(\w|\d)*',
        '(\w|\d)*Mode(\w|\d)*',
        '(\w|\d)*bisPulseVoltage(\w|\d)*',
        '(\w|\d)*switchARatio(\w|\d)*',
        '(\w|\d)*switchBRatio(\w|\d)*',
        '(\w|\d)*TrckNom(\w|\d)*',
        '(\w|\d)*bagk(\w|\d)*',
        '(\w|\d)*dcpsIcRef(\w|\d)*',
        '(\w|\d)*pllCtrlState(\w|\d)*',
        '(\w|\d)*State(\w|\d)*'
    ]
    """
    round_values_to = 3
    remove_fourier = False
    remove_SW = False


    # idea: ranges of statistics as input to filter => easier in dash


    # code
    if not overwrite_time_interval:
        time_interval = cfg["pipeline"]["time_interval"]
    if raw_prepped_variance == "raw":
        filename = cfg["local_preprocessing"]["raw_data_filename"]
        index = cfg["spark"]["index"]
        f_path = io.raw_data_path
    elif raw_prepped_variance == "prepped":
        filename = cfg["local_preprocessing"]["train_data_filename"]
        index = cfg["pipeline"]["index"]
        f_path = io.preprocessed_data_path
    elif raw_prepped_variance == "variance":
        filename = cfg["pipeline"]["preprocessing"]["variance_filtered_filename"]
        index = cfg["pipeline"]["index"]
        f_path = io.pipeline_data_path
    print("Plotting feature statistics for {}".format(filename))

    df = pd.read_csv(f_path + filename)
    df = set_features_index(df, index=index)
    df = sort_on_index(df, ascending=True)

    df = df[time_interval[0]:time_interval[1]]

    assert not (filter_with_regexes_show and filter_with_regexes_drop)
    cols_to_show = []
    if not filter_with_regexes_show:
        cols_to_show = list(df.columns)
        if filter_with_regexes_drop:
            for regex in columns_to_drop_regexes:
                mask = get_mask_for_regex(df.columns, regex)
                for c in mask:
                    if c in cols_to_show:
                        cols_to_show.remove(c)
    else:
        for regex in columns_to_show_regexes:
            mask = get_mask_for_regex(df.columns, regex)
            for c in mask:
                if c not in cols_to_show:
                    cols_to_show.append(c)

    if remove_fourier:
        non_fourier_cols = []
        for c in cols_to_show:
            if "fft" not in c:
                non_fourier_cols.append(c)
        cols_to_show = non_fourier_cols

    if remove_SW:
        non_SW_cols = []
        for c in cols_to_show:
            if "SW" not in c:
                non_SW_cols.append(c)
        cols_to_show = non_SW_cols

    y = []
    if len(y) == 0:
        # add the cols_to_show as first column
        y.append([])
        for c in cols_to_show:
            y[0].append(c)

    df = df[cols_to_show]
    columns = df.columns
    for i in range(len(y[0])):
        assert y[0][i] == columns[i]

    dataframe_shape = df.shape
    print(dataframe_shape)

    # add statistics to y
    for s in statistics_to_show:
        if s == "min":
            y.append([round(i, round_values_to) for i in df.min().tolist()])
        if s == "min":
            y.append([round(i, round_values_to) for i in df.max().tolist()])
        if s == "mean":
            y.append([round(i, round_values_to) for i in df.mean().tolist()])
        if s == "variance":
            y.append([round(i, round_values_to) for i in df.var().tolist()])
        if s == "std":
            y.append([round(i, round_values_to) for i in df.std().tolist()])
        if s == "median":
            y.append([round(i, round_values_to) for i in df.median().tolist()])
        if s == "skew":
            y.append([round(i, round_values_to) for i in df.skew().tolist()])
        if s == "mode":
            modes_list = []
            mode = df.mode()
            for col in y[0]:
                modes_list.append(round(mode[col][0], round_values_to))
            y.append(modes_list)

    if sort_on:
        transposed_y = list(map(list, zip(*y)))
        index_to_sort_on = 0
        for i in range(len(statistics_to_show)):
            if sort_on == statistics_to_show[i]:
                index_to_sort_on = i+1  # first column is not a statistic (column name)
        transposed_y = sorted(transposed_y, key=lambda x: x[index_to_sort_on], reverse=not ascending)
        y = list(map(list, zip(*transposed_y)))




    # plot
    # TODO: show colors if under/above certain threshold or fade colors dependant on value
    black = 'rgb(0,0,0)'

    light_orange = 'rgb(255, 217, 179)'
    light_green = 'rgb(198, 255, 179)'
    colors = [light_orange, light_green]

    fill_color = []
    for i in range(len(statistics_to_show) + 1):
        colors_col = []
        for c in y[0]:
            if ("B1" in c) or ("LHC1" in c):
                colors_col.append(np.array(colors)[0])
            if ("B2" in c) or ("LHC2" in c):
                colors_col.append(np.array(colors)[1])
        fill_color.append(colors_col)


    text_color = [black]
    text_align = ['left']
    column_width = [100]
    line_color = ['white']
    for i in range(len(statistics_to_show)):
        text_color.append(black)
        text_align.append('left')
        column_width.append(25)
        line_color.append('white')

    annotation_texts = ["Dataframe shape: {}".format(dataframe_shape),
                        "Colors seperate Beam 1 and 2",
                        "Sorted {} on {}".format("ascending" if ascending else "descending", sort_on),
                        "remove_fft: {} | remove_SW: {}".format(remove_fourier, remove_SW)]

    layout_annotations = [go.layout.Annotation(
        showarrow=False,
        text=annotation_texts[i],
        y=-0.02 - (0.025 * (i+1)),
        x=0.5
    ) for i in range(len(annotation_texts))]

    header_values = ["Feature"] + statistics_to_show

    layout = go.Layout(annotations=layout_annotations)
    #layout = go.Layout()
    fig = go.FigureWidget(data=[go.Table(
        columnwidth=column_width,
        header=dict(
            values=header_values,
            line_color='white',
            fill_color='white',
            align=text_align, font=dict(color='black', size=12)
        ),
        cells=dict(
            values=y,
            fill_color=fill_color,
            line_color=fill_color,
            align=text_align, font=dict(color=text_color, size=11)
        ))
    ], layout=layout)

    # pr curve?
    # plot_precision_recall_curve(precision, recall, auc, show=False, filename=fig_filename)
    fig.update_layout(
        title_text="{} Feature statistics in period {} to {} / Beam {}".format(cfg["machine"], str(df.index[0]),
                                                                              str(df.index[-1]),
                                                                              str(beam)),
        showlegend=True)
    fig.show()





if __name__ == '__main__':
    # config
    parser = argparse.ArgumentParser(description="Python script plot useful graphs")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()



    # code
    machine = cfg['machine'].lower()
    logbook_fn = cfg['pipeline']['logbook']

    # Anomaly vs detectors parameters
    detector_params = {}
    ad = cfg['pipeline']['anomaly_detection']
    for d in ['gmm', 'iforest', 'hbos']:
        detector_params[d] = ad[d]

    #plot_anomalies_vs_detectors(cfg, machine, logbook_fn, detector_params)
    #plot_confusion_matrix_vs_parameter(machine, logbook_fn)
    plot_feature_statistics(machine, cfg)





