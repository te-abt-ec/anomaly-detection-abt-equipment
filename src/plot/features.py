import os

import numpy as np
import pandas as pd
from sklearn.neighbors import KernelDensity

import util
from plot import pyplot as plt


def get_segment_lengths_in_minutes(segments):
    return [segment.length_time() / np.timedelta64(1, "m") for segment in segments if len(segments) > 1]


def segment_length_histogram(segments, title="Histogram of segment length", axis=None, show=True):
    """
    Creates a histogram of segment lengths.
    If axis is provided, no new figure will be created.
    :param segments: list of Segments
    :param title: Title of the plot
    :param axis: Axis to plot the data on
    :param show: bool, whether or not to show the figure
    """
    lengths = get_segment_lengths_in_minutes(segments)

    if axis is None:
        fig, axis = plt.subplots()
    else:
        fig = None

    bin_width = 10
    counts, _, _ = axis.hist(lengths, bins=range(0, 210, bin_width), edgecolor='black')
    axis.set_xticks(range(0, 210, bin_width))
    for i, count in enumerate(counts):
        axis.text(i * 10 + bin_width / 2, count + 0.01 * max(counts), str(int(count)), ha='center')

    axis.set_title(title)
    axis.set_xlabel("minutes")
    axis.set_ylabel("Amount of segments")
    plt.tight_layout()

    if show:
        plt.show()
    if fig:
        plt.close(fig)


def segment_length_histogram_beams(segments_b1, segments_b2, suptitle, subtitle_1, subtitle_2, show=True, fname=None):
    """
    Creates histograms of Segment lengths for beam 1 and beam 2.
    :param segments_b1: list of Segments for beam 1
    :param segments_b2: list of Segments for beam 2
    :param suptitle: Title of the figure
    :param subtitle_1: Title of subplot 1
    :param subtitle_2: Title of subplot 2
    :param show: bool, whether or not to show the figure
    :param fname: If given, name of the file the image will be saved to
    """
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharex="col", sharey="row")

    segment_length_histogram(segments_b1, subtitle_1, axis=ax1, show=False)
    segment_length_histogram(segments_b2, subtitle_2, axis=ax2, show=False)
    ax2.set_ylabel("")
    plt.tight_layout(rect=(0, 0, 1, 0.95))
    plt.suptitle(suptitle, fontsize=15)

    if fname is not None:
        plt.savefig(fname + util.FILE_EXTENSION)
    if show:
        plt.show()
    if fig:
        plt.close(fig)


def segment_length_density(segments, title="Density estimation of segment length", axis=None, show=True):
    """
    Applies Kernel Density Estimation using a Guassian kernel on segment lengths and displays the estimation.
    If axis is provided, no new figure will be created.
    :param segments: list of Segments
    :param title: Title of the plot
    :param axis: Axis to plot the data on
    :param show: bool, whether or not to show the figure
    """
    lengths = get_segment_lengths_in_minutes(segments)

    if axis is None:
        fig, axis = plt.subplots()
    else:
        fig = None

    kde = KernelDensity(kernel='gaussian', bandwidth=3)
    kde.fit(np.array(lengths)[:, np.newaxis])

    x_plot = np.linspace(0, 200, 1000)[:, np.newaxis]
    log_dens = kde.score_samples(x_plot)
    axis.plot(x_plot, np.exp(log_dens))

    # The area under the curve (should be 1) can be estimated as follows:
    # np.trapz(np.exp(log_dens), x_plot.ravel())

    axis.set_title(title)
    axis.set_xlabel("minutes")
    axis.set_ylabel("Density")
    plt.tight_layout()

    if show:
        plt.show()
    if fig:
        plt.close(fig)


def segment_length_density_beams(segments_b1, segments_b2, suptitle, subtitle_1, subtitle_2, show=True, fname=None):
    """
    Creates Density Estimation plots of Segment lengths for beam 1 and beam 2.
    :param segments_b1: list of Segments for beam 1
    :param segments_b2: list of Segments for beam 2
    :param suptitle: Title of the figure
    :param subtitle_1: Title of subplot 1
    :param subtitle_2: Title of subplot 2
    :param show: bool, whether or not to show the figure
    :param fname: If given, name of the file the image will be saved to
    """
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharex="col", sharey="row")

    segment_length_density(segments_b1, subtitle_1, axis=ax1, show=False)
    segment_length_density(segments_b2, subtitle_2, axis=ax2, show=False)
    ax2.set_ylabel("")
    plt.tight_layout(rect=(0, 0, 1, 0.95))
    plt.suptitle(suptitle, fontsize=15)

    if fname is not None:
        plt.savefig(fname + util.FILE_EXTENSION)
    if show:
        plt.show()
    if fig:
        plt.close(fig)


def segment_length_density_multiple(segment_lists, distances, title, show, fname):
    """
    Creates multiple Density Estimation plots for a list of lists of segments
    :param segment_lists: list of lists of Segments
    :param distances: list of distances that were used for generating the given segments
    :param title: string, title of the figure
    :param show: bool, whether or not to show the figure
    :param fname: If given, name of the file the image will be saved to
    """
    fig, axis = plt.subplots()

    for segments, d in zip(segment_lists, distances):
        lengths = get_segment_lengths_in_minutes(segments)

        kde = KernelDensity(kernel='gaussian', bandwidth=3)
        kde.fit(np.array(lengths)[:, np.newaxis])

        x_plot = np.linspace(0, 200, 1000)[:, np.newaxis]
        log_dens = kde.score_samples(x_plot)
        axis.plot(x_plot, np.exp(log_dens), label="d = {} ({} segments)".format(d, len(segments)))

    axis.set_title(title)
    axis.set_xlabel("minutes")
    axis.set_ylabel("Density")
    axis.set_xticks(range(0, 210, 10))
    axis.legend(loc="upper right")
    plt.tight_layout()

    if fname is not None:
        plt.savefig(fname + util.FILE_EXTENSION)
    if show:
        plt.show()

    plt.close(fig)


def sliding_windows(data, sw_dfs, unit=""):
    """
    :param data: DataFrame as given by query
    :param sw_dfs: list of DataFrames
    :param unit: unit of y-axis
    """
    fig, axes = plt.subplots(nrows=1, ncols=1)
    mean = pd.Series(data.mean()[0], index=data.index)

    data.plot(ax=axes)
    for df in sw_dfs:
        df.plot(ax=axes)
    mean.plot(ax=axes)

    labels = [data.columns[0]] + [df.columns[0] for df in sw_dfs] + ["mean"]
    axes.legend(labels, loc="upper left")
    axes.set_title(data.columns[0] + " with sliding windows")
    axes.set_ylabel(unit)
    axes.set_xlabel("Time")
    plt.tight_layout()
    plt.show()
    plt.close(fig)


def plot_feature_selection(history, rank, filename=None):
    """
    :param history: Numpy 1D array with the best AUC value for each iteration
    :param filename: If given, name of the file the image will be saved to
    """
    x = [i + 1 for i in range(history.size)]
    y = list(history)

    fig = plt.figure()
    plt.plot(x, y, 'o-')
    plt.xlabel('Iteration', fontsize=20)
    plt.ylabel('Best ' + rank, fontsize=20)

    if rank == 'AUC':
        plt.ylim([0, 1])
    else:
        plt.ylim([min(history), max(history)])

    plt.xticks(x)
    fig.suptitle('Feature selection', fontsize=20)

    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, fname + util.FILE_EXTENSION))


def plot_feature_importance(feature_importance, columns, n=10, filename=None):
    """
    :param feature_importance: the feature importances of an estimator
    :param columns: columns of the df used to fit the estimator
    :param filename: If given, name of the file the image will be saved to
    """
    feat_importance = pd.Series(feature_importance, index=columns)
    feat_importance.nlargest(n).plot(kind='barh')

    plt.title('Top 10 most important features')
    plt.tight_layout()

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
