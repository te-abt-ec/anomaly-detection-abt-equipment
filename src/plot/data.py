import numpy as np

import util
import plot.pyplot as plt


def continuous(data, title, unit=None, ts_anomalies=None, ts_predictions=None, axis=None, show=True, filename=None):
    """
    Creates a plot of continuous data.
    If axis is provided, no new figure will be created.
    :param data: DataFrame as given by query
    :param title: string, title of plot
    :param unit: string, unit of y-axis
    :param ts_anomalies: list of segments to overlay on the graphs
    :param axis: Axis to plot the data on.
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    if axis is None:
        fig, axis = plt.subplots()
    else:
        fig = None

    data.sort_index(axis=1).plot(ax=axis)

    if ts_anomalies is not None:
        axis.scatter(x=ts_anomalies, y=data.mean(axis=1)[ts_anomalies], c="red", label="Anomalies", s=200, zorder=999)

    if ts_predictions is not None:
        axis.scatter(x=ts_predictions, y=data.mean(axis=1)[ts_predictions], c="blue", label="Predictions", s=100,
                     zorder=999)

    axis.set_title(title, y=1.03)
    axis.set_xlabel("Time")
    if unit is not None:
        axis.set_ylabel(unit)
    # axis.legend(loc="upper left")
    axis.legend(loc="upper right", bbox_to_anchor=(1.1, 1), labelspacing=0.1)
    plt.tight_layout(rect=(0, 0, 0.92, 1))

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    if fig:
        plt.close(fig)


def continuous_beams(data, measurement, unit, axes=None, show=True, filename=None):
    """
    Splits continuous data and creates subplots of beam 1 data and of beam 2 data.
    If axes are provided, no new figure will be created.
    :param data: DataFrame as given by query
    :param measurement: string, name of measurement
    :param unit: string, unit of y-axis
    :param axes: 2 axes to plot the data on.
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    if axes is None:
        fig, axes = plt.subplots(nrows=1, ncols=2, sharex="col", sharey="row")
    else:
        fig = None
    ax1, ax2 = axes

    continuous(data.filter(regex=".*B1:.*"), "{}".format(measurement), unit, axis=ax1, show=False)
    continuous(data.filter(regex=".*B2:.*"), "{}".format(measurement), unit, axis=ax2, show=False)
    plt.tight_layout(w_pad=3, rect=(0, 0, 0.96, 1))

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    if fig:
        plt.close(fig)


def continuous_beams_filtered(data, data_filtered, measurement, unit, show=True, filename=None):
    """
    Plots continuous data and filtered continuous data of beams 1 and 2.
    :param data: DataFrame as given by query
    :param data_filtered: DataFrame as given by query
    :param measurement: string, name of measurement
    :param unit: string, unit of y-axis
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    fig, axes = plt.subplots(nrows=2, ncols=2, sharex="col", sharey="row")

    plt.rcParams["legend.fontsize"] = "large"
    continuous_beams(data, measurement, unit, axes=axes[0], show=False)
    continuous_beams(data_filtered, measurement + ", filtered", unit, axes=axes[1], show=False)
    # plt.tight_layout()

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    plt.close(fig)
    plt.rcParams["legend.fontsize"] = "x-large"


def ipoc(data, title, unit=None, axis=None, show=True, filename=None):
    """
    Creates a scatter plot of IPOC data.
    If axis is provided, no new figure will be created.
    :param data: DataFrame as given by query
    :param title: string, title of plot
    :param unit: string, unit of y-axis
    :param axis: Axis to plot the data on
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    if axis is None:
        fig, axis = plt.subplots()
    else:
        fig = None

    for series in data.sort_index(axis=1):
        if len(data):
            axis.scatter(data.index, data[series], s=2)

    axis.set_title(title)
    axis.set_xlabel("Time")
    if unit is not None:
        axis.set_ylabel(unit)
    axis.set_xlim(data.index.min(), data.index.max())
    yrange_offset = 0.05 * (data.max().max() - data.min().min())
    axis.set_ylim(data.min().min() - yrange_offset, data.max().max() + yrange_offset)

    colormap = plt.cm.get_cmap('gist_ncar')
    color_list = [colormap(i) for i in np.linspace(0, 0.9, len(axis.collections))]
    for i, c in enumerate(axis.collections):
        c.set_color(color_list[i])
    legend = axis.legend(loc="lower right", bbox_to_anchor=(1.1, 0.1), labelspacing=0.1)
    for i in range(len(data.columns)):
        legend.legendHandles[i]._sizes = [50]
    plt.tight_layout(rect=(0, 0, 0.92, 1))

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    if fig:
        plt.close(fig)


def ipoc_beams(data, measurement, unit, axes=None, show=True, filename=None):
    """
    Splits IPOC data and creates subplots of beam 1 data and of beam 2 data.
    If axes are provided, no new figure will be created.
    :param data: DataFrame as given by query
    :param measurement: string, name of measurement
    :param unit: string, unit of y-axis
    :param axes: 2 axes to plot the data on
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    if axes is None:
        fig, axes = plt.subplots(nrows=1, ncols=2, sharex="col", sharey="row")
    else:
        fig = None
    ax1, ax2 = axes

    ipoc(data.filter(regex=".*B1:.*"), "{}, Beam 1".format(measurement), unit, ax1, show=False)
    ipoc(data.filter(regex=".*B2:.*"), "{}, Beam 2".format(measurement), unit, ax2, show=False)
    plt.tight_layout(w_pad=3, rect=(0, 0, 0.96, 1))

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    if fig:
        plt.close(fig)


def ipoc_beams_filtered(data, data_filtered, measurement, unit, show=True, filename=None):
    """
    Plots ipoc data and filtered ipoc data of beams 1 and 2.
    :param data: DataFrame as given by query
    :param data_filtered: DataFrame as given by query
    :param measurement: string, name of measurement
    :param unit: string, unit of y-axis
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    fig, axes = plt.subplots(nrows=2, ncols=2, sharex="col", sharey="row")

    plt.rcParams["legend.fontsize"] = "large"
    ipoc_beams(data, measurement, unit, axes=axes[0], show=False)
    ipoc_beams(data_filtered, measurement + " filtered", unit, axes=axes[1], show=False)
    axes[0][0].set_xlabel("")
    axes[0][1].set_xlabel("")
    axes[0][1].set_ylabel("")
    axes[1][1].set_ylabel("")

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    plt.close(fig)
    plt.rcParams["legend.fontsize"] = "x-large"


def timestamp_differences(index, differences, unit, hline_min, title=None, show=True, filename=None):
    """
    Creates a scatter plot of differences between consecutive timestamps.
    :param index: array_like, the index the differences have been calculated on
    :param differences: array_like, the differences to show
    :param unit: string, the unit of the differences
    :param hline_min: int, the minute at which to plot a horizontal line
    :param title: string, the title of the figure
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    :return:
    """
    fig = plt.figure()
    plt.plot_date(index, differences, markersize=2, label="difference")
    if hline_min is not None:
        plt.axhline(y=60 * hline_min, linewidth=1, color="blue", label="{} min".format(hline_min))

    plt.xlabel("Time")
    plt.ylabel(unit)
    plt.ylim(bottom=0)
    plt.title(title if title is not None else "Differences between consecutive timestamps")
    legend = plt.legend(loc="upper right")
    legend.legendHandles[0]._sizes = [50]
    plt.tight_layout()

    if filename is not None:
        plt.savefig(filename + util.FILE_EXTENSION)
    if show:
        plt.show()
    plt.close(fig)
