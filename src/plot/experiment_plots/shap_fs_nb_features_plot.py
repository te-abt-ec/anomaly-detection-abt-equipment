#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, RESULTS_DIR
from IO.IO import IO
from scripts.experiments.shap_feature_selection import run_experiment

from itertools import chain, combinations
from datetime import datetime
import util

import matplotlib.pyplot as plt

np.random.seed(456)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds_variance_gridsearch.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')

    pred_types = ["TP", "FN", "FP", "TN"]
    add_magnets = True

    # import data to plot
    # note, the "bests" csv lists the pipeline runs with the best AUC, so plotting bests for rank is not logical
    filenames = [
        "shap_feature_selection_experiment_['TP', 'FP']_bests_auc_2021-05-01 09:13:44.313922.csv",
        "shap_feature_selection_experiment_['TP', 'FP']_result_2021-05-01 09:13:44.316871.csv",
        "shap_feature_selection_experiment_['TP', 'FN', 'FP', 'TN']_bests_auc_2021-05-02 00:35:15.718267.csv",
        "shap_feature_selection_experiment_['TP', 'FN', 'FP', 'TN']_result_2021-05-02 00:35:15.721947.csv"
    ]

    shap_filenames = {
        'best_auc': "shap_feature_selection_experiment_['TP', 'FN', 'FP']_bests_auc_2021-05-03 03:37:48.880574.csv",
        'best_rank': "shap_feature_selection_experiment_['TP', 'FN', 'FP']_bests_rank_2021-05-03 03:37:48.882669.csv",
        'average': "shap_feature_selection_experiment_['TP', 'FN', 'FP']_result_2021-05-03 03:37:48.886060.csv"
    }

    shap_filenames = {
        'best_auc': "shap_feature_selection_experiment_['FP']_bests_auc_2021-05-03 11:31:54.894881.csv",
        'best_rank': "shap_feature_selection_experiment_['FP']_bests_rank_2021-05-03 11:31:54.897728.csv",
        'average': "shap_feature_selection_experiment_['FP']_result_2021-05-03 11:31:54.903018.csv"
    }


    pred_types = ["FP"]
    add_magnets = True

    shap_dfs = {}
    for name, fn in shap_filenames.items():
        shap_dfs[name] = io.read_csv(fn, RESULTS_DIR + '/experiments/shap_ranking/')

    x_to_plot = 'nb_cols'

    for i, (name, df) in enumerate(shap_dfs.items()):
        selected = df[df['add_magnets'] == add_magnets]
        shap_dfs[name] = selected.sort_values(by=[x_to_plot])

    x_name = "number of features"
    y_to_plot = {'plot1': ['best_auc', 'average'],
                 'plot2': ['best_rank', 'average']}
    ys = ['auc', 'rank']
    y_names = ["AUC", "rank"]

    colors = ['r-o', 'b-x']
    names = ["best [FP]",
             "average [FP]"]
    for j, (plotname, dfs_to_plot) in enumerate(y_to_plot.items()):
        plt.figure(figsize=(7.5, 5))
        for i, y in enumerate(dfs_to_plot):
            plt.plot(shap_dfs[y][x_to_plot], shap_dfs[y][ys[j]], colors[i])
        plt.title(
            "shap feature selection | add_magnets: {} | {} Beam {}".format(add_magnets, io.start_year, io.beam),
            fontsize=10)
        plt.legend(names)
        plt.xlabel(x_name)
        plt.ylabel(y_names[j])

    plt.show()