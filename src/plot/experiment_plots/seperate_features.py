#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, RESULTS_DIR
from IO.IO import IO

import util

import matplotlib.pyplot as plt

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    df_path = io.seperate_feature_results_path
    beam = io.beam
    ad = cfg["pipeline"]["anomaly_detection"]["anomaly_detector"]
    nb_iterations = 5

    filename = "seperate_features_means_2021-05-23 02:24:37.896330.csv"
    iterations_filenames = ["seperate_features_pipeline_{}.csv".format(i) for i in range(nb_iterations)]

    save_figures = True

    # get min and max auc/rank for error bars
    df_auc = io.read_csv(filename, df_path)
    df_rank = io.read_csv(filename, df_path)
    iter_dfs = [io.read_csv(i, df_path) for i in iterations_filenames]
    df_auc["feature"] = df_auc.index
    df_rank["feature"] = df_rank.index
    for i, df in enumerate(iter_dfs):
        iter_dfs[i]["feature"] = df.index

    aucs_df = pd.DataFrame(data={str(i): list(iter_dfs[i]["auc"]) for i in range(nb_iterations)},
                           index=iter_dfs[0]["feature_set_name"])
    ranks_df = pd.DataFrame(data={str(i): list(iter_dfs[i]["rank"]) for i in range(nb_iterations)},
                           index=iter_dfs[0]["feature_set_name"])
    error_auc = pd.DataFrame(data={"min_auc": df_auc["auc"] - aucs_df.min(axis=1),
                                    "max_auc": aucs_df.max(axis=1) - df_auc["auc"],
                                   "auc": df_auc["auc"]},
                              index=iter_dfs[0]["feature_set_name"])
    error_rank = pd.DataFrame(data={"min_rank": df_rank["rank"] - ranks_df.min(axis=1),
                                    "max_rank": ranks_df.max(axis=1) - df_rank["rank"],
                                    "rank": df_rank["rank"]},
                              index=iter_dfs[0]["feature_set_name"])



    x_to_plot = 'feature'
    df_auc = df_auc.sort_values(by=["auc"], ascending=False)
    df_rank = df_rank.sort_values(by=["rank"])
    error_auc = error_auc.sort_values(by=["auc"], ascending=False)
    error_rank = error_rank.sort_values(by=["rank"])

    dfs = [df_auc, df_rank]

    x_name = "Feature name"

    y_to_plot = {'plot1': ['seperate_features_auc'],
                 'plot2': ['seperate_features_rank']}
    infixes = [
        "seperate_features_auc (averaged over 5 pipelines)",
        "seperate_features_rank (averaged over 5 pipelines)"
    ]
    ys = ['auc', 'rank']
    y_names = ["AUC", "rank"]

    colors = ['bo', 'rx', 'gv']
    e_colors = ["#045EDB"]
    for j, (plotname, dfs_to_plot) in enumerate(y_to_plot.items()):
        plt.figure(figsize=(7.5, 5))
        for i, y in enumerate(dfs_to_plot):
            if "auc" in y:
                y_auc = dfs[j][ys[j]]
                plt.errorbar(dfs[j][x_to_plot], y_auc, yerr=[list(error_auc["min_auc"]), list(error_auc["max_auc"])],
                             ecolor=e_colors[i],
                             fmt=colors[i])
            elif "rank" in y:
                y_rank = dfs[j][ys[j]]
                plt.errorbar(dfs[j][x_to_plot], y_rank, yerr=[list(error_rank["min_rank"]), list(error_rank["max_rank"])],
                             ecolor=e_colors[i],
                             fmt=colors[i])
        plt.title(
            "Seperate feature sets | {} | {} {} Beam {}".format(infixes[j], ad, io.start_year, beam),
            fontsize=10)
        plt.legend([y_names[j]])
        plt.xlabel(x_name)
        plt.ylabel(y_names[j])
        if save_figures:
            infix = "Seperate feature selection"
            filename = '{}_{}_{}_{}.png'.format(infix, x_to_plot, infixes[j], ad)
            io.save_figure(plt, df_path, filename)

    plt.show()