#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, RESULTS_DIR
from IO.IO import IO

import util

import matplotlib.pyplot as plt

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    beam = io.beam
    ad = cfg["pipeline"]["anomaly_detection"]["anomaly_detector"]

    gs_df_path = RESULTS_DIR + '/statistics/{}/{}/'.format(io.machine.lower(), io.start_year)
    pl_df_path = RESULTS_DIR + '/statistics/multiple_pipelines/{}/{}/{}/'.format(ad, io.machine.lower(), io.start_year)

    gs_filenames = {'xpoc+data': 'grid_search_statistics_B2_iforestres_DATA+XPOC.csv',
                 'data': 'grid_search_statistics_B2_iforestres_DATA ONLY.csv',
                 'xpoc': 'grid_search_statistics_B2_iforestres_XPOC ONLY.csv'}

    pl_filenames = {'xpoc+data': 'grid_search_statistics_multiple_pipelines_400_DATA+XPOC.csv',
                    'data': 'grid_search_statistics_multiple_pipelines_400 DATA ONLY.csv',
                    'xpoc': 'grid_search_statistics_multiple_pipelines_400_XPOC ONLY.csv'}

    feature_set_size = {'xpoc+data': "1195",
                        'data': '1046',
                        'xpoc': '147'}

    save_figures = True
    # get dfs
    gs_dfs = {}
    pl_dfs = {}
    for k, v in gs_filenames.items():
        gs_dfs[k] = pd.read_csv(gs_df_path + '/' +  v)
    for k, v in pl_filenames.items():
        pl_dfs[k] = pd.read_csv(pl_df_path + '/' + v)
    # get min and max auc/rank for error bars
    mean_aucs_df = pd.DataFrame(data={'auc': [pl_dfs[k]['auc'].mean() for k,v in pl_filenames.items()]}, index=list(pl_dfs.keys()))
    mean_ranks_df = pd.DataFrame(data={'rank': [pl_dfs[k]['rank'].mean() for k, v in pl_filenames.items()]}, index=list(pl_dfs.keys()))

    error_auc = pd.DataFrame(data={"min_auc": [mean_aucs_df["auc"].loc[k] - pl_dfs[k]['auc'].min() for k in pl_filenames.keys()],
                                   "max_auc": [pl_dfs[k]['auc'].max() - mean_aucs_df["auc"].loc[k] for k in pl_filenames.keys()],
                                   "auc": mean_aucs_df["auc"].loc[k]},
                              index=pl_filenames.keys())
    error_auc = pd.DataFrame(
        data={"min_auc_err": [mean_aucs_df["auc"].loc[k] - pl_dfs[k]['auc'].min() for k in pl_filenames.keys()],
              "max_auc_err": [pl_dfs[k]['auc'].max() - mean_aucs_df["auc"].loc[k] for k in pl_filenames.keys()],
              "auc": mean_aucs_df["auc"].loc[k]},
        index=pl_filenames.keys())
    error_rank = pd.DataFrame(
        data={"min_rank_err": [mean_ranks_df["rank"].loc[k] - pl_dfs[k]['rank'].min() for k in pl_filenames.keys()],
              "max_rank_err": [pl_dfs[k]['rank'].max() - mean_ranks_df["rank"].loc[k] for k in pl_filenames.keys()],
              "rank": mean_ranks_df["rank"].loc[k]},
        index=pl_filenames.keys())

    dfs = [mean_aucs_df, mean_ranks_df]

    x_name = "Feature set"

    y_to_plot = {'plot1': ['auc'],
                 'plot2': ['rank']}
    infixes = [
        "auc (averaged over 100 pipelines)",
        "rank (averaged over 100 pipelines)"
    ]
    ys = ['auc', 'rank']
    y_names = ["AUC", "rank"]

    colors = ['bo', 'rx', 'gv']
    e_colors = ["#045EDB"]
    for j, (plotname, dfs_to_plot) in enumerate(y_to_plot.items()):
        plt.figure(figsize=(8.5, 5))
        for i, y in enumerate(dfs_to_plot):
            if "auc" in y:
                y_auc = dfs[j][ys[j]]
                plt.errorbar(dfs[j].index + ' (' + list(feature_set_size.values()) + ')',
                             y_auc,
                             yerr=[list(error_auc["min_auc_err"]), list(error_auc["max_auc_err"])],
                             ecolor=e_colors[i],
                             fmt=colors[i])
            elif "rank" in y:
                y_rank = dfs[j][ys[j]]
                plt.errorbar(dfs[j].index + ' (' + list(feature_set_size.values()) + ')',
                             y_rank,
                             yerr=[list(error_rank["min_rank_err"]), list(error_rank["max_rank_err"])],
                             ecolor=e_colors[i],
                             fmt=colors[i])
        plt.title(
            "Regular data vs xpoc (AD params chosen for best rank) | {} | {} {} Beam {}".format(infixes[j], ad, io.start_year, beam),
            fontsize=10)
        plt.legend([y_names[j]])
        plt.xlabel(x_name)
        plt.ylabel(y_names[j])
        if save_figures:
            infix = "Regular vs XPOC"
            filename = '{}_{}_{}.png'.format(infix, infixes[j], ad)
            io.save_figure(plt, pl_df_path, filename)

    plt.show()