#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline import pipeline
from evaluation import evaluation
from util import get_config, POSITIVES_LIMIT
from IO.IO import IO

from datetime import datetime, timedelta
import util

# scatter and bar plot imports
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.graph_objs import *

# heatmap imports
import plotly.io as pio
import plotly.express as px

def get_logbook(machine, logbook_fn, beam):
    try:
        # LOAD LOGBOOK
        f_path = util.CERN_DATA_DIR + '/'
        print("Loading logbook data from {} ...".format(logbook_fn), end='')
        logbook = pd.read_csv(f_path + logbook_fn)
        # Set start time as logbook index
        logbook = logbook.set_index(list(logbook)[0])
        logbook.index = pd.to_datetime(logbook.index)
        # Filter on anomalies
        if machine == "mki":
            logbook = logbook[logbook["TAG"] == "anomaly"]
        # Only keep anomalies for current beam (if this column exists)
        if 'BEAM' in logbook:
            logbook = logbook[logbook['BEAM'] == beam]
        elif 'Beam' in logbook:
            logbook = logbook[logbook['Beam'] == beam]
        else:
            print("No beam specified for logbook anomalies, assuming all labeled anomalies are for current beam")
        print("done!")
    except FileNotFoundError:
        print("ERROR - missing {}, continuing without labels".format(logbook_fn))
        logbook = None
    return logbook


def anomaly_in_detections_list(anomaly, detections_list):
    """
    Checks if the anomaly timestamp lies inbetween an interval of the given detections_list.
    It is possible that more anomalies can be labeled as "detected" if they lie close to eachother (12 hours).
    This is because they are seen as 1 anomaly and 1 TP in the anomaly detector.
    """
    anomaly_in_list = False
    for index, detection in detections_list.iterrows():
        # The TP "segment" must be greater than 1 day in length around the actual anomaly timestamp
        # which is here approximated by the row timestamp = quick, dirty fix which will work in most cases
        # TODO: either change the timestamp_max and timestamp_min in the truth_and_pred_df to include the
        # (-12h, 12h) range around the actual anomaly timestamp as a lower bound or do a search here
        # for the actual anomaly timestamp first that lies in (timestamp_min-12h,timestamp_max+12h)
        # This issue came up when the number of rows in the TP segment was 1 (timestamp_min=timestamp_max)
        # because the anomaly timestamp will always lie outside the interval in that case.
        start_d_segment = datetime.strptime(detection["timestamp_min"], '%Y-%m-%d %H:%M:%S')
        end_d_segment = datetime.strptime(detection["timestamp_max"], '%Y-%m-%d %H:%M:%S')
        anomaly_ts = datetime.strptime(anomaly["Timestamp"], '%Y-%m-%d %H:%M:%S')
        if start_d_segment - timedelta(hours=12) <= anomaly_ts <= end_d_segment + timedelta(hours=12):
            anomaly_in_list = True
            break

    return anomaly_in_list


def get_detected_and_undetected_anomalies_manual(truth_and_pred_df, labels, threshold):
    TP, FN, FP, TN = get_tp_fp_fn_tn(truth_and_pred_df, threshold)

    # Get all anomalies with Timestamp in the TP (timestamp_min, timestamp_max) ranges
    detected_anomalies = pd.DataFrame(columns=labels.columns)
    non_detected_anomalies = pd.DataFrame(columns=labels.columns)
    for index, anomaly in labels.iterrows():
        if anomaly_in_detections_list(anomaly, TP):
            detected_anomalies = detected_anomalies.append(anomaly)
        if anomaly_in_detections_list(anomaly, FN):
            non_detected_anomalies = non_detected_anomalies.append(anomaly)
    return detected_anomalies, non_detected_anomalies


def get_detected_and_undetected_anomalies(truth_and_pred_df, labels, threshold):
    """
    Gets all anomalies (Fault numbers) from the TP and FN of the truth_and_pred_df and returns the label data.
    """
    TP, FN, FP, TN = get_tp_fp_fn_tn(truth_and_pred_df, threshold)

    tp_mask = list(TP['label_id'].astype(int))
    detected_anomalies = labels.loc[labels['Fault  number'].isin(tp_mask)]

    fn_mask = list(FN['label_id'].astype(int))
    undetected_anomalies = labels.loc[labels['Fault  number'].isin(fn_mask)]
    return detected_anomalies, undetected_anomalies

def get_anomalies_data(truth_and_pred_df):
    return truth_and_pred_df[truth_and_pred_df.y_true == 1].sort_values(by=['timestamp_min'], ascending=True)


def get_non_anomalies_data(truth_and_pred_df):
    return truth_and_pred_df[truth_and_pred_df.y_true == 0].sort_values(by=['timestamp_min'], ascending=True)


def get_nb_anomalies(truth_and_pred_df):
    # number of anomalies in predictions
    anomalys_data = get_anomalies_data(truth_and_pred_df)
    return anomalys_data.shape[0]


def get_tp_fp_fn_tn(truth_and_pred_df, threshold):
    TP = get_tp_data(truth_and_pred_df, threshold)
    FN = get_fn_data(truth_and_pred_df, threshold)
    FP = get_fp_data(truth_and_pred_df, threshold)
    TN = get_tn_data(truth_and_pred_df, threshold)
    return TP, FN, FP, TN


def get_tp_data(truth_and_pred_df, threshold):
    anomalys_data = get_anomalies_data(truth_and_pred_df)
    threshold_score = truth_and_pred_df['y_pred'].quantile(threshold)
    #print("threshold value: {}".format(str(threshold_score)))
    return anomalys_data[anomalys_data.y_pred >= threshold_score]


def get_fn_data(truth_and_pred_df, threshold):
    anomalys_data = get_anomalies_data(truth_and_pred_df)
    threshold_score = truth_and_pred_df['y_pred'].quantile(threshold)
    return anomalys_data[anomalys_data.y_pred < threshold_score]


def get_fp_data(truth_and_pred_df, threshold):
    non_anomalys_data = get_non_anomalies_data(truth_and_pred_df)
    threshold_score = truth_and_pred_df['y_pred'].quantile(threshold)
    return non_anomalys_data[truth_and_pred_df.y_pred >= threshold_score]


def get_tn_data(truth_and_pred_df, threshold):
    non_anomalys_data = get_non_anomalies_data(truth_and_pred_df)
    threshold_score = truth_and_pred_df['y_pred'].quantile(threshold)
    return non_anomalys_data[truth_and_pred_df.y_pred < threshold_score]


def get_plot_data(cfg, truth_and_pred_dfs, detected_color='green', undetected_color='red'):
    """
    Extracts the necessary data from the truth_and_pred_dfs and adds it to the dictionaries
    Args:
        cfg: The config file
        truth_and_pred_dfs: List of dictionaries with data/metadata about the specific experiment
        (every dictionary will become a category on the x axis).
    """
    io = IO(cfg, 'pipeline')
    for i in range(len(truth_and_pred_dfs)):
        print("Plotting anomalies for experiment {}".format(i))
        df = truth_and_pred_dfs[i]["df"]
        beam = truth_and_pred_dfs[i]["beam"]
        threshold = truth_and_pred_dfs[i]["threshold"]
        ad = cfg['pipeline']['anomaly_detection']['anomaly_detector']
        if cfg['grid_search']['anomaly_detection'][ad]['scoring'] == "practical_recall":
            threshold = pipeline.get_practical_recall_threshold(truth_and_pred_dfs[i]["df"], POSITIVES_LIMIT)

        try:
            machine = cfg["machine"]
            logbook_fn = cfg["pipeline"]["logbook"]
            scoring = cfg['grid_search']['anomaly_detection'][ad]['scoring']
        except KeyError:
            print("KeyError occurred, exiting...")
        labels = get_logbook(machine, logbook_fn, beam)

        pred_to_use = 'y_pred'
        if scoring == "top_k":
            pred_to_use = "y_pred_top_k"
        if scoring == "top_percentage":
            pred_to_use = "y_pred_top_percentage"
        # get TN, TP, FN, FP for threshold
        TN, TP, FN, FP, segments = evaluation.cluster_predictions(truth_and_pred_dfs[i]["df"], threshold, pred_to_use)
        confusion_matrix_stats = {"TP": len(TP), "FP": len(FP), "FN": len(FN), "TN": len(TN)}

        # EVALUATION: Area Under PR-Curve
        auc, precision, recall = pipeline.evaluate_pr(df)
        auprg, precision_gain, recall_gain = pipeline.evaluate_prg(df)
        # EVALUATION: Ranking
        rank = pipeline.evaluate_rank(df)
        # EVALUATION: Recall for set number of positives
        practical_recall = pipeline.get_recall(confusion_matrix_stats)
        print("|| Practical recall for {} positives: {}".format(POSITIVES_LIMIT, practical_recall))

        truth_and_pred_dfs[i]['auc'] = auc
        truth_and_pred_dfs[i]['auprg'] = auprg
        truth_and_pred_dfs[i]['rank'] = rank
        truth_and_pred_dfs[i]['practical_recall'] = practical_recall

        detected_anomalies, undetected_anomalies = get_detected_and_undetected_anomalies(df, labels, threshold)
        detected_anomalies_data = [{'timestamp': a["Timestamp"],
                                    'id': a["Fault  number"],
                                    'color': detected_color,
                                    'comment': a['comment'],
                                    'description': "Description: {}. <br>Comment: {}</br>".format(a['Description '],
                                                                                                  a['comment'])} for
                                   index, a in detected_anomalies.iterrows()]
        undetected_anomalies_data = [{'timestamp': a["Timestamp"],
                                      'id': a["Fault  number"],
                                      'color': undetected_color,
                                      'comment': a['comment'],
                                      'description': "Description: {}. <br>Comment: {}</br>".format(a['Description '],
                                                                                                    a['comment'])} for
                                     index, a in undetected_anomalies.iterrows()]

        detected_anomalies_data = sorted(detected_anomalies_data, key=lambda k: k['timestamp'], reverse=False)
        undetected_anomalies_data = sorted(undetected_anomalies_data, key=lambda k: k['timestamp'], reverse=False)
        truth_and_pred_dfs[i]['detected_anomalies_data'] = detected_anomalies_data
        truth_and_pred_dfs[i]['undetected_anomalies_data'] = undetected_anomalies_data

    return truth_and_pred_dfs


def get_experiment_name(name, auc, auprg, rank, practical_recall, tp, fn, fp, tn, verbose):
    if verbose:
        return "<b>{}</b><br>AUPR={:.2f}</br>AUPRG={:.2f}<br>rank={}</br>Practical_recall={:.2f}<br>TP:{}|FN:{}|FP:{}|TN:{}</br>".format(
                        name,
                        auc,
                        auprg,
                        rank,
                        practical_recall,
                        tp, fn, fp, tn)
    return "<b>{}</b>".format(name)


def get_experiment_names(exp_data, verbose_labels):
    """
    Constructs the experiment names to appear on the x axis of the detected anomalies plot
    Args:
        exp_data: The experiment data. List of dictionaries containing the data for each experiment.
        verbose_labels: Whether to include more information in the label
    """
    detected_x = []
    undetected_x = []

    for i in range(len(exp_data)):
        TP, FN, FP, TN = get_tp_fp_fn_tn(exp_data[i]["df"], exp_data[i]["threshold"])
        detected_x.extend([
            get_experiment_name(
                exp_data[i]['name'],
                exp_data[i]['auc'],
                exp_data[i]['auprg'],
                int(exp_data[i]['rank']),
                exp_data[i]['practical_recall'],
                len(TP), len(FN), len(FP), len(TN), verbose_labels) for j in
            range(len(exp_data[i]['detected_anomalies_data']))])
        undetected_x.extend([
            get_experiment_name(
                exp_data[i]['name'],
                exp_data[i]['auc'],
                exp_data[i]['auprg'],
                int(exp_data[i]['rank']),
                exp_data[i]['practical_recall'],
                len(TP), len(FN), len(FP), len(TN), verbose_labels) for j in
            range(len(exp_data[i]['undetected_anomalies_data']))])
    return detected_x, undetected_x


def sort_experiments(data, order_key):
    reversed = True
    if order_key == 'rank':
        reversed = False
    return sorted(data, key=lambda k: k[order_key], reverse=reversed)


def plot_detected_anomalies(cfg, truth_and_pred_dfs, order_key='auc', title="", font_size=18, x_label_size=16, y_label_size=16, hover_label_size=18, marker_size=35, verbose_labels=True):
    """
    Returns a scatter plot figure showing the detected anomalies for given predictions (column for each predictions list).
    X-axis will contain the truth and pred dfs, y-axis the anomalies.
    Args:
          cfg: The configuration file
          truth_and_pred_dfs: The predictions Dataframes. List of dictionaries containing the df and all metadata about
          this predictions list (treshold to use, beam, ...).
    """
    # sort experiments on order_key value
    truth_and_pred_dfs = sort_experiments(truth_and_pred_dfs, order_key)

    # x (experiments)
    detected_x, undetected_x = get_experiment_names(truth_and_pred_dfs, verbose_labels)

    # y (anomalies)
    detected_y = []
    undetected_y = []
    detected_marker_color = []
    undetected_marker_color = []
    detected_descriptions = []
    undetected_descriptions = []
    for i in range(len(truth_and_pred_dfs)):
        detected_y.extend(["{}".format(a['timestamp']) for a in truth_and_pred_dfs[i]['detected_anomalies_data']])
        undetected_y.extend(["{}".format(a['timestamp']) for a in truth_and_pred_dfs[i]['undetected_anomalies_data']])
        detected_marker_color.extend([a['color'] for a in truth_and_pred_dfs[i]['detected_anomalies_data']])
        undetected_marker_color.extend([a['color'] for a in truth_and_pred_dfs[i]['undetected_anomalies_data']])
        detected_descriptions.extend([a['description'] for a in truth_and_pred_dfs[i]['detected_anomalies_data']])
        undetected_descriptions.extend([a['description'] for a in truth_and_pred_dfs[i]['undetected_anomalies_data']])

    pio.templates.default = "plotly_white"
    fig = go.Figure(data=go.Scatter(
        name="Detected anomaly",
        x=detected_x,
        y=detected_y,
        mode='markers',
        marker=dict(size=[marker_size for i in range(len(detected_x))],
                    color=detected_marker_color),
        hovertemplate=
        '<i>Pipeline: %{x}</i>' +
        '<br><i>Anomaly</i>: %{y}<br>' +
        '<b>%{text}</b>',
        text=['{}'.format(d) for d in detected_descriptions]
    ),
        layout=Layout(
            xaxis_type='category',
            yaxis_type='category',
            xaxis_title='Pipeline',
            yaxis_title='Anomaly',
    ))
    fig.add_trace(go.Scatter(
        name="Undetected anomaly",
        x=undetected_x,
        y=undetected_y,
        mode='markers',
        marker=dict(size=[marker_size for i in range(len(undetected_x))],
                    color=undetected_marker_color),
        hovertemplate=
        '<i>Pipeline: %{x}</i>' +
        '<br><i>Anomaly</i>: %{y}<br>' +
        '<b>%{text}</b>',
        text=['{}'.format(d) for d in undetected_descriptions]))
    fig.update_yaxes(categoryorder='category descending',
                     tickfont_size=y_label_size)
    fig.update_xaxes(tickfont_size=x_label_size)
    io = IO(cfg, "grid_search")
    detector = cfg['grid_search']['anomaly_detection']['anomaly_detector']

    fig.update_layout(
        title={
            'text': get_title(cfg['machine'],
                             title,
                             io.start_year,
                             truth_and_pred_dfs[i]["beam"],
                             cfg['grid_search']['anomaly_detection']['anomaly_detector'],
                             truth_and_pred_dfs[i]["threshold"],
                              POSITIVES_LIMIT,
                              cfg['grid_search']['anomaly_detection'][detector]['scoring']),
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        font=dict(
            size=font_size,
        ),
        hoverlabel=dict(
            font_size=hover_label_size,
        )
    )
    return fig


def get_title(machine, title, start_year, beam, detector, threshold, max_positives, scoring):
    if scoring == "practical_recall":
        return "{} Detected anomalies | {} ".format(machine,
                                                                                               title)
    else:
        return "{} Detected anomalies | {} | {} | Beam {} | {} | threshold: {:.3f}".format(machine,
                                                                                           title,
                                                                                           start_year,
                                                                                           beam,
                                                                                           detector,
                                                                                           threshold)


def get_tp_fn(data, sort_key='timestamp'):
    detected = data['detected_anomalies_data']
    undetected = data['undetected_anomalies_data']

    # Some anomalies are twice in the list (probably because of the 12h interval mechanism)
    # To make sure every encoding is the same length, all anomalies must be unique.
    seen_timestamps = []

    merged = []
    merged.extend(detected)
    merged.extend(undetected)
    merged = sorted(merged, key=lambda k: k[sort_key], reverse=False)

    encoding_with_timestamp = []
    for i in merged:
        if i['timestamp'] not in seen_timestamps:
            if i in detected:
                encoding_with_timestamp.append((i['timestamp'], 1))
            elif i in undetected:
                encoding_with_timestamp.append((i['timestamp'], 0))
            else:
                print("Anomaly not found in detected/undetected")
            seen_timestamps.append(i['timestamp'])
        else:
            print("Skipping double anomaly: {}".format(i))
    return encoding_with_timestamp


def get_tp_fn_encoding(data, sort_key='timestamp'):
    encoding_with_timestamp = get_tp_fn(data, sort_key)
    encoding = []
    for enc in encoding_with_timestamp:
        encoding.append(enc[1])
    return encoding


def anomalies_correlations(plot_data):
    encodings = []
    for i in range(len(plot_data)):
        encodings.append(get_tp_fn_encoding(plot_data[i]))
    return pearson_corr(encodings[0], encodings[1:])


def anomalies_covariance(plot_data):
    encodings = []
    for i in range(len(plot_data)):
        encodings.append(get_tp_fn_encoding(plot_data[i]))
    return pearson_corr(encodings[0], encodings[1:])

def covariance_matrix(a, b):
    return np.cov(a, b)

def pearson_corr(a, b):
    """
    Calculates the Pearson correlation matrix between a and all entries of b.
    """
    return np.corrcoef(a, b)


def corr_means(corr_mtx):
    """
    Return the mean of the correlations between the observed encoding (current index) and all other encodings
    """
    return [(sum(i) - 1) / (len(i) - 1) for i in corr_mtx]


def anomaly_frequencies(plot_data):
    """
    Counts the anomaly TP frequencies with anomalies ascending in timestamp value
    """
    sums = np.zeros(len(get_tp_fn_encoding(plot_data[0])))
    for i in range(len(plot_data)):
        for j, k in enumerate(get_tp_fn_encoding(plot_data[i])):
            sums[j] += k

    frequencies = sums / len(plot_data)
    return frequencies


def get_best_encoding(plot_data, metric='practical_recall'):
    metrics = []
    for i, d in enumerate(plot_data):
        metrics.append(d[metric])
    if metric == 'rank':
        indx = metrics.index(min(metrics))
    else:
        indx = metrics.index(max(metrics))
    encoding_with_timestamp = get_tp_fn(plot_data[indx])
    return encoding_with_timestamp


def plot_correlations(cfg, data):
    """
    Creates a plot showing the mean correlation between an experiment and all other experiments
    Not used anymore.
    """
    io = IO(cfg, 'grid_search')
    correlation_matrix = anomalies_correlations(data)
    means = corr_means(correlation_matrix)
    x = [i['name'] for i in data]

    pio.templates.default = "plotly_white"
    fig = go.Figure(data=go.Scatter(
        name="Mean correlation between detected anomalies",
        x=x,
        y=means,
        mode='markers+lines',
        marker=dict(size=10,
                    color='red')
    ),
        layout=Layout(
            xaxis_type='category',
            xaxis_title='Experiment',
            yaxis_title='Mean correlation',
        ))
    fig.update_layout(
        title={
            'text': "{} Mean correlation between detected anomalies | {} | Beam {} | {} | threshold: {}".format(cfg['machine'],
                                                                                             io.start_year,
                                                                                             data[0]["beam"],
                                                                                             cfg['grid_search'][
                                                                                                 'anomaly_detection'][
                                                                                                 'anomaly_detector'],
                                                                                             data[0]["threshold"]),
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        font=dict(
            size=15,
        )
    )
    return fig


def get_all_anomaly_timestamps(data, sort_key='timestamp'):
    detected = data['detected_anomalies_data']
    undetected = data['undetected_anomalies_data']

    # Some anomalies are twice in the list (probably because of the 12h interval mechanism)
    # To make sure every encoding is the same length, all anomalies must be unique.

    merged = []
    merged.extend(detected)
    merged.extend(undetected)
    merged = sorted(merged, key=lambda k: k[sort_key], reverse=False)

    timestamps = []
    for i in merged:
        if i['timestamp'] not in timestamps:
            timestamps.append(i['timestamp'])
        else:
            print("Skipping double anomaly: {}".format(i))
    return timestamps


def plot_anomaly_frequencies(cfg, data, metric_for_best='practical_recall'):
    io = IO(cfg, 'grid_search')
    standard_webapp_color = '#084c94'

    frequencies = anomaly_frequencies(data)
    x = get_all_anomaly_timestamps(data[0])
    y = frequencies

    # sort data from high to low frequency
    sortx = [i for _, i in sorted(zip(y, x), reverse=True)]
    sorty = sorted(y, reverse=True)

    marker_color = []
    best = get_best_encoding(data, metric=metric_for_best)
    for x_i in sortx:
        for anom in best:
            if anom[0] == x_i:
                if anom[1] == 1:
                    marker_color.append(standard_webapp_color)
                elif anom[1] == 0:
                    marker_color.append('red')
                else:
                    print("error")
                break
    for i in best:
        print(i)
    print(marker_color)
    pio.templates.default = "plotly_white"
    fig = go.Figure(data=go.Bar(
        name="Mean anomaly detection frequency",
        x=sortx,
        y=sorty,
        marker_color=marker_color
    ),
        layout=Layout(
            xaxis_type='category',
            xaxis_title='Anomaly',
            yaxis_title='Detection frequency',
        ))
    fig.update_layout(
        title={
            'text': "{} Anomaly detection frequency | R@k=100".format(cfg['machine']),
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        font=dict(
            size=15,
        )
    )
    return fig


def plot_covariance(cfg, plot_data, order_key=None, show_half=False, verbose_labels=True):
    io = IO(cfg, 'grid_search')
    # sort experiments on order_key value
    if order_key is not None:
        plot_data = sort_experiments(plot_data, order_key)
    experiment_names = []
    for experiment in plot_data:
        TP, FN, FP, TN = get_tp_fp_fn_tn(experiment['df'], experiment['threshold'])
        experiment_names.append(get_experiment_name(experiment['name'],
                                            experiment['auc'],
                                            experiment['auprg'],
                                            int(experiment['rank']),
                                            experiment['practical_recall'],
                                            len(TP), len(FN), len(FP), len(TN),
                                            verbose_labels))
    covariance_matrix = anomalies_covariance(plot_data)
    if show_half:
        mask = np.triu(np.ones_like(covariance_matrix, dtype=bool))
        covariance_matrix = pd.DataFrame(data=covariance_matrix).mask(mask).transpose().values
    print(covariance_matrix)

    pio.templates.default = "plotly_white"
    fig = go.Figure(data=go.Heatmap(
        z=covariance_matrix,
        x=experiment_names,
        y=experiment_names,
        colorscale=px.colors.diverging.RdBu,
        #zmin=-1,
        #zmax=1
    ), layout=Layout(
            xaxis_title='Pipeline',
            yaxis_title='Pipeline',
        ))
    fig.update_layout(
        title={
            'text': "{} Covariance matrix for detected anomaly encoding | Sorted by {} | {} | Beam {} | {} | threshold: {}".format(cfg['machine'],
                                                                                                                          order_key,
                                                                                                      io.start_year,
                                                                                                      plot_data[0]["beam"],
                                                                                                      cfg['grid_search'][
                                                                                                          'anomaly_detection'][
                                                                                                          'anomaly_detector'],
                                                                                                      plot_data[0][
                                                                                                          "threshold"]),
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        font=dict(
            size=13,
        )
    )
    return fig


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to plot detected anomalies")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    mode = 'grid_search'
    ad = cfg[mode]['anomaly_detection']['anomaly_detector']
    threshold = cfg[mode]['anomaly_detection'][ad]['threshold']
    beam = cfg[mode]["beam"]

    if mode == 'pipeline':
        # default mode
        io = IO(cfg, 'pipeline')
        pred_df = io.get_pred_with_params(ad)
    elif mode == "grid_search":
        # grid search mode (best performing predictions file)
        io = IO(cfg, 'grid_search')
        pred_df = io.read_csv(io.get_grid_search_best_output_filename(ad),
                                        io.get_grid_search_output_path(ad))
    truth_and_pred_dfs = [{'df': pred_df, 'threshold': threshold, 'beam': beam, 'name': "Type 1"},
                          {'df': pred_df, 'threshold': threshold, 'beam': beam, 'name': "Type 2"}]
    plot_data = get_plot_data(cfg, truth_and_pred_dfs)
    #fig = plot_detected_anomalies(cfg, plot_data, title="Test", y_label_size=15)
    #fig.show()

    freq_fig = plot_anomaly_frequencies(cfg, plot_data)
    freq_fig.show()

    #corr_fig = plot_correlations(cfg, plot_data)
    #corr_fig.show()

    #cov_fig = plot_covariance(cfg, plot_data, order_key='rank')
    #cov_fig.show()

    best = get_best_encoding(plot_data, metric='rank')
    print(best)


