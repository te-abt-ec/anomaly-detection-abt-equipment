#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, RESULTS_DIR
from IO.IO import IO
from scripts.experiments.shap_feature_selection import run_experiment

from itertools import chain, combinations
from datetime import datetime
import util

import matplotlib.pyplot as plt

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds_variance_gridsearch.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    shap_path = io.fs_shap_path
    histogram_path = io.fs_histogram_path
    random_path = io.fs_histogram_path
    ad = "iforest"
    beam = io.beam
    beam = 1
    pred_types = ["TP"]

    # 2016 beam 1 from LXPLUS regular
    shap_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/regular/experiments/shap feature selection/".format(
        ad,
        io.start_year,
        io.start_month,
        io.end_month,
        beam)
    histogram_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/regular/experiments/histogram feature selection/".format(
        ad,
        io.start_year,
        io.start_month,
        io.end_month,
        beam)
    random_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/regular/experiments/random feature selection/".format(
        ad,
        io.start_year,
        io.start_month,
        io.end_month,
        beam)

    # 2016 beam 1 from LXPLUS noise
    shap_path_noise = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/noise as data/experiments/shap feature selection/".format(ad,
                                                                                                          io.start_year,
                                                                                                          io.start_month,
                                                                                                          io.end_month,
                                                                                                          beam)
    histogram_path_noise = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/noise as data/experiments/histogram feature selection/".format(ad,
                                                                                                          io.start_year,
                                                                                                          io.start_month,
                                                                                                          io.end_month,
                                                                                                          beam)
    random_path_noise = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/noise as data/experiments/random feature selection/".format(ad,
                                                                                                                     io.start_year,
                                                                                                                     io.start_month,
                                                                                                                     io.end_month,
                                                                                                                     beam)

    filenames = {
        'best_auc_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_best_auc_result_2021-05-14 23 26 20.224699.csv",
        'best_rank_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_best_rank_result_2021-05-14 23 26 20.246374.csv",
        'average_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_result_2021-05-14 23 26 20.191601.csv",
        'best_auc_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_best_auc_result_2021-05-14 07 40 11.780663.csv",
        'best_rank_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_best_rank_result_2021-05-14 07 40 11.802634.csv",
        'average_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_result_2021-05-14 07 40 11.699320.csv",
        'best_auc_random': "averaged_best_auc.csv",
        'best_rank_random': "averaged_best_rank.csv",
        'average_random': "averaged_means.csv"
    }

    noise_data_filenames = {
        'noise_best_auc_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_best_auc_result_2021-05-23 04:09:24.110628.csv",
        'noise_best_rank_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_best_rank_result_2021-05-23 04:09:24.236777.csv",
        'noise_average_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_result_2021-05-23 04:09:24.026242.csv",
        'noise_best_auc_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_best_auc_result_2021-05-23 01:44:41.023201.csv",
        'noise_best_rank_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_best_rank_result_2021-05-23 01:44:41.042323.csv",
        'noise_average_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_result_2021-05-23 01:44:41.003444.csv",
        'noise_best_auc_random': "averaged_best_auc.csv",
        'noise_best_rank_random': "averaged_best_rank.csv",
        'noise_average_random': "averaged_means.csv"
    }

    save_figures = True
    realistic_y_scale = False

    dfs = {}
    for name, fn in filenames.items():
        if 'shap' in name:
            dfs[name] = io.read_csv(fn, shap_path)
        elif 'histogram' in name:
            dfs[name] = io.read_csv(fn, histogram_path)
        elif 'random' in name:
            dfs[name] = io.read_csv(fn, random_path)

    noise_dfs = {}
    for name, fn in noise_data_filenames.items():
        if 'shap' in name:
            noise_dfs[name] = io.read_csv(fn, shap_path_noise)
        elif 'histogram' in name:
            noise_dfs[name] = io.read_csv(fn, histogram_path_noise)
        elif 'random' in name:
            noise_dfs[name] = io.read_csv(fn, random_path_noise)

    x_to_plot = 'max_nb_features'

    for i, (name, df) in enumerate(dfs.items()):
        dfs[name] = df.sort_values(by=[x_to_plot])
    for i, (name, df) in enumerate(noise_dfs.items()):
        noise_dfs[name] = df.sort_values(by=[x_to_plot])

    if x_to_plot == 'nb_cols':
        x_name = "number of features"
    elif x_to_plot == "max_nb_features":
        x_name = "Maximum number of features"
    elif x_to_plot == 'value':
        x_name = "threshold value v"

    kinds_of_y = ["shap", "histogram", "random"]
    y_to_plot = {'plot1': ['best_auc_{}'.format(i) for i in kinds_of_y] +
                           ['noise_best_auc_{}'.format(i) for i in kinds_of_y],
                 'plot2': ['best_rank_{}'.format(i) for i in kinds_of_y] +
                           ['noise_best_rank_{}'.format(i) for i in kinds_of_y]}
    infixes = [
        "best AUC (averaged over 5 pipelines)",
        "best rank (averaged over 5 pipelines)"
    ]

    ys = ['auc', 'rank']
    y_names = ["AUC", "rank"]

    colors = ['b-o', 'r-o', 'g-o', 'b-x', 'r-x', 'g-x']
    names = ["Regular data {}".format(i) for i in kinds_of_y] + \
             ["Noise {}".format(i) for i in kinds_of_y]
    for j, (plotname, dfs_to_plot) in enumerate(y_to_plot.items()):
        plt.figure(figsize=(9, 5))
        for i, y in enumerate(dfs_to_plot):
            dfs_to_take = dfs
            if "noise" in y:
                dfs_to_take = noise_dfs
            plt.plot(dfs_to_take[y][x_to_plot], dfs_to_take[y][ys[j]], colors[i])
            if realistic_y_scale:
                if ys[j] == "auc":
                    plt.ylim([0, 0.1])
                elif ys[j] == "rank":
                    plt.ylim([0, 200])
        plt.title(
            "Feature selection ({}), regular vs noise data | {} | {} Beam {}".format(kinds_of_y, infixes[j], io.start_year, beam),
            fontsize=10)
        plt.legend(names)
        plt.xlabel(x_name)
        plt.ylabel(y_names[j])
        if save_figures:
            infix = "Regular vs noise data {}".format(kinds_of_y)
            filename = '{}_{}_{}.png'.format(infix, x_to_plot, infixes[j])
            io.save_figure(plt, io.get_figures_path(ad, beam=beam), filename)

    plt.show()