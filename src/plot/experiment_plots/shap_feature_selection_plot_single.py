#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.subplots as ps

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from scripts.shap_values import run_shap_values
from analysis.drop_cols import update_drop_cols_config

np.random.seed(456)

def update_keep_cols_config(io, colns):
    """
    Updates the local_preprocessing:keep_columns_list attribute of the config yaml file
    Args:
         io: The dataframe
         colns: The column names to keep
    """
    try:
        print("Updating the kept columns in config")
        io.config["local_preprocessing"]["keep_columns_list"] = []
        for coln in colns:
            try:
                curr_cols_list = io.config["local_preprocessing"]["keep_columns_list"]
                if curr_cols_list is None:
                    io.config["local_preprocessing"]["keep_columns_list"] = [coln]
                else:
                    io.config["local_preprocessing"]["keep_columns_list"].append(coln)
            except KeyError:
                io.config["local_preprocessing"]["keep_columns_list"] = [coln]
        io.update_config_file()
        return 1
    except:
        print("Error updating dropped column names in config")
        return 0

def aggregate_SHAP_values(shap_df, method='mean'):
    """
    Aggregates the given SHAP values per feature with given method
    """
    # default is sum
    agg_df = shap_df.sum()
    if method == 'mean':
        agg_df = shap_df.mean()
    elif method == 'max':
        agg_df = shap_df.max()
    elif method == 'min':
        agg_df = shap_df.min()
    return agg_df

from datetime import datetime

def split_prediction(prediction):
    [start_time, end_time] = prediction.split(" to ")
    start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
    end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
    return start_time, end_time

def get_shap_data(io, shap_file_selection, ad, type='single', selected_prediction=None):
    shap_path = io.get_model_and_shap_path(ad)
    shap_df = io.read_csv(shap_file_selection, shap_path)
    if type == 'aggregated':
        return aggregate_SHAP_values(shap_df, method='mean')
    else:
        start_time, end_time = split_prediction(selected_prediction)
        return shap_df[start_time:end_time]

def create_horizontal_bar_plot(data, ranking_type, selected_features=None):
    y_name = "SHAP Ranking" if ranking_type == "SHAP" else "Ranking"
    x = []
    y = []
    for data_entry in data:
        x.append(data_entry[y_name])
        y.append(data_entry["Feature"])

    # reverse data such that most anomalous (highest) ranking is on top
    x.reverse()
    y.reverse()

    if ranking_type == "Histogram":
        # logarithmic plot for better visualization
        x = [math.log(i, 10) for i in x]
        colors = [standard_webapp_color] * len(y)
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln not in selected_features:
                    colors[i] = 'orange'
    elif ranking_type == "SHAP":
        colors = []
        for r in x:
            if r > 0:
                colors.append('green')
            elif r < 0:
                colors.append('red')
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln in selected_features:
                    colors[i] = 'orange'

    return go.Bar(
        x=x,
        y=y,
        orientation='h',
        marker_line_width=0,
        marker_color=colors)


def get_shap_ranking_data(io, shap_file_selection, ad):
    # aggregate SHAP values of all predictions
    shap_df = get_shap_data(io, shap_file_selection, ad, type='single', selected_prediction="2016-04-17 23:48:59 to 2016-04-18 21:04:55")
    #shap_df = shap_df.transpose()

    print(shap_df)

    d = {}
    for coln in shap_df.columns:
        res = shap_df[coln].iloc[0]
        if res != 0:
            d[coln] = res
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}
    print(d)

    data = []
    for i, (k, v) in enumerate(d.items()):
        entry = {'Feature': "{}".format(k), 'SHAP Ranking': round(v, 8)}
        data.append(entry)

    return data


def select_features(io, prediction_types, subsampling_samples, nb_samples, nb_features_to_select, method, strict_sets, cleanup_forbidden_features, plot=False):
    ad = "iforest"

    datas = {}
    for pred_type in prediction_types:
            datas[pred_type] = get_shap_ranking_data(io,
                                               "shap_values_iforest_B{}_0.95_y_pred_{}.csv".format(
                                                   io.beam, pred_type),ad)
    selected_features = set()
    forbidden_features = set()
    for pred_type, data in datas.items():
        print("pred_type: {}".format(pred_type))
        print("# SHAP values: {}".format(len(data)))
        pos_features = [i["Feature"] for i in data if i["SHAP Ranking"] > 0]
        neg_features = [i["Feature"] for i in data if i["SHAP Ranking"] < 0]
        print("# +SHAP values: {}".format(len(pos_features)))
        print("# -SHAP values: {}".format(len(neg_features)))
        selection_type = nb_features_to_select["type"]
        selection_value = nb_features_to_select["value"]

        print(len(selected_features))
        features = [i["Feature"] for i in data]
        if method == "high_tpfn-low_fptn":
            if pred_type == "TP" or pred_type == "FN":
                print("Selecting top SHAP values...")
                nb_features = get_nb_features(selection_type, selection_value, max_value=len(pos_features))
                candidate_features = features[:nb_features]
            elif pred_type == "FP" or pred_type == "TN":
                print("Selecting bottom SHAP values...")
                nb_features = get_nb_features(selection_type, selection_value, max_value=len(neg_features))
                candidate_features = features[-nb_features:-1]
        elif method == "high_tpfn_delete_high_fptn":
            if pred_type == "TP" or pred_type == "FN":
                nb_features = get_nb_features(selection_type, selection_value, max_value=len(pos_features))
                candidate_features = features[:nb_features]
            elif pred_type == "FP" or pred_type == "TN":
                nb_features = get_nb_features(selection_type, selection_value, max_value=len(neg_features))
                candidate_features = features[nb_features:]
        elif method == "pos_tpfn-neg_fptn":
            if pred_type == "TP" or pred_type == "FN":
                candidate_features = pos_features
            elif pred_type == "FP" or pred_type == "TN":
                candidate_features = neg_features
        selected_features, forbidden_features = update_selected_features(selected_features, forbidden_features,
                                                                         candidate_features, features,
                                                                         pred_type, strict_sets)
        if cleanup_forbidden_features:
            selected_features = selected_features.difference(forbidden_features)
        print(len(selected_features))

    """
    for pred_type, data in datas.items():
        # manually remove top selected FP
        if pred_type == "FP":
            pos_selected_features = [i["Feature"] for i in data if i["Feature"] in selected_features]
            for i in range(20):
                selected_features.remove(pos_selected_features[i])
    """
    selected_features = []
    # plot
    nb_rows = 1
    nb_cols = len(prediction_types)
    rows = []
    cols = []
    for i in range(nb_rows):
        for j in range(nb_cols):
            rows.append(i + 1)
            cols.append(j + 1)

    if plot:
        figures = []
        for pred_type, data in datas.items():
            figures.append(create_horizontal_bar_plot(data, "SHAP", selected_features=list(selected_features)))
        fig = ps.make_subplots(rows=nb_rows, cols=nb_cols, subplot_titles=prediction_types)
        fig.update_layout(title="SHAP values (TP) | {} Beam {}".format(
                                                                                               io.start_year,
                                                                                               io.beam
                                                                                               ),
                          title_x=0.5,
                          title_font_size=30,
                          xaxis_title="SHAP value",
                          yaxis_title="Feature",
                          bargap=0)

        #fig.update_xaxes(title_font=dict(size=10))
        #fig.update_yaxes(title_font=dict(size=3))

        fig.update_xaxes(tickfont=dict(size=20))
        fig.update_yaxes(tickfont=dict(size=5))

        fig.add_traces(
            figures,
            rows=rows,
            cols=cols)
        fig.show()
    return list(selected_features)


def get_nb_features(selection_type, selection_value, max_value):
    nb_features = None
    if selection_type == "absolute":
        nb_features = selection_value if selection_value < max_value else max_value
    elif selection_type == "percentage":
        nb_features = round(max_value * selection_value)
    return nb_features


def update_selected_features(selected_features, forbidden_features, candidate_features, features, pred_type, strict_sets=False):
    verbose = False
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
        print("\nforbidden:")
        for i in forbidden_features:
            print(i)
        print("\ncandidate")
        for i in candidate_features:
            print(i)
    if strict_sets:
        features_to_select = [i for i in candidate_features if i not in forbidden_features]
    else:
        features_to_select = candidate_features
    selected_features.update(features_to_select)
    if strict_sets:
        if pred_type == "FP" or pred_type == "TN":
            #forbidden_features.update(candidate_features)
            forbidden_features.update(set(features).difference(candidate_features))
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
        print("\nforbidden:")
        for i in forbidden_features:
            print(i)
    return selected_features, forbidden_features


def run_experiment(cfg, prediction_types=None, nb_features_to_select=None, method=None, strict_sets=None, subsampling_samples=None, nb_samples=None, cleanup_forbidden_features=None, plot=False, update_config=False):
    io = IO(cfg, 'grid_search')

    # initialize default parameters
    if prediction_types is None:
        prediction_types = ['TP']
    # type = absolute or percentage
    if nb_features_to_select is None:
        nb_features_to_select = {'type': 'percentage',
                                 'value': 1}
    if method is None:
        #method = "high_tpfn-low_fptn"
        #method = "high_tpfn_delete_high_fptn"
        method = "pos_tpfn-neg_fptn"

    if subsampling_samples is None:
        subsampling_samples = {'TP': 100000, 'FN': 100000, 'FP': 1000, 'TN': 100}
    if nb_samples is None:
        nb_samples = {'TP': 100, 'FN': 100, 'FP': 100, 'TN': 100}

    if strict_sets is None:
        strict_sets = False
    if cleanup_forbidden_features is None:
        cleanup_forbidden_features = True
    selected_features = select_features(io, prediction_types, subsampling_samples, nb_samples, nb_features_to_select,
                                        method, strict_sets, cleanup_forbidden_features, plot=plot)

    # create new config
    if update_config:
        update_keep_cols_config(io, selected_features)

    return selected_features


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="SHAP feature selection")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_experiment(cfg, plot=True, update_config=True)






