#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, RESULTS_DIR
from IO.IO import IO
from scripts.experiments.shap_feature_selection import run_experiment

from itertools import chain, combinations
from datetime import datetime
import util

import matplotlib.pyplot as plt

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds_variance_gridsearch.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    shap_path = io.fs_shap_path
    histogram_path = io.fs_histogram_path
    random_path = io.fs_histogram_path
    ad = "iforest"
    beam = io.beam
    beam = 1
    pred_types = ["TP"]


    # 2016 beam 1 from LXPLUS
    shap_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/experiments/shap feature selection/".format(ad,
                                                                                                          io.start_year,
                                                                                                          io.start_month,
                                                                                                          io.end_month,
                                                                                                          beam)
    histogram_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/experiments/histogram feature selection/".format(ad,
                                                                                                          io.start_year,
                                                                                                          io.start_month,
                                                                                                          io.end_month,
                                                                                                          beam)
    random_path = RESULTS_DIR + "/lxplus_results/{}/{}/{}-{}/B{}/experiments/random feature selection/".format(ad,
                                                                                                                     io.start_year,
                                                                                                                     io.start_month,
                                                                                                                     io.end_month,
                                                                                                                     beam)

    filenames = {
        'best_auc_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_best_auc_result_2021-05-14 23 26 20.224699.csv",
        'best_rank_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_best_rank_result_2021-05-14 23 26 20.246374.csv",
        'average_histogram': "high_tpfn_delete_high_fptn_histogram_feature_selection_experiment_['TP']_means_result_2021-05-14 23 26 20.191601.csv",
        'best_auc_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_best_auc_result_2021-05-14 07 40 11.780663.csv",
        'best_rank_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_best_rank_result_2021-05-14 07 40 11.802634.csv",
        'average_shap': "pos_tpfn_delete_pos_fptn_shap_feature_selection_experiment_['TP']_means_result_2021-05-14 07 40 11.699320.csv",
        'best_auc_random': "averaged_best_auc.csv",
        'best_rank_random': "averaged_best_rank.csv",
        'average_random': "averaged_means.csv"
    }

    add_magnets = True
    save_figures = True

    dfs = {}
    for name, fn in filenames.items():
        if 'shap' in name:
            dfs[name] = io.read_csv(fn, shap_path)
        elif 'histogram' in name:
            dfs[name] = io.read_csv(fn, histogram_path)
        elif 'random' in name:
            dfs[name] = io.read_csv(fn, random_path)

    x_to_plot = 'max_nb_features'

    for i, (name, df) in enumerate(dfs.items()):
        dfs[name] = df.sort_values(by=[x_to_plot])

    if x_to_plot == 'nb_cols':
        x_name = "number of features"
    elif x_to_plot == "max_nb_features":
        x_name = "Maximum number of features"
    elif x_to_plot == 'value':
        x_name = "threshold value v"
    y_to_plot = {'plot1': ['best_auc_histogram', 'best_auc_shap', 'best_auc_random'],
                 'plot2': ['best_rank_histogram', 'best_rank_shap', 'best_rank_random'],
                 'plot3': ['average_histogram', 'average_shap', 'average_random'],
                 'plot4': ['average_histogram', 'average_shap', 'average_random']}
    infixes = [
        "best AUC (averaged over 100 pipelines)",
        "best rank (averaged over 100 pipelines)",
        "average grid search AUC",
        "average grid search rank"
    ]
    ys = ['auc', 'rank', 'auc', 'rank']
    y_names = ["AUC", "rank", "AUC", "rank"]
    realistic_y_scale = True

    colors = ['b-o', 'r-x', 'g-v']
    names = ["Histogram values [TP]",
             "SHAP values [TP]",
             'Random selection (avg of 20 diff combinations)']
    for j, (plotname, dfs_to_plot) in enumerate(y_to_plot.items()):
        plt.figure(figsize=(7.5, 5))
        for i, y in enumerate(dfs_to_plot):
            if str(io.start_year) == "2016" and str(beam) == "1" and "random" in y:
                # remove wrong value
                plt.plot(dfs[y][x_to_plot][1:], dfs[y][ys[j]][1:], colors[i])
                if realistic_y_scale:
                    if ys[j] == "auc":
                        plt.ylim([0, 0.1])
                    elif ys[j] == "rank":
                        plt.ylim([0, 200])
            else:
                plt.plot(dfs[y][x_to_plot], dfs[y][ys[j]], colors[i])
        plt.title(
            "Histogram vs SHAP feature selection | {} | {} Beam {}".format(infixes[j], io.start_year, beam),
            fontsize=10)
        plt.legend(names)
        plt.xlabel(x_name)
        plt.ylabel(y_names[j])
        if save_figures:
            infix = "Histogram-vs-SHAP" if len(y_to_plot['plot1']) == 2 else "Histogram-vs-SHAP-vs-Random"
            filename = '{}_{}_{}.png'.format(infix, x_to_plot, infixes[j])
            io.save_figure(plt, io.get_figures_path(ad), filename)

    plt.show()