#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.subplots as ps

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from scripts.shap_values import run_shap_values
from analysis.drop_cols import update_drop_cols_config

np.random.seed(456)

confusion_matrix_colors = {"TP": 'rgb(0, 0, 0)',
                           "FP": 'rgb(205, 12, 24)',
                           "FN": 'rgb(255, 165, 0)',
                           "TN": 'rgb(31, 119, 180)'}

standard_webapp_color = '#084c94'

def update_keep_cols_config(io, colns):
    """
    Updates the local_preprocessing:keep_columns_list attribute of the config yaml file
    Args:
         io: The dataframe
         colns: The column names to keep
    """
    try:
        print("Updating the kept columns in config")
        io.config["local_preprocessing"]["keep_columns_list"] = []
        for coln in colns:
            try:
                curr_cols_list = io.config["local_preprocessing"]["keep_columns_list"]
                if curr_cols_list is None:
                    io.config["local_preprocessing"]["keep_columns_list"] = [coln]
                else:
                    io.config["local_preprocessing"]["keep_columns_list"].append(coln)
            except KeyError:
                io.config["local_preprocessing"]["keep_columns_list"] = [coln]
        io.update_config_file()
        return 1
    except:
        print("Error updating dropped column names in config")
        return 0

def create_horizontal_bar_plot(data, ranking_type, selected_features=None, pred_type=None):
    if ranking_type == "SHAP":
        y_name = "SHAP Ranking"
    elif ranking_type == "Histogram":
        y_name = "Histogram ranking"

    x = []
    y = []
    for data_entry in data:
        x.append(data_entry[y_name])
        y.append(data_entry["Feature"])

    # reverse data such that most anomalous (highest) ranking is on top
    x.reverse()
    y.reverse()

    if ranking_type == "Histogram":
        # logarithmic plot for better visualization
        #x = [math.log(i, 10) for i in x]
        colors = [standard_webapp_color] * len(y)
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln in selected_features:
                    colors[i] = 'orange'
    elif ranking_type == "SHAP":
        colors = []
        for r in x:
            if r > 0:
                colors.append('green')
            elif r < 0:
                colors.append('red')

    # clean plot
    y = [str(i) for i in range(len(y))]

    """
    return go.Scatter(
        name=str(pred_type),
        x=x,
        y=y,
        orientation='h',
        fill='tozerox',
        marker=dict(
            color=confusion_matrix_colors[pred_type],
            size=10)
        )
    """
    return go.Bar(
        name=str(pred_type),
        x=x,
        y=y,
        orientation='h',
        marker_line_width=0,
        marker_color=colors
    )

def get_histogram_ranking_data(io, pred_type, threshold, scoring_method, nbins, nhours):
    rankings_df = io.read_csv("histogram-rankings_B{}_all {}_{}_{}_nbins={}_nhours={}.csv".format(
        io.beam,
        pred_type,
        threshold,
        scoring_method,
        nbins,
        nhours
    ), io.get_histogram_rankings_path())

    d = {}
    for i, coln in enumerate(rankings_df["Feature"]):
        d[coln] = rankings_df["Histogram ranking"].iloc[i]
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}

    data = []
    for i, (k, v) in enumerate(d.items()):
        entry = {'Feature': "{}".format(k), 'Histogram ranking': round(v, 8)}
        data.append(entry)

    return data


def select_features(io, prediction_types, nb_features_to_select, method, threshold, scoring_method, nbins, nhours, strict_sets=False, plot=False):
    ad = "iforest"

    datas = {}
    for pred_type in prediction_types:
        datas[pred_type] = get_histogram_ranking_data(io, pred_type, threshold, scoring_method, nbins, nhours)

    selected_features = set()
    forbidden_features = set()
    for pred_type, data in datas.items():
        print("pred_type: {}".format(pred_type))
        print("# Hist ranking values: {}".format(len(data)))
        selection_type = nb_features_to_select["type"]
        selection_value = nb_features_to_select["value"]

        print(len(selected_features))
        features = [i["Feature"] for i in data]
        nb_features = None
        if selection_type == "absolute":
            nb_features = selection_value if selection_value < len(features) else len(features)
        elif selection_type == "percentage":
            nb_features = round(len(features) * selection_value)
        if method == "high_tpfn-low_fptn":
            if pred_type == "TP" or pred_type == "FN":
                print("Selecting top values...")
                candidate_features = features[:nb_features]
                selected_features, forbidden_features = update_selected_features(selected_features, forbidden_features, candidate_features, features, pred_type, strict_sets)
            elif pred_type == "FP" or pred_type == "TN":
                print("Selecting bottom values...")
                candidate_features = features[-nb_features:-1]
                selected_features, forbidden_features = update_selected_features(selected_features, forbidden_features, candidate_features, features, pred_type, strict_sets)
        elif method == "high_tpfn_delete_high_fptn":
            if pred_type == "TP" or pred_type == "FN":
                candidate_features = features[:nb_features]
                selected_features, forbidden_features = update_selected_features(selected_features, forbidden_features, candidate_features, features, pred_type, strict_sets)
            elif pred_type == "FP" or pred_type == "TN":
                candidate_features = features[nb_features:]
                selected_features, forbidden_features = update_selected_features(selected_features, forbidden_features, candidate_features, features, pred_type, strict_sets)

        # remove features that were added to forbidden features
        selected_features = selected_features.difference(forbidden_features)
        print(len(selected_features))

    fig = go.Figure(create_horizontal_bar_plot(datas[prediction_types[0]], "Histogram", selected_features=list(selected_features), pred_type=prediction_types[0]))

    fig.update_layout(title="Histogram values (TP) | {} Beam {}".format(
                                                                       io.start_year,
                                                                       io.beam),
                      title_x=0.5,
                      title_font_size=30,
                      xaxis_title="Mean of the histogram value over all TP",
                      yaxis_title="Feature",
                      bargap=0)
    if plot:
        fig.show()

    #random feature selection
    #selected_features = [datas["FN"][15]["Feature"]]
    return list(selected_features)


def update_selected_features(selected_features, forbidden_features, candidate_features, features, pred_type, strict_sets=False):
    verbose = False
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
        print("\nforbidden:")
        for i in forbidden_features:
            print(i)
        print("\ncandidate")
        for i in candidate_features:
            print(i)
    if strict_sets:
        features_to_select = [i for i in candidate_features if i not in forbidden_features]
    else:
        features_to_select = candidate_features
    selected_features.update(features_to_select)
    if strict_sets:
        if pred_type == "FP" or pred_type == "TN":
            #forbidden_features.update(candidate_features)
            forbidden_features.update(set(features).difference(candidate_features))
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
        print("\nforbidden:")
        for i in forbidden_features:
            print(i)
    return selected_features, forbidden_features


def run_experiment(cfg, prediction_types=None, nb_features_to_select=None, method=None, params=None, strict_sets=None, plot=False, update_config=False):
    io = IO(cfg, 'grid_search')

    # initialize default parameters
    if prediction_types is None:
        prediction_types = ['TP']
    # type = absolute or percentage
    if nb_features_to_select is None:
        nb_features_to_select = {'type': 'percentage',
                                 'value': 0.1}
    if method is None:
        # method = "high_tpfn-low_fptn"
        method = "high_tpfn_delete_high_fptn"

    if params is None:
        params = {
            "threshold": 0.95,
            "scoring_method": "max",
            "nbins": 50,
            "nhours": 9000
        }
    if strict_sets is None:
        strict_sets = True

    selected_features = select_features(io, prediction_types, nb_features_to_select, method, **params,
                                        strict_sets=strict_sets, plot=plot)
    # create new config
    if update_config:
        update_keep_cols_config(io, selected_features)

    return selected_features



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Histogram feature selection")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_experiment(cfg, plot=True, update_config=True)








