#!/usr/bin/env python3

import argparse
import json
import os
import re
import sys
import traceback
from urllib.parse import urlparse, parse_qsl
from base64 import urlsafe_b64decode
from datetime import datetime, timedelta
import math
import copy

import numpy as np
import pandas as pd

# Dash and Plotly
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import dash_table

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

# Application specific imports
from evaluation import evaluation
import util
import webapp.components as drc
import webapp.cards as cards
import webapp.plot_graph as pg
from IO.IO import IO
from analysis.drop_cols import update_drop_cols_config
import scripts.experiments.histogram_frequency_feature_selection as fs_experiments

np.random.seed(456)

# Note: Some global code is inbetween the definitions

# Some global constants
app = dash.Dash(__name__)
application = app.server
predictions = False
truth_and_pred_filename = ''
AR0 = "xaxis.range[0]"
AR1 = "xaxis.range[1]"
app.scripts.config.serve_locally = True  # needed for
app.config.suppress_callback_exceptions = True  # needed for url states
app.title = "CERN Anomaly Detection Dash app"
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    dcc.Location(id='dummy', refresh=False),
    html.Div(id='page-layout')
])

has_rendered = False

parser = argparse.ArgumentParser(description="CERN Anomaly detection data explorer")
parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                    help='config file used to create data that will be viewed in webapp')
parser.add_argument('--debug', dest='debug_app', action='store_true',
                    help='True if the webapp has to be run in debug mode')
parser.set_defaults(short=False)
args = parser.parse_args()
debug_app = args.debug_app

# absolute path to the config folder
path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
fn = path + '/' + args.configfn
if os.path.exists(fn):
    cfg = util.get_config(fn)
else:
    f_name = fn.split('/')[-1]
    print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
    exit()


# WEBAPP config
io = IO(cfg, 'pipeline')
machine = cfg['machine']
logbook_type = cfg['grid_search']['logbook_type']

w_cfg = cfg['webapp']
# raw, preprocessed or variance_filtered (default is "variance_filtered")
input_features_type = w_cfg['input_features_type']
show_fft = w_cfg['show_fft_features']
show_SW = w_cfg['show_SW_features']

#defaults
df_cfg = w_cfg['defaults']
# TODO: move defaults here instead of hardcode
default_beam = df_cfg['default_beam']
default_data_1 = df_cfg['default_data_1']
default_data_2 = df_cfg['default_data_2']
default_labels = ['anomaly']


if machine == 'MKI':
    logbook_index = 'timestamps'
    beam_index = 'BEAM'
    anomaly_description_index = 'SUBSTR_COMMENT_512_'
    anomaly_id_index = 'EVENT_ID'
elif machine == 'LBDS':
    logbook_index = 'Start Time '
    beam_index = 'Beam'
    anomaly_description_index = 'Description '  # or/plus comment?
    anomaly_id_index = 'Fault  number'

year = cfg['pipeline']['time_interval'][0].split('-')[0]

fn = cfg['pipeline']['preprocessing']['variance_filtered_filename']
fn = fn.format(io.beam) if "{}" in fn else fn
index_name = cfg['pipeline']['index']
f_path = io.pipeline_data_path
if input_features_type == "raw":
    fn = cfg['local_preprocessing']['raw_data_filename']
    index_name = cfg['spark']['index']
    f_path = io.raw_data_path
elif input_features_type == "preprocessed":
    fn = cfg['local_preprocessing']['train_data_filename']
    index_name = cfg['local_preprocessing']['index']
    f_path = io.preprocessed_data_path

# anomaly detector
default_anomaly_detector = cfg['pipeline']['anomaly_detection']['anomaly_detector']

# thresholds
min_threshold = w_cfg['threshold_slider']['min']
max_threshold = w_cfg['threshold_slider']['max']
default_threshold = cfg['pipeline']['anomaly_detection'][default_anomaly_detector]['threshold']
threshold_step_size = w_cfg['threshold_slider']['step_size']
threshold_to_cache_step_size = 0.01

# Import data
print("Loading CERN data from {}".format(f_path + fn))
features = pd.read_csv(f_path + fn)
features = features.sort_values(by=index_name)
index_to_be = features[index_name]
if index_to_be.dtype != 'object':
    features = features.set_index(pd.to_datetime(features[index_name], unit='s'))
else:
    features = features.set_index(pd.to_datetime(features[index_name]))
logbook_fn = cfg['local_preprocessing']['logbook']

# precomputed to optimise performance
# filter fft and sw
if not show_fft:
    non_fft_colns = [c for c in list(features) if "fft" not in c]
    features = features[non_fft_colns]
if not show_SW:
    non_sw_colns = [c for c in list(features) if "SW" not in c]
    features = features[non_sw_colns]

print("Building column subsets for each beam")
cols = list(features)
feature_cols = {1: util.get_cols_for_beam(1, cols, machine), 2: util.get_cols_for_beam(2, cols, machine)}


# set default cols
# TODO: cleaner code
for i in [1, 2]:
    for d in range(len(default_data_1)):
        for j in range(len(feature_cols[i])):
            if default_data_1[d] in feature_cols[i][j]:
                default_data_1[d] = feature_cols[i][j]
                break
            if j == len(feature_cols[i]) - 1:
                default_data_1[d] = feature_cols[i][0]
    for d in range(len(default_data_2)):
        for j in range(len(feature_cols[i])):
            if default_data_2[d] in feature_cols[i][j]:
                default_data_2[d] = feature_cols[i][j]
                break
            if j == len(feature_cols[i]) - 1:
                default_data_2[d] = feature_cols[i][0]


# cache features
initial_threshold_values_caching = w_cfg['threshold_slider']['initial_threshold_values_caching']
print("Caching data for faster GUI...", end='')
cached_x_values = features.index.tolist()
cached_y_values = {}
for coln in features:
    cached_y_values[coln] = features[coln]
print("done!")

start = str(pd.to_datetime(features.index[0], unit='s'))
end = str(pd.to_datetime(features.index[-1], unit='s'))
print("Imported features time interval: {} {}".format(start, end))
year = pd.to_datetime(features.index[0], unit='s').year

# default prediction file
def get_prediction_options(selected_beam, ad):
    print("Setting predictions dropdown options for beam {}".format(selected_beam))
    prediction_list = list(filter(lambda name: (ad in name), util.list_predictions(machine, year, selected_beam)))
    # add the prediction with id of the current pipeline setting
    a = [{'label': os.path.basename(i), 'value': i} for i in prediction_list]
    pipeline_fn = io.get_pred_with_params_filename(ad)
    if pipeline_fn is not None:
        a.append({'label': os.path.basename(pipeline_fn), 'value': pipeline_fn})
    return a

predictions_list = get_prediction_options(default_beam, cfg['pipeline']['anomaly_detection']['anomaly_detector'])
default_prediction_file = predictions_list[0]['value'] if len(predictions_list) else ''
default_score_method = df_cfg['default_score_method']
default_pred_to_use = util.get_pred_to_use(default_score_method)

# label colors
label_colors = {"anomaly": 'rgba(220,20,60,1.0)',
                "fault": 'rgba(219,180,20,1.0)',
                "info": 'rgba(20,219,180,1.0)',
                "intervention": 'rgba(20,60,219,1.0)',
                "research": 'rgba(75,0,130,1.0)'}

confusion_matrix_colors = {"TP": 'rgb(0, 0, 0)',
                           "FP": 'rgb(205, 12, 24)',
                           "FN": 'rgb(255, 165, 0)',
                           "TN": 'rgb(31, 119, 180)'}

standard_webapp_color = '#084c94'

# Histograms
default_nb_bins = df_cfg['default_nb_bins']
default_nb_hours = df_cfg['default_nb_hours']

# Anomalies vs detectors
default_threshold_slider_values = {}
for d in util.DETECTORS:
    default_threshold_slider_values[d] = cfg['pipeline']['anomaly_detection'][d]['threshold']




# Rankings
round_rankings = 8  # Amount of decimals to round the rankings to
long_table_calc_disabled = w_cfg['long_table_calc_disabled']
write_rankings_aggregated = w_cfg['write_rankings_aggregated']
write_rankings_single = w_cfg['write_rankings_single']
default_min_freq = df_cfg['default_min_freq']

# SHAP
shap_aggregator_method = df_cfg['shap_aggregator_method']

# Feature selection (drop columns)
default_feature_selection = []
dropped_cols = []
if cfg['local_preprocessing']['drop_columns'] is not None:
    for k, v in cfg['local_preprocessing']['drop_columns'].items():
        if v is not None:
            dropped_cols.extend(v)
for coln in feature_cols[default_beam]:
    if coln not in dropped_cols:
        default_feature_selection.append(coln)
default_feature_selection_groups = copy.deepcopy(util.DATASET_TYPES[machine])
for dataset_type in copy.deepcopy(default_feature_selection_groups):
    for coln in dropped_cols:
        if dataset_type in coln and dataset_type in default_feature_selection_groups:
            default_feature_selection_groups.remove(dataset_type)

if not dropped_cols:
    default_feature_selection_groups.append('all')

#scored_train_data = pd.read_csv(io.pipeline_data_path + "data_{}_scored.csv".format(machine.lower()))['score']
scored_train_data = pd.read_csv(io.pipeline_data_path + "data_{}_scored.csv".format(machine.lower()))[['timestamps', 'score']]
scored_train_data = scored_train_data.sort_values(by=index_name)
index_to_be = scored_train_data[index_name]
if index_to_be.dtype != 'object':
    scored_train_data = scored_train_data.set_index(pd.to_datetime(scored_train_data[index_name], unit='s'))
else:
    scored_train_data = scored_train_data.set_index(pd.to_datetime(scored_train_data[index_name]))



def build_card_column(style, cards):
    return html.Div(
        className='three columns',
        style=style,
        children=cards
    )

def build_banner(title, image):
    return html.Div(className='banner', children=[
        html.Div(className='container scalable', children=[
            html.H2(title),
            html.Img(src=image)
        ]),

    ])


def build_body(params):
    column1_style = {
        'width': '550px',
        'overflow-y': 'auto',
        'overflow-x': 'hidden',
        'position': 'fixed'
    }
    column2_style = {
        'min-width': '75%',
        'max-width': '100%',
        'overflow-y': 'auto',
        'overflow-x': 'hidden',
        'padding-left': '550px'
    }
    column3_style = {
        'min-width': '10%',
        'overflow-y': 'auto',
        'overflow-x': 'hidden'
    }

    column1_cards = [
        cards.information_card([machine,
                                input_features_type,
                                features.shape[0],
                                features.shape[1],
                                start,
                                end,
                                get_total_nb_anomalies(logbook_fn, 1),
                                get_total_nb_anomalies(logbook_fn, 2)]),
        cards.data_selection_card(apply_default_value, params, [default_data_1,
                                                                default_data_2,
                                                                default_labels,
                                                                default_beam,
                                                                machine,
                                                                start,
                                                                end]),
        cards.anomaly_detection_card(apply_default_value, params, [default_prediction_file,
                                                                   default_score_method,
                                                                   min_threshold,
                                                                   max_threshold,
                                                                   threshold_step_size,
                                                                   default_threshold,
                                                                   default_anomaly_detector])
    ]
    column2_cards = [
        cards.data_exploration_card(),
        cards.data_analysis_card()
    ]
    column3_cards = [
        cards.evaluation_card(),
        cards.fp_table_card(),
        cards.fn_table_card(),
    ]
    return html.Div(id='body', className='container scalable', children=[
        # 3 columns
        build_card_column(column1_style, column1_cards),
        build_card_column(column2_style, column2_cards),
        build_card_column(column3_style, column3_cards),

        # hidden parts
        html.Div(id='hidden-div', style={'display': 'none'}),
        html.Div(id='hidden-div-statemode', style={'display': 'none'}),
        html.Div(id='hidden-div-segments', style={'display': 'none'}),
        ])


def build_layout(params):
    layout = [
        build_banner(title="CERN Data Explorer", image="assets/cern_white.png"),
        build_body(params),
    ]
    global has_rendered
    has_rendered = True
    return layout


def current_beam(filename):
    if 'B1' in filename:
        return 1
    if 'B2' in filename:
        return 2
    return default_beam  # default beam for web explorer


@app.callback(Output('beam-dropdown', 'value'),
              [Input('predictions', 'value')])
def set_beam_after_predictionfile_change(filename):
    return current_beam(filename)


@app.callback(Output('column-dropdown1', 'options'),
              [Input('beam-dropdown', 'value')])
def set_dataset_options(selected_beam):
    print("Setting column dropdown 1 options for beam {}".format(selected_beam))
    a = [{'label': i, 'value': i} for i in feature_cols[selected_beam]]
    return a


@app.callback(Output('column-dropdown2', 'options'),
              [Input('beam-dropdown', 'value')])
def set_dataset_options(selected_beam):
    print("Setting column dropdown 2 options for beam {}".format(selected_beam))
    return [{'label': i, 'value': i} for i in feature_cols[selected_beam]]


def get_label_options(beam):
    if logbook_type == 'all':
        logbook = filter_logbook_beam(read_logbook(logbook_fn), beam)
        return [{'label': i, 'value': i} for i in sorted(logbook['TAG'].unique())]
    return [{'label': i, 'value': i} for i in ['anomaly']]


@app.callback(Output('label-checkboxes', 'options'),
              [Input('beam-dropdown', 'value')])
def set_label_options(selected_beam):
    print("Setting label dropdown options for beam {}".format(selected_beam))
    return get_label_options(selected_beam)


@app.callback(Output('predictions', 'options'),
              [Input('beam-dropdown', 'value'),
               Input('anomaly-detector-dropdown', 'value')])
def set_prediction_options(selected_beam, ad):
    return get_prediction_options(selected_beam, ad)


def update_prediction_file(prediction_file):
    global truth_and_pred_filename
    global truth_and_pred_df
    global predictions

    # only update on change
    if prediction_file == truth_and_pred_filename:
        return
    print("Updating prediction file to {}".format(prediction_file))


    try:
        truth_and_pred_filename, truth_and_pred_df = util.load_df_from_csv_with_name(prediction_file)
        predictions = True
    except Exception as e:
        print("Error occurred when getting prediction.\n {}".format(e))
        truth_and_pred_filename, truth_and_pred_df = '', ''
        predictions = False

cached_evaluations = {}
cached_predictions_for_threshold = {}
def cache_initial_threshold_evaluation():
    update_prediction_file(default_prediction_file)
    # cache evaluation for each threshold
    # (for default prediction file and pred_to_use only, other prediction files/pred_to_use are added dynamically)
    print("Caching evaluation for different thresholds for {} with {}...".format(default_prediction_file, default_pred_to_use))

    thresholds_to_cache = list(np.linspace(min_threshold, max_threshold, int((max_threshold-min_threshold)/threshold_to_cache_step_size + 1)))
    for threshold in thresholds_to_cache:
        print("caching threshold {}/{}".format(threshold, max_threshold))
        cache_args_tuple = (default_prediction_file, threshold, default_pred_to_use)
        calc_args_tuple = (truth_and_pred_df, threshold, default_pred_to_use)

        cached_evaluations[cache_args_tuple] = evaluation.cluster_predictions(*calc_args_tuple)
        cached_predictions_for_threshold[cache_args_tuple] = evaluation.make_predictions_for_threshold(*calc_args_tuple)

# initial caching
if initial_threshold_values_caching:
    cache_initial_threshold_evaluation()
print("done! The app is ready to launch.")


def get_cached_evaluation(threshold, pred_to_use):
    try:
        return cached_evaluations[truth_and_pred_filename, threshold, pred_to_use]
    except KeyError:
        res = evaluation.cluster_predictions(truth_and_pred_df, threshold, pred_to_use)
        cached_evaluations[truth_and_pred_filename, threshold, pred_to_use] = res
        return res

def get_cached_predictions_for_threshold(threshold, threshold_score, pred_to_use):
    try:
        return cached_predictions_for_threshold[truth_and_pred_filename, threshold, pred_to_use]
    except KeyError:
        res = evaluation.make_predictions_for_threshold(truth_and_pred_df, threshold_score, pred_to_use)
        cached_predictions_for_threshold[truth_and_pred_filename, threshold, pred_to_use] = res
        return res


@app.callback(Output('evaluation-card', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value'),
               Input('anomaly-detector-dropdown', 'value')])
def update_evaluation(prediction_file, segment_score, threshold, ad):
    update_prediction_file(prediction_file)

    if not predictions:
        return 'No predicting performed. Select a prediction file.'

    pred_to_use = util.get_pred_to_use(segment_score)
    threshold_score = truth_and_pred_df[pred_to_use].quantile(threshold)
    _, precision, recall, tp, fp, fn, tn = get_cached_predictions_for_threshold(threshold,
                                                                                threshold_score,
                                                                                pred_to_use)
    return html.Div([
        dcc.Markdown('''
        ##### **Evaluation**  
        Detector type: {}  
        Threshold score: {:.4f}  
        TP:  {:4} | FP:  {:4}  || sum: {:4}  
        FN:  {:4} | TN:  {:4}  || sum: {:4}  
        PRECISION: {:.2f}, RECALL: {:.2f}
        '''.format(ad, threshold_score, tp, fp, tp + fp, fn, tn, fn + tn, precision, recall))
    ])


@app.callback(Output('fp-table', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def update_fp_table(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        return 'No predicting performed. Select a prediction file.'

    pred_to_use = util.get_pred_to_use(segment_score)

    _, _, _, FP, _ = get_cached_evaluation(threshold, pred_to_use)
    x_FP, y_FP, label_FP = segments_to_plot(FP)
    fp_df = pd.DataFrame(columns=['start', 'end'])
    fp_df = segment_list_to_pandas_df(x_FP, fp_df)
    table = drc.NamedDataTable(
        name='False Positives',
        columns=[{"name": i, "id": i} for i in fp_df.columns],
        data=fp_df.to_dict("records"),
        style_table={
            'overflowX': 'scroll',
            'overflowY': 'scroll',
            'maxHeight': '500'
        }
    )
    return table


@app.callback(Output('fn-table', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def update_fn_table(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        return 'No predicting performed. Select a prediction file.'

    pred_to_use = util.get_pred_to_use(segment_score)

    _, _, FN, _, _ = get_cached_evaluation(threshold, pred_to_use)
    x_FN, y_FN, label_FN = segments_to_plot(FN)
    fn_df = pd.DataFrame(columns=['start', 'end'])
    fn_df = segment_list_to_pandas_df(x_FN, fn_df)
    table = drc.NamedDataTable(
        name='False Negatives',
        columns=[{"name": i, "id": i} for i in fn_df.columns],
        data=fn_df.to_dict("rows"),
        style_table={
            'overflowX': 'scroll',
            'overflowY': 'scroll',
            'maxHeight': '500'
        }
    )
    return table


def segment_list_to_pandas_df(segments, df):
    i = 1
    segment_list = []
    for el in segments:
        if i == 3:
            df.loc[len(df)] = segment_list
            segment_list = []
            i = 1
        else:
            segment_list.append(el)
            i = i + 1
    df['start'] = pd.to_datetime(df['start']).dt.strftime("%Y-%m-%d %H:%M")
    df['end'] = pd.to_datetime(df['end']).dt.strftime("%Y-%m-%d %H:%M")
    return df.reset_index()


def segments_to_plot(segments):
    x = []
    y = []
    label = []
    for segment in segments:
        x.extend([segment.ts_min(), segment.ts_max(), None])
        y.extend([0, 1, None])
        info = 'label: {}<br> y_true: {:d}'.format(segment.get_label(), segment.ground_truth())
        label.extend([info, info, None])
    return x, y, label


@app.callback(Output('hidden-div-segments', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def segments_calculation(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        return 'No segments, since no -best.csv file was found. Select a prediction file.'

    pred_to_use = util.get_pred_to_use(segment_score)

    TN, TP, FN, FP, _ = get_cached_evaluation(threshold, pred_to_use)
    x_TN, y_TN, label_TN = segments_to_plot(TN)
    x_TP, y_TP, label_TP = segments_to_plot(TP)
    x_FN, y_FN, label_FN = segments_to_plot(FN)
    x_FP, y_FP, label_FP = segments_to_plot(FP)

    datadict = {'x_TN': x_TN, 'y_TN': y_TN, 'label_TN': label_TN,
                'x_TP': x_TP, 'y_TP': y_TP, 'label_TP': label_TP,
                'x_FN': x_FN, 'y_FN': y_FN, 'label_FN': label_FN,
                'x_FP': x_FP, 'y_FP': y_FP, 'label_FP': label_FP}
    return json.dumps(datadict)


@app.callback(Output('segments-graph', 'figure'),
              [Input('hidden-div-segments', 'children'),
               Input('data1-graph', 'relayoutData'),
               Input('data2-graph', 'relayoutData'),
               Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date')])
def update_segments_graph(jdata, rld1, rld2, startd, endd):
    try:
        if jdata is not None:
            data = json.loads(jdata)
    except:
        print("Error in fetching graph data, which returned '{}'".format(jdata))
        return {}

    # get zoomlevel from other graphs
    zrange = get_zoom_level([rld1, rld2], startd, endd)

    graphfig = {
        'data': [go.Scattergl(x=data['x_TN'], y=data['y_TN'], text=data['label_TN'], mode='lines+markers', name='TN',
                              line=dict(color=confusion_matrix_colors['TN'])),
                 go.Scattergl(x=data['x_TP'], y=data['y_TP'], text=data['label_TP'], mode='lines+markers', name='TP',
                              line=dict(color=confusion_matrix_colors['TP'])),
                 go.Scattergl(x=data['x_FP'], y=data['y_FP'], text=data['label_FP'], mode='lines+markers', name='FP',
                              line=dict(color=confusion_matrix_colors['FP'], dash='dot')),
                 go.Scattergl(x=data['x_FN'], y=data['y_FN'], text=data['label_FN'], mode='lines+markers', name='FN',
                              line=dict(color=confusion_matrix_colors['FN'], dash='dot'))],
        'layout': {
            'title': 'Segments (based on Xpoc data)',
            'height': 150,
            'legend': dict(orientation="h", y=1.5),
            'margin': {'b': 30, 'r': 60, 'l': 60, 't': 60},
            'xaxis': zrange,
            'yaxis': dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                showticklabels=False)
        }
    }
    return graphfig


@app.callback(Output('hidden-div', 'children'),
              [Input('segments-graph', 'relayoutData')])
def print_debug(rld):
    # below line added to disable debug output
    return 0
    print("printing segments relayoutData:")
    print(json.dumps(rld, indent=2))
    print("printing statemode relayoutData:")
    print(json.dumps(rld2, indent=2))
    return 0


# below callback to reduce the execution-time for the lower statemode-graph figure callback
@app.callback(Output('hidden-div-statemode', 'children'),
              [Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date'),
               Input('beam-dropdown', 'value')])
def state_mode_query(startd, endd, beam):
    ua_num = "UA23" if beam == 1 else "UA87"
    data = pd.DataFrame(features.index)
    return data.to_json(date_format='iso', orient='split')  # json.dumps fails for pd


def to_labels(df):
    labels = pd.Series([], dtype='object')
    for i, (comment, event_id) in enumerate(zip(df[anomaly_description_index], df[anomaly_id_index])):
        if type(comment) != str:
            comment = str(comment)
        labels[i] = 'ID: ' + str(event_id) + '<br>' + comment.replace("\n", "<br />")
    return labels


def insert_br(s):
    return "<br>".join(re.findall("(?s).{,64}", s))[:-1]


def label_scatter(data, labels, name, ticker, color):
    if not data.empty:
        y = [data[ticker].min() for _ in labels.index]
    else:
        y = [0 for _ in labels.index]
    return go.Scattergl(x=labels.index,
                        y=pd.Series(y),
                        mode='markers',
                        marker=dict(size=10, color=color),
                        text=to_labels(labels),
                        name=name)


@app.callback(Output('pr-curve-graph', 'figure'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value')])
def update_pr_curve(prediction_file, segment_score):
    update_prediction_file(prediction_file)

    pred_to_use = util.get_pred_to_use(segment_score)

    if not predictions:
        print("PR curve could not be plotted, select a prediction file.")
        return {}

    # Obtain data for PR curve
    predictions_df = truth_and_pred_df.copy()
    y_true = predictions_df.y_true.astype('float')
    y_pred = predictions_df[pred_to_use].astype('float')
    auc, precision, recall = evaluation.pr_curve(y_true=y_true, y_pred=y_pred, fig_filename=None)

    graphfig = {
        'data': [
            {'x': recall, 'y': precision, 'name': 'PR curve'},
        ],
        'layout': {
            'title': 'PR curve (AUC={:.2f})'.format(auc),
            'xaxis': {
                'title': 'Recall'
            },
            'yaxis': {
                'title': 'Precision'
            },
            'margin': {
                'l': 40,
                'r': 20
            }
        }
    }
    return graphfig


@app.callback(Output('data1-graph', 'figure'),
              [Input('column-dropdown1', 'value'),
               Input('label-checkboxes', 'value'),
               Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date'),
               Input('beam-dropdown', 'value'),
               Input('segments-graph', 'relayoutData'),
               Input('data2-graph', 'relayoutData'),
               Input('lines-markers-radio', 'value')
               ])
def update_data1_graphs(selected_cols, labels, startd, endd, beam, rld1, rld2, marker_type):
    return update_data_graphs(selected_cols, labels, startd, endd, beam, rld1, rld2, marker_type, 1)


@app.callback(Output('data2-graph', 'figure'),
              [Input('column-dropdown2', 'value'),
               Input('label-checkboxes', 'value'),
               Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date'),
               Input('beam-dropdown', 'value'),
               Input('segments-graph', 'relayoutData'),
               Input('data1-graph', 'relayoutData'),
               Input('lines-markers-radio', 'value')
               ])
def update_data2_graphs(selected_cols, labels, startd, endd, beam, rld1, rld2, marker_type):
    return update_data_graphs(selected_cols, labels, startd, endd, beam, rld1, rld2, marker_type, 2)


def update_data_graphs(selected_cols, labels, startd, endd, beam, rld1, rld2, marker_type, nb):
    data_graphs = []
    col = ''
    try:
        for i, col in enumerate(selected_cols):
            #column = features[col]  # changed by cached_y_values
            data_fig = go.Scattergl(
                x=cached_x_values,
                y=cached_y_values[col],
                mode=marker_type,
                marker=dict(size=4),
                name=col)
            data_graphs.append(data_fig)
    except Exception as exception:
        print("error occured during update_graph, ignoring for now..\n", exception)
        traceback.print_exc()

    if col == '':
        return {}

    try:
        try:
            logbook = read_logbook(logbook_fn)
            logbook = filter_logbook_beam(logbook, beam)
        except FileNotFoundError:
            print("ERROR - missing {}, continuing without labels".format(logbook_fn))
            logbook = None

        if logbook_type == 'all':
            for label in labels:
                print(label)
                logbook = logbook[start:end]
                anomaly_logbook = logbook[logbook['TAG'] == label]
                label_df = anomaly_logbook[[anomaly_description_index, anomaly_id_index]]
                data_graphs.append(label_scatter(features, label_df, label, col, label_colors[label]))
        else:
            if 'anomaly' in labels:
                logbook = logbook[start:end]
                anomaly_df = logbook[[anomaly_description_index, anomaly_id_index]]
                data_graphs.append(label_scatter(features, anomaly_df, 'anomaly', col, 'rgba(220,20,60,1.0)'))
    except Exception as exception:
        print('error occured when fetching or plotting labels, ignoring for now..\n', exception)
        traceback.print_exc()

    # get zoomlevel from other graphs
    zrange = get_zoom_level([rld1, rld2], startd, endd)

    graphfig = {
        'data': data_graphs,
        'layout': {
            'title': 'Data ' + str(nb),
            'height': 350,
            'legend': dict(orientation='h', y=1.5),
            'showlegend': True,
            'margin': {'b': 30, 'r': 60, 'l': 60, 't': 60},
            'xaxis': zrange,
            'yaxis': dict(
                autorange=True,
            )
        }
    }
    return graphfig

@app.callback(Output('histogram1-graph', 'figure'),
              [Input('column-dropdown1', 'value'),
               Input('nb-bins-histogram', 'value'),
               Input('tp-fp-fn-tn-dropdown', 'value'),
               Input('tp-fp-fn-tn-selection-radio', 'value'),
               Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')
               ])
def update_histogram(selected_cols, n_bins, selected_prediction, confusion_matrix_selection, prediction_file, segment_score, threshold):
    return update_histogram1_graph(selected_cols, n_bins, selected_prediction, confusion_matrix_selection, prediction_file, segment_score, threshold)


@app.callback(Output('histogram1-2-graph', 'figure'),
              [Input('column-dropdown2', 'value'),
               Input('nb-bins-histogram', 'value'),
               Input('tp-fp-fn-tn-dropdown', 'value'),
               Input('tp-fp-fn-tn-selection-radio', 'value'),
               Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')
               ])
def update_histogram(selected_cols, n_bins, selected_prediction, confusion_matrix_selection, prediction_file, segment_score, threshold):
    return update_histogram1_graph(selected_cols, n_bins, selected_prediction, confusion_matrix_selection, prediction_file, segment_score, threshold)

def update_histogram1_graph(selected_cols, n_bins, selected_prediction, confusion_matrix_selection, prediction_file, segment_score, threshold):
    """
    Args:
        selected prediction: String of "[ts_min()] to [ts_max()]" of the prediction segment
    """
    if selected_prediction is None or selected_cols is None:
        return {}
    if len(selected_cols) == 0:
        return {}
    if len(selected_cols) > 1:
        print("Error: Please select only 1 feature from the list to plot a histogram.")
        traceback.print_exc()
        return {}
    data_graphs = []
    col = selected_cols[0]
    col_data = features[start:end][col]
    try:
        data_fig = go.Histogram(
                x=col_data,
                nbinsx=n_bins,
                name="Feature Histogram",
                histnorm='probability',
                marker_color=[standard_webapp_color] * n_bins
        )
        data_graphs.append(data_fig)
    except Exception as exception:
        print("error occured during update_graph, ignoring for now..\n", exception)
        traceback.print_exc()

    if 'all' in confusion_matrix_selection:
        TN, TP, FN, FP, _ = get_confusion_matrix_options(prediction_file, segment_score, threshold)
        if confusion_matrix_selection == 'all':
            # plot all tp, fp, tn, fn on scatter plot
            pred_list = [TN, TP, FP, FN]
            names = ["TN", "TP", "FP", "FN"]
            ys = [1, 0.95, 0.9, 0.85]
            for p in range(len(pred_list)):
                data_graphs.append(create_predictions_scatter_plot(col_data, pred_list[p], names[p], ys[p], confusion_matrix_colors[names[p]]))
        elif confusion_matrix_selection == 'all TP':
            data_graphs.append(create_predictions_scatter_plot(col_data, TP, "TP", 1, confusion_matrix_colors["TP"]))
        elif confusion_matrix_selection == 'all FP':
            data_graphs.append(create_predictions_scatter_plot(col_data, FP, "FP", 1, confusion_matrix_colors["FP"]))
        elif confusion_matrix_selection == 'all FN':
            data_graphs.append(create_predictions_scatter_plot(col_data, FN, "FN", 1, confusion_matrix_colors["FN"]))
        elif confusion_matrix_selection == 'all TN':
            data_graphs.append(create_predictions_scatter_plot(col_data, TN, "TN", 1, confusion_matrix_colors["TN"]))
    elif selected_prediction != "":
        # add scatter with single datapoint for prediction marking
        prediction_data_value = util.get_prediction_data_value(machine, col_data, scored_train_data, selected_prediction)
        data_graphs.append(go.Scattergl(x=pd.Series([prediction_data_value]),
                            y=pd.Series([0]),
                            mode='markers',
                            marker=dict(size=15, color=label_colors["anomaly"]),
                            name="Prediction"))

    graphfig = {
        'data': data_graphs,
        'layout': {
            'title': str(col),
            'xaxis': {
                'title': 'Feature value'
            },
            'legend': dict(orientation='h', y=5),
            'showlegend': True,
        }
    }
    return graphfig


def create_predictions_scatter_plot(col_data, prediction_segments, name, y, marker_color):
    p_time = ["{} to {}".format(j.ts_min(), j.ts_max()) for j in prediction_segments]
    x_values = [util.get_prediction_data_value(machine, col_data, scored_train_data, j) for j in p_time]
    y_values = [y] * len(x_values)
    return go.Scattergl(x=pd.Series(x_values),
                        y=pd.Series(y_values),
                        mode='markers',
                        marker=dict(size=15, color=marker_color),
                        name=name)

@app.callback(Output('data-analysis-tab-content', 'children'),
              [Input('data-analysis-tabs', 'value')])
def build_data_analysis_tab_content(tab):
    if tab == 'data-analysis-histogram-tab':
        print("data-analysis-histogram-tab selected")
        return cards.histogram_card(default_prediction)
    elif tab == 'data-analysis-avd-tab':
        print("data-analysis-avd-tab selected")
        return cards.get_avd_body(default_threshold_slider_values)
    elif tab == 'data-analysis-feature-selection':
        print("data-analysis-feature-selection selected")
        return cards.get_feature_selection_body(default_feature_selection, default_feature_selection_groups, machine)


@app.callback(Output('histogram-tab-content', 'children'),
              [Input('histogram-tabs', 'value')])
def build_histogram_tab_content(tab):
    if tab == 'histogram-tab1':
        print("histogram tab 1 selected")
        return cards.get_global_histogram_body(default_nb_bins, start, end)
    elif tab == 'histogram-tab2':
        print("histogram tab 2 selected")
        return cards.get_local_histogram_body(default_nb_bins, default_nb_hours)


@app.callback(Output('ranking-tab-content', 'children'),
              [Input('ranking-tabs', 'value')])
def build_rankings_tab_content(tab):
    if tab == 'ranking-tab':
        print("ranking tab selected")
        return cards.get_ranking_body(default_nb_bins, default_nb_hours, default_min_freq)
    elif tab == 'shap-ranking-tab':
        print("shap-ranking-tab selected")
        return cards.get_shap_ranking_body()


@app.callback(Output('histogram2-graph', 'figure'),
              [Input('column-dropdown1', 'value'),
               Input('nb-bins-histogram', 'value'),
               Input('nb-hours-histogram', 'value'),
               Input('tp-fp-fn-tn-dropdown', 'value')
               ])
def update_histogram(selected_cols, n_bins, n_hours, selected_prediction):
    return update_histogram2_graph(selected_cols, n_bins, n_hours, selected_prediction)


@app.callback(Output('histogram2-2-graph', 'figure'),
              [Input('column-dropdown2', 'value'),
               Input('nb-bins-histogram', 'value'),
               Input('nb-hours-histogram', 'value'),
               Input('tp-fp-fn-tn-dropdown', 'value')
               ])
def update_histogram(selected_cols, n_bins, n_hours, selected_prediction):
    return update_histogram2_graph(selected_cols, n_bins, n_hours, selected_prediction)


def update_histogram2_graph(selected_cols, n_bins, n_hours, selected_prediction):
    """
    Args:
        selected prediction: String of "[ts_min()] to [ts_max()]" of the prediction segment
    """
    if len(selected_cols) == 0 or selected_prediction == "" or n_bins is None or n_hours is None:
        return {}
    if len(selected_cols) > 1:
        print("Error: Please select only 1 feature from the list to plot a histogram.")
        traceback.print_exc()
        return {}
    data_graphs = []
    col = selected_cols[0]
    col_data = features[col]
    try:
        # TODO: This works for mki but not lbds (no segmentation), still needs a fix for lbds
        start_time, end_time = split_prediction(selected_prediction)
        start_time = start_time - timedelta(hours=n_hours)
        end_time = end_time + timedelta(hours=n_hours)
        col_data = col_data[start_time:end_time]

        data_fig = go.Histogram(
            x=col_data,
            nbinsx=n_bins,
            name="Feature Histogram",
            histnorm='probability',
            marker_color=[standard_webapp_color] * n_bins
        )
        data_graphs.append(data_fig)

        prediction_data_value = util.get_prediction_data_value(machine, col_data, scored_train_data, selected_prediction)
        data_graphs.append(go.Scattergl(x=pd.Series([prediction_data_value]),
                                        y=pd.Series([0]),
                                        mode='markers',
                                        marker=dict(size=15, color=label_colors["anomaly"]),
                                        name="Prediction"))

    except Exception as exception:
        print("error occured during update_graph, ignoring for now..\n", exception)
        traceback.print_exc()

    graphfig = {
        'data': data_graphs,
        'layout': {
            'title': str(col),
            'xaxis': {
                'title': 'Feature value'
            },
            'legend': dict(orientation='h', y=5),
            'showlegend': True,
        }
    }
    return graphfig

def split_prediction(prediction):
    [start_time, end_time] = prediction.split(" to ")
    start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
    end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
    return start_time, end_time

def get_confusion_matrix_options(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        print("No predicting performed.")
        return [], [], [], [], 'No predicting performed.'

    pred_to_use = util.get_pred_to_use(segment_score)

    return get_cached_evaluation(threshold, pred_to_use)

# default prediction (necessary such that histogram ranking table exists on webapp creation for table interactivity)
TN, TP, FN, FP, _ = get_confusion_matrix_options(default_prediction_file, 'max', default_threshold)
default_prediction = "{} to {}".format(TP[0].ts_min(), TP[0].ts_max()) if len(TP) else ''


@app.callback(Output('tp-fp-fn-tn-dropdown', 'options'),
              [Input('beam-dropdown', 'value'),
               Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value'),
               Input('tp-fp-fn-tn-selection-radio', 'value')
               ])
def set_options(selected_beam, prediction_file, segment_score, threshold, confusion_matrix_selection):
    return set_confusion_matrix_options(selected_beam, prediction_file, segment_score, threshold, confusion_matrix_selection)


def set_confusion_matrix_options(selected_beam, prediction_file, segment_score, threshold, confusion_matrix_selection):
    TN, TP, FN, FP, _ = get_confusion_matrix_options(prediction_file, segment_score, threshold)
    if confusion_matrix_selection == "TP":
        predictions_list = TP
    elif confusion_matrix_selection == "FP":
        predictions_list = FP
    elif confusion_matrix_selection == "FN":
        predictions_list = FN
    elif confusion_matrix_selection == "TN":
        predictions_list = TN
    else:
        predictions_list = []
    print("Setting confusion matrix dropdown values for beam: {}, prediction: {}, segment score: {} and threshold: {}".format(selected_beam, prediction_file, segment_score, threshold))
    #s = predictions_list[0]
    #segment_stats = [str(s), len(s), s.get_df(), s.max_freq_temp(), s.max_freq_pressure(), s.min_freq_pressure(), s.is_labeled(), s.get_label(), s.ts_min(), s.ts_max(),
    #                 s.ground_truth()]
    #print("segment_stats: ")
    #for i in segment_stats:
    #    print(i)
    #    print(s.get_label())
    return [{'label': "{} {} | {} to {} | anomaly_score: {:.3f}".format(confusion_matrix_selection, i.get_label(), i.ts_min(), i.ts_max(), i.get_anomaly_score()),
             'value': "{} to {}".format(i.ts_min(), i.ts_max())} for i in predictions_list]


@app.callback(Output('ranking-div', 'children'),
              [Input('beam-dropdown', 'value'),
               Input('tp-fp-fn-tn-dropdown', 'value'),
               Input('nb-bins-ranking', 'value'),
               Input('nb-hours-ranking', 'value'),
               Input('tp-fp-fn-tn-selection-radio', 'value'),
               Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value'),
               Input('min-freq', 'value')
               ])
def set_ranking_table(beam, selected_prediction, n_bins, n_hours, confusion_matrix_selection, prediction_file, segment_score, threshold, min_freq):
    if long_table_calc_disabled and (confusion_matrix_selection == 'all' or
                                     confusion_matrix_selection == 'all TN' or
                                     confusion_matrix_selection == 'all FP'):
        return html.Div(["Calculating rankings for many aggregated predictions ('all', 'all TN' and 'all FP') can take a long time, enable this in the config (long_table_calc_disabled=False) and restart the webapp"])
    if n_bins is None or n_hours is None:
        return html.Div(["Please fill in # bins and n_hours"])
    if confusion_matrix_selection in ['all TP', 'all FP', 'all FN', 'all TN']:
        return get_all_predictions_ranking_body(beam, n_bins, n_hours, confusion_matrix_selection, prediction_file, segment_score, threshold, min_freq)
    return get_single_prediction_ranking_body(beam, selected_prediction, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold, min_freq)


def get_all_predictions_ranking_body(beam, n_bins, n_hours, confusion_matrix_selection, prediction_file, segment_score, threshold, min_freq):
    data = get_all_predictions_ranking_data(beam, n_bins, n_hours, confusion_matrix_selection, prediction_file, segment_score, threshold)

    # experiment
    input_freq = 0 if min_freq is None else min_freq
    input_freq = input_freq / 100 if input_freq > 0 else 0
    input_freq = input_freq if input_freq < 1 else 1
    selected_features = fs_experiments.select_features_frequency_bound(data, f=input_freq, add_one_feature=True)
    io.write_to_csv(pd.DataFrame(selected_features), "selected_features_{}.csv".format(confusion_matrix_selection), io.histogram_frequency_feature_selection_path)

    ranking_plot = create_horizontal_bar_plot(data, 'Histogram', selected_features=selected_features, nb_bins=n_bins, nb_hours=n_hours)
    return [html.Div(id='all-ranking-bar-div', children=[dcc.Graph(id='all-ranking-bar-plot', figure=ranking_plot)]),
            create_ranking_datatable(data, id='ranking-table'),
            get_bar_plot_info_div(confusion_matrix_selection, n_bins, n_hours, threshold)]


def get_single_prediction_ranking_body(beam, selected_prediction, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold, min_freq):
    data = get_single_prediction_ranking_data(beam, selected_prediction, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold)

    # experiment
    input_freq = 0 if min_freq is None else min_freq
    input_freq = input_freq / 100 if input_freq > 0 else 0
    input_freq = input_freq if input_freq < 1 else 1
    selected_features = fs_experiments.select_features_frequency_bound(data, f=input_freq, add_one_feature=True)
    io.write_to_csv(pd.DataFrame(selected_features), "selected_features_{}.csv".format(confusion_matrix_selection), io.histogram_frequency_feature_selection_path)

    ranking_plot = create_horizontal_bar_plot(data, 'Histogram', selected_features=selected_features, nb_bins=n_bins, nb_hours=n_hours)
    return [html.Div(id='single-ranking-bar-div', children=[dcc.Graph(id='single-ranking-bar-plot', figure=ranking_plot)]),
            create_ranking_datatable(data, id='ranking-table'),
            get_bar_plot_info_div("{}: {}".format(confusion_matrix_selection, selected_prediction), n_bins, n_hours, threshold)]


def create_horizontal_bar_plot(data, ranking_type, selected_features=None, nb_bins=None, nb_hours=None):
    y_name = "SHAP Ranking" if ranking_type == "SHAP" else "Ranking"
    x = []
    y = []
    for data_entry in data:
        x.append(data_entry[y_name])
        y.append(data_entry["Feature"])

    # reverse data such that most anomalous (highest) ranking is on top
    x.reverse()
    y.reverse()

    if ranking_type == "Histogram":
        # logarithmic plot for better visualization
        #x = [math.log(i, 10) for i in x]
        colors = [standard_webapp_color] * len(y)
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln not in selected_features:
                    colors[i] = confusion_matrix_colors["FP"]
    elif ranking_type == "SHAP":
        colors = []
        for r in x:
            if r > 0:
                colors.append('green')
            elif r < 0:
                colors.append('red')

    data_graph = []
    data_fig = go.Bar(
        x=x,
        y=y,
        orientation='h',
        marker_color=colors)
    data_graph.append(data_fig)

    if nb_hours is not None and nb_bins is not None:
        histogram_type = "global" if nb_hours > 365 * 24 else "{} hours neighbourhood".format(nb_hours)
        fig_title = "{} Ranking | {} | {} bins".format(ranking_type, histogram_type, nb_bins)
        x_axis_title = 'log10(Histogram score)'
    else:
        fig_title = "{} Ranking".format(ranking_type)
        x_axis_title = "SHAP value"

    bar_plot = {
        'data': data_graph,
        'layout': {
            'title': fig_title,
            'xaxis': {
                'title': x_axis_title,
                'automargin': True,
            },
            'yaxis': {
                'title': 'Feature name',
                'automargin': True
            },
            'bargap': 0
        }
    }
    return bar_plot

def get_bar_plot_info_div(prediction, n_bins, n_hours, threshold):
    return html.Div([dcc.Markdown('''**Prediction:** {}  
                                     **Threshold:** {}  
                                     **# bins:** {}  
                                     **# hours margin:** {}'''.format(prediction, threshold, n_bins, n_hours))])

def get_all_predictions_ranking_data(beam, n_bins, n_hours, confusion_matrix_selection, prediction_file, segment_score, threshold):
    TN, TP, FN, FP, _ = get_confusion_matrix_options(prediction_file, segment_score, threshold)
    if confusion_matrix_selection == 'all TP':
        pred_list = TP
    elif confusion_matrix_selection == 'all FP':
        pred_list = FP
    elif confusion_matrix_selection == 'all FN':
        pred_list = FN
    elif confusion_matrix_selection == 'all TN':
        pred_list = TN
    elif confusion_matrix_selection == 'all':
        pred_list = TP + FP + FN + TN
    else:
        print("Non-legal confusion matrix selection selected")

    d = pd.DataFrame(data={'Feature': feature_cols[beam]})
    p_time = ["{} to {}".format(j.ts_min(), j.ts_max()) for j in pred_list]
    for i, prediction in enumerate(p_time):
        print("prediction {}/{}".format(i+1, len(p_time)))
        start_time, end_time = split_prediction(prediction)
        start_time = start_time - timedelta(hours=n_hours)
        end_time = end_time + timedelta(hours=n_hours)
        df = features[start_time:end_time]
        # TODO: "normalize" by dividing by the highest_bin_height (not possible with fast version) or
        #   divide by the total entries in the dataframe (possible)

        rankings = []
        for col in feature_cols[beam]:
            ranking = get_pred_feature_ranking_fast(df[col], n_bins, prediction)
            if ranking is None:
                return []
            rankings.append(ranking)
        d[i] = pd.Series(rankings) / df.shape[0]  # divide by # timestamps to make predictions comparable
        d[i] = 1 / d[i]  # flip values such that higher is more outlier
    d["Ranking"] = d.mean(axis=1)
    d = d[["Feature", "Ranking"]]

    d_dict = {}
    for i in range(d.shape[0]):
        d_dict[d["Feature"].iloc[i]] = d["Ranking"].iloc[i]
    d_dict = {k: v for k, v in sorted(d_dict.items(), key=lambda item: item[1], reverse=True)}

    # write rankings to file
    if write_rankings_aggregated:
        write_ranking_to_file(d_dict, io.get_histogram_rankings_path(), beam, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold)


    data = []
    for k, v in d_dict.items():
        entry = {'Feature': k, 'Ranking': round(math.log(v, 10), round_rankings)}
        data.append(entry)

    return data


def write_ranking_to_file(rankings_dict, f_path, beam, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold):
    """
    Writes the generated rankings to the /results/ directory
    Args:
        rankings_dict: A dictionary containing the feature names and rankings: { 'Feature name1': 'ranking1', ...}
    """
    dict_to_write = {'Feature': [], 'Histogram ranking': []}
    for f, r in rankings_dict.items():
        dict_to_write['Feature'].append(f)
        dict_to_write['Histogram ranking'].append(r)
    filename = "histogram-rankings_B{}_{}_{}_{}_nbins={}_nhours={}.csv".format(beam,
                                                                               confusion_matrix_selection,
                                                                               threshold,
                                                                               segment_score,
                                                                               n_bins,
                                                                               n_hours)
    io.write_to_csv(pd.DataFrame.from_dict(dict_to_write), filename, f_path)


def get_single_prediction_ranking_data(beam, selected_prediction, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold):
    if selected_prediction == "":
        return html.Div(["Please select a prediction"])
    start_time, end_time = split_prediction(selected_prediction)
    start_time = start_time - timedelta(hours=n_hours)
    end_time = end_time + timedelta(hours=n_hours)
    df = features[start_time:end_time]

    d = {}
    for col in feature_cols[beam]:
        ranking = get_pred_feature_ranking_fast(df[col], n_bins, selected_prediction)
        if ranking is None:
            return []
        d[col] = ranking / df.shape[0]
        d[col] = 1 / d[col]  # flip values such that higher is more outlier
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}

    if write_rankings_single:
        write_ranking_to_file(d, io.get_histogram_rankings_path() + 'single_predictions/', beam, n_bins, n_hours, confusion_matrix_selection, segment_score, threshold)

    data = []
    for k, v in d.items():
        entry = {'Feature': k, 'Ranking': round(math.log(v, 10), round_rankings)}
        data.append(entry)

    return data

def create_ranking_datatable(data, id):
    rank_column_name = 'SHAP Ranking' if id == 'shap-table' else 'Ranking'
    conditional_data_styling = []
    if id == 'shap-table':
        conditional_data_styling.append({
            'if': {
                'filter_query': '{SHAP Ranking} > 0',
                'column_id': 'SHAP Ranking'
            },
            'backgroundColor': 'green',
            'color': 'white'
        })
        conditional_data_styling.append({
            'if': {
                'filter_query': '{SHAP Ranking} < 0',
                'column_id': 'SHAP Ranking'
            },
            'backgroundColor': 'red',
            'color': 'white'
        })

    return dash_table.DataTable(
        id=id,
        columns=[{"name": i, "id": i} for i in ['Feature', rank_column_name]],
        data=data,
        sort_action='native',
        page_action="none",
        fixed_rows={'headers': True},
        style_header={
            'fontWeight': 'bold'
        },
        style_cell={'textAlign': 'left'},
        style_cell_conditional=[
            {
                'if': {'column_id': rank_column_name},
                'width': 80
            }
        ],
        style_data_conditional=conditional_data_styling,
        style_table={'height': 400,
                     'width': 500}
    )

def get_pred_feature_ranking_fast(col_data, n_bins, selected_prediction):
    bins = np.linspace(col_data.min(), col_data.max(), n_bins + 1)
    pred_value = util.get_prediction_data_value(machine, col_data, scored_train_data, selected_prediction)
    if pred_value is None:
        print("Could not calculate histogram ranking for prediction {}".format(selected_prediction))
        return None
    pred_ind = np.digitize(pred_value, bins)
    if pred_ind == len(bins):
        pred_ind = len(bins) - 1
    bin_height = 0
    for entry in col_data.tolist():
        if bins[pred_ind-1] <= entry <= bins[pred_ind]:
            bin_height += 1
    return bin_height

def get_pred_feature_ranking(df, col, n_bins, selected_prediction):
    col_data = df[col]
    bins = np.linspace(col_data.min(), col_data.max(), n_bins + 1)
    inds = np.digitize(col_data.to_numpy(), bins)
    for i, bin in enumerate(inds):
        if bin == len(bins):
            inds[i] = len(bins) - 1

    counts = pd.Series(inds).value_counts()
    pred_value = util.get_prediction_data_value(machine, col_data, scored_train_data, selected_prediction)
    pred_ind = np.digitize(pred_value, bins)
    if pred_ind == len(bins):
        pred_ind = len(bins) - 1
    if pred_ind not in counts:
        # TODO: mki, choose closest bin that has values
        # could be that the pred_value is the only value in the bin though, but this would be really strange
        # TODO: fix this for mki with a correct aggregator, the current aggregator (mean) produces a value that isn't in a bin with other values most of the time
        ranking = counts.loc[find_nearest(counts.index, pred_ind)]
    else:
        ranking = counts.loc[pred_ind]
    return ranking


def find_nearest(array, value):
    """
    Source: https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
    """
    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or math.fabs(value - array[idx-1]) < math.fabs(value - array[idx])):
        return array[idx-1]
    else:
        return array[idx]


@app.callback(Output('shap-dropdown', 'options'),
              [Input('beam-dropdown', 'value'),
               Input('tp-fp-fn-tn-selection-radio', 'value'),
               Input('anomaly-detector-dropdown', 'value'),
               Input('threshold-slider', 'value')])
def set_shap_options(selected_beam, confusion_matrix_selection, ad, selected_threshold):
    if 'all' in confusion_matrix_selection and confusion_matrix_selection != 'all':
        confusion_matrix_selection = confusion_matrix_selection.split('all ')[1]
    print(confusion_matrix_selection)
    shap_values_files = util.list_shap_files(io, selected_beam)
    # filter on pred_type (TP, FP, TN, FN)
    pred_type_ff = lambda fn: (confusion_matrix_selection in fn)
    shap_values_files = list(filter(pred_type_ff, shap_values_files))
    shap_values_files = list(filter(lambda name: (ad in name), shap_values_files))
    shap_values_files = list(filter(lambda name: (str(selected_threshold) in name), shap_values_files))
    a = [{'label': os.path.basename(i), 'value': os.path.basename(i)} for i in shap_values_files]
    return a


@app.callback(Output('shap-ranking-div', 'children'),
              [Input('tp-fp-fn-tn-dropdown', 'value'),
               Input('shap-dropdown', 'value'),
               Input('anomaly-detector-dropdown', 'value'),
               Input('tp-fp-fn-tn-selection-radio', 'value')])
def set_shap_ranking_body(selected_prediction, shap_file_selection, ad, confusion_matrix_selection):
    if confusion_matrix_selection == 'all':
        return html.Div(["Select \"TP\", \"FP\", \"FN\", \"TN\", \"all TP\", \"all FP\" or \"all FN\""])
    if shap_file_selection == "" or shap_file_selection is None:
        return html.Div(["Select a SHAP file"])
    if selected_prediction == "" or selected_prediction is None:
        return html.Div(["Select a prediction"])
    data = get_shap_ranking_data(selected_prediction, shap_file_selection, ad, confusion_matrix_selection, method='mean')
    if len(data) == 0:
        return html.Div(["Selected SHAP file does not include timestamp data for selected prediction. "
                         "Have you selected the correct predictions file and set the threshold to the value in the filename?"])

    shap_ranking_plot = create_horizontal_bar_plot(data, "SHAP")
    return [html.Div(id='shap-ranking-bar-div', children=[dcc.Graph(id='shap-ranking-bar-plot', figure=shap_ranking_plot)]),
            create_ranking_datatable(data, 'shap-table')]


def get_shap_ranking_data(selected_prediction, shap_file_selection, ad, confusion_matrix_selection, method='mean'):
    if 'all' in confusion_matrix_selection and confusion_matrix_selection != 'all':
        # aggregate SHAP values of all predictions
        shap_df = get_shap_data(shap_file_selection, ad, type='aggregated')
        shap_df = shap_df.to_frame().transpose()
    else:
        # single prediction case
        shap_df = get_shap_data(shap_file_selection, ad, type='single', selected_prediction=selected_prediction)
    if shap_df.empty:
        return []
    d = {}
    for coln in shap_df.columns:
        res = 0
        if method == 'sum':
            res = shap_df[coln].sum()
        if method == 'mean':
            res = shap_df[coln].mean()
        elif method == 'max':
            res = shap_df[coln].max()
        elif method == 'min':
            res = shap_df[coln].min()
        if res != 0:
            d[coln] = res
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}

    data = []
    for k, v in d.items():
        entry = {'Feature': k, 'SHAP Ranking': round(v, round_rankings)}
        data.append(entry)

    return data


def get_shap_data(shap_file_selection, ad, type='single', selected_prediction=None):
    shap_path = io.get_default_results_path() if 'default' in shap_file_selection else io.get_model_and_shap_path(ad)
    shap_df = io.read_csv(shap_file_selection, shap_path)
    if type == 'aggregated':
        return aggregate_SHAP_values(shap_df, method=shap_aggregator_method)
    else:
        start_time, end_time = split_prediction(selected_prediction)
        return shap_df[start_time:end_time]


def aggregate_SHAP_values(shap_df, method='mean'):
    """
    Aggregates the given SHAP values per feature with given method
    """
    agg_df = shap_df.sum()
    if method == 'mean':
        agg_df = shap_df.mean()
    elif method == 'max':
        agg_df = shap_df.max()
    elif method == 'min':
        agg_df = shap_df.min()
    return agg_df


def create_shap_feature_importance_table(df, method='sum'):
    """
    Args:
        df: The dataframe containing all the rows of the SHAP dataframe inside the selected prediction time bounds
        method: The method to aggregate the rows in df if there are multiple (currently should be 1 row)
    """

    return


@app.callback(Output('avd-table', 'figure'),
              [Input('ad-checklist', 'value'),
               Input('gmm-threshold-slider', 'value'),
               Input('iforest-threshold-slider', 'value'),
               Input('hbos-threshold-slider', 'value')])
def create_avd_figure(ad_options, gmm_threshold, iforest_threshold, hbos_threshold):
    if ad_options == [] or gmm_threshold is None or iforest_threshold is None or hbos_threshold is None:
        return {}
    thresholds = {"gmm": gmm_threshold,
                  "iforest": iforest_threshold,
                  "hbos": hbos_threshold}
    for ad, th in thresholds.items():
        thresholds[ad] = 0 if th < 0 else thresholds[ad]
        thresholds[ad] = 1 if th > 1 else thresholds[ad]
    io = IO(cfg, 'grid_search')
    return pg.anomalies_vs_detectors(io, cfg, machine.lower(), logbook_fn, ad_options, thresholds)


@app.callback(Output('feature-selection-checklist', 'options'),
              [Input('beam-dropdown', 'value')])
def set_feature_selection_options(selected_beam):
    print("Setting feature selection dropdown options for beam {}".format(selected_beam))
    a = [{'label': i, 'value': i} for i in feature_cols[selected_beam]]
    return a

@app.callback(Output('feature-selection-checklist', 'value'),
              [Input('beam-dropdown', 'value'),
               Input('feature-selection-groups-checklist', 'value')])
def set_feature_selection_checklist_value(selected_beam, selected_groups):
    if 'all' in selected_groups:
        return feature_cols[selected_beam]
    cols_to_select = []
    for coln in feature_cols[selected_beam]:
        for dataset_type in selected_groups:
            if dataset_type in coln:
                cols_to_select.append(coln)
    return cols_to_select


@app.callback(Output('config-button-message-div', 'children'),
              [Input('update-config-button', 'n_clicks'),
               Input('beam-dropdown', 'value')],
              [State('feature-selection-checklist', 'value')])
def set_config_button_message(n_clicks, selected_beam, selected_colns):
    cols_to_drop = [coln for coln in feature_cols[selected_beam] if coln not in selected_colns]
    success = update_drop_cols_config(io, cols_to_drop)
    if n_clicks == 0:
        return ""
    if success:
        return "The config file {} has been updated!".format(io.GENERATED_CONFIG_FILENAME)
    else:
        return "Error updating the config file!"

def filter_logbook_beam(logbook, beam):
    return logbook[logbook[beam_index] == beam]


def read_logbook(logbook_fn):
    print("Loading logbook data")
    logbook = pd.read_csv(util.CERN_DATA_DIR + '/' + logbook_fn)
    logbook = logbook.set_index(logbook_index)
    logbook.index = pd.to_datetime(logbook.index)
    return logbook

def get_total_nb_anomalies(logbook_fn, beam):
    logbook = read_logbook(logbook_fn)
    logbook = filter_logbook_beam(logbook, beam)
    if cfg['grid_search']['logbook_type'] == 'all':
        # filter logbook on anomalies
        logbook = logbook[logbook['TAG'] == 'anomaly']
    return len(logbook[start:end])


def get_zoom_level(rlds, startd, endd):
    relayoutData = None
    for rld in rlds:
        if rld is not None and AR0 in rld.keys():
            relayoutData = rld
            break

    if relayoutData is not None:  # zoom was changed
        zrange = [relayoutData[AR0], relayoutData[AR1]]
        print("Info: new zoomlevel for graph\n{}".format(json.dumps(zrange, indent=2)))
    else:  # no zoom change
        zrange = [startd, endd]

    return dict(range=zrange)


# Methods for parsing the url state
# maybe this will be included in Dash later: https://github.com/plotly/dash/issues/188
def apply_default_value(params):
    def wrapper(func):
        def apply_value(*args, **kwargs):
            if 'id' in kwargs and kwargs['id'] in params:
                the_value_keys = params[kwargs['id']][0]
                the_values = params[kwargs['id']][1]
                if type(the_values) is list:
                    if len(the_value_keys) == len(the_values):
                        for ix in range(len(the_value_keys)):
                            kwargs[the_value_keys[ix]] = the_values[ix]
                    else:
                        print('We could not properly map keys to values! Please fill in *all* values in component_ids.')
                # kwargs['value'] = params[kwargs['id']][1]
            return func(*args, **kwargs)

        return apply_value

    return wrapper


def parse_state(url):
    parse_result = urlparse(url)
    query_string = parse_qsl(parse_result.query)  # returns list with tuples of params in url
    if query_string:
        encoded_state = query_string[0][1]
        state = dict(json.loads(urlsafe_b64decode(encoded_state)))
    else:
        state = dict()
    return state


@app.callback(Output('page-layout', 'children'),
              inputs=[Input('url', 'href')])
def page_load(href):
    if not href:
        return []
    state = parse_state(href)
    return build_layout(state)


component_ids = {
    'beam-dropdown': ['value'],
    'column-dropdown1': ['value'],
    'column-dropdown2': ['value'],
    'label-checkboxes': ['value'],
    'date-picker': ['start_date', 'end_date'],
    'predictions': ['value'],
    'segment-score-method': ['value'],
    'threshold-slider': ['value']
}


@app.callback(Output('url', 'search'),
              inputs=[Input(id, param[i]) for id, param in component_ids.items() for i in
                      range(len(param))] if has_rendered else [Input('dummy', 'search')])
def update_url_state(*values):
    if not values:
        return dict()
    l = []
    idx = 0
    for k in component_ids.values():
        amount_to_take = len(k)
        l.append(values[idx:idx + amount_to_take])
        idx = idx + amount_to_take
    state = dict(zip(list(component_ids.keys()), list(zip(component_ids.values(), l))))
    print('Info: current state is {}'.format(state))
    return util.generate_params(state)


if __name__ == '__main__':
    app.run_server(debug=debug_app)
