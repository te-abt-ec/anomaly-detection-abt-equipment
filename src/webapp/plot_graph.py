import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from plot.plot_evaluation import plot_anomalies_vs_detectors
import util


def anomalies_vs_detectors(io, cfg, machine, logbook_fn, ad_options, thresholds):
    """
    Args:
        io: io to get the prediction files
        cfg_pl: pipeline config
        cfg_gs: grid search config
        machine: The used machine (mki/lbds)
        logbook_fn: The logbook filename
        ad_options: The anomaly detectors to plot
    Returns:
        A figure compatible with Dash "figure" property
    """
    # Anomaly vs detectors parameters
    detector_params = {}
    for d in util.DETECTORS:
        detector_params[d] = cfg['pipeline']['anomaly_detection'][d]

    fig = plot_anomalies_vs_detectors(io, cfg['pipeline'], machine, logbook_fn, detector_params, ad_options, thresholds)
    print("plotting avd graph")
    graphfig = {
        'data': [fig],
        'layout': {
            'title': 'TP (green) and FN (red) vs detectors | {} Beam {}'.format(machine, cfg['pipeline']['beam'])
        }
    }
    return graphfig
