# CERN Data Explorer webapp manual
Webapp parameters can be set in the config (webapp section)
- input_features_type: The input features datatype
    - raw = Output from Spark script
    - preprocessed = Output from local preprocessing
    - variance_filtered = preprocessed + variance filter

The webapp is split up in sections called cards:

- [Dataset Information Card](#dataset-information)
- [Data Selection Card](#data-selection)
- [Anomaly Detection Card](#anomaly-detection)
- [Data Exploration Card](#data-exploration)
- [Data Analysis Card](#data-analysis)  
- [Evaluation Card](#evaluation)



## Dataset Information
This card displays basic information about the imported dataset:

- Machine: The machine (MKI|LBDS)
- Type: The type of the input data (raw|preprocessed|variance_filtered)
Raw data is currently not working because it contains non-numerical data.
- Rows: The amount of entries in the input data
- Columns: The amount of features in the input data
- Date range: The first and last timestamp of the dataset (These are the IPOC timestamps, because the data is resampled on these timestamps)
- Anomalies: The amount of actual anomalies in the logbook that occurred during above date range for both beams

## Data selection
Here you can select which part of the input data to show. It is also possible to plot the actual labels of the logbook.

- Beam dropdown: Select the beam (Beam 1| Beam 2)
- Column dropdown 1: Select the feature to plot in the first time series graph and first histogram figure
- Column dropdown 2: Select the feature to plot in the second time series graph and second histogram figure
- Label selection: Select which actual labels to show from the logbook (MKI: info,intervention,research,fault,anomaly | LBDS: anomaly)
- Show date range: Select the x-axis range of the time series data and date period of the global histogram

## Anomaly detection
All options related to the detection and prediction are listed in this card. Modifying an attribute in this card can currently sometimes take a while.

- Anomaly detector dropdown: Select the detector you used in the pipeline/grid search. This filters all possible remaining dropdown selections on this anomaly detector (in filenames)
- Prediction file dropdown: Every time the *pipeline* is launched, a unique predictions file is generated and saved linked to your used anomaly detector parameters specified in the config.
The file with the parameters you specified in the config under anomaly detection should appear here (unique id in the filename)
Every time a *grid search* is launched, the best performing prediction file (based on the given metric, default AUC) is listed here (best in filename).
- Score method dropdown: Select the score method of segments (predictions). Default is maximum
- Threshold slider: Set the threshold of the anomaly scores (anomaly score > threshold is considered anomalous)

## Data Exploration
This card shows the segmentation plot, the 2 time series plots and a marker selection. When not many data points are plotted, it can be useful to change to 'lines'.

- Segmentation plot: A line is plotted for each segment which starts at the minimum timestamp of the segment and ends at the maximum timestamp.
The segments are color coded and you can turn them on/off by clicking on the legend. Zooming in is also possible by selecting the desired period.
- Time series plot 1 and 2: Shows the features selected in the column dropdowns from the [data selection card](#data-selection)
The actual labels are shown as color coded dots.
- Marker type: Select the desired marker type.

## Data Analysis
When working with a lot of features, it can be helpful to use statistics and pinpoint certain features based on a ranking of an evaluation metric.

### Histograms & rankings tab
In this tab, a prediction can be selected (TP|FP|FN|TN). Two histograms are then plotted with the prediction as a red dot.


#### Global histogram
Plot a histogram of the selected data in the global date range and first feature from the [data selection card](#data-selection).
The prediction feature value is shown in red.
- n_bins: Choose the number of bins of this histogram

#### Local histogram
Plot a histogram of a time slice of the data around the selected prediction with the selected time margin.  
For example: prediction time stamp = 2017-05-03 16:03:54 with a time margin of [-12h, 12h] gives a data range selection from
2017-05-03 04:03:54 to 2017-05-04 04:03:54.  
The data that is chosen is again the first feature from the [data selection card](#data-selection).
The prediction feature value is shown in red.
- nb_bins: Choose the number of bins of this histogram
- nb_hours: Choose the number of hours to select data around the prediction timestamp

#### Histogram Ranking
Generate a sorted ranked list of all features given the number of bins and time margin.
Like in the [local histogram](#local-histogram), a histogram is constructed, but this time for each feature.
The ranking is calculated as follows:
1) Select the height of the bin where the prediction timestamp occurs (bin under the red dot)
2) Divide by the amount of timestamps/rows occurring in the data from which the histogram is constructed (to make predictions comparable)
3) Take the inverse (1/x) such that the bigger the number, the more outlier.
To see the histogram, fill in the feature in one of the column dropdowns from [data selection card](#data-selection)
   
- nb_bins: Choose the number of bins of this histogram
- nb_hours: Choose the number of hours to select data around the prediction timestamp


#### SHAP Ranking
To show the SHAP rankings, a SHAP file has to be generated first: 
Consult the [shap_values.py section](src/scripts/README.md) in the scripts README. 
Set the analysis part of the configuration file first.

```bash
python src/webapp/shap_values.py -c [config.yaml]
```
After generation, the SHAP file can be selected in the dropdown menu and the ranking will be listed.
Click [here](https://github.com/slundberg/shap) a detailed guide about SHAP values.

### Anomalies vs detectors tab
This tab shows the detected (TP) in green and undetected (FN) anomalies in red for each detector.
Anomaly score thresholds can be modified.

### Feature Selection tab
Specific feature sets can be selected here to include in the analysis.
When all columns that have to be kept in the dataset are selected, click the "update config" button to
generate a new config (in [configs](src/config_files)) named ```[used-config]_analysis.yaml``` with the unlisted
columns in the local_preprocessing<drop-columns setting. You can now use this config to run the pipeline/grid search with
only the selected features and restart the webapp with the ```[used-config]_analysis.yaml```.

For feature selection using the SHAP or histogram ranking heuristics, the ```feature_selection.py``` script must be runned first
as explained in the feature selection section of the [scripts README](src/scripts/README.md).

## Evaluation
All evaluation metrics of the selected predictions file from [Anomaly Detection Card](#anomaly-detection) 
are listed here with the corresponding PR-curve.  
A table with all false positives and false negatives is also shown.




