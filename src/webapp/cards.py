import dash_core_components as dcc
import dash_html_components as html

import numpy as np

import webapp.components as drc
import util
from datetime import datetime as dt


# tab styles
def get_tabs_styles():
    tabs_styles = {
        'height': '44px'
    }
    return tabs_styles


def get_tab_style():
    tab_style = {
        'borderBottom': '1px solid #d6d6d6',
        'padding': '6px',
        'fontWeight': 'bold'
    }
    return tab_style


def get_tab_selected_style():
    tab_selected_style = {
        'borderTop': '1px solid #d6d6d6',
        'borderBottom': '1px solid #d6d6d6',
        'backgroundColor': '#084c94',
        'color': 'white',
        'padding': '6px'
    }
    return tab_selected_style


# first column cards
def information_card(args):
    blueprint = '''
            ##### **Dataset Information**  
            Machine: {}  
            Type: {}  
            Rows: {}  
            Columns: {}  
            Date range: {} to {}  
            Anomalies: beam 1: {} | beam 2: {}
            '''
    input_args = [args[i] for i in range(blueprint.count('{}'))]
    return drc.Card([
        html.Div([
            dcc.Markdown(blueprint.format(*tuple(input_args)))
        ])
    ])


def data_selection_card(apply_default_value, params, args):
    [default_data_1, default_data_2, default_labels, default_beam, machine, start, end] = args
    return drc.Card([
        dcc.Markdown('''##### **Data selection**'''),
        apply_default_value(params)(drc.NamedDropdown)(
            name='Show beam',
            id='beam-dropdown',
            options=[{'label': 'Beam {} ({})'.format(i, util.temp_beam_to_num(i, machine)), 'value': i} for i in util.BEAMS],
            value=default_beam,
            multi=False,
            clearable=False
        ),
        apply_default_value(params)(drc.NamedDropdown)(
            name='Show datasets (view 1)',
            id='column-dropdown1',
            value=default_data_1,
            multi=True
        ),
        apply_default_value(params)(drc.NamedDropdown)(
            name='Show datasets (view 2)',
            id='column-dropdown2',
            value=default_data_2,
            multi=True
        ),
        # TODO: add intervention, fault, info, research for mki
        apply_default_value(params)(drc.NamedDropdown)(
            name='Show labels',
            id='label-checkboxes',
            value=default_labels,
            multi=True
        ),

        apply_default_value(params)(drc.NamedDatePicker)(
            name='Show date range',
            id='date-picker',
            min_date_allowed=dt.strptime(start, '%Y-%m-%d %H:%M:%S'),
            max_date_allowed=dt.strptime(end, '%Y-%m-%d %H:%M:%S'),
            start_date=dt.strptime(start, '%Y-%m-%d %H:%M:%S'),
            end_date=dt.strptime(end, '%Y-%m-%d %H:%M:%S'),
            initial_visible_month=dt.strptime(start, '%Y-%m-%d %H:%M:%S'),
        )
    ])


def anomaly_detection_card(apply_default_value, params, args):
    [default_prediction_file, default_score_method, min_threshold, max_threshold, threshold_step_size, default_threshold, default_anomaly_detector] = args
    return drc.Card([
        dcc.Markdown('''##### **Anomaly Detection**'''),
        apply_default_value(params)(drc.NamedDropdown)(
            name='Select anomaly detector',
            id='anomaly-detector-dropdown',
            options=[{'label': i, 'value': i} for i in util.DETECTORS],
            value=default_anomaly_detector,
            clearable=False
        ),
        apply_default_value(params)(drc.NamedDropdown)(
            name='Select CSV file with prediction',
            id='predictions',
            value=default_prediction_file,
            clearable=False
        ),
        apply_default_value(params)(drc.NamedDropdown)(
            name='Set score method of segments',
            id='segment-score-method',
            options=[
                {'label': 'maximum', 'value': 'max'},
                {'label': 'top k', 'value': 'top_k'},
                {'label': 'top %', 'value': 'top_percentage'}
            ],
            value=default_score_method,
            clearable=False,
            searchable=False,
        ),
        apply_default_value(params)(drc.NamedSlider)(
            name='Set threshold',
            id='threshold-slider',
            min=min_threshold,
            max=max_threshold,
            step=threshold_step_size,
            marks={i: {'label': '{}%'.format(int(i * 100))} for i in
                   np.linspace(min_threshold, max_threshold, int((max_threshold - min_threshold) * 100 + 1))},
            value=default_threshold
        )
    ])


# middle column cards
def data_exploration_card():
    return drc.Card([
        dcc.Markdown('''##### **Data Exploration**'''),
        html.Div(id='segments-div', children=[dcc.Graph(id='segments-graph')]),
        html.Div(id='data1-div', children=[dcc.Graph(id='data1-graph')]),
        html.Div(id='data2-div', children=[dcc.Graph(id='data2-graph')]),
        dcc.Markdown('''Marker type:'''),
        dcc.RadioItems(
            id='lines-markers-radio',
            options=[{'label': k, 'value': k} for k in ['markers', 'lines', 'markers+lines']],
            value='markers'
        )
    ])


def data_analysis_card():
    return drc.Card([
        dcc.Markdown('''##### **Data Analysis**'''),
        dcc.Tabs(id="data-analysis-tabs", value='data-analysis-histogram-tab', children=[
            dcc.Tab(label='Histograms & Rankings', value='data-analysis-histogram-tab', style=get_tab_style(), selected_style=get_tab_selected_style()),
            dcc.Tab(label='Anomalies vs detectors', value='data-analysis-avd-tab', style=get_tab_style(), selected_style=get_tab_selected_style()),
            dcc.Tab(label='Feature Selection', value='data-analysis-feature-selection', style=get_tab_style(), selected_style=get_tab_selected_style()),
        ], style=get_tabs_styles()),
        html.Div(id='data-analysis-tab-content'),
    ])


def histogram_card(default_prediction):
    return get_confusion_matrix_selection(default_prediction) + [
        html.Div(children=[
            html.Div([
                drc.Card([
                    dcc.Tabs(id="ranking-tabs", value='ranking-tab', children=[
                        dcc.Tab(label='Histogram Ranking', value='ranking-tab', style=get_tab_style(), selected_style=get_tab_selected_style()),
                        dcc.Tab(label='SHAP Ranking', value='shap-ranking-tab', style=get_tab_style(), selected_style=get_tab_selected_style()),
                    ], style=get_tabs_styles()),
                    html.Div(id='ranking-tab-content'),
                ])
            ], style={'overflow': 'hidden',
                      'width': '100%'}),
            html.Div(children=[
                drc.Card([
                    dcc.Tabs(id="histogram-tabs", value='histogram-tab1', children=[
                        dcc.Tab(label='Global histogram', value='histogram-tab1', style=get_tab_style(),
                                selected_style=get_tab_selected_style()),
                        dcc.Tab(label='Local histogram', value='histogram-tab2', style=get_tab_style(),
                                selected_style=get_tab_selected_style()),
                    ], style=get_tabs_styles()),
                    html.Div(id='histogram-tab-content'),
                ])
            ], style={'float': 'left',
                      'min-width': '550px',
                      'width': '100%'}),
        ], style={'overflow': 'hidden'})
        ]


def get_global_histogram_body(default_nb_bins, start, end):
    return [
        html.Div(["# bins: ",
                  dcc.Input(id='nb-bins-histogram', value=default_nb_bins, type='number')]),
        html.Div(id='histogram1-div', children=[dcc.Graph(id='histogram1-graph')]),
        html.Div(id='histogram1-2-div', children=[dcc.Graph(id='histogram1-2-graph')]),
        html.Div(children=["Feature data from {} to {}".format(start, end)], style={'text-align': 'center'})
    ]


def get_local_histogram_body(default_nb_bins, default_nb_hours):
    return [
        html.Div(["# bins: ",
                  dcc.Input(id='nb-bins-histogram', value=default_nb_bins, type='number')]),
        html.Div(["Time margin around prediction (hours): ",
                  dcc.Input(id='nb-hours-histogram', value=default_nb_hours, type='number')]),
        html.Div(id='histogram2-div', children=[dcc.Graph(id='histogram2-graph')]),
        html.Div(id='histogram2-2-div', children=[dcc.Graph(id='histogram2-2-graph')]),
    ]


def get_ranking_body(default_nb_bins, default_nb_hours, default_min_freq):
    return [
        html.Div(["# bins: ",
                  dcc.Input(id='nb-bins-ranking', value=default_nb_bins, type='number')]),
        html.Div(["Time margin around prediction (hours): ",
                  dcc.Input(id='nb-hours-ranking', value=default_nb_hours, type='number')]),
        html.Div(["Minimal frequency (% of #features): ",
                  dcc.Input(id='min-freq', value=default_min_freq, type='number')]),
        html.Div(id='ranking-div'),
        #html.Div(["The ranking is the height of the histogram bin where the prediction is " +
        #          "observed, divided by the number of timestamp entries in the used data slice (around " +
        #          "the prediction) to construct this histogram, inversed (1/x) such that a higher ranking is more outlier"])
    ]

def get_shap_ranking_body():
    return [
        drc.NamedDropdown(name="Select shap file",
                          id='shap-dropdown',
                          value="",
                          searchable=True
                          ),
        html.Div(id='shap-ranking-div')
    ]

def get_avd_body(default_threshold_slider_values):
    """
    Args:
        default_threshold_slider_values: dict with the default threshold slider values
    """
    return [
        dcc.Checklist(
            id="ad-checklist",
            options=[{'label': i, 'value': i} for i in util.DETECTORS],
            value=util.DETECTORS,
            labelStyle={'display': 'inline-block',
                        'margin-right': '15px'},
        )] + get_threshold_sliders(default_threshold_slider_values) + [
        html.Div(id='avd-div', children=[dcc.Graph(id='avd-table')])
    ]


def get_feature_selection_body(default_feature_selection, default_feature_selection_groups, machine):
    return [
        html.Div(["Select features to include"]),
        dcc.Checklist(
            id="feature-selection-groups-checklist",
            options=[{'label': i, 'value': i} for i in ['all'] + util.DATASET_TYPES[machine]],
            value=default_feature_selection_groups,
            labelStyle={'display': 'inline-block',
                        'margin-right': '15px'},
        ),
        html.Button('Update config', id='update-config-button', n_clicks=0),
        html.Div(id='config-button-message-div', children=''),
        dcc.Checklist(
            id="feature-selection-checklist",
            value=default_feature_selection,
        )]

def get_threshold_sliders(default_threshold_slider_values):
    sliders = []
    for d in util.DETECTORS:
        sliders.append(
            html.Div(["{} threshold: ".format(d),
                      dcc.Input(id='{}-threshold-slider'.format(d), value=default_threshold_slider_values[d], type='number')])
        )
    return sliders

def get_confusion_matrix_selection(default_prediction):
    return [
        dcc.RadioItems(
            id='tp-fp-fn-tn-selection-radio',
            options=[{'label': k, 'value': k} for k in ['TP', 'FP', 'FN', 'TN', 'all', 'all TP', 'all FP', 'all FN', 'all TN']],
            value='TP',
            labelStyle={'display': 'inline-block',
                        'margin-right': '15px'},
        ),
        drc.NamedDropdown(name="Select prediction",
                          id='tp-fp-fn-tn-dropdown',
                          value=default_prediction,
                          searchable=True,
                          clearable=True
                          )
    ]


# last column cards
def evaluation_card():
    return drc.Card([
        html.Div(id='evaluation-card'),
        html.Div(id='pr-curve-div', children=[dcc.Graph(id='pr-curve-graph')])
    ])


def fp_table_card():
    return drc.Card([html.Div(id='fp-table')])


def fn_table_card():
    return drc.Card([html.Div(id='fn-table')])