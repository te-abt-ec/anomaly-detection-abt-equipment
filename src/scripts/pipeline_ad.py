#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import numpy as np
from pipeline.pipeline import pipeline
from preprocessing import builder
from util import get_training_data_from_file, get_config, CERN_DATA_DIR, PIPELINE_DATA_DIR
from preprocessing.preprocessor import *
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex

np.random.seed(456)


def run_pipeline(cfg):
    try:
        pl = cfg['pipeline']
        start_time, end_time = pl['time_interval']
        preprocessing = pl['preprocessing']
        drop_low_variance_cols = preprocessing['drop_low_variance_columns']
        ad = pl["anomaly_detection"]
        detector = ad['anomaly_detector']
        output_filename = pl['output_filename']  # was fno TODO
        beam = pl['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    print("PIPELINE RUN FOR: {}".format(detector))

    # IO for all read/write operations
    io = IO(cfg, 'pipeline')

    # load the dataset, state:mode dataset (optional) and the logbook dataset (labels)
    df, state_mode, labels = get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg, use_case='pipeline')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting data, exiting...")
        exit()
    if pl['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop and keep columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])
    if cfg['local_preprocessing']['keep_columns']:
        df = keep_columns_list(df, cfg['local_preprocessing']['keep_columns_list'])
        df = keep_columns_regex(df, cfg['local_preprocessing']['keep_columns_regex'])

    selected_features = list(df.columns)

    # dump variance handled features for inspection
    print("Writing variance filtered features to {}".format(io.pipeline_data_path))
    var_fn = preprocessing["variance_filtered_filename"]
    var_fn = var_fn.format(io.beam) if "{}" in var_fn else var_fn
    io.write_to_csv(df, var_fn, io.pipeline_data_path)

    try:
        ad_config = ad[detector]
        scale_data = ad_config["scale_data"]
        segment_score = ad_config["segment_score"]
        detector_params = ad_config["params"]
        scoring = cfg['grid_search']['anomaly_detection'][detector]['scoring']
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    # SET ANOMALY DETECTOR AND PARAMETERS
    print("Launching pipeline with data from ({} - {}), this might take a while..".format(start_time, end_time))
    _ = pipeline(
        beam=beam,
        scale_data=scale_data,
        anomaly_detector=detector,
        detector_params=detector_params,
        labels=labels,
        segment_score=segment_score,
        x_train=df[selected_features],
        scoring=scoring,
        state_mode=state_mode,
        io=io,
        write_output=True,
        config=cfg,
        task='pipeline')
    print("Done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    run_pipeline(cfg)
