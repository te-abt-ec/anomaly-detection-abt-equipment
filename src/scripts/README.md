# Scripts
This file contains a guide to each script in this directory.

- [shap_values.py](#shap_valuespy)
- [feature_selection.py](#feature_selectionpy)

### shap_values.py
TODO

### feature_selection.py

#### SHAP feature selection
1) To perform the SHAP feature selection, you must first have generated the necessary SHAP files 
   for the wanted prediction type as described in [shap_values.py](#shap_valuespy)
2) After having the necessary SHAP files in ```results/shap_values_and_model_dump/[machine]/[detector]/[year]/[months]/```,
   this script can be used. The following parameters can be tuned in the [configuration](src/config_files/) file:
    - **generated_config_filename**: The name of the output config file containing the selected features.
    Make sure that ```keep_columns``` is set to True.
    - **type**: The type of feature selection. Should be set to "SHAP" in this case.
    - **prediction_types**: The prediction types that are considered and plotted. When selecting features, priority is given according to
      the prediction_types order. Only possible if the corresponding SHAP file of this prediction type is available.
    - **method**: The feature selection method. For every method, a "total selection feature set" is defined as the set of features from which can be selected. 
      This set is called **FS**. The exact number of features that are selected is determined by ```nb_features_to_select```.
      Features are always selected from highest to lowest (absolute) SHAP value. 
      Current supported methods are:
        1) "pos_tpfn-neg_fptn": Dependant on the first prediction_types, **FS** is the set of features with positive SHAP values (TP and FN) or with negative SHAP values (FP and TN).
        2) "pos_tpfn_delete_pos_fptn": FS is the set of features with positive SHAP values of the first prediction type in prediction_types.
    - **nb_features_to_select**:
        - **type**: The type that is used in determining the nb_features in ```method```.
            1) "percentage": nb_features is a percentage of **FS** (dependant on the ```method```)
            2) "absolute": nb_features is an absolute number of **FS** (dependant on the ```method```)
       - **value**: The value used to determine nb_features. A float between [0, 1] if ```type="percentage"``` and an integer > 0 if ```type="absolute"```
   - **add_other_magnets**: Whether to keep the same features for different magnets in groups during feature selection
   - **plot_results**: Visualises the selected features.
    
3) After running ```python src/scripts/feature_selection.py -c [config file].yaml```, the pipeline with SHAP feature selection can be run
with ```python src/scripts/pipeline_ad.py -c [config file]_analysis.yaml```

#### Histogram feature selection
TODO