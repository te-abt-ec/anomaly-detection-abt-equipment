#!/usr/bin/env python3
import os
import argparse

import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from preprocessing.prep_factory import PreprocessorFactory
from util import get_config


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Python script to run the preprocessing")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    try:
        pf = PreprocessorFactory()
        prep_type = cfg["machine"].lower()
        prep = pf.create(prep_type, cfg)
        prep.preprocess()
    except KeyError as e:
        print("{} not found in config, exiting ...".format(e))
        exit()
