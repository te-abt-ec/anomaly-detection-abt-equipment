#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import numpy as np
from datetime import datetime, timedelta
from pipeline.pipeline import pipeline
from preprocessing import builder
from util import get_training_data_from_file, get_config, CERN_DATA_DIR, PIPELINE_DATA_DIR
from preprocessing.preprocessor import *
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex

np.random.seed(456)


def run_live_evaluation(cfg):
    try:
        le = cfg["live_evaluation"]
        le_type = le["type"]
        start_time_predict, end_time_predict = le["time_interval_to_predict"]
        start_time_manual, end_time_manual = le["time_interval_to_train_manual"]
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    start_time_predict_dt = datetime.strptime(start_time_predict, '%Y-%m-%d %H:%M:%S.%f')
    end_time_predict_dt = datetime.strptime(end_time_predict, '%Y-%m-%d %H:%M:%S.%f')
    if le_type == 'whole_year_before':
        year = start_time_predict_dt.year
        start_time = '{}-01-01 00:00:00.000'.format(year - 1)
        end_time = '{}-01-01 00:00:00.000'.format(year)
    elif le_type == 'same_period_year_before':
        start_time = (start_time_predict_dt - timedelta(days=365)).strftime('%Y-%m-%d %H:%M:%S.%f')
        end_time = (end_time_predict_dt - timedelta(days=365)).strftime('%Y-%m-%d %H:%M:%S.%f')
    elif le_type == 'manual_date_range':
        start_time = start_time_manual
        end_time = end_time_manual

    print("||LIVE EVALUATION: Starting to construct model from {}...".format(le_type))

    # Train model on data
    print("\n||LIVE EVALUATION: training model from {} to {}".format(start_time, end_time))
    pretrained_detector = run_pipeline(cfg, start_time, end_time, type='train_detector')

    # Predict on unseen data
    print("\n||LIVE EVALUATION: predicting from {} to {}".format(start_time_predict, end_time_predict))
    run_pipeline(cfg, start_time_predict, end_time_predict, type='predict', pretrained_detector=pretrained_detector)

    print("||LIVE EVAULATION DONE")



def run_pipeline(cfg, start_time, end_time, type=None, pretrained_detector=None):
    try:
        pl = cfg['pipeline']
        preprocessing = pl['preprocessing']
        drop_low_variance_cols = preprocessing['drop_low_variance_columns']
        ad = pl["anomaly_detection"]
        detector = ad['anomaly_detector']
        output_filename = pl['output_filename']  # was fno TODO
        beam = pl['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    print("PIPELINE RUN FOR: {}".format(detector))

    # load the dataset, state:mode dataset (optional) and the logbook dataset (labels)
    df, state_mode, labels = get_training_data_from_file(drop_low_variance_cols, start_time, end_time, cfg, 'pipeline')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting data, exiting...")
        exit()
    if pl['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])

    selected_features = list(df.columns)

    # dump variance handled features for inspection
    # TODO: add caching of this file to grid_search to avoid doing the same filtering for each pipeline run
    #       (gridsearch is always over the same timeframe, so this is possible)
    fp = PIPELINE_DATA_DIR + '/' + cfg["machine"].lower() + '/' + start_time.split('-')[0] + '/' + preprocessing["variance_filtered_filename"]
    df.to_csv(fp)

    try:
        ad_config = ad[detector]
        scale_data = ad_config["scale_data"]
        segment_score = ad_config["segment_score"]
        detector_params = ad_config["params"]
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()
    print("============= {} ===================".format(type))
    if type == 'train_detector':
        # SET ANOMALY DETECTOR AND PARAMETERS
        print("Launching pipeline with data from ({} - {}), this might take a while..".format(start_time, end_time))
        pretrained_model = pipeline(
            beam=beam,
            scale_data=scale_data,
            anomaly_detector=detector,
            detector_params=detector_params,
            labels=labels,
            segment_score=segment_score,
            x_train=df[selected_features],
            state_mode=state_mode,
            write_output=False,
            config=cfg,
            task='live_evaluation')
        print("Done")
        return pretrained_model
    elif type == 'predict':
        # SET ANOMALY DETECTOR AND PARAMETERS
        print("Launching pipeline with data from ({} - {}), this might take a while..".format(start_time, end_time))
        _ = pipeline(
            beam=beam,
            scale_data=scale_data,
            anomaly_detector=detector,
            detector_params=detector_params,
            labels=labels,
            segment_score=segment_score,
            x_train=df[selected_features],
            state_mode=state_mode,
            write_output=True,
            config=cfg,
            task='pipeline',
            live_evaluation=pretrained_detector)
        print("Done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run the live evaluation pipeline")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run used in the live evaluation')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    run_live_evaluation(cfg)
