#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, DATASET_TYPES
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO
from scripts.experiments.histogram_feature_selection import run_experiment

from itertools import chain, combinations
from datetime import datetime
import util

np.random.seed(456)

def run_gs(cfg, feature_set_name, features_to_keep):
    try:
        gs = cfg['grid_search']
        start_time, end_time = gs['time_interval']
        ad = gs['anomaly_detection']
        detector = ad['anomaly_detector']
        preprocess = gs['preprocessing']
        drop_low_variance_cols = preprocess['drop_low_variance_columns']
        max_runtime = gs['max_runtime']
        output_filename = gs['output_filename']
        beam = gs['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    # IO for all read/write operations
    io = IO(cfg, 'grid_search')

    print("GRID SEARCH FOR: {}".format(detector))

    df, state_mode, labels = get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg, use_case='grid_search')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting LBDS data, exiting...")
        exit()
    if gs['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])

    feature_selection = [('all', list(df.columns))]

    try:
        gs_config = gs["anomaly_detection"][detector]
        scale_data = gs_config['scale_data']
        segment_score = gs_config['segment_score']
        scoring = gs_config['scoring']
        fs = gs_config['feature_selection']
        feature_selection = [(fs, list(df.columns))]
        detector_params = gs_config['params']
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    feature_selection = [(feature_set_name, features_to_keep)]
    print("grid search has {} ({}) features to keep".format(feature_set_name, len(features_to_keep)))

    print("Launching grid search with data from ({} - {}), this might take a while..".format(start_time, end_time))

    # PERFORM GRID SEARCH
    results = grid_search(
        feature_selection=feature_selection,
        beam=beam,
        scale_data=scale_data,
        anomaly_detector=detector,
        detector_params=detector_params,
        labels=labels,
        segment_score=segment_score,
        x_train=df,
        state_mode=state_mode,
        scoring=scoring,
        max_runtime=max_runtime,
        filename=output_filename,
        config=cfg,
        short=short,
        io=io
    )

    #for res in results:
        #if not math.isnan(res['rank']):
            #path = 'grid_search_' + beam_to_str(beam) + '_' + detector + '_' + str(int(res['rank'])) + '_' + '-best.csv'
            #res['truth_and_pred_df'].to_csv()
        #else:
        #    print("Note: One of the resulting grid search AUC values has NaN as output, consider using a bigger time interval to include anomalies")

    if len(results):
        features_res = [res['feature'] for res in results]
        auc_res = [res['auc'] for res in results]
        rank_res = [res['rank'] for res in results]
        scale = [res['scale'] for res in results]
        time_res = [res['time'] for res in results]
        TP_res = [res['confusion_matrix_stats']['TP'] for res in results]
        FP_res = [res['confusion_matrix_stats']['FP'] for res in results]
        FN_res = [res['confusion_matrix_stats']['FN'] for res in results]
        TN_res = [res['confusion_matrix_stats']['TN'] for res in results]
        id_res = [res['id'] for res in results]

        parameter_results = {}
        for param in detector_params:
            parameter_results[param] = [res['params'][param] for res in results]

        data = {'auc': auc_res, 'rank': rank_res, 'TP': TP_res, 'FN': FN_res, 'FP': FP_res, 'TN': TN_res, 'features': features_res}
        for param, param_res in parameter_results.items():
            data[param] = param_res
        data['scale'] = scale
        data['time'] = time_res
        data['id'] = id_res

        statistics = pd.DataFrame(data)
        statistics.to_csv('grid_search_statistics_' + beam_to_str(beam) + '_' + detector + output_filename + '_' + feature_set_name + '.csv')
        return statistics

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds_variance_gridsearch.yaml',
                        help='config file for pipeline run')
    parser.add_argument('--short', dest='short', action='store_true',
                        help='True if only 1 configuration has to run (for CI code check)')
    parser.set_defaults(short=False)

    args = parser.parse_args()
    short = args.short

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    h_fs_threshold_values = [0]
    feature_set_names = ["type={} | value={}".format("percentage", value) for value in h_fs_threshold_values]

    exp_stats = {}
    for i, value in enumerate(h_fs_threshold_values):
        selected_features = run_experiment(cfg, nb_features_to_select={'type': 'percentage', 'value': value})

        print("Starting gs for value={} | selected_features={}".format(value, len(selected_features)))
        stats = run_gs(cfg, feature_set_names[i], selected_features)
        exp_stats[feature_set_names[i]] = (stats, len(selected_features))

    # Calculate means of df
    exp_means = {}
    for k, v in exp_stats.items():
        stats, nb_features = v
        exp_means[k] = stats.mean(axis=0, skipna=True).append(pd.Series([nb_features], index=["nb_cols"]))

    # print and save the result
    series_results = []
    print("Experiment result:")
    for i, (k, v) in enumerate(exp_means.items()):
        print("{} | {}".format(i, k))
        print(v)
        series_results.append(v)
    
    experiment_result = pd.concat(series_results, axis=1)
    experiment_result.columns = [str(i) for i in feature_set_names]
    experiment_result = experiment_result.transpose()
    experiment_result.to_csv('results/experiments/histogram_ranking/histogram_feature_selection_experiment_result_' + str(datetime.now()) + '.csv')



