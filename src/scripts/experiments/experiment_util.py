
import numpy as np
import pandas as pd

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import get_training_data_from_file
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

import random


def set_seed(seed):
    if seed is None:
        np.random.seed(456)
        random.seed(456)
    else:
        np.random.seed(seed)
        random.seed(seed)


def write_exp_results(io, filename, series_results, feature_set_names, configurations, type=None):
    """
    Args:
        type: The type of experiment (shap, histogram, random)
    """
    experiment_result = pd.concat(series_results, axis=1)
    experiment_result.columns = [str(i) for i in feature_set_names]
    experiment_result = experiment_result.transpose()
    experiment_result["max_nb_features"] = [i["max_nb_features"] for i in configurations]
    if type == "shap":
        io.write_to_csv(experiment_result, filename, io.fs_shap_path)
    elif type == "histogram":
        io.write_to_csv(experiment_result, filename, io.fs_histogram_path)
    elif type == "random":
        io.write_to_csv(experiment_result, filename, io.fs_histogram_path)
        return experiment_result


def run_gs(cfg, feature_set_name=None, features_to_keep=None):
    """
    Modified version of the grid search script code for the feature selection experiments
    Args:
        cfg: The passed configuration file
        feature_set_name: (string) The name of the feature set used in this grid search that will be displayed in the result
        features_to_keep: (list of strings) The feature set
    """
    try:
        gs = cfg['grid_search']
        start_time, end_time = gs['time_interval']
        ad = gs['anomaly_detection']
        detector = ad['anomaly_detector']
        preprocess = gs['preprocessing']
        drop_low_variance_cols = preprocess['drop_low_variance_columns']
        max_runtime = gs['max_runtime']
        output_filename = gs['output_filename']
        beam = gs['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    # IO for all read/write operations
    io = IO(cfg, 'grid_search')

    print("GRID SEARCH FOR: {}".format(detector))

    df, state_mode, labels = get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg,
                                                         use_case='grid_search')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting LBDS data, exiting...")
        exit()
    if gs['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])

    if features_to_keep is None:
        feature_selection = [('all', list(df.columns))]

    try:
        gs_config = gs["anomaly_detection"][detector]
        scale_data = gs_config['scale_data']
        segment_score = gs_config['segment_score']
        scoring = gs_config['scoring']
        fs = gs_config['feature_selection']
        feature_selection = [(fs, list(df.columns))]
        detector_params = gs_config['params']
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    feature_selection = [(feature_set_name, features_to_keep)]
    print("grid search has {} ({}) features to keep".format(feature_set_name, len(features_to_keep)))

    print("Launching grid search with data from ({} - {}), this might take a while..".format(start_time, end_time))
    short = False

    # PERFORM GRID SEARCH
    results = grid_search(
        feature_selection=feature_selection,
        beam=beam,
        scale_data=scale_data,
        anomaly_detector=detector,
        detector_params=detector_params,
        labels=labels,
        segment_score=segment_score,
        x_train=df,
        state_mode=state_mode,
        scoring=scoring,
        max_runtime=max_runtime,
        filename=output_filename,
        config=cfg,
        short=short,
        io=io
    )

    results_sorted = sorted(results, key=lambda res: res[scoring], reverse=True)
    results_sorted_rank = sorted(results, key=lambda  res: res['rank'])

    if len(results_sorted):
        best = results_sorted[0]
        best_auc = best
        best_rank = results_sorted_rank[0]

    if len(results):
        features_res = [res['feature'] for res in results]
        auc_res = [res['auc'] for res in results]
        rank_res = [res['rank'] for res in results]
        scale = [res['scale'] for res in results]
        time_res = [res['time'] for res in results]
        TP_res = [res['confusion_matrix_stats']['TP'] for res in results]
        FP_res = [res['confusion_matrix_stats']['FP'] for res in results]
        FN_res = [res['confusion_matrix_stats']['FN'] for res in results]
        TN_res = [res['confusion_matrix_stats']['TN'] for res in results]
        id_res = [res['id'] for res in results]

        parameter_results = {}
        for param in detector_params:
            parameter_results[param] = [res['params'][param] for res in results]

        data = {'auc': auc_res, 'rank': rank_res, 'TP': TP_res, 'FN': FN_res, 'FP': FP_res, 'TN': TN_res,
                'features': features_res}
        for param, param_res in parameter_results.items():
            data[param] = param_res
        data['scale'] = scale
        data['time'] = time_res
        data['id'] = id_res

        statistics = pd.DataFrame(data)
        return statistics, best_auc, best_rank


def fs_stats_to_means(exp_stats, verbose=False):
    # Calculate means of df
    exp_means = {}
    for name, v in exp_stats.items():
        stats, nb_features = v
        exp_means[name] = stats.mean(axis=0, skipna=True).append(pd.Series([nb_features], index=["nb_cols"]))

    # print and save the result
    series_results = []
    print("Experiment result:")
    for i, (k, v) in enumerate(exp_means.items()):
        if verbose:
            print("{} | {}".format(i, k))
            print(v)
        series_results.append(v)
    return series_results

def fs_construct_bests(exp_stats_bests, configurations, feature_set_names):
    bests_nb_features = []
    bests = []
    for i, (name, v) in enumerate(exp_stats_bests.items()):
        best, nb_features = v
        bests_nb_features.append(nb_features)
        bests.append(best)

    bests_auc_res = [res['auc'] for res in bests]
    bests_rank_res = [res['rank'] for res in bests]
    bests_TP_res = [res['confusion_matrix_stats']['TP'] for res in bests]
    bests_FP_res = [res['confusion_matrix_stats']['FP'] for res in bests]
    bests_FN_res = [res['confusion_matrix_stats']['FN'] for res in bests]
    bests_TN_res = [res['confusion_matrix_stats']['TN'] for res in bests]

    bests_parameter_results = {}
    for param in bests[0]['params'].keys():
        bests_parameter_results[param] = [res['params'][param] for res in bests]

    data = {'name': feature_set_names,
            'max_nb_features': [i["max_nb_features"] for i in configurations],
            'nb_cols': bests_nb_features,
            'auc': bests_auc_res,
            'rank': bests_rank_res,
            'TP': bests_TP_res, 'FN': bests_FN_res, 'FP': bests_FP_res, 'TN': bests_TN_res}
    for param, param_res in bests_parameter_results.items():
        data[param] = param_res
    return pd.DataFrame(data)


def modify_config(cfg, year=None, beam=None, best=None, features_to_keep=None, output_filename=None, detector=None):
    """
    Modifies the time_interval, beam and AD parameters sections of the pipeline and grid_search sections of the config.
    """
    if year is not None:
        cfg['pipeline']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['pipeline']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year)+1))
        cfg['grid_search']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['grid_search']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year) + 1))
    if beam is not None:
        cfg['pipeline']['beam'] = beam
        cfg['grid_search']['beam'] = beam
    if best is not None:
        ad = cfg['pipeline']['anomaly_detection']['anomaly_detector']
        ad_params = cfg['pipeline']['anomaly_detection'][ad]['params']

        ad_params['n_estimators'] = int(best['n_estimators'])
        ad_params['max_samples'] = int(best['max_samples'])
        ad_params['contamination'] = float(best['contamination'])
        ad_params['max_features'] = float(best['max_features'])

        # set in cfg
        cfg['pipeline']['anomaly_detection'][ad]['params'] = ad_params
    if features_to_keep is not None:
        cfg['local_preprocessing']['keep_columns_regex'] = features_to_keep
    if output_filename is not None:
        cfg['grid_search']['output_filename'] = output_filename
        cfg['pipeline']['output_filename'] = output_filename
    if detector is not None:
        cfg['pipeline']['anomaly_detection']["anomaly_detector"] = detector
        cfg['grid_search']['anomaly_detection']["anomaly_detector"] = detector
    return cfg