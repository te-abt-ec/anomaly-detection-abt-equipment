#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))


from util import get_config
from IO.IO import IO
from scripts.experiments.shap_fs_nb_features import run_shap_fs
from scripts.experiments.histogram_fs_nb_features import run_histogram_fs
from scripts.experiments.random_fs_nb_features import run_random_fs

from datetime import datetime
import util
import random


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script for a full feature selection experiment. "
                                                 "Contains 3 sub-experiments: feature selection based on "
                                                 "SHAP values, Histogram values and random selection")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    start = datetime.now()
    seed = 456
    random.seed(seed)

    pred_types = ['TP']  # used in SHAP values and Histogram ranking feature selection experiments

    # The x-axis values (maximum number of features/columns)
    max_nb_features_list = [15, 50, 75, 100, 150, 200, 250,
                            300, 350, 400, 450, 500, 550, 600,
                            650, 700, 750, 800, 850, 900, 950, 1000, 1050]

    # number of iterations of the multiple pipeline run to perform after determining the best AD parameter set (for best AUC/rank) of 1 grid search
    iterations_for_bests_average = 20

    # number of times to perform the random feature selection experiment (result is the mean/variance of these)
    nb_random_iterations = 5

    # SHAP experiment
    print("===STARTING SHAP EXPERIMENT===")
    run_shap_fs(cfg,
                pred_types=pred_types,
                max_nb_features_list=max_nb_features_list,
                iterations_for_bests_average=iterations_for_bests_average,
                seed=seed)

    # Histogram experiment
    print("===STARTING HISTOGRAM EXPERIMENT===")
    run_histogram_fs(cfg,
                     pred_types=pred_types,
                     max_nb_features_list=max_nb_features_list,
                     iterations_for_bests_average=iterations_for_bests_average,
                     seed=seed)

    # Random experiment
    print("===STARTING RANDOM EXPERIMENT===")
    # each iteration of the random experiment gets a predetermined seed
    random_iter_seeds = random.sample(range(0, 1000), nb_random_iterations)
    for iteration in range(nb_random_iterations):
        exp_results_means, exp_results_bests_auc_mean, exp_results_bests_rank_mean = run_random_fs(cfg,
                      pred_types=pred_types,
                      max_nb_features_list=max_nb_features_list,
                      iteration=iteration,
                      seed=random_iter_seeds[iteration])

    print("Finished total experiment in {}".format(datetime.now() - start))