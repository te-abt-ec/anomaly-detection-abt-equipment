#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from util import get_config, get_training_data_from_file
from IO.IO import IO
from scripts.grid_search_ad import run_grid_search
from scripts.experiments.multiple_pipeline_ad import run_multiple_pipeline

from datetime import datetime
import util
import random
import json


def modify_config(cfg, year=None, beam=None, best=None, features_to_keep=None, output_filename=None):
    """
    Modifies the time_interval, beam and AD parameters sections of the pipeline and grid_search sections of the config.
    """
    if year is not None:
        cfg['pipeline']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['pipeline']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year)+1))
        cfg['grid_search']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['grid_search']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year) + 1))
    if beam is not None:
        cfg['pipeline']['beam'] = beam
        cfg['grid_search']['beam'] = beam
    if best is not None:
        ad = cfg['pipeline']['anomaly_detection']['anomaly_detector']
        ad_params = cfg['pipeline']['anomaly_detection'][ad]['params']

        ad_params['n_estimators'] = int(best['n_estimators'])
        ad_params['max_samples'] = int(best['max_samples'])
        ad_params['contamination'] = float(best['contamination'])
        ad_params['max_features'] = float(best['max_features'])

        # set in cfg
        cfg['pipeline']['anomaly_detection'][ad]['params'] = ad_params
    if features_to_keep is not None:
        cfg['local_preprocessing']['keep_columns_regex'] = features_to_keep
    if output_filename is not None:
        cfg['grid_search']['output_filename'] = output_filename
        cfg['pipeline']['output_filename'] = output_filename
    return cfg


def feature_to_regexname(feature):
    return '(\w|\d)*{}(\w|\d)*'.format(feature)


def regex_to_normal(list_of_features):
    res = []
    for feature_n in list_of_features:
        res.append(feature_n.split("*")[1].split("(")[0])
    return res


def run_pipelines(cfg, nb_runs, feature_selection, output_fn_base):
    assert len(feature_selection) < 2
    cfg = modify_config(cfg, features_to_keep=feature_selection,
                        output_filename=output_fn_base + 'all_single_features_sf={}'.format(feature_selection[0]))
    stats = run_multiple_pipeline(cfg, nb_runs)
    return stats


def get_all_column_names(cfg, drop_variance=False):
    io = IO(cfg, 'pipeline')
    pl = cfg['pipeline']
    start_time, end_time = pl['time_interval']
    df, state_mode, labels = get_training_data_from_file(io, drop_variance, start_time, end_time, cfg,
                                                         use_case='pipeline')
    return df.columns


def get_all_feature_names(all_column_names):
    feature_names = set()
    for coln in all_column_names:
        feature_n = coln.split("(")[1]
        feature_n = feature_n.split(")")[0]
        if "," in feature_n:
            feature_n = feature_n.split(",")[0]
        feature_names.add(feature_n)
    return list(feature_names)


def first(l):
    assert len(l) < 2
    return l[0]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script for a greedy feature selection algorithm")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    io = IO(cfg, 'pipeline')
    start = datetime.now()
    seed = 456
    random.seed(seed)

    # nb_runs to calculate mean to evaluate a feature set
    nb_runs = 16

    years = [2017]
    beams = [2]
    start_real = datetime.now()
    output_fn_base = cfg['pipeline']['output_filename']

    for y in years:
        for b in beams:
            cfg = modify_config(cfg, year=y, beam=b)
            all_column_names = get_all_column_names(cfg, drop_variance=True)
            all_feature_names = get_all_feature_names(all_column_names)
            #all_feature_names = all_feature_names[:1]
            print(all_feature_names)
            print(len(all_feature_names))

            feature_selections = [[feature_to_regexname(f)] for f in all_feature_names]

            final_fs_result = {'selected_features': [],
                               'auc': [],
                               'rank': [],
                               'auprg': [],
                               'practical_recall': [],
                               'nb_features': [],
                               'nb_train_features': []}
            for feature_selection in feature_selections:
                stats = run_pipelines(cfg, nb_runs, feature_selection, output_fn_base)
                final_fs_result['selected_features'].append(first(regex_to_normal(feature_selection)))
                final_fs_result['auc'].append(stats['auc'].mean())
                final_fs_result['rank'].append(stats['rank'].mean())
                final_fs_result['auprg'].append(stats['auprg'].mean())
                final_fs_result['practical_recall'].append(stats['practical_recall'].mean())
                final_fs_result['nb_features'].append(len(feature_selection))
                final_fs_result['nb_train_features'].append(str(stats['train_features'][0]))

            # write results
            experiment_results_df = pd.DataFrame(final_fs_result)
            io.write_to_csv(experiment_results_df, "all_single_features_res_{}.csv".format(datetime.now()),
                            io.get_multiple_pipeline_statistics_path(
                                cfg['pipeline']['anomaly_detection']['anomaly_detector']))

            print("Finished in {}".format(datetime.now() - start))
    print("Total time {}".format(datetime.now() - start_real))