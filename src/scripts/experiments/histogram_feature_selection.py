#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.subplots as ps
import random

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, LBDS_MAGNETS
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from scripts.shap_values import run_shap_values
from analysis.drop_cols import update_drop_cols_config

standard_webapp_color = '#084c94'

def update_keep_cols_config(io, colns):
    """
    Updates the local_preprocessing:keep_columns_list attribute of the config yaml file
    Args:
         io: The dataframe
         colns: The column names to keep
    """
    try:
        print("Updating the kept columns in config")
        io.config["local_preprocessing"]["keep_columns_list"] = []
        for coln in colns:
            try:
                curr_cols_list = io.config["local_preprocessing"]["keep_columns_list"]
                if curr_cols_list is None:
                    io.config["local_preprocessing"]["keep_columns_list"] = [coln]
                else:
                    io.config["local_preprocessing"]["keep_columns_list"].append(coln)
            except KeyError:
                io.config["local_preprocessing"]["keep_columns_list"] = [coln]
        io.update_config_file()
        return 1
    except:
        print("Error updating dropped column names in config")
        return 0

def create_horizontal_bar_plot(data, ranking_type, selected_features=None, pred_type=None):
    if ranking_type == "SHAP":
        y_name = "SHAP Ranking"
    elif ranking_type == "Histogram":
        y_name = "Histogram ranking"

    x = []
    y = []
    for data_entry in data:
        x.append(data_entry[y_name])
        y.append(data_entry["Feature"])

    # reverse data such that most anomalous (highest) ranking is on top
    x.reverse()
    y.reverse()

    if ranking_type == "Histogram":
        # logarithmic plot for better visualization
        #x = [math.log(i, 10) for i in x]
        colors = [standard_webapp_color] * len(y)
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln in selected_features:
                    colors[i] = 'orange'
    elif ranking_type == "SHAP":
        colors = []
        for r in x:
            if r > 0:
                colors.append('green')
            elif r < 0:
                colors.append('red')

    return go.Bar(
        name=str(pred_type),
        x=x,
        y=y,
        orientation='h',
        marker_line_width=0,
        marker_color=colors
    )

def get_histogram_ranking_data(io, pred_type, threshold, scoring_method, nbins, nhours):
    try:
        rankings_df = io.read_csv("histogram-rankings_B{}_all {}_{}_{}_nbins={}_nhours={}.csv".format(
            io.beam,
            pred_type,
            threshold,
            scoring_method,
            nbins,
            nhours
        ), io.get_histogram_rankings_path())
    except FileNotFound:
        print("Histogram rankings csv for type {} not found, generate with the webapp".format(pred_type))
        exit()

    d = {}
    for i, coln in enumerate(rankings_df["Feature"]):
        d[coln] = rankings_df["Histogram ranking"].iloc[i]
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}

    data = []
    for i, (k, v) in enumerate(d.items()):
        entry = {'Feature': "{}".format(k), 'Histogram ranking': round(v, 8)}
        data.append(entry)

    return data


def select_features(io, prediction_types, nb_features_to_select, method, add_other_magnets, threshold, scoring_method, nbins, nhours, max_nb_features=None, plot=False):
    """
    Imports the csv with histogram values for every feature (webapp generated) and selects a subset of these features
    based on criterion.
    """
    ad = "iforest"

    datas = {}
    for pred_type in prediction_types:
        datas[pred_type] = get_histogram_ranking_data(io, pred_type, threshold, scoring_method, nbins, nhours)

    selection_type = nb_features_to_select["type"]
    selection_value = nb_features_to_select["value"]

    # read training data to determine legal feature names (for adding magnets)
    df = io.read_csv(io.config["pipeline"]["preprocessing"]["variance_filtered_filename"], io.pipeline_data_path)
    legal_columns = list(df.columns)

    # random

    # find max features
    if max_nb_features is not None:
        pred_type, data = list(datas.items())[0]
        nb_features_list = [i for i in range(len(data))]
        if pred_type == "FP" or pred_type == "TN":
            nb_features_list.reverse()
        print("Finding maximal number of features...")
        abs_value = -1
        for j, v in enumerate(nb_features_list):
            curr_selected_features = get_selected_features(io, datas, method, "absolute", v,
                                                           add_other_magnets, legal_columns, verbose=False)
            if len(curr_selected_features) > max_nb_features:
                abs_value = nb_features_list[j - 1]
                break
            elif j == len(nb_features_list)-1:
                abs_value = nb_features_list[-1]
        selected_features = get_selected_features(io, datas, method, "absolute", abs_value,
                                                  add_other_magnets, legal_columns, verbose=False)
        while max_nb_features and len(selected_features) > max_nb_features:
            selected_features = get_selected_features(io, datas, method, "absolute", abs_value,
                                                      add_other_magnets, legal_columns, verbose=False)
        print(len(selected_features))
    else:
        # get selected values for given decision parameter selection_value
        selected_features = get_selected_features(io, datas, method, selection_type, selection_value,
                                                  add_other_magnets, legal_columns, verbose=True)

    # plot
    nb_rows = 1
    nb_cols = len(prediction_types)
    rows = []
    cols = []
    for i in range(nb_rows):
        for j in range(nb_cols):
            rows.append(i + 1)
            cols.append(j + 1)
    if plot:
        figures = []
        for pred_type, data in datas.items():
            figures.append(create_horizontal_bar_plot(data, "Histogram", selected_features=list(selected_features), pred_type=pred_type))

        fig = ps.make_subplots(rows=nb_rows, cols=nb_cols, subplot_titles=prediction_types)
        fig.update_layout(title="{} | {} | {} Beam {} ad={}".format(prediction_types, method,
                                                                                       io.start_year,
                                                                                       io.beam,
                                                                                       ad),
                          title_x=0.5,
                          title_font_size=30,
                          bargap=0,
                          paper_bgcolor='rgba(0,0,0,0)',
                          plot_bgcolor='rgba(0,0,0,0)'
                          )

        fig.add_traces(
            figures,
            rows=rows,
            cols=cols)
        fig.show()
    return list(selected_features)


def get_selected_features(io, datas, method, selection_type, selection_value, add_other_magnets, legal_columns, verbose=False):
    selected_features = set()
    pred_type, data = list(datas.items())[0]
    if verbose:
        print("pred_type: {}".format(pred_type))
        print("# nb_features with ranking: {}".format(len(data)))
        print("Current selected features: {}".format(len(selected_features)))
    features = [i["Feature"] for i in data]
    nb_features = get_nb_features(selection_type, selection_value, max_value=len(features))
    if method == "high_tpfn-low_fptn":
        if pred_type == "TP" or pred_type == "FN":
            print("Selecting top values...")
            candidate_features = features[:nb_features]
        elif pred_type == "FP" or pred_type == "TN":
            print("Selecting bottom values...")
            candidate_features = features[-nb_features:-1]
    elif method == "high_tpfn_delete_high_fptn":
        if pred_type == "TP" or pred_type == "FN":
            candidate_features = features[:nb_features]
        elif pred_type == "FP" or pred_type == "TN":
            candidate_features = features[nb_features:]
    elif method == "FP_top":
        if pred_type == "FP":
            print(nb_features)
            candidate_features = features[nb_features:]
        else:
            candidate_features = selected_features
    elif method == "random":
        random_list = random.sample(range(0, len(data)), nb_features)
        print(random_list)
        candidate_features = list(np.array(features)[random_list])
        selected_features.update(candidate_features)
    selected_features = update_selected_features(selected_features, candidate_features, verbose=False)
    if verbose:
        print("Current selected features: {}".format(len(selected_features)))

    if add_other_magnets:
        # When a feature is selected for a one of the 15 magnets (A...O), include the same feature for all other magnets too
        selected_features = add_other_magnets_features(io, selected_features, legal_columns)
    return selected_features


def add_other_magnets_features(io, selected_features, legal_columns):
    """
    Adds all features that are the same type, but for a different magnet to the selected features
    Args:
        io: IO object to read the legal features
        selected_features: The current selected features
        legal_columns: All original columns
    """
    print("Adding same features for other magnets...")

    features_to_add = set()
    for coln in selected_features:
        keyword = "B{}".format(io.beam)
        if keyword in coln:
            for magnet_id in LBDS_MAGNETS:
                first = coln.split(keyword)[0]
                second = coln.split(keyword)[1]
                to_add = first[:len(first) - 1] + magnet_id + keyword + second
                if to_add in legal_columns:
                    features_to_add.add(to_add)
    selected_features = selected_features.union(features_to_add)
    print("Features after adding magnets: {}".format(len(selected_features)))
    return selected_features

def get_nb_features(selection_type, selection_value, max_value):
    if selection_type == "absolute":
        nb_features = selection_value if selection_value < max_value else max_value
        return nb_features
    elif selection_type == "percentage":
        nb_features = round(max_value * selection_value)
        return nb_features
    else:
        print("Choose 'absolute' or 'percentage' selection type")
        exit()

def update_selected_features(selected_features, candidate_features, verbose=False):
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
        print("\ncandidate")
        for i in candidate_features:
            print(i)
    selected_features.update(candidate_features)
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
    return selected_features


def run_experiment(cfg, max_nb_features=None, prediction_types=None, nb_features_to_select=None, method=None, params=None, add_other_magnets=None, plot=False, update_config=False, seed=None):
    io = IO(cfg, 'grid_search')

    if seed is None:
        np.random.seed(456)
        random.seed(456)
    else:
        np.random.seed(seed)
        random.seed(seed)

    # initialize default parameters
    if prediction_types is None:
        prediction_types = ['TP']
    # type = absolute or percentage
    if nb_features_to_select is None:
        nb_features_to_select = {'type': 'percentage',
                                 'value': 0.4}
    if method is None:
        method = "high_tpfn_delete_high_fptn"
        #method = "random"

    if params is None:
        params = {
            "threshold": 0.95,
            "scoring_method": "max",
            "nbins": 50,
            "nhours": 9000
        }
    if add_other_magnets is None:
        add_other_magnets = True
    selected_features = select_features(io, prediction_types, nb_features_to_select, method,
                                        add_other_magnets, **params, max_nb_features=max_nb_features, plot=plot)
    # create new config
    if update_config:
        update_keep_cols_config(io, selected_features)

    return selected_features


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Histogram feature selection")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_experiment(cfg,  method="random", max_nb_features=400, plot=False, update_config=True)








