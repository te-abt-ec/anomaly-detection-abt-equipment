#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import get_training_data_from_file, get_config, RESULTS_DIR
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex, keep_columns_list, keep_columns_regex
from scripts.experiments.experiment_util import *
from IO.IO import IO
import random
from uuid import uuid4


def run_multiple_pipeline(cfg, nb_runs=4, scale_data_best=None, segment_score_best=None, detector_params_best=None, feature_set_name=None, features_to_keep=None, seed=None):
    """
    Modified version of the grid search code to run multiple pipelines in parallel with the same given AD parameters.
    Returns the statistics of these pipeline runs
    Args:
        cfg: The configuration file
        nb_runs: The number of pipeline iterations to perform
        scale_data_best: The scale_data AD parameter to use
        segment_score_best: The segment_score AD parameter to use
        detector_params_best: The AD detector parameters (dictionary)
        feature_set_name: The name of the given feature set (if performing feature selection)
        features_to_keep: The feature set on which is filtered (list of strings)
        seed: The seed to use
    """
    try:
        gs = cfg['grid_search']
        start_time, end_time = gs['time_interval']
        ad = gs['anomaly_detection']
        detector = ad['anomaly_detector']
        preprocess = gs['preprocessing']
        drop_low_variance_cols = preprocess['drop_low_variance_columns']
        max_runtime = gs['max_runtime']
        output_filename = gs['output_filename']
        beam = gs['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    # IO for all read/write operations
    io = IO(cfg, 'grid_search')
    set_seed(seed)

    print("GRID SEARCH FOR: {}".format(detector))

    df, state_mode, labels = get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg, use_case='grid_search')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting LBDS data, exiting...")
        exit()
    if gs['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop and keep columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])
    if cfg['local_preprocessing']['keep_columns']:
        df = keep_columns_list(df, cfg['local_preprocessing']['keep_columns_list'])
        df = keep_columns_regex(df, cfg['local_preprocessing']['keep_columns_regex'])

    if features_to_keep is None:
        feature_selection = [('all', list(df.columns))]

    try:
        gs_config = gs["anomaly_detection"][detector]
        pl_config = cfg["pipeline"]["anomaly_detection"][detector]
        if scale_data_best is None:
            scale_data = [pl_config['scale_data'] for i in range(len(gs_config['scale_data']))]
        else:
            scale_data = [scale_data_best for i in range(len(gs_config['scale_data']))]
        if segment_score_best is None:
            segment_score = [pl_config['segment_score'] for i in range(len(gs_config['segment_score']))]
        else:
            segment_score = [segment_score_best for i in range(len(gs_config['segment_score']))]
        scoring = gs_config['scoring']
        fs = gs_config['feature_selection']
        feature_selection = [(fs, list(df.columns))]
        if detector_params_best is None:
            detector_params = {}
            for k, v in gs_config['params'].items():
                detector_params[k] = [pl_config['params'][k] for i in range(len(v))]
        else:
            detector_params = {}
            for k, v in detector_params_best.items():
                detector_params[k] = [detector_params_best[k] for i in range(1)]

    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    if features_to_keep is not None:
        feature_selection = [(feature_set_name, features_to_keep)]
        print("grid search has {} ({}) features to keep".format(feature_set_name, len(features_to_keep)))

    print("Launching grid search with data from ({} - {}), this might take a while..".format(start_time, end_time))

    # PERFORM GRID SEARCH
    results = grid_search(
        feature_selection=feature_selection,
        beam=beam,
        scale_data=scale_data,
        anomaly_detector=detector,
        detector_params=detector_params,
        labels=labels,
        segment_score=segment_score,
        x_train=df,
        state_mode=state_mode,
        scoring=scoring,
        max_runtime=max_runtime,
        filename=output_filename,
        config=cfg,
        nb_runs=nb_runs,
        seed=seed,
        io=io
    )

    for res in results:
        if math.isnan(res['rank']):
            print("Note: One of the resulting grid search AUC values has NaN as output, consider using a bigger time interval to include anomalies")

    if len(results):
        features_res = [res['feature'] for res in results]
        auc_res = [res['auc'] for res in results]
        auprg_res = [res['auprg'] for res in results]
        rank_res = [res['rank'] for res in results]
        practical_recall_res = [res['practical_recall'] for res in results]
        scale = [res['scale'] for res in results]
        time_res = [res['time'] for res in results]
        TP_res = [res['confusion_matrix_stats']['TP'] for res in results]
        FP_res = [res['confusion_matrix_stats']['FP'] for res in results]
        FN_res = [res['confusion_matrix_stats']['FN'] for res in results]
        TN_res = [res['confusion_matrix_stats']['TN'] for res in results]
        train_features_res = [list(df.columns) for i in range(len(results))]
        id_res = [res['id'] for res in results]
        seed_res = [res['seed'] for res in results]

        parameter_results = {}
        for param in detector_params:
            parameter_results[param] = [res['params'][param] for res in results]

        data = {'auc': auc_res, 'auprg': auprg_res, 'rank': rank_res, 'practical_recall': practical_recall_res,
                'TP': TP_res, 'FN': FN_res, 'FP': FP_res, 'TN': TN_res, 'features': features_res, 'train_features': train_features_res}
        for param, param_res in parameter_results.items():
            data[param] = param_res
        data['scale'] = scale
        data['time'] = time_res
        data['id'] = id_res
        data['seed'] = seed_res

        statistics = pd.DataFrame(data)
        statistics_filename = io.get_multiple_pipeline_statistics_filename(nb_runs, detector)
        f_path = io.get_multiple_pipeline_statistics_path(detector)
        print("Writing statistics results to {}".format(f_path + statistics_filename))
        io.write_to_csv(statistics, statistics_filename, f_path)
        return statistics


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run multiple pipelines with the same AD parameters"
                                                 " in parallel using the grid search code")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    parser.add_argument('-runs', dest='runs', default=20,
                        help='amount of pipeline runs for this experiment')

    args = parser.parse_args()
    runs = args.runs

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_multiple_pipeline(cfg, nb_runs=runs)