#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from util import get_config, get_training_data_from_file
from IO.IO import IO
from scripts.grid_search_ad import run_grid_search
from scripts.experiments.multiple_pipeline_ad import run_multiple_pipeline

from datetime import datetime
import util
import random


def modify_config(cfg, year=None, beam=None, best=None, features_to_keep=None, output_filename=None):
    """
    Modifies the time_interval, beam and AD parameters sections of the pipeline and grid_search sections of the config.
    """
    if year is not None:
        cfg['pipeline']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['pipeline']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year)+1))
        cfg['grid_search']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['grid_search']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year) + 1))
    if beam is not None:
        cfg['pipeline']['beam'] = beam
        cfg['grid_search']['beam'] = beam
    if best is not None:
        ad = cfg['pipeline']['anomaly_detection']['anomaly_detector']
        ad_params = cfg['pipeline']['anomaly_detection'][ad]['params']

        ad_params['n_estimators'] = int(best['n_estimators'])
        ad_params['max_samples'] = int(best['max_samples'])
        ad_params['contamination'] = float(best['contamination'])
        ad_params['max_features'] = float(best['max_features'])

        # set in cfg
        cfg['pipeline']['anomaly_detection'][ad]['params'] = ad_params
    if features_to_keep is not None:
        cfg['local_preprocessing']['keep_columns_regex'] = features_to_keep
    if output_filename is not None:
        cfg['grid_search']['output_filename'] = output_filename
        cfg['pipeline']['output_filename'] = output_filename
    return cfg


def feature_to_regexname(feature):
    return '(\w|\d)*{}(\w|\d)*'.format(feature)


def regex_to_normal(list_of_features):
    res = []
    for feature_n in list_of_features:
        res.append(feature_n.split("*")[1].split("(")[0])
    return res

def run_greedy_feature_selection(cfg, years, beams, nb_runs, starting_featureset, all_feature_names, nb_iterations, metric_to_use):
    io = IO(cfg, 'pipeline')
    for year in years:
        for beam in beams:
            print("===STARTING {} beam {}===".format(year, beam))
            output_fn_base = cfg['pipeline']['output_filename']
            cfg = modify_config(cfg, year=year, beam=beam, features_to_keep=starting_featureset,
                                output_filename=output_fn_base + '_sf=0')
            stats = run_multiple_pipeline(cfg, nb_runs)
            remaining_feature_names = all_feature_names
            selected_features = starting_featureset
            final_fs_result = {'selected_features': [regex_to_normal(starting_featureset)],
                               'auc': [stats['auc'].mean()],
                               'rank': [stats['rank'].mean()],
                               'auprg': [stats['auprg'].mean()],
                               'practical_recall': [stats['practical_recall'].mean()],
                               'nb_features': [1]}
            for i in range(nb_iterations):
                results = {f_name: {'auc': -1, 'rank': -1, 'auprg': -1, 'practical_recall': -1} for f_name in remaining_feature_names}
                for j, feature_name in enumerate(remaining_feature_names):
                    current_features = selected_features + [feature_to_regexname(feature_name)]
                    cfg = modify_config(cfg, year=year, beam=beam, features_to_keep=current_features,
                                        output_filename=output_fn_base + '_sf={}_{}'.format(len(selected_features), feature_name))

                    print("===STARTING {} {}===".format(i, j))
                    statistics = run_multiple_pipeline(cfg, nb_runs)
                    results[feature_name]['auc'] = statistics['auc'].mean()
                    results[feature_name]['rank'] = statistics['rank'].mean()
                    results[feature_name]['auprg'] = statistics['auprg'].mean()
                    results[feature_name]['practical_recall'] = statistics['practical_recall'].mean()

                # get best feature
                best_feature = None
                current_best = 9999 if metric_to_use == "rank" else -1
                best_res = None
                for feature_name, res in results.items():
                    if metric_to_use == "rank":
                        if res[metric_to_use] < current_best:
                            best_feature = feature_name
                            current_best = res[metric_to_use]
                            best_res = res
                    else:
                        if res[metric_to_use] > current_best:
                            best_feature = feature_name
                            current_best = res[metric_to_use]
                            best_res = res
                selected_features = selected_features + [feature_to_regexname(best_feature)]
                final_fs_result['selected_features'].append(regex_to_normal(selected_features))
                final_fs_result['auc'].append(best_res['auc'])
                final_fs_result['rank'].append(best_res['rank'])
                final_fs_result['auprg'].append(best_res['auprg'])
                final_fs_result['practical_recall'].append(best_res['practical_recall'])
                final_fs_result['nb_features'].append(len(selected_features))

                # remove best feature from remaining features
                remaining_feature_names.remove(best_feature)

                # write intermediate results
                experiment_results_df = pd.DataFrame(final_fs_result)
                io.write_to_csv(experiment_results_df, "greedy_feature_selection_res_{}_{}.csv".format(i, datetime.now()),
                                io.get_multiple_pipeline_statistics_path(
                                    cfg['pipeline']['anomaly_detection']['anomaly_detector']))


def get_all_column_names(cfg, drop_variance=False):
    io = IO(cfg, 'pipeline')
    pl = cfg['pipeline']
    start_time, end_time = pl['time_interval']
    df, state_mode, labels = get_training_data_from_file(io, drop_variance, start_time, end_time, cfg,
                                                         use_case='pipeline')
    return df.columns


def get_all_feature_names(all_column_names):
    feature_names = set()
    for coln in all_column_names:
        feature_n = coln.split("(")[1]
        feature_n = feature_n.split(")")[0]
        if "," in feature_n:
            feature_n = feature_n.split(",")[0]
        feature_names.add(feature_n)
    return list(feature_names)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script for a greedy feature selection algorithm")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    start = datetime.now()
    seed = 456
    random.seed(seed)

    years = [2016]
    beams = [2]

    # nb_runs to calculate mean to evaluate a feature set
    nb_runs = 16
    starting_featureset = ['(\w|\d)*switchARatio(\w|\d)*']
    metric_to_use = 'rank'

    all_column_names = get_all_column_names(cfg, drop_variance=True)
    all_feature_names = get_all_feature_names(all_column_names)
    all_feature_names.remove('switchARatio')
    #all_feature_names = all_feature_names[:5]
    print(all_feature_names)
    print(len(all_feature_names))

    # number of iterations in the greedy algorithm (= nb datapoints on plot - 1)
    nb_iterations = len(all_feature_names)
    nb_iterations = 20

    assert len(all_feature_names) >= nb_iterations
    run_greedy_feature_selection(cfg, years, beams, nb_runs, starting_featureset, all_feature_names, nb_iterations, metric_to_use)

    print("Finished in {}".format(datetime.now() - start))