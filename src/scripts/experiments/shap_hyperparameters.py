#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.subplots as ps

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from scripts.shap_values import run_shap_values

np.random.seed(456)

def aggregate_SHAP_values(shap_df, method='sum'):
    """
    Aggregates the given SHAP values per feature with given method
    """
    agg_df = shap_df.sum()
    if method == 'mean':
        agg_df = shap_df.mean()
    elif method == 'max':
        agg_df = shap_df.max()
    elif method == 'min':
        agg_df = shap_df.min()
    return agg_df

def get_shap_data(io, shap_file_selection, ad, type='single', selected_prediction=None):
    shap_path = io.get_model_and_shap_path(ad)
    shap_df = io.read_csv(shap_file_selection, shap_path)
    if type == 'aggregated':
        return aggregate_SHAP_values(shap_df)
    else:
        start_time, end_time = split_prediction(selected_prediction)
        return shap_df[start_time:end_time]

def create_horizontal_bar_plot(data, ranking_type, selected_features=None):
    y_name = "SHAP Ranking" if ranking_type == "SHAP" else "Ranking"
    x = []
    y = []
    for data_entry in data:
        x.append(data_entry[y_name])
        y.append(data_entry["Feature"])

    # reverse data such that most anomalous (highest) ranking is on top
    x.reverse()
    y.reverse()

    if ranking_type == "Histogram":
        # logarithmic plot for better visualization
        x = [math.log(i, 10) for i in x]
        colors = [standard_webapp_color] * len(y)
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln not in selected_features:
                    colors[i] = confusion_matrix_colors["FP"]
    elif ranking_type == "SHAP":
        colors = []
        for r in x:
            if r > 0:
                colors.append('green')
            elif r < 0:
                colors.append('red')

    return go.Bar(
        x=x,
        y=y,
        orientation='h',
        marker_color=colors)


def get_shap_ranking_data(io, shap_file_selection, ad, method='sum'):
    # aggregate SHAP values of all predictions
    shap_df = get_shap_data(io, shap_file_selection, ad, type='aggregated')
    shap_df = shap_df.to_frame().transpose()

    d = {}
    for coln in shap_df.columns:
        res = 0
        if method == 'sum':
            res = shap_df[coln].sum()
        elif method == 'max':
            res = shap_df[coln].max()
        elif method == 'min':
            res = shap_df[coln].min()
        if res != 0:
            d[coln] = res
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}

    data = []
    for i, (k, v) in enumerate(d.items()):
        entry = {'Feature': "{}".format(k), 'SHAP Ranking': round(v, 8)}
        data.append(entry)

    return data

def plot_experiment_results(cfg, subsampling_samples, nb_samples):
    io = IO(cfg, 'pipeline')
    confusion_matrix_selection = "all TP"
    ad = "iforest"


    datas = []
    for ss_s in subsampling_samples:
        for n_s in nb_samples:
            datas.append(get_shap_ranking_data(io,
                         "shap_values_iforest_B2_0.95_y_pred_TN_subsample={}_nb_samples={}_identity.csv".format(ss_s, n_s), ad, method='sum'))

    figures = []
    for data in datas:
        figures.append(create_horizontal_bar_plot(data, "SHAP"))

    nb_rows = len(subsampling_samples)
    nb_cols = len(nb_samples)
    rows = []
    cols = []
    for i in range(nb_rows):
        for j in range(nb_cols):
            rows.append(i+1)
            cols.append(j+1)

    plot_titles = []
    for i in subsampling_samples:
        for j in nb_samples:
            plot_titles.append("subsampling_samples= {} | nb_samples = {}".format(i, j))

    fig = ps.make_subplots(rows=nb_rows, cols=nb_cols, subplot_titles=plot_titles)

    fig.add_traces(
        figures,
        rows=rows,
        cols=cols)
    fig.show()




def run_experiment(subsampling_samples, nb_samples):
    for ss_s in subsampling_samples:
        for n_s in nb_samples:
            print("====== STARTING SHAP RUN WITH subsampling_samples={} | nb_samples={}".format(ss_s, n_s))
            run_shap_values(cfg, {"link": {"iforest": "identity", "gmm": "identity", "hbos": "identity"},
                                  "subsampling_samples": ss_s, "nb_samples": n_s})


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="SHAP hyperparameters experiment")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    subsampling_samples = [100]
    nb_samples = [10]

    #run_experiment(subsampling_samples, nb_samples)
    plot_experiment_results(cfg, subsampling_samples, nb_samples)






