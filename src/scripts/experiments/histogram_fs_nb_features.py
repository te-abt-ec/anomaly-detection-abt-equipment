#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from util import get_config
from IO.IO import IO
from scripts.experiments.histogram_feature_selection import run_experiment
from scripts.experiments.multiple_pipeline_ad import run_multiple_pipeline
from scripts.experiments.experiment_util import *

from datetime import datetime
import random


def run_histogram_fs(cfg, max_nb_features_list=None, pred_types=None, iterations_for_bests_average=None, scoring_type='auc', seed=None):
    start_time = datetime.now()
    io = IO(cfg, 'grid_search')

    set_seed(seed)

    if max_nb_features_list is None:
        max_nb_features_list = [200]
    method = "high_tpfn_delete_high_fptn"
    if pred_types is None:
        pred_types = ['TP']

    # Set the number of iterations of the multiple pipeline run to perform after determining the best AD parameter set (for best AUC/rank)
    # make a multiple of 4 to run faster (multiprocessed)
    # TODO: The multiprocessing seems to generate exactly the same results for each core (with the same AD parameters) even with random seed disabled
    # the amount of unique results is thus iterations_for_bests_average / 4
    if iterations_for_bests_average is None:
        iterations_for_bests_average = 400

    configurations = []
    for max_value in max_nb_features_list:
        configurations.append({"max_nb_features": max_value})

    feature_set_names = []
    for i in configurations:
        feature_set_names.append(
            "max_nb_features={}".format(i["max_nb_features"]))

    # run a grid search for each configuration and collect for each configuration:
    # 1) the statistics for each of the grid search pipeline runs
    # 2) The result with best AUC
    # 3) The result with best rank
    # run iterations_for_bests_average number of iterations of the pipeline for the best AD parameters (only to make a better plot)
    # 4) The average results over a number of pipeline runs for best AUC AD parameters
    # 5) The average results over a number of pipeline runs for best rank AD parameters
    exp_stats = {}
    exp_stats_bests_auc = {}
    exp_stats_bests_rank = {}
    multiple_best_auc_res = {}
    multiple_best_rank_res = {}
    seeds = random.sample(range(0, 100), len(configurations))
    for i, c in enumerate(configurations):
        print("Selecting features for max_nb_features:{}".format(c["max_nb_features"]))
        selected_features = run_experiment(cfg,
                                           prediction_types=pred_types,
                                           method=method,
                                           max_nb_features=c["max_nb_features"])
        if len(selected_features) == 0:
            print("No selected features, skipping this grid search...")
            continue
        print("Starting gs for max_nb_features={} | selected_features={}".format(c["max_nb_features"],
                                                                                                len(selected_features)))
        stats, best_auc, best_rank = run_gs(cfg, feature_set_names[i], selected_features)
        exp_stats[feature_set_names[i]] = (stats, len(selected_features))
        exp_stats_bests_auc[feature_set_names[i]] = (best_auc, len(selected_features))
        exp_stats_bests_rank[feature_set_names[i]] = (best_rank, len(selected_features))
        multiple_best_auc = run_multiple_pipeline(cfg, scale_data_best=best_auc["scale"],
                                                  segment_score_best=best_auc["segment_score"],
                                                  detector_params_best=best_auc["params"],
                                                  nb_runs=iterations_for_bests_average,
                                                  feature_set_name=feature_set_names[i],
                                                  features_to_keep=selected_features,
                                                  seed=seeds[i])
        multiple_best_auc_res[feature_set_names[i]] = (multiple_best_auc, len(selected_features))
        multiple_best_rank = run_multiple_pipeline(cfg, scale_data_best=best_rank["scale"],
                                                   segment_score_best=best_rank["segment_score"],
                                                   detector_params_best=best_rank["params"],
                                                   nb_runs=iterations_for_bests_average,
                                                   feature_set_name=feature_set_names[i],
                                                   features_to_keep=selected_features,
                                                   seed=seeds[i])
        multiple_best_rank_res[feature_set_names[i]] = (multiple_best_rank, len(selected_features))

    series_results = fs_stats_to_means(exp_stats, verbose=True)
    mean_of_best_auc = fs_stats_to_means(multiple_best_auc_res, verbose=True)
    mean_of_best_rank = fs_stats_to_means(multiple_best_rank_res, verbose=True)

    # 1)
    # convert bests AUC to a dataframe and write
    bests_df = fs_construct_bests(exp_stats_bests_auc, configurations, feature_set_names)
    filename = '{}_histogram_feature_selection_experiment_{}_bests_auc_{}.csv'.format(method, pred_types, str(datetime.now()))
    io.write_to_csv(bests_df, filename, io.fs_histogram_path)

    # 2)
    # convert bests rank to a dataframe and write
    bests_df = fs_construct_bests(exp_stats_bests_rank, configurations, feature_set_names)
    filename = '{}_histogram_feature_selection_experiment_{}_bests_rank_{}.csv'.format(method, pred_types, str(datetime.now()))
    io.write_to_csv(bests_df, filename, io.fs_histogram_path)

    # 3)
    # write means experiment results
    write_exp_results(io, '{}_histogram_feature_selection_experiment_{}_means_result_{}.csv'.format(method, pred_types, datetime.now()),
                      series_results,
                      feature_set_names,
                      configurations,
                      type='histogram')
    # 4)
    # write means of multiple pipelines with ad parameters with best auc
    write_exp_results(io, '{}_histogram_feature_selection_experiment_{}_means_best_auc_result_{}.csv'.format(method, pred_types,
                                                                                                     datetime.now()),
                      mean_of_best_auc,
                      feature_set_names,
                      configurations,
                      type='histogram')
    # 5)
    # write means of multiple pipelines with ad parameters with best rank
    write_exp_results(io, '{}_histogram_feature_selection_experiment_{}_means_best_rank_result_{}.csv'.format(method, pred_types,
                                                                                                      datetime.now()),
                      mean_of_best_rank,
                      feature_set_names,
                      configurations,
                      type='histogram')

    print("Finished execution after {} time".format(datetime.now() - start_time))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_histogram_fs(cfg)




