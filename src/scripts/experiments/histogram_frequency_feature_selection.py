import itertools

def select_features_frequency_bound(hist_data, f=0.1, add_one_feature=False):
    """
    Select all features in the hist_df (index) that occur with a frequency less than f
    If this frequency is greater than half of all features, this collection is also selected
    Args:
        hist_data: List of dicts containing the colnames and histogram rankings
        f: The frequency threshold
    """
    if hist_data is None or len(hist_data) == 0:
        print("Given dataframe was empty, no features selected.")
        return []
    min_frequency = len(hist_data) * f
    features = [i["Feature"] for i in hist_data]
    rankings = [i["Ranking"] for i in hist_data]

    max_feature = None
    max_ranking = max(rankings)
    for i, f in enumerate(features):
        if rankings[i] == max_ranking:
            max_feature = features[i]

    # group features based on unique ranking
    group_func = lambda x: x["Ranking"]
    grouped_features = {key: list(group) for key, group in itertools.groupby(hist_data, group_func)}

    # feature selection
    selected_features = []
    for unique_ranking, features_with_this_ranking in grouped_features.items():
        features_g = [k["Feature"] for k in features_with_this_ranking]
        if len(features_with_this_ranking) < min_frequency or len(features_with_this_ranking) > len(hist_data) / 2:
            selected_features.extend(features_g)
        else:
            if add_one_feature:
                selected_features.append(features_g[0])

    print("\nFEATURE SELECTION: ")
    print("Max: {} | Ranking: {}".format(max_feature, max_ranking))
    print("Minimal frequency: {}".format(min_frequency))
    #print("Selected features:")
    #for f in selected_features:
    #    print(f)
    print("Selected {}/{} features".format(len(selected_features), len(hist_data)))
    return selected_features