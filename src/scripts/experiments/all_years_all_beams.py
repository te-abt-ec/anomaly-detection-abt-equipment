#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from util import get_config
from IO.IO import IO
from scripts.grid_search_ad import run_grid_search
from scripts.experiments.multiple_pipeline_ad import run_multiple_pipeline

from datetime import datetime
import util
import random


def modify_config(cfg, year=None, beam=None, best=None):
    """
    Modifies the time_interval, beam and AD parameters sections of the pipeline and grid_search sections of the config.
    """
    if year is not None:
        cfg['pipeline']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['pipeline']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year)+1))
        cfg['grid_search']['time_interval'][0] = '{}-01-01 00:00:00.000'.format(str(year))
        cfg['grid_search']['time_interval'][1] = '{}-01-01 00:00:00.000'.format(str(int(year) + 1))
    if beam is not None:
        cfg['pipeline']['beam'] = beam
        cfg['grid_search']['beam'] = beam
    if best is not None:
        ad = cfg['pipeline']['anomaly_detection']['anomaly_detector']
        ad_params = cfg['pipeline']['anomaly_detection'][ad]['params']

        ad_params['n_estimators'] = int(best['n_estimators'])
        ad_params['max_samples'] = int(best['max_samples'])
        ad_params['contamination'] = float(best['contamination'])
        ad_params['max_features'] = float(best['max_features'])

        # set in cfg
        cfg['pipeline']['anomaly_detection'][ad]['params'] = ad_params
    return cfg


def run_all_years_all_beams(cfg, years, beams, nb_runs):
    """
    Runs nb_runs pipelines with the best AD parameters selected from the grid search.
    Results of the grid search are found in results/statistics/[machine]
    Results are found in results/statistics/multiple_pipelines/[ad]

    """
    for year in years:
        for beam in beams:
            print("===STARTING {} beam {}===".format(year, beam))
            cfg = modify_config(cfg, year=year, beam=beam)
            best = run_grid_search(cfg)
            print("best grid search run parameters:", best)
            cfg = modify_config(cfg, best=best)
            run_multiple_pipeline(cfg, nb_runs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to run multiple pipelines script for best AD parameters"
                                                 "for all years and beams")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')

    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    start = datetime.now()
    seed = 456
    random.seed(seed)

    years = [2016, 2017, 2018]
    beams = [1, 2]
    nb_runs = 100

    run_all_years_all_beams(cfg, years, beams, nb_runs)

    print("Finished in {}".format(datetime.now() - start))