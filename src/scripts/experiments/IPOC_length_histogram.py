#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.subplots as ps

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from scripts.shap_values import run_shap_values
from analysis.drop_cols import update_drop_cols_config


def get_histogram_data(cfg):
    io = IO(cfg, 'pipeline')
    fn = cfg["pipeline"]["preprocessing"]["variance_filtered_filename"]
    f_path = io.pipeline_data_path
    df = io.read_csv(fn, f_path)
    ts = df.index

    print(ts)
    d = IPOC_ts_differences(list(ts))

    # convert pandas time deltas to minutes
    d = [i.seconds / 60 for i in d]

    # calculate frequencies
    freq = {}
    for delta in d:
        if delta in freq:
            freq[delta] += 1
        else:
            freq[delta] = 1
    freq = {k: v for k, v in sorted(freq.items(), key=lambda item: item[1], reverse=True)}

    #print("Top 10 frequent deltas (minutes):")
    #for i in range(30):
    #    print_freq = list(freq.items())[i]
    #    print("#{} | minutes: {:.2f} freq: {}".format(i+1, print_freq[0], print_freq[1]))

    thresholds = [1, 2, 5, 10, 60, 120]
    for j in thresholds:
        threshold_mins = j
        smaller_eq_to_threshold = 0
        rest = 0
        for mins, f in freq.items():
            if mins <= threshold_mins:
                smaller_eq_to_threshold += f
            else:
                rest += f
        print()
        print("delta minutes <= {} | freq: {}".format(threshold_mins, smaller_eq_to_threshold))
        print("delta minutes >  {} | freq: {}".format(threshold_mins, rest))


    data_fig = go.Figure(go.Histogram(
        x=d,
        nbinsx=100
    ))
    data_fig.update_layout(
        title="IPOC time deltas for {} Beam {} histogram".format(io.start_year, io.beam),
        xaxis_title="Time delta",
        title_x=0.5,
        font=dict(
            size=18
        )
    )

    data_fig.show()


def IPOC_ts_differences(ts):
    differences = []
    for i in range(len(ts)-1):
        differences.append(ts[i+1]-ts[i])
    return differences


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Histogram feature selection")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    get_histogram_data(cfg)