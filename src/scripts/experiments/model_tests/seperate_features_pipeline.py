#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../../..'))

import numpy as np
from pipeline.pipeline import pipeline
from preprocessing import builder
from util import get_training_data_from_file, get_config, CERN_DATA_DIR, PIPELINE_DATA_DIR
from preprocessing.preprocessor import *
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from datetime import datetime


def run_pipeline(cfg, feature_set=None):
    try:
        pl = cfg['pipeline']
        start_time, end_time = pl['time_interval']
        preprocessing = pl['preprocessing']
        drop_low_variance_cols = False
        ad = pl["anomaly_detection"]
        detector = ad['anomaly_detector']
        beam = pl['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    print("PIPELINE RUN FOR: {}".format(detector))

    # IO for all read/write operations
    io = IO(cfg, 'pipeline')

    # load the dataset, state:mode dataset (optional) and the logbook dataset (labels)
    df, state_mode, labels = get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg, use_case='pipeline')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting data, exiting...")
        exit()
    if pl['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop and keep columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])
    if cfg['local_preprocessing']['keep_columns']:
        df = keep_columns_list(df, cfg['local_preprocessing']['keep_columns_list'])
        df = keep_columns_regex(df, cfg['local_preprocessing']['keep_columns_regex'])

    selected_features = list(df.columns)
    if feature_set is not None:
        selected_features = feature_set


    # dump variance handled features for inspection
    # TODO: add caching of this file to grid_search to avoid doing the same filtering for each pipeline run
    #       (gridsearch is always over the same timeframe, so this is possible)
    io.write_to_csv(df, preprocessing["variance_filtered_filename"], io.pipeline_data_path)

    try:
        ad_config = ad[detector]
        scale_data = ad_config["scale_data"]
        segment_score = ad_config["segment_score"]
        detector_params = ad_config["params"]
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    # SET ANOMALY DETECTOR AND PARAMETERS
    print("Launching pipeline with data from ({} - {}), this might take a while..".format(start_time, end_time))
    res = pipeline(
        beam=beam,
        scale_data=scale_data,
        anomaly_detector=detector,
        detector_params=detector_params,
        labels=labels,
        segment_score=segment_score,
        x_train=df[selected_features],
        state_mode=state_mode,
        io=io,
        write_output=False,
        config=cfg,
        task='pipeline')
    print("Done")
    return res


def get_subdivided_features(colnames):
    """
    Returns a dictionary with keys the feature names and values lists with features containing that feature name
    Args:
        colnames: The feature list to subdivide
    """
    feature_names = set()
    for coln in colnames:
        if "false" in coln:
            last_part = coln.split('(')[1]
            featurename = last_part.split(',')[0]
            feature_names.add(featurename)
        else:
            last_part = coln.split('(')[1]
            featurename = last_part.split(')')[0]
            feature_names.add(featurename)

    actual_feature_sets = {i: [] for i in list(feature_names)}
    for coln in colnames:
        for feature_name in list(feature_names):
            infix1 = "(" + feature_name + ",false)"
            infix2 = "(" + feature_name + ")"
            if infix1 in coln or infix2 in coln:
                actual_feature_sets[feature_name].append(coln)
                break
    return actual_feature_sets


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    start = datetime.now()
    seed = 456
    np.random.seed(seed)

    nb_iterations_per_feature = 5
    io = IO(cfg, 'pipeline')
    ad = cfg["pipeline"]["anomaly_detection"]["anomaly_detector"]

    # import feature names to use
    filename = io.config['pipeline']['preprocessing']['variance_filtered_filename']
    df = pd.read_csv(io.pipeline_data_path + filename)
    df = set_features_index(df, index='timestamps')
    df = sort_on_index(df, ascending=True)

    # get list of different feature sets
    features_sets = get_subdivided_features(list(df.columns))
    # 68 feature types for 2016 beam 1 data (variance filtered)
    # 78 feature types for 2016 beam 2 data (variance filtered)

    # do experiment
    print("Starting experiment for {} feature sets".format(len(features_sets)))
    results = []
    for i in range(nb_iterations_per_feature):
        exp_results = {'feature_set_name': [],
                       'auc': [],
                       'rank': [],
                       'nb_features': [],
                       'system': []}
        for feature_set_name, feature_set in features_sets.items():
            print()
            print("========STARTING PIPELINE FOR : {} {} ({})".format(feature_set[0].split('_')[0], feature_set_name, len(feature_set)))
            res = run_pipeline(cfg, feature_set)
            exp_results["feature_set_name"].append(feature_set_name)
            exp_results["nb_features"].append(len(feature_set))
            exp_results["auc"].append(res["auc"])
            exp_results["rank"].append(res["rank"])
            exp_results["system"].append(feature_set[0].split('_')[0])
        results.append(exp_results)

    # write individual experiment results
    for i, exp_results in enumerate(results):
        io.write_to_csv(pd.DataFrame(data=exp_results), "seperate_features_pipeline_{}_{}.csv".format(ad, i), io.seperate_feature_results_path)

    # calculate means and write mean results
    auc_res = {str(i): [] for i in range(nb_iterations_per_feature)}
    rank_res = {str(i): [] for i in range(nb_iterations_per_feature)}
    for i, exp_results in enumerate(results):
        auc_res[str(i)] = exp_results["auc"]
        rank_res[str(i)] = exp_results["rank"]
    auc_df_iter = pd.DataFrame(data=auc_res, index=list(features_sets.keys()))
    rank_df_iter = pd.DataFrame(data=rank_res, index=list(features_sets.keys()))
    auc_df = auc_df_iter.mean(axis=1)
    rank_df = rank_df_iter.mean(axis=1)

    means_df = pd.DataFrame(data={"auc": list(auc_df),
                                  "rank": list(rank_df),
                                  "nb_features": results[0]["nb_features"],
                                  "system": results[0]["system"]}, index=list(features_sets.keys()))

    io.write_to_csv(means_df, "seperate_features_means_{}_{}.csv".format(ad, datetime.now()), io.seperate_feature_results_path)
    print("Ended experiment after {}".format(datetime.now() - start))





