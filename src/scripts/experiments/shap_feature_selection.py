#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.subplots as ps

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config, LBDS_MAGNETS
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex
from IO.IO import IO

from scripts.shap_values import run_shap_values
from analysis.drop_cols import update_drop_cols_config


def update_keep_cols_config(io, colns):
    """
    Updates the local_preprocessing:keep_columns_list attribute of the config yaml file
    Args:
         io: The dataframe
         colns: The column names to keep
    """
    try:
        print("Updating the kept columns in config")
        io.config["local_preprocessing"]["keep_columns_list"] = []
        for coln in colns:
            try:
                curr_cols_list = io.config["local_preprocessing"]["keep_columns_list"]
                if curr_cols_list is None:
                    io.config["local_preprocessing"]["keep_columns_list"] = [coln]
                else:
                    io.config["local_preprocessing"]["keep_columns_list"].append(coln)
            except KeyError:
                io.config["local_preprocessing"]["keep_columns_list"] = [coln]
        io.update_config_file()
        return 1
    except:
        print("Error updating dropped column names in config")
        return 0

def aggregate_SHAP_values(shap_df, method='mean'):
    """
    Aggregates the given SHAP values per feature with given method
    """
    # default is sum
    agg_df = shap_df.sum()
    if method == 'mean':
        agg_df = shap_df.mean()
    elif method == 'max':
        agg_df = shap_df.max()
    elif method == 'min':
        agg_df = shap_df.min()
    return agg_df

def get_shap_data(io, shap_file_selection, ad, type='single', selected_prediction=None):
    shap_path = io.get_model_and_shap_path(ad)
    shap_df = io.read_csv(shap_file_selection, shap_path)
    if type == 'aggregated':
        return aggregate_SHAP_values(shap_df, method='mean')
    else:
        start_time, end_time = split_prediction(selected_prediction)
        return shap_df[start_time:end_time]

def create_horizontal_bar_plot(data, ranking_type, selected_features=None, pred_type=None):
    y_name = "SHAP Ranking" if ranking_type == "SHAP" else "Ranking"
    x = []
    y = []
    for data_entry in data:
        x.append(data_entry[y_name])
        y.append(data_entry["Feature"])

    # reverse data such that most anomalous (highest) ranking is on top
    x.reverse()
    y.reverse()

    if ranking_type == "Histogram":
        # logarithmic plot for better visualization
        x = [math.log(i, 10) for i in x]
        colors = [standard_webapp_color] * len(y)
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln not in selected_features:
                    colors[i] = 'orange'
    elif ranking_type == "SHAP":
        colors = []
        for r in x:
            if r > 0:
                colors.append('green')
            elif r < 0:
                colors.append('red')
        if selected_features is not None:
            for i, coln in enumerate(y):
                if coln in selected_features:
                    colors[i] = 'orange'

    return go.Bar(
        name=pred_type,
        x=x,
        y=y,
        orientation='h',
        marker_line_width=0,
        marker_color=colors)


def get_shap_ranking_data(io, shap_file_selection, ad):
    # aggregate SHAP values of all predictions
    shap_df = get_shap_data(io, shap_file_selection, ad, type='aggregated')
    shap_df = shap_df.to_frame().transpose()

    d = {}
    for coln in shap_df.columns:
        res = shap_df[coln].iloc[0]
        if res != 0:
            d[coln] = res
    d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}

    data = []
    for i, (k, v) in enumerate(d.items()):
        entry = {'Feature': "{}".format(k), 'SHAP Ranking': round(v, 8)}
        data.append(entry)

    return data


def select_features(io, max_nb_features, prediction_types, subsampling_samples, nb_samples, nb_features_to_select, method, add_other_magnets, plot=False):
    ad = "iforest"

    datas = {}
    for pred_type in prediction_types:
            datas[pred_type] = get_shap_ranking_data(io,
                                               "shap_values_iforest_B{}_0.95_y_pred_{}_subsample={}_nb_samples={}_identity.csv".format(
                                                   io.beam, pred_type, subsampling_samples[pred_type], nb_samples[pred_type]), ad)

    selection_type = nb_features_to_select["type"]
    selection_value = nb_features_to_select["value"]

    # read training data to determine legal feature names (for adding magnets)
    df = io.read_csv(io.config["pipeline"]["preprocessing"]["variance_filtered_filename"], io.pipeline_data_path)
    legal_columns = list(df.columns)

    # find max features
    if max_nb_features is not None:
        pred_type, data = list(datas.items())[0]
        pos_features = [i["Feature"] for i in data if i["SHAP Ranking"] > 0]
        nb_features_list = [i for i in range(len(pos_features))]
        if pred_type == "FP" or pred_type == "TN":
            nb_features_list.reverse()
        print("Finding maximal number of features...")
        abs_value = -1
        for j, i in enumerate(nb_features_list):
            curr_selected_features = get_selected_features(io, datas, method, "absolute", i,
                                                   add_other_magnets, legal_columns, verbose=False)
            if len(curr_selected_features) > max_nb_features:
                abs_value = nb_features_list[j-1]
                break
            print(len(curr_selected_features))
        selected_features = get_selected_features(io, datas, method, "absolute", abs_value,
                                                   add_other_magnets, legal_columns, verbose=False)
    else:
        selected_features = get_selected_features(io, datas, method, selection_type, selection_value,
                                                  add_other_magnets, legal_columns, verbose=True)

    # plot
    nb_rows = 1
    nb_cols = len(prediction_types)
    rows = []
    cols = []
    for i in range(nb_rows):
        for j in range(nb_cols):
            rows.append(i + 1)
            cols.append(j + 1)

    if plot:
        figures = []
        for p_t, d in datas.items():
            figures.append(create_horizontal_bar_plot(d, "SHAP", selected_features=list(selected_features), pred_type=p_t))
        fig = ps.make_subplots(rows=nb_rows, cols=nb_cols, subplot_titles=prediction_types)
        fig.update_layout(title="{} | {} | {} Beam {} | {}".format(prediction_types,
                                                                   method,
                                                                   io.start_year,
                                                                   io.beam,
                                                                   ad),
                          title_x=0.5,
                          title_font_size=30,
                          bargap=0,
                          paper_bgcolor='rgba(0,0,0,0)',
                          plot_bgcolor='rgba(0,0,0,0)'
                          )

        fig.add_traces(
            figures,
            rows=rows,
            cols=cols)
        fig.show()
    return list(selected_features)


def get_selected_features(io, datas, method, selection_type, selection_value, add_other_magnets, legal_columns, verbose=False):
    selected_features = set()
    pred_type, data = list(datas.items())[0]
    if verbose:
        print("pred_type: {}".format(pred_type))
        print("# SHAP values: {}".format(len(data)))
    features = [i["Feature"] for i in data]
    pos_features = [i["Feature"] for i in data if i["SHAP Ranking"] > 0]
    neg_features = [i["Feature"] for i in data if i["SHAP Ranking"] < 0]
    if verbose:
        print("# +SHAP values: {}".format(len(pos_features)))
        print("# -SHAP values: {}".format(len(neg_features)))

        print(len(selected_features))
    # regular methods
    if method == "pos_tpfn-neg_fptn":
        if pred_type == "TP" or pred_type == "FN":
            print("Selecting top SHAP values...")
            nb_features = get_nb_features(selection_type, selection_value, max_value=len(pos_features))
            candidate_features = features[:nb_features]
        elif pred_type == "FP" or pred_type == "TN":
            print("Selecting bottom SHAP values...")
            nb_features = get_nb_features(selection_type, selection_value, max_value=len(neg_features))
            candidate_features = features[-nb_features:-1]
    elif method == "pos_tpfn_delete_pos_fptn":
        nb_features = get_nb_features(selection_type, selection_value, max_value=len(pos_features))
        if pred_type == "TP" or pred_type == "FN":
            candidate_features = pos_features[:nb_features]
        elif pred_type == "FP" or pred_type == "TN":
            candidate_features = pos_features[nb_features:]
    elif method == "FP_pos":
        if pred_type == "FP":
            nb_features = get_nb_features(selection_type, selection_value, max_value=len(pos_features))
            candidate_features = features[nb_features:len(pos_features)]
        else:
            candidate_features = selected_features
    selected_features = update_selected_features(selected_features, candidate_features, verbose=False)
    if verbose:
        print(len(selected_features))

    if add_other_magnets:
        # add features from other magnets
        selected_features = add_other_magnets_features(io, selected_features, legal_columns)
    return selected_features

def get_nb_features(selection_type, selection_value, max_value):
    if selection_type == "absolute":
        nb_features = selection_value if selection_value < max_value else max_value
        return nb_features
    elif selection_type == "percentage":
        nb_features = round(max_value * selection_value)
        return nb_features
    else:
        print("Choose 'absolute' or 'percentage' selection type")
        exit()


def update_selected_features(selected_features, candidate_features, verbose=False):
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
        print("\ncandidate")
        for i in candidate_features:
            print(i)
    features_to_select = candidate_features
    selected_features.update(features_to_select)
    if verbose:
        print("\nselected:")
        for i in selected_features:
            print(i)
    return selected_features


def add_other_magnets_features(io, selected_features, legal_columns):
    """
    Adds all features that are the same type, but for a different magnet to the selected features
    Args:
        io: IO object to read the legal features
        selected_features: The current selected features
        legal_columns: All original columns
    """
    print("Adding same features for other magnets...")

    features_to_add = set()
    for coln in selected_features:
        keyword = "B{}".format(io.beam)
        if keyword in coln:
            for magnet_id in LBDS_MAGNETS:
                first = coln.split(keyword)[0]
                second = coln.split(keyword)[1]
                to_add = first[:len(first) - 1] + magnet_id + keyword + second
                if to_add in legal_columns:
                    features_to_add.add(to_add)
    selected_features = selected_features.union(features_to_add)
    print("Features after adding magnets: {}".format(len(selected_features)))
    return selected_features


def run_experiment(cfg, max_nb_features=None, prediction_types=None, nb_features_to_select=None, method=None, subsampling_samples=None, nb_samples=None, add_other_magnets=None, plot=False, update_config=False):
    io = IO(cfg, 'grid_search')

    # initialize default parameters
    if prediction_types is None:
        prediction_types = ['FP']
    # type = absolute or percentage
    if nb_features_to_select is None:
        nb_features_to_select = {'type': 'percentage',
                                 'value': 0.975}
    if method is None:
        #method = "pos_tpfn-neg_fptn"
        method = "FP_pos"
        method = "pos_tpfn_delete_pos_fptn"

    shap_params = cfg['analysis']['shap']['params']
    if subsampling_samples is None:
        subsampling_samples = shap_params['subsampling_samples']
    if nb_samples is None:
        samples = shap_params['nb_samples']
        nb_samples = {'TP': samples, 'FN': samples, 'FP': samples, 'TN': samples}

    if add_other_magnets is None:
        add_other_magnets = True
    selected_features = select_features(io, max_nb_features, prediction_types, subsampling_samples, nb_samples, nb_features_to_select,
                                        method, add_other_magnets, plot=plot)

    # create new config
    if update_config:
        update_keep_cols_config(io, selected_features)

    return selected_features


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="SHAP feature selection")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_experiment(cfg, max_nb_features=200, plot=True, update_config=True)






