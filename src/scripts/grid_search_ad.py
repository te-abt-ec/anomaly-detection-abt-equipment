#!/usr/bin/env python3
import math
import os
import sys
import argparse

import numpy as np
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str, get_training_data_from_file, get_config
from preprocessing.preprocess_modules import drop_columns, drop_columns_regex, keep_columns_list, keep_columns_regex
from IO.IO import IO

np.random.seed(456)


def run_grid_search(cfg, short=False):
    try:
        gs = cfg['grid_search']
        start_time, end_time = gs['time_interval']
        ad = gs['anomaly_detection']
        detector = ad['anomaly_detector']
        preprocess = gs['preprocessing']
        drop_low_variance_cols = preprocess['drop_low_variance_columns']
        max_runtime = gs['max_runtime']
        output_filename = gs['output_filename']
        beam = gs['beam']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    print("GRID SEARCH FOR: {}".format(detector))

    # IO for all read/write operations
    io = IO(cfg, 'grid_search')

    df, state_mode, labels = get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg, use_case='grid_search')
    if df is None or state_mode is None or labels is None:
        print("Problem with getting LBDS data, exiting...")
        exit()
    if gs['logbook_type'] == 'all':
        labels = builder.anomalies(labels)

    # drop columns (also done in local preprocessing, but here for analysis)
    df = drop_columns(df, cfg['local_preprocessing']['drop_columns'])
    df = drop_columns_regex(df, cfg['local_preprocessing']['drop_columns_regex'])
    if cfg['local_preprocessing']['keep_columns']:
        df = keep_columns_list(df, cfg['local_preprocessing']['keep_columns_list'])
        df = keep_columns_regex(df, cfg['local_preprocessing']['keep_columns_regex'])

    feature_selection = [('all', list(df.columns))]

    try:
        gs_config = gs["anomaly_detection"][detector]
        scale_data = gs_config['scale_data']
        segment_score = gs_config['segment_score']
        scoring = gs_config['scoring']
        fs = gs_config['feature_selection']
        feature_selection = [(fs, list(df.columns))]
        detector_params = gs_config['params']
    except KeyError as e:
        print("{} was not found in config file".format(e))
        exit()

    print("Launching grid search with data from ({} - {}), this might take a while..".format(start_time, end_time))

    # PERFORM GRID SEARCH
    results = grid_search(
        feature_selection=feature_selection,
        beam=beam,
        scale_data=scale_data,
        anomaly_detector=detector,
        detector_params=detector_params,
        labels=labels,
        segment_score=segment_score,
        x_train=df,
        state_mode=state_mode,
        io=io,
        scoring=scoring,
        max_runtime=max_runtime,
        filename=output_filename,
        config=cfg,
        short=short
    )

    for res in results:
        if not math.isnan(res['rank']):
            path = 'grid_search_' + beam_to_str(beam) + '_' + detector + '_' + str(int(res['rank'])) + '-best.csv'
            res['truth_and_pred_df'].to_csv()
        else:
            print("Note: One of the resulting grid search AUC values has NaN as output, consider using a bigger time interval to include anomalies")

    if len(results):
        features_res = [res['feature'] for res in results]
        auc_res = [res['auc'] for res in results]
        auprg_res = [res['auprg'] for res in results]
        rank_res = [res['rank'] for res in results]
        practical_recall_res = [res['practical_recall'] for res in results]
        scale = [res['scale'] for res in results]
        time_res = [res['time'] for res in results]
        TP_res = [res['confusion_matrix_stats']['TP'] for res in results]
        FP_res = [res['confusion_matrix_stats']['FP'] for res in results]
        FN_res = [res['confusion_matrix_stats']['FN'] for res in results]
        TN_res = [res['confusion_matrix_stats']['TN'] for res in results]
        id_res = [res['id'] for res in results]
        seed_res = [res['seed'] for res in results]

        parameter_results = {}
        for param in detector_params:
            parameter_results[param] = [res['params'][param] for res in results]

        data = {'auc': auc_res, 'auprg': auprg_res, 'rank': rank_res, 'practical_recall': practical_recall_res,
                'TP': TP_res, 'FN': FN_res, 'FP': FP_res, 'TN': TN_res, 'features': features_res}
        for param, param_res in parameter_results.items():
            data[param] = param_res
        data['scale'] = scale
        data['time'] = time_res
        data['id'] = id_res
        data['seed'] = seed_res

        statistics = pd.DataFrame(data)
        io.write_to_csv(statistics, 'grid_search_statistics_' + beam_to_str(beam) + '_' + detector + output_filename + '.csv', io.grid_search_statistics_path)
        best = statistics.iloc[statistics[scoring].argmax()]
        if scoring == 'rank':
            best = statistics.iloc[statistics[scoring].argmin()]
    return best


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python script to run the pipeline to train an anomaly detector")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    parser.add_argument('--short', dest='short', action='store_true',
                        help='True if only 1 configuration has to run (for CI code check)')
    parser.set_defaults(short=False)

    args = parser.parse_args()
    short = args.short

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = get_config(fn)
    else:
        print("Config file {} couldn't be found at {}, exiting".format(args.configfn, fn))
        exit()

    run_grid_search(cfg, short)

