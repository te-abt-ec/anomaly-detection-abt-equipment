#!/usr/bin/env python3
import argparse
import os
import sys

import numpy as np
import pandas as pd
from datetime import datetime
import shap
import math

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from scripts.pipeline_ad import run_pipeline
import util
from IO.IO import IO
from evaluation import evaluation


def get_all_shap_values(io, shap_params, predictions_to_explain, anomaly_detector, truth_and_pred_df, threshold, segment_score_type, trained_model, scored_training_data):
    """
    Calculates and writes the SHAP values of the given predictions to explain to a csv file.
    Args:
        io: Input output to write the SHAP values to file
        predictions_to_explain: List of types of predictions to explain (TP, FP, TN, FN)
        anomaly_detector: Type anomaly detector
        truth_and_pred_df: The pipeline/grid search output file with predictions
        threshold: The anomaly score threshold
        segment_score_type: The type of scoring of segments
        trained_model: The trained model (dumped with Pickle during pipeline)
        scored_training_data: The training data with anomaly score column (dumped during pipeline)
    """
    print("Getting predictions...")
    TN, TP, FN, FP, predictions_df = evaluation.cluster_predictions(truth_and_pred_df, threshold, segment_score_type)

    segments_to_explain = {}
    for p_type in predictions_to_explain:
        if p_type == "TP":
            segments_to_explain[p_type] = TP
        elif p_type == "FP":
            segments_to_explain[p_type] = FP
        elif p_type == "FN":
            segments_to_explain[p_type] = FN
        elif p_type == "TN":
            segments_to_explain[p_type] = TN

    shap_v = {}
    scored_training_data['timestamps'] = scored_training_data.index
    for p_type, segment_list in segments_to_explain.items():
        ts_to_select = []
        # create prediction data as a subset of the training data and select the one with max anomaly score (aggregation like in postprocessing)
        prediction_data = scored_training_data[segment_list[0].ts_min():segment_list[0].ts_max()]
        ts = prediction_data.iloc[prediction_data['score'].argmax()]['timestamps']
        ts_to_select.append(ts)

        for s in segment_list[1:]:
            new_pred_rows = scored_training_data[s.ts_min():s.ts_max()]
            if len(new_pred_rows.index) == 0:
                print("train data did not contain the given prediction timestamp, use the same time_intervals for the train dataframe and prediction dataframe")
            ts = new_pred_rows.iloc[new_pred_rows['score'].argmax()]['timestamps']
            ts_to_select.append(ts)

        # select the maximal anomalous rows per segment
        prediction_data = scored_training_data.loc[ts_to_select]
        prediction_data = prediction_data.drop('score', axis=1)
        prediction_data = prediction_data.drop('timestamps', axis=1)
        training_data = scored_training_data.drop('score', axis=1)
        training_data = training_data.drop('timestamps', axis=1)
        shap_df = calculate_shap_values(shap_params, p_type, trained_model, training_data, prediction_data, anomaly_detector)
        shap_v[p_type] = shap_df

        print("writing {} SHAP values".format(p_type))
        io.dump_shap_values(shap_df, anomaly_detector, p_type, segment_score_type, threshold, shap_params)


def calculate_shap_values(shap_params, p_type, trained_model, train_data, data_to_explain, anomaly_detector):
    """
    Calculates the SHAP values for selected feature vectors given the training data and trained model.
    Subsampling info:
    The subsampling is random with replacement (scikit-learn default):
    https://github.com/slundberg/shap/blob/9411b68e8057a6c6f3621765b89b24d82bee13d4/shap/utils/_general.py#L166
    Args:
        trained_model: The trained model
        train_data: The train data
        data_to_explain: The selected feature vectors to explain
        anomaly_detector: The anomaly detector type used
    """
    start = datetime.now()
    index = data_to_explain.index

    if anomaly_detector == 'iforest':
        model_predict_f = iforest_anomaly_scores_wrapper(trained_model)
        link = shap_params['link']['iforest']
    elif anomaly_detector == 'gmm':
        model_predict_f = gmm_anomaly_scores_wrapper(trained_model)
        link = shap_params['link']['gmm']
    elif anomaly_detector == 'hbos':
        model_predict_f = hbos_anomaly_scores_wrapper(trained_model)
        link = shap_params['link']['hbos']

    print_data_info(train_data, data_to_explain)

    # reduce in case of MemoryError:
    #  Subsampling amount
    samples = shap_params['subsampling_samples'][p_type]
    #  Number of times to re-evaluate the model when explaining each prediction
    nsamples = shap_params['nb_samples']

    print("Reducing the large training dataset (k-means or subsampling) to n={}..".format(samples))
    # Info on sampling in the definition header
    train_data_red = shap.sample(train_data, samples)
    print_data_info(train_data_red, data_to_explain)

    # use Kernel SHAP from shap library to explain selected feature vectors
    explainer = shap.KernelExplainer(model_predict_f, train_data_red, link=link, keep_index=True)  # link = logit / identity
    print("Calculating SHAP values")
    shap_values = explainer.shap_values(data_to_explain, nsamples=nsamples, keep_index=True)

    shap_df = pd.DataFrame(shap_values)
    shap_df['timestamps'] = index
    shap_df.set_index('timestamps', drop=True, inplace=True)
    shap_df.columns = data_to_explain.columns

    end = datetime.now()
    print("total time to calculate SHAP values: {}".format(end - start))
    return shap_df


def print_data_info(train_data, data_to_explain):
    print("Training data dimensions: {}".format(train_data.shape))
    print("Training data date range: {} to {}".format(train_data.index[0], train_data.index[-1]))
    print("Data to explain dimensions: {}".format(data_to_explain.shape))
    print("Data to explain date range: {} to {}".format(data_to_explain.index[0], data_to_explain.index[-1]))


def iforest_anomaly_scores_wrapper(trained_model):
    def iforest_anomaly_scores(df):
        scores = trained_model.iforest.decision_function(df)
        scores = -scores
        return scores
    return iforest_anomaly_scores


def gmm_anomaly_scores_wrapper(trained_model):
    def gmm_anomaly_scores(df):
        scores = trained_model.gmm.score_samples(df)
        scores = -scores
        return scores
    return gmm_anomaly_scores


def hbos_anomaly_scores_wrapper(trained_model):
    def hbos_anomaly_scores(df):
        return trained_model.hbos.predict_proba(df)[:, 1]
    return hbos_anomaly_scores


def run_shap_values(cfg, shap_params=None):
    try:
        shap_cfg = cfg['analysis']['shap']
        mode = shap_cfg['mode']
        if shap_params is None:
            shap_params = shap_cfg['params']
        prediction_sets_to_explain = shap_cfg['predictions']
        anomaly_detector = cfg[mode]['anomaly_detection']['anomaly_detector']

        selected_cfg = cfg[mode]
        threshold = selected_cfg['anomaly_detection'][anomaly_detector]['threshold']
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    # create IO
    io = IO(cfg, 'pipeline')

    if mode == 'pipeline':
        # default mode
        truth_and_pred_df = io.get_pred_with_params(anomaly_detector)
    elif mode == "grid_search":
        # grid search mode (best performing predictions file)
        truth_and_pred_df = io.read_csv(io.get_grid_search_best_output_filename(anomaly_detector),
                                        io.get_grid_search_output_path(anomaly_detector))
    segment_score_type = util.get_pred_to_use(selected_cfg['anomaly_detection'][anomaly_detector]['segment_score'])

    training_results = {
        "trained_model": io.read_pipeline_model(anomaly_detector),
        "scored_training_data": io.read_scored_training_data()
    }

    prediction_parameters = {
        "truth_and_pred_df": truth_and_pred_df,
        "threshold": threshold,
        "segment_score_type": segment_score_type
    }

    predictions_to_explain = []
    for k, v in prediction_sets_to_explain.items():
        if v:
            predictions_to_explain.append(k)

    print("Explaining predictions".format(predictions_to_explain))
    print("calculating SHAP values")
    get_all_shap_values(io, shap_params, predictions_to_explain, anomaly_detector, **prediction_parameters, **training_results)
    print("Done")


if __name__ == '__main__':
    """
    Script to calculate the SHAP values of chosen predictions in the configuration file.
    See https://github.com/slundberg/shap for detailed info.
    The pipeline or grid_search should first be run before running this script to generate the necessary trained model and (scored) train data.
    """
    parser = argparse.ArgumentParser(description="Python script to calculate the SHAP values of the trained detector with given config file")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for pipeline run')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = util.get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    run_shap_values(cfg)


