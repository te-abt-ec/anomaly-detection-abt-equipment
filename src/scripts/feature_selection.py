#!/usr/bin/env python3
import argparse
import os
import sys

import numpy as np
import pandas as pd
from datetime import datetime
import shap
import math

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import util
from IO.IO import IO
from scripts.experiments.shap_feature_selection import run_experiment as shap_fs
from scripts.experiments.histogram_feature_selection import run_experiment as histogram_fs



def run_feature_selection(cfg):
    try:
        fs_type = cfg["feature_selection"]["type"]
        fs_params = cfg["feature_selection"][fs_type]
        pred_types = fs_params["prediction_types"]
        method = fs_params["method"]
        nb_features_to_select = fs_params["nb_features_to_select"]
        add_other_magnets = fs_params["add_other_magnets"]
        plot = fs_params["plot_result"]
    except KeyError as e:
        print("Config file ({}) is missing configuration variable {}".format(fn, e))
        exit()

    if fs_type == "SHAP":
        shap_fs(cfg,
                prediction_types=pred_types,
                nb_features_to_select=nb_features_to_select,
                method=method,
                add_other_magnets=add_other_magnets,
                plot=plot,
                update_config=True)
    elif fs_type == "histogram":
        histogram_fs(cfg,
                prediction_types=pred_types,
                nb_features_to_select=nb_features_to_select,
                method=method,
                add_other_magnets=add_other_magnets,
                plot=plot,
                update_config=True)


if __name__ == '__main__':
    """
    Script to select features based on the parameters in the configuration file.
    See README for detailed information.
    SHAP value rankings and/or histogram score rankings need to be created before running this script.
        - SHAP values can be generated with src/scripts/shap_values.py
        - Histogram values can be generated through the webapp (See webapp README)
    """
    parser = argparse.ArgumentParser(description="Python script perform feature selection with given config file")
    parser.add_argument('-c', dest='configfn', default='config_lbds.yaml',
                        help='config file for feature selection parameters')
    args = parser.parse_args()

    # absolute path to the config folder
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))
    fn = path + '/' + args.configfn
    if os.path.exists(fn):
        cfg = util.get_config(fn)
    else:
        f_name = fn.split('/')[-1]
        print("Config file {} couldn't be found at {}, exiting".format(f_name, fn))
        exit()

    run_feature_selection(cfg)