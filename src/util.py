import glob
import json
import logging
import os
from base64 import urlsafe_b64encode
from operator import itemgetter
from urllib.parse import urlencode
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
import yaml
import re

from preprocessing import variance_preprocessing as vprep
from analysis.dump_cols import dump_cols_to_txt, dump_cols_delta
import random

CERN_DIR = os.path.join(os.path.dirname(__file__), "..")
CERN_DATA_DIR = os.path.join(CERN_DIR, "data-cern")
RAW_DATA_DIR = os.path.join(CERN_DATA_DIR, "ml-data/raw")
PREPROCESSED_DATA_DIR = os.path.join(CERN_DATA_DIR, "ml-data/preprocessed")
PIPELINE_DATA_DIR = os.path.join(CERN_DATA_DIR, "ml-data/pipeline")
RESULTS_DIR = os.path.join(CERN_DIR, "results")
CERN_LABELS = ["info", "intervention", "research", "fault", "anomaly"]

DETECTORS = ["gmm", "iforest", "hbos"]

# Earliest and latest measurement timestamp common to all collections
DATETIME_EARLIEST = "2016-03-10 00:00:00"
DATETIME_LATEST = "2019-01-01 00:00:00"

# Earliest and latest date used for training
TRAINTIME_EARLIEST = "2017-05-01 00:00:00"
TRAINTIME_LATEST = "2017-08-01 00:00:00"

# Used for saving figures
FILE_EXTENSION = ".png"

# Timestamp column used as index in spark data
# TODO: delete this and change all remaining usages to INDEX_COL in config
INDEX_COL = 'acqStamp'

# Use case options for the get_data_from_file function
USE_CASES = ['grid_search', 'pipeline']

# datasets
DATASET_TYPES = {
    "MKI": ["IPOC", "SCSS", "BEAM", "BUNCH", "KITS"],
    "LBDS": ["IPOC", "SCSS", "BEAM", "BUNCH", "BEI", "BEM", "TSU", "XPOC"]
}


def beam_to_num(beam):
    """
    TODO: Add "machine" argument to define the beam numbers for every machine (MKI, LBDS)
    MKI 1: UA23
        2: UA87
    LBDS 1: UA63
         2: UA67
    """
    if beam == 1:
        return "UA23"
    elif beam == 2:
        return "UA87"
    else:
        raise ValueError('Error: beam should be 1 or 2.')

def temp_beam_to_num(beam, machine):
    """
    MKI 1: UA23
        2: UA87
    LBDS 1: UA63
         2: UA67
    """
    if machine == 'MKI':
        if beam == 1:
            return "UA23"
        elif beam == 2:
            return "UA87"
        else:
            raise ValueError('Error: beam should be 1 or 2.')
    elif machine == 'LBDS':
        if beam == 1:
            return "UA63"
        elif beam == 2:
            return "UA67"
        else:
            raise ValueError('Error: beam should be 1 or 2.')

def beam_to_str(beam):
    if beam == 1:
        return "B1"
    elif beam == 2:
        return "B2"
    else:
        raise ValueError('Error: beam should be 1 or 2.')


def num_to_pred(num):
    if num == 0:
        return 'TP'
    elif num == 1:
        return 'TN'
    elif num == 2:
        return 'FP'
    elif num == 3:
        return 'FN'
    else:
        raise ValueError('Only 0, 1, 2 and 3 are supported.')

def get_pred_to_use(segment_score):
    pred_to_use = 'y_pred'
    if segment_score == "top_k":
        pred_to_use = "y_pred_top_k"
    if segment_score == "top_percentage":
        pred_to_use = "y_pred_top_percentage"
    return pred_to_use

def set_seed(seed):
    if seed is None:
        np.random.seed(456)
        random.seed(456)
    else:
        np.random.seed(seed)
        random.seed(seed)

def get_prediction_data_value(machine, col_data, scored_train_data, selected_prediction):
    """
    Locates the value of given prediction segment in the feature data (col_data).
    If the segment consists of multiple rows, the value with the highest anomaly score is returned
    (if multiple rows have the same maximal anomaly score, the first row is selected).
    Otherwise, the value of the single row is returned.
    Args:
        machine: The used machine
        col_data: The data of the feature to get the value from
        scored_train_data: Series of anomaly scores (index is timestamps)
        selected_prediction: The start and end timestamp of the prediction segment to get the value from
    """
    start = selected_prediction.split(' to ')[0]
    end = selected_prediction.split(' to ')[1]
    if len(scored_train_data[start:end]) == 0:
        print("No scoring data for prediction {}".format(selected_prediction))
        return None
    max_score_timestamp = scored_train_data[start:end]['score'].idxmax()
    if machine == "LBDS":
        value = col_data.iloc[col_data.index.get_loc(max_score_timestamp)]
        if type(value) is np.ndarray:
            return value[:1]
        else:
            return value
    elif machine == "MKI":
        #TODO: a good aggregator for mki segments (currently mean)
        return col_data[start:end].mean()

# maximum distance in HOURS between anomaly occurring and label being entered in the ELOGBOOK
MAX_LOGGING_DELAY = 12


# interval in which an anomaly occurred in HOURS:
# ANOMALY_OCCURRING_INTERVAL[0] is the number of HOURS to look into the past from the ELOGBOOK (detection) timestamp
# ANOMALY_OCCURRING_INTERVAL[1] is the number of HOURS to look into the future from the ELOGBOOK (detection) timestamp
ANOMALY_OCCURRING_INTERVAL = (12, 2)

# Sliding window size = 10 minutes recommended by Pieter: Dit is rekening houdend met een spanningsoverslag
# anomalie waar het tot 10 minuten kan duren voor het vacuüm zich herstelt; langer vraagt het normaal gezien niet.
SLIDING_WINDOW_SIZE = 10 * 60

# The maximal amount of predictions that can be labeled positive
POSITIVES_LIMIT = 100

MAGNETS = "(A|B|C|D)"
LBDS_MAGNETS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"]
BEAMS = [1, 2]

# arrays of (query, measurement_name, unit)
MEASUREMENTS_LHC = [
    (r"^LHC\.BCTFR\.A6R4.B(1|2):BEAM_INTENSITY$", "BEAM_INTENSITY", "Total charge"),
    (r"^LHC\.BQM\.B(1|2):BUNCH_LENGTH_MEAN$", "BUNCH_LENGTH_MEAN", "s")
]

MEASUREMENTS_CONTINUOUS = [
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):PRESSURE$", "PRESSURE", "mbar"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_MAGNET_DOWN$", "TEMP_MAGNET_DOWN", "°C"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_MAGNET_UP$", "TEMP_MAGNET_UP", "°C"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_TUBE_DOWN$", "TEMP_TUBE_DOWN", "°C"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_TUBE_UP$", "TEMP_TUBE_UP", "°C")
]

MEASUREMENTS_CONTROLLER = [
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_COUNT_TOPLAY$", "KICK_COUNT_TOPLAY", "???"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_DELAY_TOPLAY$", "KICK_DELAY_TOPLAY", "ns"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_ENABLE_TOPLAY$", "KICK_ENABLE_TOPLAY", "???"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_LENGTH_TOPLAY$", "KICK_LENGTH_TOPLAY", "ns"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_STRENGTH_TOPLAY$", "KICK_STRENGTH_TOPLAY", "V"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_TIME_TOPLAY$", "KICK_TIME_TOPLAY", "ns"),
]

MEASUREMENTS_IPOC = [
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):E_KICK$", "E_KICK", "GeV"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):I_STRENGTH$", "I_STRENGTH", "kA"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_DELAY$", "T_DELAY", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_FALLTIME$", "T_FALLTIME", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_LENGTH$", "T_LENGTH", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_RISETIME$", "T_RISETIME", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_START_TH$", "T_START_TH", "μs"),
]

MEASUREMENTS_STATE = [
    (r"^MKI\.UA(23|87)\.STATE:CONTROL$", "CONTROL", None),
    (r"^MKI\.UA(23|87)\.STATE:MODE$", "MODE", None),
    (r"^MKI\.UA(23|87)\.STATE:SOFTSTARTSTATE$", "SOFTSTARTSTATE", None),
    (r"^MKI\.UA(23|87)\.STATE:STATUS$", "STATUS", None),
]


def load_df_from_csv_with_name(filename):
    """
    Returns a DataFrame loaded from a certain csv file. If filename is not found exactly but as part of other files,
    returns the one that was modified latest.
    :param filename: string, name of the file
    :return: DataFrame built from a csv file
    """
    print("Loading file '{}'.".format(filename))

    if not os.path.exists(filename):
        alternatives = glob.glob(filename + "*")
        paths_sorted = sorted([(f, os.path.getmtime(f)) for f in alternatives], key=itemgetter(1))
        if len(paths_sorted):
            filename = paths_sorted[-1][0]
            print("File not found, loading '{}' instead.".format(filename))

    return filename, pd.read_csv(filename, index_col=0, parse_dates=True)


def load_df_from_csv(filename):
    _, df = load_df_from_csv_with_name(filename)
    return df


def list_predictions_old(machine):
    if machine == 'MKI':
        path = './../../results/'
        files = glob.glob(path + 'grid_search_*.csv')
        files.extend(glob.glob(path + 'pipeline_*.csv'))
        files.extend(glob.glob(path + 'feature_selection_*.csv'))
    elif machine == 'LBDS':
        # moved to main results folder
        path = './../../data-cern/gmm_results/'
        files = glob.glob(path + 'grid_search_*.csv')
        files.extend(glob.glob(path + 'pipeline_*.csv'))
        files.extend(glob.glob(path + 'feature_selection_*.csv'))
        path = './../../data-cern/iforest_results/'
        files.extend(glob.glob(path + 'grid_search_*.csv'))
        path = './../../results/'
        files.extend(glob.glob(path + 'grid_search_*.csv'))
        # Remove statistics files from predictions list
        files = list(filter(lambda x: 'statistics' not in x, files))
    return sorted(files)


def list_predictions(machine, year, beam):
    files = []
    for d in DETECTORS:
        path = RESULTS_DIR + "/{}_results/{}/{}/best/".format(d, machine.lower(), year)
        files.extend(glob.glob(path + '*.csv'))
    # filter statistics
    files = list(filter(lambda x: 'statistics' not in x, files))
    # filter on beam
    beam_ff = get_beam_filter_function(machine, beam)
    files = list(filter(beam_ff, files))
    return sorted(files)

def list_shap_files(io, beam):
    files = []
    for d in DETECTORS:
        f_path = io.get_model_and_shap_path(d)
        files.extend(glob.glob(f_path + 'shap_values_*.csv'))
    # filter on beam
    beam_ff = get_beam_filter_function(io.machine, beam)
    files = list(filter(beam_ff, files))
    return sorted(files)


def first(iterable, default=None, key=None):
    if key is None:
        for el in iterable:
            if el:
                return el
    else:
        for el in iterable:
            if key(el):
                return el
    return default


def log_and_print(text, **kwargs):
    logging.info(text)
    print(text, **kwargs)


def generate_params(state):
    encoded = urlsafe_b64encode(json.dumps(state).encode())
    params = urlencode(dict(params=encoded))
    return f'?{params}'


def get_html_url_for_timespan(filename, timestamp_min, timestamp_max, beam=1):
    if beam == 1:
        cols1 = ['MKI.A5L2.B1:PRESSURE', 'MKI.B5L2.B1:PRESSURE', 'MKI.C5L2.B1:PRESSURE', 'MKI.D5L2.B1:PRESSURE']
        cols2 = ['MKI.A5L2.B1:TEMP_MAGNET_UP', 'MKI.B5L2.B1:TEMP_MAGNET_UP',
                 'MKI.C5L2.B1:TEMP_MAGNET_UP', 'MKI.D5L2.B1:TEMP_MAGNET_UP']
    else:
        cols1 = ['MKI.A5R8.B2:PRESSURE', 'MKI.B5R8.B2:PRESSURE', 'MKI.C5R8.B2:PRESSURE', 'MKI.D5R8.B2:PRESSURE']
        cols2 = ['MKI.A5R8.B2:TEMP_MAGNET_UP', 'MKI.B5R8.B2:TEMP_MAGNET_UP',
                 'MKI.C5R8.B2:TEMP_MAGNET_UP', 'MKI.D5R8.B2:TEMP_MAGNET_UP']

    state = {
        'beam-dropdown': (['value'], (1,)),
        'dataset1-dropdown': (['value'], (cols1,)),
        'dataset2-dropdown': (['value'], (cols2,)),
        'date-picker': (['start_date', 'end_date'], (timestamp_min, timestamp_max)),
        'predictions': (['value'], ('./' + filename,)),
        'segment-score-method': (['value'], ('max',)),
    }
    return '<a href="http://127.0.0.1:8050/{0}">Open in explorer</a>'.format(generate_params(state))


def get_config(fn):
    """
    Load config file
    Args:
        fn (str): file name of the config to load
        path (str): path to location of config file
    """
    print("Reading config file")
    with open(fn, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    return cfg


def get_training_data_from_file(io, drop_low_variance_cols, start_time, end_time, cfg, eps=None, use_case='pipeline'):
    """
    Get all data to run the pipeline or grid search
    Args:
        drop_low_variance_cols (bool): preprocess feature vectors by applying column filters if True
        start_time (str): start_time of time interval where data is selected from
        end_time (str): end_time of time interval where data is selected from
        cfg: The parsed config file
        use_case (str): 'pipeline' or 'grid_search'
    """
    print("Started process of loading data")

    if use_case not in USE_CASES:
        print("Invalid use case, please use one of the following: {}".format(USE_CASES))
        return
    try:
        # LOAD FEATURES
        print("Loading feature vectors")
        f_path = io.preprocessed_data_path
        try:
            filename = cfg['local_preprocessing']['train_data_filename']
            if '{}' in filename:
                filename = filename.format(io.beam)
            use_cached_file = cfg['pipeline']['preprocessing']['use_cached_file']
            if use_cached_file:
                f_path = io.pipeline_data_path
                filename = cfg[use_case]['preprocessing']['variance_filtered_filename']
                filename = filename.format(io.beam) if "{}" in filename else filename
            pl = cfg['pipeline']
            beam = cfg[use_case]['beam']
            beam_filter = cfg[use_case]['beam_filter']
            if beam_filter:
                print("Using data from beam {}".format(beam))
            INDEX_COL = cfg[use_case]['index']
            seg_file = cfg["spark"]['segmentation']['local_file']
            seg_type = cfg["spark"]['segmentation']['type']
            dump_cols = pl['dump_colnames']
            features_to_keep_regexes = cfg["pipeline"]["preprocessing"]["features_to_keep_during_variance_regex"]
        except KeyError as e:
            print("Config file is missing configuration variable {}".format(e))
            exit()

        print("Reading input features from {}".format(f_path))
        df = io.read_csv(filename, f_path)
        print("First and last timestamp of input data: {} {}".format(df.index[0], df.index[-1]))
        print()

        if dump_cols:
            dump_path = CERN_DATA_DIR + '/analysis/dumps/pipeline/'
            dump_cols_to_txt(df, "{}_1_pipeline_input.txt".format(cfg['machine']), dump_path)

        cols = list(df)
        if beam_filter:
            print("Filtering data on beam {}, columns: {}".format(str(beam), df.shape[1]))
            #TODO: move hardcoded "LHC" to config
            mask = list(filter(lambda col: beam_to_str(beam) in col or "LHC" + str(beam) in col or temp_beam_to_num(beam, cfg["machine"]) in col, cols))
            if INDEX_COL in cols:
                mask.append(INDEX_COL)
            df = df[mask]
            print("Filtered data on beam {}, filtered {} columns, remaining columns: {}".format(str(beam), len(cols)-df.shape[1], df.shape[1]))
        if INDEX_COL in cols:
            # sort not really necessary (done in local preprocessing)
            print("Setting index to {}...".format(INDEX_COL))
            df = df.set_index(INDEX_COL, drop=True).sort_index()
        else:
            print("The column {} is not present in dataframe {}".format(INDEX_COL, filename))
        if df.index.dtype != 'object':
            df.index = pd.to_datetime(df.index, unit='s')
        else:
            df.index = pd.to_datetime(df.index)

        if dump_cols:
            dump_cols_to_txt(df, "{}_2_after_beam_filter.txt".format(cfg['machine']), dump_path)
        if str(df.index[0]) > end_time or str(df.index[-1]) < start_time:
            print("The period ({} - {}) does not fall within ({} - {})".format(start_time, end_time, df.index[0],
                                                                               df.index[-1]))
            print("Training will now happen on unfiltered (time interval) feature vectors.")
        else:
            df = df.loc[start_time: end_time]
        print("Succesfully loaded features with dimensions {}".format(df.shape))

        # Drop low variance columns
        if drop_low_variance_cols and not use_cached_file:
            # determine columns to keep through the variance filter
            features_to_keep_variance = get_columns_from_regexes(df.columns, features_to_keep_regexes)
            if eps is None:
                eps = float(cfg[use_case]['preprocessing']['variance_threshold'])
            print("Starting variance filtering...")
            df = vprep.variance_preprocessing(df, eps, cols_to_keep=features_to_keep_variance)
            print("Done with variance filtering")
            if dump_cols:
                dump_cols_to_txt(df, "{}_3_after_variance_filter.txt".format(cfg['machine']), dump_path)


        # LOAD SEGMENT DATA
        print("Working with \"{}\" segmentation type... ".format(seg_type), end="")
        if seg_type == 'statemode_based':
            if "{}" in seg_file:
                seg_file = seg_file.format(beam_to_str(beam))
            print("Loading segment timestamps from {} file".format(seg_file))
            # Statemode based segmentation (MKI)
            state_mode = pd.read_csv(CERN_DATA_DIR + '/' + seg_file)
        elif seg_type == 'timestamp_based':
            print("No segmentation necessary, using timestamps as \"segments\"")
            # Timestamp based segmentation
            # state_mode automatically takes on the date range of the feature vectors
            state_mode = pd.DataFrame(df.index)
        else:
            print("The segmentation type: {}, is not supported. Please use \"statemode_based\" or \"timestamp_based\"".format(seg_type))
            exit()
        # Setting timestamp column as index to comply with segment creation algorithm
        if INDEX_COL in state_mode:
            state_mode = state_mode.set_index(INDEX_COL)
            state_mode.index = pd.to_datetime(state_mode.index)
        print("done!")
        print("Succesfully loaded STATE_MODE with dimensions {}".format(state_mode.shape))
    except FileNotFoundError as err:
        print("ERROR - missing {} or {}".format(fn, seg_file))
        # error can reside in loading of state mode file, so print the entire message just to be sure
        print(err)
        return

    try:
        # LOAD LOGBOOK
        logbook_f_name = cfg[use_case]['logbook']
        f_path = CERN_DATA_DIR + '/'
        print("Loading logbook data from {} ...".format(logbook_f_name), end='')
        logbook = pd.read_csv(f_path + logbook_f_name)
        # Set start time as logbook index
        logbook = logbook.set_index(list(logbook)[0])
        logbook.index = pd.to_datetime(logbook.index)
        # Only keep anomalies for current beam (if this column exists)
        if 'BEAM' in logbook:
            logbook = logbook[logbook['BEAM'] == beam]
        elif 'Beam' in logbook:
            logbook = logbook[logbook['Beam'] == beam]
        else:
            print("No beam specified for logbook anomalies, assuming all labeled anomalies are for current beam")
        # Only keep anomalies between the given time interval (-12h,+12h)
        start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S.%f') - timedelta(hours=12)
        end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S.%f') + timedelta(hours=12)
        logbook = logbook.loc[start_time: end_time]
        print("done!")
    except FileNotFoundError:
        print("ERROR - missing {}, continuing without labels".format(logbook_f_name))
        logbook = None
    print("Finished loading data")
    return df, state_mode, logbook


def get_beam_filter_function(machine, beam):
    return lambda name: ('B' + str(beam) + '_' in name or
                 temp_beam_to_num(beam, machine) + '.' in name or
                 'LHC' + str(beam) + '_' in name)


def get_cols_for_beam(beam, cols, machine):
    """
    Get subset of columns corresponding to the given beam
    Args:
        beam (int): beam number
        cols (list): list of all column names
        machine (string): The machine to take into consideration
    """
    beam_f = get_beam_filter_function(machine, beam)
    return list(filter(beam_f, cols))


def get_parameters_string(scale_data, segment_score, parameters):
    """
    Returns a string with the used detector parameters for logging purposes
    """
    s = "_scale_data={}_segment_score={}".format(scale_data, segment_score)
    for k, v in parameters.items():
        s = s + '_' + str(k) + '=' + str(v)
    return s


def get_columns_from_regexes(columns, regexes):
    """
    Returns all names from columns that are in the mask of the regexes
    """
    if regexes is None or len(regexes) == 0:
        return columns
    m = []
    for regex in regexes:
        mask = get_mask_for_regex(list(columns), regex)
        for c in mask:
            if c not in m:
                m.append(c)
    return m


def get_mask_for_regex(cols, regex):
    search = lambda x: re.search(regex, x)
    mask = list(filter(search, cols))
    return mask


def get_start_year(time_interval):
    return time_interval[0].split("-")[0]


def get_end_year(time_interval):
    return time_interval[1].split("-")[0]


def get_start_month(time_interval):
    return time_interval[0].split("-")[1]


def get_end_month(time_interval):
    return time_interval[1].split("-")[1]


def add_entry_to_metadata(metadata, entry):
    """
    Adds a new pipeline result entry to the results metadata.
    Overwrites the id with same parameters such that no duplicate parameter sets are in the metadata.
    Args:
        metadata (dictionary): The metadata with keys the unique id's (uuid4) and values the used pipeline parameters
        entry ([id, params]): The new entry that has to be added
    Returns:
        (dictionary): The metadata
        (string): The id that has been deleted
    """
    id, yaml_params_data = entry
    for k, v in metadata.items():
        if v == yaml_params_data:
            metadata[id] = yaml_params_data
            del metadata[k]
            return metadata, k
    metadata[id] = yaml_params_data
    return metadata, None


def get_best_truth_and_pred_df(io, ad):
    f_path = RESULTS_DIR + "/{}_results/{}/{}/best/".format(ad, io.machine.lower(), get_start_year(io.time_interval))
    f_name = "grid_search_{}_B{}_{}_auc_res-best.csv".format(io.machine, io.beam, ad)
    df = io.read_csv(f_name, f_path)
    return df
