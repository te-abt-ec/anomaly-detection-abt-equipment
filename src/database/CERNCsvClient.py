import pandas as pd

import util
from database.CERNClient import CERNClient


class CERNCsvClient(CERNClient):
    """
    Client for fetching CERN data using csv file
    """

    def __init__(self, data=None, logbook=None, logbook_index='timestamps', logging=True):
        """
        :param data: path to csv file containing CERN MKI data
        :param logbook: path to logbook file
        """
        self.db = pd.read_csv(data) #is this enough? was the only mention of json in entire file
        print("Read feature vectors with dimensions {}".format(self.db.shape))
        self.db.dropna(how='all', axis=1, inplace=True)
        self.db.set_index('acqStamp', inplace=True)
        self.db.index = pd.to_datetime(self.db.index, unit='s')
        self.db.sort_index(inplace=True)

        self.logbook = pd.read_csv(logbook, index_col=logbook_index)
        self.logbook.sort_index(inplace=True)

        self.logging = logging

    def get_all_collections(self):
        """
        :return: list of strings with all column names
        """
        pass

    def query(self, pattern, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, filters=False, resample_to=None,
              fillna=True):

        # make sure that start and end dates are strings
        start = str(start)
        end = str(end)

        if self.logging:
            print("Fetching data for pattern {0} from {1} to {2}".format(pattern, start, end))

        collection = self.db.filter(regex=pattern)
        df = self._query_collection(collection, start, end)
        df = df[~df.index.duplicated(keep='first')]  # drop duplicates, needed for overlapping archives

        # drop data 2018 with missing IPOC measurements
        missing_ipoc_indices = ['2018-11-11 15:12:21.949000120',
                                '2018-10-22 08:31:48.134000063',
                                '2018-10-30 10:02:20.870000124']
        for i in missing_ipoc_indices:
            if i in df.index:
                df.drop([pd.to_datetime(i)], inplace=True)

        # Apply filters on whole DataFrame instead of separate columns, because some IPOC measurements are filtered
        # by filters for other IPOC measurements since IPOC measurement timestamps are equal within one beam.
        #if filters:
        #    df = filter_extremes.filter_extremes(df)
        # Always resample after the data has been filtered. Otherwise, incorrect values will be added to the results.
        # Never resample when IPOC data is mixed with continuous data. IPOC data should not be resampled.
        #if resample_to is not None:
        #    df = df.resample(resample_to).mean()

        if self.logging:
            print()

        if fillna:
            # Hold values in case of NaN, first forwards then backwards in case first value is NaN
            df = df.fillna(method='ffill').fillna(method='backfill')

        return df

    @staticmethod
    def _query_collection(collection, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST):
        """
        Queries a collection
        :param collection_name: subset of database data
        :param start: first entry according to start_time field
        :param end: last entry (not included) according to start_time field
        :return: resampled dataframe, columns is collection
        """

        if collection.shape[0] == 0:
            print("Error: Empty or non-existing collection!")
        timestamps = []
        values = []
        # Set the start a week back, because the current start could lie in a document of the week
        # before start that contains data beyond start. Similarly, set the end a week later, because the current end
        # could lie somewhere in the beginning of the next document ('start-time' isn't always correct, or something
        # could be off about the timezone of the timestamps)
        # Afterwards, trim the results to the correct range.
        query_start_datetime = str(pd.to_datetime(start) - pd.DateOffset(weeks=1))
        query_end_datetime = str(pd.to_datetime(end) + pd.DateOffset(weeks=1))
        df = collection.loc[query_start_datetime: query_end_datetime]

        df.rename_axis('timestamps', inplace=True)
        df.rename_axis("series", axis="columns", inplace=True)
        # Filter out data outside of the given datetime interval. Data given by the find query can lie outside this
        # interval because the documents can contain data for a whole week.
        df = df[start:end]
        return df

    def query_elogbook(self, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, beam=None):
        # Filter out with start and end.
        logbook = self.logbook.loc[start:end]

        # Add beam number.
        if "BEAM" not in logbook.columns:
            logbook["BEAM"] = [1 if installation == "MKI2" else 2 for installation in logbook["VALUE"]]

        # Hardcoded fix for C0 column which occurs in certain logs
        if 'C0' in logbook.columns:
            logbook = logbook.drop(["C0"], axis=1)

        if beam == 1:
            return logbook[logbook["VALUE"] == "MKI2"]
        elif beam == 2:
            return logbook[logbook["VALUE"] == "MKI8"]
        else:
            return logbook
