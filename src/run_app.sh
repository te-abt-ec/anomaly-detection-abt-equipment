#!/usr/bin/env bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 [lbds|mki] <options [-d] download remote data; [-ci] ci compatible script>"
    exit 1
fi

# read args
MACHINE="$1"
if [[ "$2" == "-d" ]]; then
  DOWNLOAD=true
fi

echo "Starting the app..."
if [ "$MACHINE" == "mki" ]; then
    if [ $DOWNLOAD ]; then
      pipenv run data-cern/download_features.sh mki
    fi
    echo "RUN_APP starting PREPROCESSING"
    pipenv run src/scripts/preprocess_features.py -c config_mki.yaml
    echo "RUN_APP starting PIPELINE"
    pipenv run src/scripts/pipeline_ad.py -c config_mki.yaml
    echo "RUN_APP starting WEBAPP"
    pipenv run src/webapp/wsgi.py -c config_mki.yaml
elif [ "$MACHINE" == "lbds" ]; then
    if [ $DOWNLOAD ]; then
      pipenv run data-cern/download_features.sh lbds
    fi
    echo "RUN_APP starting PREPROCESSING"
    pipenv run src/scripts/preprocess_features.py -c config_lbds.yaml
    echo "RUN_APP starting PIPELINE"
    pipenv run src/scripts/pipeline_ad.py -c config_lbds.yaml
    echo "RUN_APP starting WEBAPP"
    pipenv run src/webapp/wsgi.py -c config_lbds.yaml
else
    echo "Error, $MACHINE is not a valid machine"
fi
