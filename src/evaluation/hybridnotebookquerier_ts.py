import sys
import os
import matplotlib.pyplot as plt
import pandas as pd

sys.path.append(os.path.abspath(os.path.join('../cobras_ts')))

import cobras_ts.querier
from IPython import display
from IPython.display import HTML


def _query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".

    Taken from: http://code.activestate.com/recipes/577058/
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def _nb_to_class(nb):
    if nb == 0.:
        return "TP"
    elif nb == 1.:
        return "TN"
    elif nb == 2.:
        return "FP"
    elif nb == 3.:
        return "FN"
    else:
        raise ValueError("Invalid label")


# Source: https://stackoverflow.com/questions/38783027/jupyter-notebook-display-two-pandas-tables-side-by-side
def display_side_by_side(*args):
    html_str = ''
    pd.set_option('display.max_colwidth', -1)  # pandas silently shortens rows for output (breaks urls)
    for df in args:
        html_str += df.to_html(escape=False)
    display.display_html(html_str.replace('table', 'table style="display:inline"'), raw=True)


class HybridNotebookQuerierTS(cobras_ts.querier.Querier):

    def __init__(self, data, labels, metadata):
        super(HybridNotebookQuerierTS, self).__init__()
        self.data = data
        self.labels = labels
        self.metadata = metadata

    def query_points(self, idx1, idx2):
        if self.labels[idx1] == 2. or self.labels[idx2] == 2.:
            display.clear_output(wait=True)
            df1 = pd.Series(self.metadata.iloc[idx1]).to_frame()
            df2 = pd.Series(self.metadata.iloc[idx2]).to_frame()
            display_side_by_side(df1, df2)

            inst1 = str(idx1) + " (" + _nb_to_class(self.labels[idx1]) + ")"
            inst2 = str(idx2) + " (" + _nb_to_class(self.labels[idx2]) + ")"
            return _query_yes_no("Should segment " + inst1 + " and segment " + inst2 + " be in the same cluster?")
        else:
            return self.labels[idx1] == self.labels[idx2]

    def update_clustering(self, clustering):
        plt.clf()
        n_clusters = len(clustering.clusters)
        for cluster_idx, cluster in enumerate(clustering.clusters):
            for clusterid in cluster.get_all_points():
                plt.subplot(1, n_clusters, cluster_idx + 1)
                plt.plot(self.data[clusterid, :], alpha=0.5)
        display.clear_output(wait=True)
        display.display(plt.gcf())

        return True
