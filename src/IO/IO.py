import numpy as np
import pandas as pd
from uuid import uuid4

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from os import path
from pathlib import Path
import yaml
import pickle

import util
from random import randint

np.random.seed(456)


class IO:
    def __init__(self, config, task):
        self.config = config
        self.machine = self.config["machine"]
        self.GENERATED_CONFIG_FILENAME = config["feature_selection"]["generated_config_filename"]
        self.CONFIG_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config_files/"))

        self.beam = self.config[task]['beam']
        self.time_interval = self.config[task]['time_interval']

        self.start_year = util.get_start_year(self.time_interval)
        self.end_year = util.get_end_year(self.time_interval)
        self.start_month = util.get_start_month(self.time_interval)
        self.end_month = util.get_end_month(self.time_interval)

        self.raw_data_path = util.RAW_DATA_DIR + '/' + self.machine.lower() + '/' + self.start_year + '/'
        self.preprocessed_data_path = util.PREPROCESSED_DATA_DIR + '/' + self.machine.lower() + '/' + self.start_year + '/'
        self.pipeline_data_path = util.PIPELINE_DATA_DIR + '/' + self.machine.lower() + '/' + self.start_year + '/'

        self.grid_search_statistics_path = util.RESULTS_DIR + '/statistics/' + self.machine.lower() + '/' + self.start_year + '/'

        self.metadata_fn = 'results_metadata.yaml'
        self.temp_pipeline_model_path = util.RESULTS_DIR + "/shap_values_and_model_dump/" + self.machine.lower() + '/'
        self.default_results_path = util.RESULTS_DIR + '/' + "default_results" + '/' + self.machine.lower() + '/' + \
                                    self.start_year + '/'
        self.best_id_fn = "best_grid_search_result_metadata.yaml"

        self.histogram_rankings_path = util.RESULTS_DIR + '/webapp_generated/rankings/histogram/' + \
                                       self.machine.lower() + '/' + \
                                       self.start_year + '/' + \
                                       self.start_month + '-' + \
                                       self.end_month + '/'
        self.shap_rankings_path = util.RESULTS_DIR + '/webapp_generated/rankings/SHAP/' + \
                                       self.machine.lower() + '/' + \
                                       self.start_year + '/' + \
                                       self.start_month + '-' + \
                                       self.end_month + '/'
        self.histogram_frequency_feature_selection_path = util.RESULTS_DIR + '/webapp_generated/histogram_frequency_feature_selection/' + \
                                       self.machine.lower() + '/' + \
                                       self.start_year + '/' + \
                                       self.start_month + '-' + \
                                       self.end_month + '/'

        # experiment paths
        self.fs_shap_path = util.RESULTS_DIR + '/experiments/shap_ranking/'
        self.fs_histogram_path = util.RESULTS_DIR + '/experiments/histogram_ranking/'

        self.figures_base_path = util.RESULTS_DIR + '/experiments/figures/'

        # model tests path
        self.seperate_feature_results_path = util.RESULTS_DIR + '/model_tests/seperate_feature_results/' + \
                                       self.machine.lower() + '/' + \
                                       self.start_year + '/' + \
                                       self.start_month + '-' + \
                                       self.end_month + '/' + \
                                       "B{}".format(self.beam) + '/'

    def get_metadata_fn(self):
        return self.metadata_fn

    def get_pipeline_output_filename(self, anomaly_detector, id, seed):
        """
        Returns the filename of the pipeline result output
        """
        return self.machine + '_pipeline_' + self.start_year + '_' + util.beam_to_str(self.beam) + '_' + anomaly_detector + '_' + id + '_' + str(seed) + '.csv'

    def get_pipeline_output_path(self, anomaly_detector):
        return util.RESULTS_DIR + '/' + anomaly_detector + '_results/' + self.machine.lower() + '/' + self.start_year + '/' + self.start_month + '-' + self.end_month + '/'

    def get_grid_search_output_path(self, anomaly_detector):
        return util.RESULTS_DIR + '/' + anomaly_detector + '_results/' + self.machine.lower() + '/' + self.start_year + '/best/'

    def get_grid_search_best_output_filename(self, anomaly_detector):
        return "grid_search_{}_B{}_{}_{}_{}-best.csv".format(self.machine,
                                                             self.beam,
                                                             anomaly_detector,
                                                             self.config['grid_search']['anomaly_detection'][anomaly_detector]['scoring'],
                                                             self.config['grid_search']['output_filename'])

    def get_histogram_rankings_path(self):
        return self.histogram_rankings_path

    def get_shap_rankings_path(self):
        return self.shap_rankings_path

    def get_multiple_pipeline_statistics_path(self, ad):
        return util.RESULTS_DIR + "/statistics/multiple_pipelines/{}/{}/{}/".format(ad, self.machine.lower(), self.start_year)

    def get_multiple_pipeline_statistics_filename(self, nb_runs, ad):
        return 'grid_search_statistics_multiple_pipelines_{}_{}_B{}_{}_{}_{}.csv'.format(self.machine.lower(),
                                                                                      self.start_year,
                                                                                      self.beam,
                                                                                      nb_runs,
                                                                                      ad,
                                                                                      self.config['grid_search']['output_filename'])

    def get_figures_path(self, ad):
        return self.figures_base_path + "{}/{}/{}/".format(self.machine.lower(), ad, self.start_year)

    def write_pipeline_result(self, result, id = None, seed=456, delete_old=False):
        """
        Writes the truth_and_pred_df of a pipeline result to the /results/ folder and updates the corresponding
        meta-data file using an unique identifier. Overwrites the id if the same parameters are used
        Args:
            result: The result of the pipeline
        """
        if id is None:
            id = str(uuid4())
        output_path = self.get_pipeline_output_path(result['anomaly_detector'])
        yaml_data = self.read_yaml(self.metadata_fn, output_path)

        yaml_params_data = {}
        for key in ['anomaly_detector', 'scale', 'segment_score', 'params']:
            yaml_params_data[key] = result[key]

        # add entry to metadata file
        yaml_data, delete_id = util.add_entry_to_metadata(yaml_data, [id, yaml_params_data])
        if delete_old and delete_id is not None:
            self.delete_csv_with_id(output_path, delete_id)

        print("Dumping input features and metadata to {}".format(output_path))
        filename = self.get_pipeline_output_filename(result['anomaly_detector'], id, seed)
        self.write_to_csv(result['truth_and_pred_df'], filename, output_path)
        self.write_to_yaml(yaml_data, 'results_metadata.yaml', output_path)
        print('| Wrote results and yaml meta-data to file {}'.format(output_path + filename))

    def delete_csv_with_id(self, f_path, id):
        try:
            for _, _, files in os.walk(f_path):
                for f in files:
                    if id in f and '.csv' in f:
                        print("removing {}".format(f))
                        os.remove(f_path + f)
                        break
        except FileNotFoundError:
            print("File not found, could not delete")

    def write_to_csv(self, df, destination_fn, destination_path):
        if not path.exists(destination_path):
            Path(destination_path).mkdir(parents=True, exist_ok=True)
        df.to_csv(destination_path + destination_fn)

    def write_to_yaml(self, data, destination_fn, destination_path):
        if not path.exists(destination_path):
            Path(destination_path).mkdir(parents=True, exist_ok=True)
        with open(destination_path + destination_fn, 'w') as f:
            print("Writing data to yaml file")
            yaml.dump(data, f)

    def read_csv(self, f_name, f_path):
        print("Reading {} file".format(f_name))
        try:
            f = pd.read_csv(f_path + f_name, index_col=0, parse_dates=True)
        except FileNotFoundError:
            print("File {} not found".format(f_path + f_name))
            f = pd.DataFrame()
        return f

    def read_yaml(self, f_name, f_path):
        print("Reading {} file".format(f_name))
        try:
            with open(f_path + f_name, 'r') as ymlfile:
                yml = yaml.safe_load(ymlfile)
        except FileNotFoundError:
            yml = {}
        return yml

    def read_csv_with_id(self, id, f_path):
        if id is None:
            return None
        try:
            for root, dirs, files in os.walk(f_path):
                for f in files:
                    if id in f and '.csv' in f:
                        return self.read_csv(f, f_path)
            print("csv with id: {} not found".format(id))
            return {}
        except FileNotFoundError:
            print("File not found, returning empty dict")
            return {}

    def get_csv_filename_with_id(self, id, f_path):
        if id is None:
            print("File with given id not found, returning None")
            return None
        try:
            for root, dirs, files in os.walk(f_path):
                for f in files:
                    if id in f and '.csv' in f:
                        return f_path + f
            print("csv with id: {} not found".format(id))
            return None
        except FileNotFoundError:
            print("File not found, returning empty None")
            return None

    def get_pred_with_params(self, ad, params=None):
        """
        Reads and returns the pred_and_truth_df with given parameters
        using the uuid's in the metadata file
        Args:
            ad: The anomaly detector type
            params: Dictionary containing the parameters of the AD
        """
        if params is None:
            params = self.config['pipeline']['anomaly_detection'][ad]
        id = self.get_id_with_params(ad, params)
        f_path = self.get_pipeline_output_path(ad)
        return self.read_csv_with_id(id, f_path)

    def get_pred_with_params_filename(self, ad, params=None):
        """
        Reads and returns the pred_and_truth_df filename with given parameters
        using the uuid's in the metadata file
        Args:
            ad: The anomaly detector type
            params: Dictionary containing the parameters of the AD
        """
        if params is None:
            params = self.config['pipeline']['anomaly_detection'][ad]
        id = self.get_id_with_params(ad, params)
        f_path = self.get_pipeline_output_path(ad)
        return self.get_csv_filename_with_id(id, f_path)

    def get_id_with_params(self, ad, params):
        """
        Gets the id corresponding to the given parameters using the metadata file
        in the default pipeline output path
        """
        if params is None:
            print("No parameters given, could not fetch corresponding id")
            return None
        f_path = self.get_pipeline_output_path(ad)
        metadata = self.read_yaml(self.metadata_fn, f_path)
        correct_id = None
        for id, metadata_params in metadata.items():
            if metadata_params["scale"] == params["scale_data"] and \
               metadata_params["segment_score"] == params["segment_score"]:
                metadata_keys = list(metadata_params['params'].keys())
                if 'verbose' in metadata_keys:
                    metadata_keys.remove('verbose')
                params_ok = True
                for ad_param in metadata_keys:
                    if not metadata_params['params'][ad_param] == params['params'][ad_param]:
                        params_ok = False
                if params_ok:
                    correct_id = id
                    break
        return correct_id


    def dump_scored_training_data(self, scored_df):
        print("dumping scored training dataset (input of the ad + score column)")
        f_path = self.pipeline_data_path
        f_name = "data_{}_scored.csv".format(self.machine.lower())
        self.write_to_csv(scored_df, f_name, f_path)

    def read_scored_training_data(self):
        f_path = self.pipeline_data_path
        f_name = "data_{}_scored.csv".format(self.machine.lower())
        return self.read_csv(f_name, f_path)

    def dump_pipeline_model(self, ad, trained_model):
        f_path = self.get_model_and_shap_path(ad)
        if not path.exists(f_path):
            Path(f_path).mkdir(parents=True, exist_ok=True)
        with open(f_path + 'trained_{}_model.dump'.format(ad), 'wb') as file_to_write:
            pickle.dump(trained_model, file_to_write)

    def read_pipeline_model(self, ad):
        f_path = self.get_model_and_shap_path(ad)
        with open(f_path + 'trained_{}_model.dump'.format(ad), 'rb') as file_to_read:
            detector = pickle.load(file_to_read)
        return detector

    def dump_shap_values(self, shap_df, ad, p_type, segment_score_type, threshold, shap_params):
        """
        Dump the shap values to a csv file
        Args:
            shap_df: The shap values
            ad: The anomaly detector used
            p_type: The prediction type (TP, FP, TN, FN)
        """
        f_path = self.get_model_and_shap_path(ad)
        f_name = self.get_shap_filename(ad, p_type, segment_score_type, threshold, shap_params)
        self.write_to_csv(shap_df, f_name, f_path)

    def read_shap_values(self, ad, p_type, segment_score_type, threshold):
        f_path = self.get_model_and_shap_path(ad)
        f_name = self.get_shap_filename(ad, p_type, segment_score_type, threshold)
        self.read_csv(f_name, f_path)

    def get_model_and_shap_path(self, ad):
        return self.temp_pipeline_model_path + ad + '/' + self.start_year + '/' + \
                 self.start_month + '-' + self.end_month + '/'

    def get_default_results_path(self):
        return self.default_results_path

    def get_shap_filename(self, ad, p_type, segment_score_type, threshold, shap_params):
        return "shap_values_" + ad + '_B' + str(self.beam) + '_' + str(threshold) + '_' + \
               segment_score_type + '_' + \
               p_type + '_' + \
               "subsample=" + str(shap_params['subsampling_samples'][p_type]) + '_' + \
               "nb_samples=" + str(shap_params['nb_samples']) + '_' + \
               str(shap_params['link'][ad]) + '.csv'

    def update_config_file(self):
        """
        Writes the config attribute of this class to the config file in src/config_files/
        """
        self.write_to_yaml(self.config, self.GENERATED_CONFIG_FILENAME, self.CONFIG_PATH + '/')

    def write_best_id(self, ad, params):
        id = self.get_id_with_params(ad, params)
        f_path = self.get_grid_search_output_path(ad)
        self.write_to_yaml({id: params}, self.best_id_fn, f_path)

    def get_figures_path(self, ad, beam=None):
        b = self.beam
        if beam is not None:
            b = beam
        return self.figures_base_path + '{}/{}/{}-{}/B{}/'.format(ad, self.start_year, self.start_month, self.end_month, b)

    def save_figure(self, fig, f_path, f_name):
        print("Saving figure at {}".format(f_path))
        if not path.exists(f_path):
            Path(f_path).mkdir(parents=True, exist_ok=True)
        fig.savefig(f_path + f_name)
