from sklearn import ensemble
import numpy as np

import preprocessing.scaler
from anomaly_detection.anomaly_detector import AnomalyDetector
from plot.features import plot_feature_importance


class IsolationForest(AnomalyDetector):
    def __init__(self, n_estimators=100, max_samples="auto", contamination=0.01, max_features=1., bootstrap=False,
                 n_jobs=None, random_state=None, verbose=0):
        self.iforest = ensemble.IsolationForest(n_estimators=n_estimators, max_samples=max_samples,
                                                contamination=contamination, max_features=max_features,
                                                bootstrap=bootstrap, n_jobs=n_jobs, random_state=random_state,
                                                verbose=verbose)

    def __str__(self):
        return str(self.iforest)

    def set_params(self, **params):
        self.iforest.set_params(**params)
        return self

    def fit(self, df):
        self.iforest.fit(df)

        if self.iforest.get_params().get("verbose") > 0:
            N = 10
            features = list(df.columns)
            all_importances = [estimator.feature_importances_ for estimator in self.iforest.estimators_]
            avg_importances = sum(all_importances) / len(self.iforest.estimators_)
            plot_feature_importance(avg_importances, features, N, filename='iforest')
            indices_avg_imp = np.argsort(avg_importances).tolist()
            avg_imp = [(features[i], avg_importances[i]) for i in indices_avg_imp]
            avg_imp = avg_imp[::-1]
            print('Feature importance (0 is most important)')
            for i, f in enumerate(avg_imp):
                print('{}:\t {}'.format(i, f))
        return self

    def anomaly_scores(self, df):
        scores = self.iforest.decision_function(df)
        # flip scores to make larger scores more anomalous
        scores = -scores
        return preprocessing.scaler.scale_array(scores)

    def predict(self, df):
        predictions = self.iforest.predict(df) == -1
        print("Predicted anomalous: ", predictions.sum())
        print("Predicted normal:    ", (~predictions).sum())
        print("Total:               ", (~predictions).sum() + predictions.sum())
        return predictions
