import numpy as np

import preprocessing.scaler
from anomaly_detection.anomaly_detector import AnomalyDetector

# strategies supported by DummyDetector
STRATEGY_UNIFORM = "uniform"
STRATEGY_CONSTANT = "constant"
STRATEGY_STRATIFIED = "stratified"


class DummyDetector(AnomalyDetector):
    """
    This detector implements several simple strategies for classification. It is useful as a simple baseline to compare
    with other classifiers. It is based on scikit-learn's DummyClassifier, which does the same thing for supervised
    learning.

    Note that the constant strategy isn't a good one to use when creating PR-curves, because there is only one
    threshold create (precision, recall) values with. The graph will have a point at (precision=1, recall=0)
    and at (precision=near 0, recall=1), which will make the area under the PR-curve 0.5, but this is not a valid
    indicator of the constant strategy's performance. With this strategy, only 1 (precision, recall)
    tuple can be calculated.
    """

    def __init__(self, strategy="uniform", constant=None, contamination=0.01):
        self.strategy = strategy
        self.constant = constant
        self.contamination = contamination

    def __str__(self):
        return "DummyDetector(strategy='{}', constant={}, contamination={})" \
            .format(self.strategy, self.constant, self.contamination)

    def set_params(self, **params):
        return self

    def fit(self, df):
        return self

    def anomaly_scores(self, df):
        if self.strategy == STRATEGY_UNIFORM:
            scores = np.random.rand(len(df))

        elif self.strategy == STRATEGY_CONSTANT:
            if self.constant is None:
                raise Exception("Strategy set to 'constant' but didn't provide the constant value to use.")
            scores = np.repeat(self.constant, len(df))

        elif self.strategy == STRATEGY_STRATIFIED:
            # Creates scores based on the different distributions of normal and anomalous values
            # the contamination parameter represents the ratio of anomalous values
            # Scores are scaled to [0, 0.7] for normal scores, and [0.7, 1] for anomalous scores
            anomaly_count = int(len(df) * self.contamination)
            normal_scores = preprocessing.scaler.scale_array(np.random.rand(len(df) - anomaly_count), 0, 0.7)
            anomaly_scores = preprocessing.scaler.scale_array(np.random.rand(anomaly_count), 0.7, 1)

            scores = np.concatenate((normal_scores, anomaly_scores))
            np.random.shuffle(scores)

        else:
            raise Exception("DummyDetector strategy not supported")

        return scores

    def predict(self, df):
        raise NotImplementedError
