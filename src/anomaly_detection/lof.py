from sklearn.neighbors import LocalOutlierFactor

import preprocessing.scaler
from anomaly_detection.anomaly_detector import AnomalyDetector


class LOF(AnomalyDetector):
    def __init__(self, n_neighbors=20, algorithm='auto', leaf_size=30, metric='minkowski', contamination='auto'):
        self.lof = LocalOutlierFactor(novelty=True, n_neighbors=n_neighbors, algorithm=algorithm,
                                      leaf_size=leaf_size, metric=metric, contamination=contamination)


    def __str__(self):
        return str(self.lof)

    def set_params(self, **params):
        self.lof.set_params(**params)
        return self

    def fit(self, df):
        self.lof.fit(df)
        return self

    def anomaly_scores(self, df):
        scores = self.lof.decision_function(df)
        # flip scores to make larger scores more anomalous
        scores = -scores
        return preprocessing.scaler.scale_array(scores)

    def predict(self, df):
        predictions = self.lof.predict(df) == -1
        print("Predicted anomalous: ", predictions.sum())
        print("Predicted normal:    ", (~predictions).sum())
        print("Total:               ", (~predictions).sum() + predictions.sum())
        return predictions
