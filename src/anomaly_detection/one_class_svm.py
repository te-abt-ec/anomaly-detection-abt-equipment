from sklearn import svm

import preprocessing.scaler
from anomaly_detection.anomaly_detector import AnomalyDetector


class OneClassSVM(AnomalyDetector):
    def __init__(self, kernel='rbf', degree=3, tol=0.5, nu=0.5,shrinking=None, verbose=0):
        self.ocsvm = svm.OneClassSVM(kernel=kernel, degree=degree, tol=tol, nu=nu, shrinking=shrinking, verbose=verbose)


    def __str__(self):
        return str(self.ocsvm)

    def set_params(self, **params):
        self.ocsvm.set_params(**params)
        return self

    def fit(self, df):
        self.ocsvm.fit(df)
        return self

    def anomaly_scores(self, df):
        scores = self.ocsvm.decision_function(df)
        # flip scores to make larger scores more anomalous
        scores = -scores
        return preprocessing.scaler.scale_array(scores)

    def predict(self, df):
        predictions = self.ocsvm.predict(df) == -1
        print("Predicted anomalous: ", predictions.sum())
        print("Predicted normal:    ", (~predictions).sum())
        print("Total:               ", (~predictions).sum() + predictions.sum())
        return predictions
