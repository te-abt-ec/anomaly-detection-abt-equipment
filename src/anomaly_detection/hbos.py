import preprocessing.scaler
from anomaly_detection.anomaly_detector import AnomalyDetector

from pyod.models.hbos import HBOS
import pandas as pd


class Hbos(AnomalyDetector):
    def __init__(self, n_bins=10, alpha=0.1, tol=0.1, contamination=0.1):
        self.hbos = HBOS(n_bins=n_bins, alpha=alpha, tol=tol, contamination=contamination)

    def __str__(self):
        return str(self.hbos)

    def set_params(self, **params):
        self.hbos.set_params(**params)
        return self

    def fit(self, df):
        return self.hbos.fit(df, None)

    def anomaly_scores(self, df):
        df, index, cols = self.df_to_np(df)
        scores = self.hbos.predict_proba(df)[:, 1]
        # flip scores to make larger scores more anomalous
        return preprocessing.scaler.scale_array(scores)

    def predict(self, df):
        df, index, cols = self.df_to_np(df)
        preds = self.hbos.predict(df)
        return self.np_to_df(preds, index, cols)

    @staticmethod
    def df_to_np(df):
        index = df.index
        cols = list(df)
        # convert pandas df to numpy array
        df = df.values
        return df, index, cols

    @staticmethod
    def np_to_df(np_array, index, cols):
        return pd.DataFrame(np_array, index=index, columns=cols)
