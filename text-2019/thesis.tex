\documentclass[master=cws,masteroption=ci,english]{kulemt}
\setup{title={Anomaly detection for the injection kicker magnets of the CERN Large Hadron Collider},
 author={Thiebout Dewitte},
 promotor={Prof.\,dr.\,ir.\ H. Blockeel\and \,Dr.\,ir.\ W. Meert},
 assessor={E. Van Wolputte\and Prof.\,dr.\,D.\,Hughes},
 assistant={E. Van Wolputte\and Ing.\ P. Van Trappen}}

\setup{
    filingcard, 
    translatedtitle={Anomalie detectie voor de injectie kicker magneten van de CERN Large Hadron Collider},
    udc=681.3,
    %
    % Hieronder, tussen { en } een korte samenvatting toevoegen.
    % Lege regels en het commando \par zijn niet toegelaten.
    % Wees voorzichtig met speciale TeX-tekens #$%&^_~{}\ !!
    shortabstract={Reliability, availability and maintainability are parameters that determine if a large-scale accelerator system can be operated in a sustainable and cost effective manner. Beam transfer equipment such as kicker systems are critical components with potential significant impact on the global performance of the entire machine complex. Identifying root causes of malfunctions is currently tedious, and will become infeasible in future systems due to increasing complexity. Machine learning can help to automate this process. For this purpose a collaboration between CERN and KU Leuven was founded. Several iterations of the study have yielded an \emph{anomaly detection pipeline} which includes preprocessing, detection, postprocessing and evaluation. Merging data of different, asynchronous sources is one of the main challenges. Currently, Gaussian Mixture Models and Isolation Forests are used as unsupervised detectors, but any detector can easily be plugged into the system. During evaluation, we compare the detector predictions to manual e-logbook entries, which constitute a noisy ground truth. Lastly, we try to incorporate expert knowledge by means of semi-supervised clustering with COBRAS. A grid search allows for hyper-parameter optimization across the entire pipeline, yielding promising results.}
}

% Kies de fonts voor de gewone tekst, bv. Latin Modern
\setup{font=lm}

\setup{extralanguage=dutch}

% Uncomment the next line for generating the cover page
%\setup{coverpageonly}

% Uncomment the next \setup to generate only the first pages (e.g., if you are a Word user.)
%\setup{frontpagesonly}

% If you want to include other LaTeX packages, do it here
\usepackage{comment}
\usepackage{gensymb}
\usepackage{siunitx}
\usepackage{amsmath}
\usepackage{adjustbox}
\usepackage{url}
\usepackage{multicol}
\usepackage{subcaption}
\usepackage{pdfpages} % insert poster
\usepackage{epstopdf} % images
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning, arrows.meta}

% Code listings
\usepackage[scaled=0.85]{FiraMono}
\usepackage{listings}
\usepackage{color}
\definecolor{mygray}{rgb}{0.47,0.47,0.33}
\lstset{
	basicstyle=\ttfamily, 
	keywordstyle=\bfseries,
	language=python,
	captionpos=t,
	numbers=left,
	stepnumber=2,
	frame=shadowbox,
	numberstyle=\tiny\color{mygray}
}

% Make lists more compact
\usepackage{enumitem}
\setitemize{noitemsep}

\newcommand{\NA}{--}
\newcommand{\us}{\SI{}{\us}}
\newcommand{\ns}{\SI{}{\ns}}
\newcommand{\volt}{\SI{}{\volt}}
\newcommand{\kA}{\SI{}{\kA}}
\newcommand{\confmatrix}[8]{
\begin{subtable}{\textwidth}
	\centering
	\begin{tabular}{|l|ll|}
		\hline
		& anomaly & normal\\ \hline
		detected & #1 & #2 \\
		undetected & #3 & #4\\ \hline
	\end{tabular} 
\end{subtable}
\par\bigskip
\begin{subtable}{\textwidth}
	\centering
	\begin{tabular}{|ll|ll|}
		\hline 
		precision & recall & \auc & \rnk \\ \hline
		#5 & #6 & #7 & #8 \\ \hline
	\end{tabular}
\end{subtable}
}

\newcommand{\confmatrixpr}[6]{
	\begin{subtable}{\textwidth}
		\centering
		\begin{tabular}{|l|ll|}
			\hline
			& anomaly & normal\\ \hline
			detected & #1 & #2 \\
			undetected & #3 & #4\\ \hline
		\end{tabular} 
	\end{subtable}
	\par\bigskip
	\begin{subtable}{\textwidth}
		\centering
		\begin{tabular}{|ll|}
			\hline 
			precision & recall \\ \hline
			#5 & #6 \\ \hline
		\end{tabular}
	\end{subtable}
}

\newcommand{\curvesconf}[9]{
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=\textwidth]{#1}
	\end{subfigure}%
	\begin{subtable}{.5\textwidth}
		\centering
		\confmatrix{#2}{#3}{#4}{#5}{#6}{#7}{#8}{#9}
	\end{subtable}
}
\newcommand{\rnk}{\texttt{rank}}
\newcommand{\auc}{\texttt{AUC}}
\newcommand{\segscore}{\texttt{segment\_score}}
\newcommand{\anomdetect}{\texttt{anomaly\_detector}}
\newcommand{\beam}{\texttt{beam}}
\newcommand{\thresh}{\texttt{threshold}}
\newcommand{\iforest}{\textsc{iForest}}
\newcommand{\gmm}{\textsc{GMM}}

% Finally the hyperref package is used for pdf files.
% This can be commented out for printed versions.
%\usepackage[pdfusetitle,colorlinks,plainpages=false]{hyperref}

\begin{document}
\begin{preface}
    \input{chapters/preface}
\end{preface}

\tableofcontents*

\begin{abstract}
    \input{chapters/abstract}
\end{abstract}

% A list of figures and tables is optional
%\listoffigures
%\listoftables
% If you only have a few figures and tables you can use the following instead
\listoffiguresandtables
% The list of symbols is also optional.
% This list must be created manually, e.g., as follows:
\include{chapters/abbreviations}

% Now comes the main text
\mainmatter

\include{chapters/introduction}
\include{chapters/background}
\include{chapters/data}
\include{chapters/problem_statement}
\include{chapters/anomaly_detection}
\include{chapters/results}
\include{chapters/conclusion}

% If you have appendices:
%\appendixpage*  % if wanted
\appendix
\include{chapters/appendices/data-collections}
\include{chapters/appendices/feature-selection-sets}
\include{chapters/appendices/poster}

\backmatter
% The bibliography comes after the appendices.
% You can replace the standard "abbrv" bibliography style by another one.
\bibliographystyle{abbrv}
\bibliography{thesis_references}

\end{document}
