\chapter{Background}
\label{ch:Background}

\section{The CERN Large Hadron Collider}
The Large Hadron Collider (LHC) is the world's largest particle accelerator and is located near the border of France and Switzerland. It consists of a 27km ring which accelerates particles in beams up to nearly the speed of light. They are then forced to collide inside particle detectors to try and detect new fundamental particles. The particles are first iteratively sped up in different particle accelerators before they are injected in the LHC. The first accelerator is the Proton Synchrotron (PS) which speeds up protons and injects them into the Super Proton Synchrotron (SPS), the SPS in turn accelerates them further for injection into the LHC \cite{SPS}. At the end of an experiment wich has been run in the LHC, the proton beams are dumped by the LBDS. An overview of the CERN accelerator complex is shown in Figure \ref{fig:lhc}.

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/LHC.jpg}
    \caption{Overview of the CERN accelerator complex \cite{CERN_acc_complex}}
    \label{fig:lhc}
\end{figure}

\subsection{LHC Beam Dump System}
Since the beams in the LHC have an extremely high energy level (around 14 TeV), an extremely reliable system has to be in place to ensure a safe dump after simulation runs. This is the task of the LBDS. A simplified schematic is given in Figure \ref{fig:lbds}. The LBDS uses extraction kicker magnets to extract the beam from the LHC tunnels towards the dump for the external beam (TDE). The beam is directed towards this large block of graphite after diluting the beam so sufficient heat dissipation can take place on impact. \cite{LHC_design_report}

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/LBDS.jpg}
    \caption{Schematic of the LBDS \cite{LBDS_img}}
    \label{fig:lbds}
\end{figure}

\subsection{Next CERN Accelerator Logging Service}
The Next CERN Accelerator Logging Service (NXCALS) is a new version of the CERN Accelerator Logging Service (CALS) which was created during the long shutdown 2 (2019 - present) to meet the increasing need for scalable data storage and analysis tools. The NXCALS team is responsible for logging all data coming from sensors and devices which operate within CERN. This new service provides a Spark Application Program Interface (Spark API) in Python through which the data for the LBDS system can relatively quickly be retrieved \cite{NXCALS1} \cite{NXCALS2}.

\section{Overview of CERN services}

\subsubsection{SWAN}
The service for web based analytics (SWAN) is a service provided by CERN through which Jupyter Notebooks can be run on preconfigured machines which support PySpark (Python API for Spark) and have access to the NXCALS cluster for retrieval of the LBDS data. Currently this service is not used for this project since better alternatives have been found but it has played a large role in testing a lot of data retrieval and preprocessing code. This is because everything worked out of the box and no extensive knowledge of the CERN systems was necessary for its setup and use.

\subsubsection{HDFS}
The Apache Hadoop File System (HDFS) is one of the file systems used by CERN (next to the Andrew File System (AFS) and EOS) and is the destination of all output files created by PySpark queries. This is an important part of the pipeline because almost all preprocessing happens using PySpark as will be discussed in depth in chapter \ref{ch:prepr}. HDFS can also be accessed using SWAN through a graphical user interface (GUI).

\subsubsection{CERNBox}
CERNBox is a file sharing tool used by CERN which provides basic file sharing functionality. The files for this service are stored on the EOS file system.
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/services_overview.png}
    \caption{Overview of how the CERN services are accessed and interacted with}
    \label{fig:services}
\end{figure}

\subsubsection{LXPLUS}
The Linux Public Login User Service (LXPLUS) is a CERN cluster consisting of machines running CERN CentOS 7 (CC7) and provides remote access to the Public Login User Service (PLUS) for all CERN employees. It can be accessed using Secure Shell (SSH) and provides the useful following services:
\begin{itemize}
    \item Remote computing service for non-CPU intensive jobs
    \item Preconfigured systems running CC7: This facilitates a relatively easy setup of PySpark to work with the NXCALS cluster for access to the LBDS data. It avoids the extra task of needing to setup a local installation of CC7 to be able to run the PySpark pipeline (chapter \ref{ch:prepr}).
    \item Remote HDFS access: access to the results of the PySpark pipeline can be scripted and automatically downloaded without needing to start a Jupyter Notebook using SWAN.
\end{itemize}

\section{Spark}
Apache Spark \cite{spark_paper} is a clustering computing system \cite{spark_desc2} (clustering here refers to clusters of computers) which is able to apply high speed operations to a large amount of physically distributed data. It is also described as a "unified analytics engine" in \cite{spark_desc}. It is built on the principle of MapReduce \cite{mapReduce} which uses a master-slave architecture works by using the following steps:

\begin{itemize}
    \item \textit{Map}: The map function which is constructed by the master is applied by each slave to its local data to see which parts of the different data partitions it has stored
    \item \textit{Shuffle}: The data stored by the slaves is redistributed so only a single partition (according to the map function) is stored locally at each slave
    \item \textit{Reduce}: Each slave processes its partition of the data in parallel
\end{itemize}


\subsection{Spark Internals}
In Spark, the driver program (master) maps jobs into stages. These stages are then further split up into jobs so they can be distributed to the correct executors (slaves). Spark is deployed in cluster mode at CERN using the Yet Another Resource Negotiator (YARN) cluster manager. Cluster mode means that the distribution of tasks to executors is decided on by the driver program but the distribution of executors on actual machines is handled by the cluster manager for effective load balancing. An overview of these interactions can be seen in Figure \ref{fig:spark_internals}.

Spark is capable of efficiently executing complex operations on large amounts of data by means of parallelizing the operations. This happens by partitioning the data across different executors where each of them completes the requested operation for its partition of data as shown in Figure \ref{fig:spark_part}. A big caveat is that if an operation requires global information, no partitions can be formed and all data must be moved into a single partition which causes serious performance degradation. The requirement of partitioning can cause performance issues in operations that usually are quite efficient as will be discussed in section \ref{ch:prepr}.

\begin{figure}
    \centering
    \includegraphics[scale=0.5]{img/spark_internals.png}
    \caption{Overview of the internal components of a distributed deployment of Spark \cite{SPARK_internals}}
    \label{fig:spark_internals}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/spark_part.png}
    \caption{Partitioning in Spark}
    \label{fig:spark_part}
\end{figure}


\section{Anomaly Detection}
Anomaly detection is a practice within data mining which tries to isolate unusual data points that differ significantly from the rest of the data. It rests on the assumption that anomalies are quite rare and the features corresponding to such events will have different values when compared to their non-anomalous counterparts.

This problem boils down to an unsupervised classification task where a machine learning model takes feature vectors as input and calculates an anomaly score which usually is a real number. This score can be interpreted as the models confidence that a certain data point is anomalous. The convention is to assign the value 1 to anomalous events and 0 to normal behaviour. To map the continuous output of anomaly detection models from the interval [0, 1] to $\{0, 1\}$, a threshold is chosen and any prediction above this value is rounded up to 1 and classified as anomalous while the others are rounded down to zero and classified as normal behaviour.

The term classification is used as an overarching term for the following classification tasks:
\begin{itemize}
    \item \textit{Unsupervised classification}: all data is unlabeled and the anomaly detector must learn which entries are anomalous without labeled information.
    \item \textit{Semi-supervised classification}: some data is labeled as (non-)anomalous, this can give the model a bit of information regarding what anomalies and/or normal behaviour looks like.
    \item \textit{Supervised classification}: all training data is labeled and the task of the model is to try and generalize this information to predict accurate anomaly scores for unseen data.
\end{itemize}

Anomalies themselves can also be divided into three categories \cite{anomaly_types}:
\begin{itemize}
    \item \textit{Point anomaly}: the simplest type of anomaly which occurs when a single point of data  differs sufficiently from the rest. This difference can varies a lot and is based on the use case.
    \item \textit{Contextual anomaly}: if data is labeled as anomalous based on the context wherein it occurred. For example, if the average amount of people residing abroad does not increase during the summer holiday period, then this can be seen as a contextual anomaly. This is because even though the trend in the data would suggest no anomaly, within the context of the period being a holiday, it is reasonable to expect more people to be abroad compared to the other months.
    \item \textit{Collective anomaly}: this relates to when a set of points in the data together represent an anomaly even though individually they would not seem anomalous. This type of anomaly is mostly relevant when combining multiple streams of data.
\end{itemize}

\subsection{Techniques}
\subsubsection{Gaussian Mixture Models}
A Gaussian mixture model (GMM) \cite{gmm} can be used for density based anomaly detection. It is a clustering method which assumes that the given dataset can be modeled as a set of $k$ distinct $n$-dimensional Gaussians. The constant $n$ refers to the dimension of the feature vectors in the dataset. The algorithm starts off with an initial guess for the parameters of the distributions and the estimates of these parameters are then iteratively improved using the EM algorithm \cite{EM}. A visualization of two fitted two-dimensional Gaussians can be seen in Figure \ref{fig:gmm_ex}.

Each Gaussian represents a single cluster and anomaly detection can be applied by calculating the posterior probability that a new instance belonging to each of the GMM's components separately. If all these probabilities are low, then there is a high probability that the instance is an anomaly. This is because GMMs are expected to model the normal behaviour of the system and if the new vector does not fit well into the model, then it is classified as an outlier. 

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/GMM_ex.png}
    \caption{Example of two GMM's fitted on two-dimensional data (source: \cite{GMM_ex})}
    \label{fig:gmm_ex}
\end{figure}

\subsubsection{Isolation Forests}
The Isolation Forest \cite{i_forest} (iForest) anomaly detector is based on the idea that an anomalous data point is easier to isolate from the rest of the data compared to normal ones. This isolation happens by recursively splitting the feature space by iterating over each dimension and choosing a valid partition. At each iteration the data points which fall outside of this partition are removed. This process is repeated until only the single data point remains. The amount of repetitions needed for this process can then be used as a measure for how anomalous a point is. If this number is low, then the entry is probably an anomaly and the opposite otherwise. This process of iteratively splitting the feature space can be seen as the construction of a decision tree and an visualization of this process is shown in Figure \ref{fig:iforest_ex}.

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{img/iforest_ex.png}
    \caption{An anomalous point (a) isolated with a low amount of repartitions compared to (b) where more than three times the amount are needed (source: \cite{iforest_ex})}
    \label{fig:iforest_ex}
\end{figure}

\subsubsection{Histogram-based Outlier Score}
The histogram-based outlier score \cite{hbos} (HBOS) is a simple anomaly detection algorithm which mainly focuses on minimizing training speed. It assumes independence between all features and models each feature as a univariate distribution using a histogram. For categorical features, a bin is created for each unique value. For numerical features either a static or dynamic bin-width is used. Static binning creates $n$ equal width bins and dynamic binning creates bins with varying widths such that they all contain the same amount of elements.

An anomaly score is calculated for a feature vector $f = [x_{1}, x_{2}, ..., x_{n}]$ as follows:
\[HBOS(f) = \sum_{i}^{n}log(\frac{1}{hist_{i}(x_{i})})\]

with $hist_{i}$ the histogram constructed for feature $i$ and $hist_{i}(x_{i}$) the area of the bin where x$_{i}$ falls into. Looking at the above formula, a feature vector is more likely to be classified as anomalous if the sum of the areas of the bins where each of its elements belong to is small. This intuitively can be explained by reasoning that an anomalous event corresponds to features with abnormal values. Since an anomaly is assumed to be infrequent, the areas of the bins where each of the abnormal values fall into is also expected to be small.

As stated in \cite{hbos}, the sum of the logarithms is taken instead the product since this decreases the severity of numerical errors during calculation. This is beneficial when working with high-dimensional data like the LBDS dataset (see chapter \ref{ch:data}).

\subsection{Evaluation}
Once an anomaly detector has assigned an anomaly score to each data point, the performance of the model can be evaluated using the metrics described below.

\subsubsection{Confusion Matrix}
Given a ground-truth labeling for all data-points, a special type of contingency table called a confusion matrix can be constructed as a basis for the evaluation procedures that will be discussed next. A confusion matrix succinctly represents how well a model has classified the given dataset based on the following types of (mis)classifications:
\begin{itemize}
    \item True positive (TP): a prediction which is labeled as positive in the ground-truth and was predicted as anomalous
    \item False positive (FP): a negatively labeled data point which was predicted as anomalous
    \item True negative (TN): a data point which was correctly predicted as negative
    \item False negative (FN): a data point which was wrongly predicted as negative
\end{itemize}

A threshold value is needed to assign an anomaly score to one of the above classes. Once this value has been chosen, the confusion matrix can be constructed. This kind of contingency table represents the counts of each of the types of classifications mentioned above in a table. A schematic is given in Table \ref{table:conf_matrix}.

\begin{table}[]
\begin{tabular}{l|l|l|}
\cline{2-3}
 & Postively labeled & Negatively labeled \\ \hline
\multicolumn{1}{|l|}{Positively predicted} & $\#$TP & $\#$FP \\
\multicolumn{1}{|l|}{Negatively predicted} & $\#$FN & $\#$TN \\ \hline
\end{tabular}
\caption{Schematic of a confusion matrix}
\label{table:conf_matrix}
\end{table}

\subsubsection{Precision-Recall Curve}

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{img/pr-good.png}
    \caption{Perfect classifier}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{img/pr-bad.png}
    \caption{Bad classifier}
    \end{subfigure}
    \caption{Examples of PR curves}
    \label{fig:pr_curve}
\end{figure}

Due to the nature of anomaly detection which is based on the assumption that anomalies do not occur frequently, often a large class imbalance is present in the dataset. There are a lot more non-anomalous data points than anomalous ones. This has to be accounted for when evaluating anomaly detector predictions since labeling all datapoints as non-anomalous still gives a high accuracy but predictions that are useless. This is why focus should be placed on the models' capability of correctly predicting anomalies and not on how well it classifies all datapoints.

In the context of confusion matrices, this means that the amount of true negatives are not as important as the other types of predictions. The concepts of precision and recall are specifically tailored for this situation and are defined as follows:

\begin{itemize}
    \item Precision (P): $\frac{\text{TP}}{\text{TP+FP}}$
    \item Recall (R): $\frac{\text{TP}}{\text{TP+FN}}$
\end{itemize}

More informally, precision can be described as the fraction of the amount of positive predictions that are actually labeled positive in the ground-truth. Recall can be seen as the accuracy of the model when restricting the data to only positive examples in the ground truth. The true negative count is never used here since that figure is not as important as discussed above and is exactly why precision and recall are useful evaluation metrics in this context.

By evaluating an anomaly detector using a range of thresholds, a Precision-Recall curve (PR curve) can be constructed. This plots precision against recall for different thresholds. Examples are shown in figure \ref{fig:pr_curve}. The left curve is that of a perfect anomaly detector, it reaches the point (1, 1) before dropping straight down to (1, 0). This indicates that for a certain threshold, no false positives or false negatives occurred. The curve on the right is that of a bad anomaly detector since when increasing the recall slightly (reducing the amount of FN), a sharp decrease in precision is observed and therefore also a sharp increase in the FP rate.

\subsubsection{AUC}
The area under the PR curve (AUC) summarizes the performance of an anomlay detector in a single number. For a perfect classifier this equals 1 and a model that guesses randomly has an AUC of zero. The AUC can therefore be used as a quick metric for how close the model performs to a perfect anomaly detector.

This is different from the AUC of an ROC curve. An ROC curve plots the true positive rate (TPR) against the false positive rate (FPR). The TPR is simply the recall while the FPR is defined as FPR = $\frac{FP}{FP+TN}$. When using an ROC curve, a random detector has an area of 0.5. This is not the same as with the AUC of a PR curve which will be the evaluation method of choice in this project.

\subsubsection{Average Anomaly Rank}
The average anomaly rank (rank) is another method of evaluation and is based on the relative ordering of anomaly scores. Assume a list of anomaly scores L $= s_{0},s_{1}, ..., s_{m}$ with $s_{i}$ for $i \in [0,m]$ anomaly scores uniquely assigned to $m+1$ feature vectors by an anomaly detector and L sorted from high to low. Let A be a sorted subset of L containing only the scores of real anomalies and assume there are $n+1$ anomalies present in the dataset. The rank of the detector is then defined as:

\[rank = \frac{\sum_{s_{i} \in A} i}{|A|}\]

A perfect classifier would assign all real anomalies higher scores than the non-anomalies, therefore the rank would be:

\[rank = \frac{0 + 1 + ... + n}{n+1} = \frac{n(n+1)}{2(n+1)} = \frac{n}{2}\]

An imperfect anomaly detector would assign at least one non-anomaly a higher score than a real anomaly. Assume an anomalous instance is assigned rank $n + 1$ instead of $k<n+1$, then the above formula becomes:
\[rank = \frac{0 + 1 + ... + (k-1) + (k+1) + ... + (n+1)}{n+1}\]

Since $n+1>k$, the numerator will be larger compared to the case of the perfect anomaly detector while the denomenator stays the same. This means better performing anomaly detectors will have a lower rank with the minimum score being $\frac{n}{2}$.

\section{Previous Work}
In 2017 W{\'e}ry  was the first to work on the anomaly detection project for the kicker injection magnets (MKI) system. He built the MongoDB server which was in use up until last year. He also proposed a GMM anomaly detector using sliding window features. The evaluation was based on a naive segmentation of the timestamps by merging nearby anomalies together. 

The following year, Halilovic continued this project by replacing the complex segmentation process with segments based on IPOC measurements. He also added the use of isolation forests and dummy detectors as additional anomaly detectors.  Anomaly thresholds were introduced to replace the top-k predictions model together with a grid search for the anomaly detector parameters. A ground truth segmentation was also added for the first time based on the e-logbook provided by CERN.

Last year Dewitte added additional state and controller variables to the MKI feature set and added additional Fourier features to the preprocessing pipeline. He modified the segmentation to use state data instead of IPOC measurements for more logical time segments coupled to machine usage. In the postporcessing he added a webapp to easily analyse the data as well as evaluate the predictions of anomaly detectors. Using the grid searches implemented by Halilovic, Dewitte was able to obtain well performing GMM and iForest anomaly detectors. As an additional anomaly detector, constraint-based repeated aggregation and splitting (COBRAS) was implemented to incorporate expert feedback into the prediction process which improved on the already good results by reducing the amount of false positives.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "thesis"
%%% End: 
