\chapter{Preprocessing}
\label{ch:prepr}
The preprocessing happens in two distinct steps. The first step is executed on the Spark cluster at CERN and is concerned with most of the heavy lifting and data reduction operations. The second step is local preprocessing which handles most of the operations which are not well suited for Spark or depend on the timeframe being analyzed. In section \ref{ch:local_prep}, an explanation will be given for why this separation was necessary.

\section{Scaleable Preprocessing}


\subsection{Motivation for migrating}
The decision to move most of the preprocessing to spark was based on the realization that not all LBDS data could be loaded into the RAM memory of a normal computer before preprocessing due to it easily exceeding 100 GB. The pandas Python library and mongoDB (what the preprocessing pipeline used up until now) both require all data to be loaded into memory which is simply not possible and also does not scale well. Another motivating factor was the fact that the NXCALS cluster which hosts the LBDS data already supports Spark operations. The preprocessed feature vectors would simply need to be downloaded which means a lot less heavy lifting for the local machine as well as a scaleable preprocessing solution. 

Spark is well known for its speed in processing distributed data which is largely attributable to the fact that the whole dataset never has to be collected in a central location for calculations. This collect operations ideally only happens once when the already processed data needs to be written to a file.

\subsection{SPARK preprocessing}
In this section a detailed overview will be given of how the preprocessing is done on the CERN Spark cluster. Again, the discussion will be split up for each type of data and at the end the process of merging these subsets will be explained.

\subsubsection{Common preprocessing}
For each of the six subsets of data (IPOC, SCSS, TSU, BETS, BEAM, BUNCH), the following operations are always performed:
\begin{itemize}
    \item Filter columns containing useless data such as duplicated data or columns only containing a single value
    \item Round the timestamps from nanoseconds to the nearest second since data is only resampled to 1s in the resample operation.
    \item Sort the data by the timestamp of when it was logged. This ensures data with comparable timestamps are in the same partitions for a slight increase in performance.
\end{itemize}

Each dataset contains a column with the the name: \texttt{device}. This contains the name of which sensor or device produced the data entry. Therefore every row of data in the dataset is linked to a specific device. Since each sensor/device publishes data on its own, multiple entries in the dataset might exist with the same timestamp. This can for example happen when a 2 sensors sample the same physical quantity of a machine but 1 is a redundant sensor. The measurements will occur at the same timestamp but for different sensors. 

Instead of only keeping a single entry at every timestamp, a transpose operation is performed on the \texttt{device} column. This is done by first grouping the data by timestamp which means that duplicate timestamps are grouped together in sets. Following this, a pivot is executed on the column containing the device names. This is a transformation which given a group of data entries, maps all column names to unique ones (this is necessary since every entry has the same column names) and adds them to a single row with the timestamp being the common timestamp for the group. Figure \ref{fig:pivot} shows this process where the timestamps in the table on the right are all unique while keeping all data from the left table.

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/pivot.png}
    \caption{Example of a pivot operation on the \texttt{device} column.}
    \label{fig:pivot}
\end{figure}

\subsubsection{IPOC}
No extra preprocessing operations are done for the IPOC data on the Spark cluster.

\subsubsection{SCSS}
Like with the IPOC data, no further preprocessing is done on Spark.

\subsubsection{TSU}
The TSU data contains columns of boolean data (True or False). This is not a numeric type and therefore must be transformed to one. These values are therefore mapped to integers (0 = False, 1 = True).

\subsubsection{BETS}
All BETS data (BEI, BEIL, BEM) is processed in the same manner. The boolean conversion is done here as well because there are columns with boolean data. The BETS data also uniquely contains columns of StructType. These are buffers of other properties already present in the data and they are dropped from the dataset since they contain no new data,. An example of a StructType column is shown in Figure \ref{fig:buffer_col}.

\begin{figure}
    \centering
    \includegraphics[scale=0.6]{img/buffer_col.png}
    \caption{Example of a buffer column from the BETS$\_$BEM data (array entries are not representative due to space constraints).}
    \label{fig:buffer_col}
\end{figure}

\subsubsection{BEAM}
The BEAM data can contain arrays for certain years, therefore when columns of the Spark ArrayType are encountered, the first element of each row is kept and the rest is thrown away. This is because there arrays only contain a single unique value.

\subsubsection{BUNCH}
The BUNCH data is processed in exactly the same way as the BEAM data by keeping the first element of each array when ArrayType columns are encountered.


\subsubsection{Resampling}
\label{ch:ipoc_resampling}
The IPOC dataset contains pulse data which is only meaningful when the LHC is used. The values measured by the IPOC equipment is published each time the machinery is used. Since an anomaly is assumed to only be detectable in the data during machine usage it is reasonable to limit the dataset for the anomaly detectors to the data gathered at the times when IPOC data was published. This also avoids detecting the same anomaly multiple times which would occur if for example a naive resampling to 1s were to be used.

To accomplish this, all non-IPOC data is resampled to the IPOC timestamps. This operation happens in the following steps (example data in Figure \ref{fig:ex_data}):

\begin{itemize}
    \item Upsample the data to 1 second intervals (Figure \ref{fig:ipoc_resample1})
    \item Forwardfill the data for the new timestamps. This is a special type of forwardfill. Instead of copying the last valid value for each feature, all values of the features present in the pre-upsampled data are used to fill in the entries corresponding to new timestamps. This means that the NaN values in the data are not removed but simply shifted over into the new timestamps (Figure \ref{fig:ipoc_resample2}).
    \item Only keep the entries which have a timestamp corresponding to an IPOC timestamp when rounded to the nearest second (Figure \ref{fig:ipoc_resample3}).
\end{itemize}

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/resample_start.png}
    \caption{Data to resample}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
    \centering
     \includegraphics[width=0.4\linewidth]{img/resample_ts.png}
    \caption{Timestamps to resample to}
    \end{subfigure}
    \caption{Example data and timestamps to illustrate resample procedure}
    \label{fig:ex_data}
\end{figure}
\begin{figure}
    \begin{subfigure}{0.32\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/resample_done.png}
    \caption{Resample to 1s \newline}
    \label{fig:ipoc_resample1}
    \end{subfigure}
    \begin{subfigure}{0.32\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/resample_ffill.png}
     \caption{Forwardfill + difference with normal forwardfill (red)}
    \label{fig:ipoc_resample2}
    \end{subfigure}
    \begin{subfigure}{0.32\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/resample_final.png}
     \caption{Removal of timestamps not in resample set (red)}
    \label{fig:ipoc_resample3}
    \end{subfigure}
    \caption{Illustration of the resampling process}
    \label{fig:ipoc_resample}
\end{figure}


This is not the exact Spark implementation because resampling to every second would increase the memory usage by far too much so instead the resampling is done directly to the IPOC timestamps skipping the steps a and b in Figure \ref{fig:ipoc_resample} completely.

The resampling step is extremely important for reducing the size of the dataset because the IPOC data is published at a much lower frequency than the SCSS, BETS, BEAM and BUNCH data. Due to the use of Spark this data is therefore also never downloaded locally which lowers the requirement of the machines wanting to run the anomaly detection pipeline.

\section{Local preprocessing}
\label{ch:local_prep}
The local preprocessing, as opposed to the Spark cluster, happens on the users machine and focuses mainly on transforming the feature vectors with an eye on anomaly detection compared to the Spark preprocessing which tries to reduce the size of the raw data and transform all data to simple numerical types.

The local preprocessing is made up of three steps. The first focuses on leveraging the smaller data size to do more brute force calculations. These of course run slower on a local machine but end up being faster in the end due to the smaller data size. The increase in efficiency compes from avoiding running complex operations on a large set of data with mostly redundant data. The second step handles the time frame dependent preprocessing and is always done before an anomaly detector is trained because that is the earliest moment when this time frame is known. In the final step, the logbook is preprocessed.

\subsection{Time Frame Independent Preprocessing}
The time frame independent preprocessing transforms the data using the following steps:

\begin{itemize}
    \item \textit{Remove empty columns}: columns containing only NaN or None values do not contain any data and are therefore removed. This check takes a lot longer in Spark because the data cannot be partitioned on any attribute and was therefore moved to the local preprocessing (see chapter \ref{ch:results} for an analysis on why non-partitionable operations perform badly in Spark).
    \item \textit{Filter invalid IPOC entries}: every year, a Year-end Technical Stop (YETS) occurs from December until the end of March. A lot of tests are done together with maintenance work which means that a lot of data from abnormal operations are logged. These have to be filtered out based on parameters decided by experts at CERN. The applied filters are given in Table \ref{tab:filters} and their effect on the data is shown in Figure \ref{fig:ipoc_filters}
    \item \textit{Forward- and backfilling the data}: The data is first forward filled and then backfilled to fill in all missing data in the dataframe. A backfill operation works the same was as a forwardfill operation but in the opposite direction. The last valid values are copied backwards to fill in any missing data. 
    
    The classic interpretation of the fill operations are used here (compared to the fill operation used in the Spark preprocessing). An example of the forwardfill operation is shown in Figure \ref{fig:ffill}.
    
    This operation is because some data is only logged when its value changes and is the reason why a forward- and backfill is allowed. The backfill is specifically only for when there is no initial value for a feature to use in the forwardfill. This step is not done in Spark because this can only be done by pooling all data and therefore negating all benefits of using Spark. The pooling step also takes a long time and there is no guarantee that a spark node can support all this gathered data.
\end{itemize}

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/ffill_pre.png}
    \caption{Before forwardfill}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/fill_post.png}
     \caption{After forwardfill}
    \end{subfigure}
    \caption{Example of a forwardfill operation}
    \label{fig:ffill}
\end{figure}

\begin{table}[]
\centering
\begin{tabular}{|l|l|}
\hline
Column & Filter \\ \hline
Control & \textless{}\textgreater 6 \\
MaxCurrent & \textgreater 0.2kA \\ \hline
\end{tabular}
\caption{IPOC filters}
\label{tab:filters}
\end{table}

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/ipoc_filter_res2.png}
    \caption{Control}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
    \centering
     \includegraphics[width=\linewidth]{img/ipoc_filter_res.png}
     \caption{MaxCurrent}
    \end{subfigure}
    \caption{Effects of the different IPOC filters on an IPOC feature}
    \label{fig:ipoc_filters}
\end{figure}


\subsection{Time Frame Dependent Preprocessing}
This final part of the preprocessing pipeline contains the following operation:

\begin{itemize}
    \item \textit{Filtering of low variance columns}: columns with an extremely low variance ($\leq 1e-20$) are removed because they can lead to numerical errors in the future and reduce the performance of the anomaly detectors. [prove this]
\end{itemize}

Compared to earlier years, segment (chapter \ref{ch:Background}) timestamps do not need to be created in the preprocessing. For the LBDS data, they are created by simply copying the timestamps of the feature vectors because these line up with the segments for the anomaly score preprocessing. This choice was made based on the recommendation from a CERN expert.

\subsection{Logbook preprocessing}

The logbook does not need much preprocessing except for removing the spelling mistakes in the column names to avoid bugs in the code. This operation is done locally because the logbooks are not accessible via Spark.



