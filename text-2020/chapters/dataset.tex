\chapter{Dataset}
\label{ch:data}
An important part of anomaly detection is having a good overview of what data is available and what it looks like. The LBDS data is a very complex dataset containing a plethora of different datatypes from different sources which are sampled at different frequencies. This chapter will give a detailed description of the size and contents of the LBDS data.

\section{Overview of dataset}
The logging and storing of the LBDS dataset has been done by the CALS team. As of 2019 this data has been moved to NXCALS which supports Spark for more efficient data retrieval \cite{LBDS_info}.

The LBDS dataset can be partitioned into 6 logical subsets: IPOC, SCSS, TSU, BETS, BEAM and BUNCH. Each of these will be discussed in depth in the following sections. An exhaustive description of the features for each subset can be found in appendix \ref{app:dataset}.

Table \ref{tab:datasets_used} shows which data is used this year compared to the previous years. This year the LBDS data is used which is completely different from previous years and all four years will be used to train anomaly detectors. Even though the data has changed, the pipeline still fully supports the MKI dataset (which was used for the past few years) but has been expanded in functionality to more easily facilitate the addition of new types of datasets.

\begin{table}[]
\centering
\begin{tabular}{l|l|l|l|l|}
\cline{2-5}
 & Current work (LBDS) & Dewitte (MKI) & Halilovic (MKI) & W{\'e}ry (MKI) \\ \hline
\multicolumn{1}{|l|}{2015} & $\checkmark$ & $\times$ & $\checkmark$ & $\checkmark$ \\
\multicolumn{1}{|l|}{2016} & $\checkmark$ & $\checkmark$ & $\times$ & $\times$ \\
\multicolumn{1}{|l|}{2017} & $\checkmark$ & $\checkmark$ & $\times$ & $\times$ \\
\multicolumn{1}{|l|}{2018} & $\checkmark$ & $\checkmark$ & $\times$ & $\times$ \\ \hline
\end{tabular}
\caption{Comparison of the data used for the current and previous work}
\label{tab:datasets_used}
\end{table}

\section{Types of Data}
\subsection{IPOC}
The Internal Post Operational Check (IPOC) data is collected from a set of digitisers which sample currents and/or voltages at the moment of the beam dump. It is logged whenever usage of the LHC starts and published data once for each pulse generator. There are 25 of these per beam meaning the data is logged once for each of these 50 devices in total. The variables for each of these devices is shown in table \ref{tab:ipoc_data}. The IPOC data usually contains around 345k entries of data per month (this number can vary based on machine usage).

\begin{table}[]
\begin{tabular}{|l|l|}
\hline
Name & Description \\ \hline
CTC1A & Current transformer in compensation circuit branch A \\
CTC1B & Current transformer in compensation circuit branch B \\
CTF1A & Current transformer in free-wheel circuit branch A \\
CTF1B & Current transformer in free-wheel circuit branch B \\
CTS1A & Current transformer in principal circuit branch A \\
CTS1B & Current transformer in principal circuit branch B \\
PTU1A & Current transformer for output current PTU branch A 1 \\
PTU1B & Current transformer for output current PTU branch B 1 \\
PTU2A & Current transformer for output current PTU branch A 2 \\
PTU2B & Current transformer for output current PTU branch B 2 \\
IPOC1 & Magnet current, measured with a Rogowski current pick-up \\
IPOC2 & Magnet current, measured with a Pearson current pick-up \\ \hline
\end{tabular}
\caption{IPOC data logged per generator/magnet \cite{LBDS_info}}
\label{tab:ipoc_data}
\end{table}

\subsection{SCSS}
The State Control and Surveillance System (SCSS) data is a representation of the state of the generator and is sampled at a fixed frequency. On average the SCSS data contains 34M rows for each month of data which makes it one of the largest subsets of data in the LBDS dataset. This number includes the data published for all the different SCSS devices.

\subsection{TSU}
The Time Synchronisation Unit (TSU) data is logged from the TSU system which is
\begin{quote}
"...responsible for synchronising the beam dump request with the beam revolution frequency and the beam's first bucket. It ensures that the magnetic field ramp occurs during the 'beam abort gap' so no beam particles will be lost on the machine. ... The TSU also groups all beam dump request users and applies various checks to ensure a (synchronous) beam dump request."
\end{quote}
as written in \cite{LBDS_info}.

Each beam has one redundant TSU of which the data is also present in the dataset. Only the data in the external post operational check (XPOC) property will be used because similarly to the IPOC data, this is data published at the start of when the LHC is used. The importance of this will be explained in Chapter \ref{ch:prepr}. The TSU data is published once per TSU device of which there are a total of 4. In general, the TSU data contains an average of only 2k rows of data per month and therefore only makes up a very small part of the complete dataset.

\subsection{BETS}
The Beam Energy Tracking System (BETS) dataset contains data from multiple BETS's which have the task of "measuring the current through the LHC bending magnets. It uses look-up tables to translate the energy in a capacitor reference voltage so that the generators are always ready to extract the beam. The Beam Energy Meter (BEM) card is connected to a slow-control PLC using PROFIBUS DP which generates this analogue reference which is sent to a high-voltage capacitor charging supply." \cite{LBDS_info}. The BETS data is split into three subsets which are detailed below.


\subsubsection{BEI}
This subset of the BETS data is published once for each of the 50 pulse generators and contains 2.1k rows of data per month.

\subsubsection{BEIL}
The BEIL data is published once per beam and has an average of 2.7M rows of data per month.

\subsubsection{BEM}
The final subset of BETS data is published by two devices for each beam which brings the total amount to 4. On average the BEM data contains 145 rows per month, making it the smallest dataset.


\subsection{BEAM}
The BEAM data contains information regarding 
The BEAM data contains an average of 6.8M rows of data per month.

\subsection{BUNCH}
[todo: ask pieter for more info]
There are in general about 2.1M rows of data per month for the BUNCH data.

\section{Logbook}
An e-logbook is provided by CERN which contains information regarding all detected anomalies in the LBDS system since 2015. It is important to note that there exists the possibility that some anomalies were missed and therefore have not been logged. This information can be used to evaluate the performance of the anomaly detectors by providing a semi-accurate ground truth.

An example of da logbook entry is shown in Figure \ref{table:logbook_ex}. The logbook extensively logs the anomalies which have been detected by the CERN experts but the most important attribute in the logbook is the \texttt{Start Time}. This gives a general idea of when the fault occurred and can be used to label the LBDS data with ground-truth labels which is vital for the development of a good anomaly detection algorithm.

\begin{table}[]
\begin{tabular}{|l|l|}
\hline
Attribute & Value \\ \hline
Start Time & 2015-04-27 09:00:31  \\
comment & repeat error same fault as 2 \\
Fault number & 3.0  \\
AFT System & MKBHG \\
System & LBDS \textgreater\textgreater MKBH   \\
Sub- system & Power generator \\
Component & Switch \\
Failure mode & SW2 \\
System Function & Actuation \\
Consequence & sync dump\\
Accelerator mode & Beam Setup \\
Operational Mode & Beam in \\
Beam Mode & SQUEEZE \\
Energy in {[}GeV{]} & 6500 \\
Bunches & 0 \\
Faulty Elemnet (sic) & MKBH.A B2 \\
Beam & 2 \\ 
Description & erratic on power supply, second time happened \\
OP duration {[}min{]} & 382.45000000926666 \\
Effective duration & 06h 22min 27s \\
Child count & 0 \\
Timestamp & 2015-04-27 09:00:31 \\ \hline
\end{tabular}
\caption{Example of a single entry in the logbook}
\label{table:logbook_ex}
\end{table}

\section{Comparison with MKI data}
The LBDS and MKI datasets are completely different datasets. This section will give an insight in how these differ and will compare some key aspects. See \cite{Dewitte2019} for an in depth description of the MKI dataset.

\subsection{Size and dimensionality comparison}
As Dewitte described in last year's work \cite{Dewitte2019}, there were 5 types of data used. These are shown in Table \ref{tab:mki_data}. As described in the section above, with the LBDS data, this number has grown from 5 to 7. This on its own is not a very big increase but the amount of data contained in each type has dramatically increased. The MKI data totals about 4.2 GB for 2016 - 2018 which in comparison to the LBDS data is about 10$\%$ of the SCSS data for only a single month of 2017. This already gives a first insight in why the preexisting preprocessing will not suffice for the LBDS data.

The dimensionality has also dramatically increased. The MKI data has a dimension of 120 compared to the LBDS data which has a dimensionality of 6726 for the year 2017. This is because as mentioned in the section above, there are multiple devices for each data type and each of these devices publish the variables described in appendix \ref{app:dataset}. The data from 2018 ups this amount from from 6726 to 7623.

One important thing to note is that when looking at a single year at a time, the LBDS dataset contains around 90$\%$ fewer rows than the MKI dataset. Fewer rows means less feature vectors in the training set. This reduced amount of rows coupled with the increased dimensionality will have a noticeable impact on the performance of the anomaly detection algorithms.

\begin{table}[]
\begin{tabular}{|l|l|l|}
\hline
Types of MKI data & Nr of variables & Description \\ \hline
Continuous data & 40 & Temperature, pressure,... \\ 
IPOC data & 56 & Internal Post Operational Check data \\
State data & 8 & Categorical state data of MKI magnets \\
LHC data & 4 & Beam intensity and length of bunches. \\
Timing controller data & 12 & Timing settings of MKI magnets \\ \hline
\end{tabular}
\caption{Overview of the MKI data \cite{Dewitte2019}}
\label{tab:mki_data}
\end{table}

\subsection{Logbook comparison}
\label{ch:log_comp}
[todo: say beam 2 contains more anomalies]
The e-logbooks contain the anomalies actually detected by CERN experts. Since these are used as ground truth for training anomaly detectors, it is reasonable to expect that more logged anomalies mean better performing anomaly detectors (see Chapter \ref{ch:results} for the anomaly detectors performance on the datasets).

\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|}
\cline{1-1} \cline{3-4} \cline{6-7}
\multirow{2}{*}{Year} &  & \multicolumn{2}{l|}{LBDS} &  & \multicolumn{2}{l|}{MKI} \\ \cline{3-4} \cline{6-7} 
 &  & Beam 1 & Beam 2 &  & Beam 1 & Beam 2 \\ \cline{1-1} \cline{3-4} \cline{6-7} 
2015 &  & 7 & 15 &  & / & / \\
2016 &  & 9 & 27 &  & 102 & 63 \\ 
2017 &  & 12 & 6 &  & 61 &  65 \\
2018 &  & 9 & 14 &  & 83 &  58 \\ \cline{1-1} \cline{3-4} \cline{6-7} 
Total &  & 37 & 62 &  & 246 & 186 \\ \cline{1-1} \cline{3-4} \cline{6-7} 
\end{tabular}
\caption{Amount of anomalies in e-logbook for LBDS and MKI data}
\label{tab:logbook}
\end{table}

Table \ref{tab:logbook} shows that there have been almost double the amount of anomalies logged for beam 2 compared to beam 1 with the majoraty logged in 2015 and 2016.

When comparing the amount of anomalies between the LBDS and MKI logbooks, it is immediately apparent that there are a lot more anomalies logged for the MKI system. MKI Beam 1 alone has more anomalies logged for 2016-2019 compared to the anomalies logged for both beams of the LBDS system for all four years. The MKI logbook for 2016 for example contains more anomalies than the LBDS logbook contains for all 4 years and for both beams. 

In total, the MKI logbook contains around 4.5 times the anomalies logged for the LBDS. This means it will be a lot harder to train an anomaly detector on the LBDS data to obtain similar performance to one which is trained on the MKI data due to the more extreme class imbalance.



