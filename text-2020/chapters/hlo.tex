\chapter{Pipeline Overview}
\label{ch:pipeline}
In this chapter a high-level overview will be given of the pipeline. First a quick overview of the workflow will be given followed by a step by step walkthrough of each of the pipeline steps. The preprocessing has undergone extensive changes this year, therefore a quick look will be given here followed by an in depth description in chapter \ref{ch:prepr}.

\section{Workflow}
In this section, the complete workflow of the pipeline will be explained step by step. It has undergone major changes due to the removal of the MongoDB database in favour of Spark. There are two parts to the workflow, the first is the data retrieval and preprocessing step which happens remotely and the second is the rest of the pipeline which is run locally as shown in Figure \ref{fig:pipeline}. All steps of the workflow will be summarised after a short motivation for why the different CERN services (as listed in chapter \ref{ch:Background}) were used.

\subsection{Motivation for usage of CERN services}
\subsubsection{HDFS}
The HDFS used at CERN is the file system where all Spark output gets written to so there was no choice but to use it.

\subsubsection{CERNBox}
This CERN file sharing service is used to store processed feature vectors for easy scripted access when running the pipeline on new machines.

\subsubsection{LXPLUS}
LXPLUS is probably the most important CERN service in the pipeline, it has been integral to being able to script and chain together the different parts of the preprocessing pipeline. It is used to remotely run Spark jobs on the CERN Spark cluster and provides access to HDFS. 

\subsection{Pipeline}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/pipeline.png}
    \caption{Global overview of the pipeline}
    \label{fig:pipeline}
\end{figure}


\subsubsection{Step 1: Data retrieval, preprocessing and setup of easy access}
\begin{itemize}
    \item Process and download feature vectors using PySpark on LXPLUS
    \item Upload full data to CERNBox for easy access
\end{itemize}

Step 1 has to only be executed once. The data uploaded to CERNBox stays valid as long as the preprocessing does not change. If new data is available, rerunning this step will ensure easy access for any new machine wanting to run the pipeline.

\subsubsection{Step 2: Anomaly Detection + Postprocessing}
\begin{itemize}
    \item Run bash script to download feature vectors of the requested year from CERNBox
    \item Run grid search or train a single anomaly detector of choice
\end{itemize}

\subsubsection{Step 3: Evaluation}
\begin{itemize}
    \item The files which are output after a grid search or pipeline run can be analyzed using the webapp (chapter \ref{ch:pipeline})
    \item Feedback from CERN experts is be used to improve the preprocessing and the machine learning sections of the pipeline.
\end{itemize}

\section{Preprocessing}
The preprocessing step transforms the raw CERN data into feature vectors which can be used to train an anomaly detector. The main focus is to remove unnecessary data, fill in missing gaps and if necessary transform the data to a useful format.

The LBDS dataset contains data of a varying data types, sampled at different frequencies and possibly containing noisy entries. All these factors need to be accounted for when transforming it into feature vectors because a wrong transformation of the data could degrade the performance of an anomaly detector severely due to the feature vectors not being an accurate representation of the LBDS.

As discussed in section \ref{ch:data}, there are 4 large subsets of data within the dataset. Each of these need to undergo different preprocessing operations based on the data they contain. This step has undergone significant changes this year and the exact steps for each partition of the dataset will be detailed in section \ref{ch:prepr}.

\section{Anomaly Detection}
The next step in the pipeline is anomaly detection. Here an anomaly detector (iForest, GMM or HBOS) is trained on a set of feature vectors for a given time frame. Either a grid search can be done using a range of parameters or a single pipeline run with single values for each parameter. The same parameters are used as last year \cite{Dewitte2019} for the iForest and GMM. They are shown here again as a quick reminder together with the parameters for HBOS:

\subsubsection{GMM}
\begin{itemize}
    \item \texttt{n$\_$components:} (\texttt{int}) The number of Gaussians to use
    \item \texttt{covariance$\_$type:} (\texttt{full, tied, diag or spherical}) What kind of covariance matrix to use
    \item \texttt{n$\_$init:} (\texttt{int}) How many times the GMM is fitted on the data
    \item \texttt{init$\_$params:} (\texttt{kmeans, random}) How the means, weights and precisions of the GMM are set
\end{itemize}

\subsubsection{Isolation Forest}
\begin{itemize}
    \item \texttt{n$\_$estimators:} (\texttt{int}) The number of decision trees in the iForest
    \item \texttt{max$\_$samples:} (\texttt{float} $\in$ [0, 1]) How many feature vectors used to train each decision tree
    \item \texttt{contamination:} (\texttt{float} $\in$ [0, 0.5]) The expected $\%$ of outliers in the dataset. The threshold is set at 0.5 because a contamination value larger than 50$\%$ implies that anomalies occur more than normal behaviour which would mean the anomalies are not anomalies but the normal behaviour is.
    \item \texttt{max$\_$features:} (\texttt{float} $\in$ [0, 1]) The amount of features used to train each tree in the iForest
\end{itemize}

\subsubsection{HBOS}
\begin{itemize}
    \item \texttt{n$\_$bins:} (\texttt{int}) The number of bins to use for each histogram using static binning (dynamic binning is not supported by the PyOD Python library)
    \item \texttt{alpha:} (\texttt{float} $\in$ [0, 1]) Regularization to prevent overflow when calculating the inverse estimated densities. This is because $\lim_{x \to +\infty} log_{2}(x) = -\infty$. Therefore adding this regularization constant reduces the numerical errors when the bin size is very small.
    \item \texttt{tol:} (\texttt{float} $\in$ [0, 1]) The tolerance to assign an element to the nearest bin when in it does not fall within any of the predefined bins.
    \item \texttt{contamination:} (\texttt{float} $\in$ [0, 0.5]) The expected $\%$ of outliers in the dataset (same as with the iForest).
\end{itemize}

\section{Postprocessing}
\label{ch:postprocessing}
Once the detector is trained it is evaluated on the data by predicting segments of time as follows:

\begin{itemize}
    \item Given a segment of time $t=[t1:t2]$ and a set of feature vectors F with timestamps $t_{f}$ for each $f \in F$, assign an anomaly score $s_{f} \in [0, 1]$ using a trained anomaly detector for each $f$ if $t_{f} \in [t1:t2]$.
    \item A timeframe is as anomalous as the most anomalous feature vector present in the interval. A segment is therefore anomalous if $max_{f} \hspace{1mm} s_{f} > th$, with $th$ the anomaly threshold.
\end{itemize}

A ground truth labeling is created (as detailed in \cite{Dewitte2019}) based on the anomaly timestamps present in the logbook. All segments within a 12 hour time frame around any logged anomaly are labeled as anomalous in the ground truth segmentation. A visualization of this process is shown in Figure \ref{fig:gt_seg}. If neighbouring segments have anomalies assigned by the anomaly detector for the same logbook anomaly, then these are merged to make no difference in evaluation based on how many times it was predicted as long as it was predicted once. Based on this segmentation, the AUC or rank of an anomaly detector's predictions are calculated so the performance of different parameter configurations can be compared.
[todo: add what constitutes a segment, ask Pieter since many different things are used in config]

\begin{figure}
    \centering
    \includegraphics[scale = 0.5]{img/gt_seg.png}
    \caption{Ground truth segmentation of segments $[t_{2}, t_{3}]$, $[t_{3}, t_{4}]$ due to a logged anomaly (red)}
    \label{fig:gt_seg}
\end{figure}

\section{Evaluation}
Using the AUC or rank calculated in the previous step, different anomaly detector configurations are compared to each other so the best performing ones can be selected for further analysis.

These two performance  metrics are used to evaluate anomaly detectors using the following criteria:
\begin{itemize}
    \item AUC: This metric, as described in section \ref{ch:Background}, lies in the interval [0,1] where a higher value means the predictions lie closer to that of a perfect classifier
    \item rank: This is the average rank that an anomaly has after sorting all predictions by their anomaly scores. Lower is better here since the real anomalies ideally have the highest predicted anomaly scores.
    
\end{itemize}
\section{Webapp}
The webapp is a visualization tool that was developed last year \cite{Dewitte2019}. Its goal is to facilitate easy exploration of the dataset and to speed up the analysis of the performance of the anomaly detectors by comparing it to the training data. The predictions of the models are shown together with any column(s) of the data that the user desires. Next to this, all FP and FN predictions are listed by timestamp together with the PC curve. The FPs and FNs are shown specifically since these are misclassifications and analyzing them might provide an insight in how to improve the anomaly detectors performance.
\begin{figure}
    \centering
    \includegraphics[scale=0.3]{img/webapp.png}
    \caption{Webapp showing LBDS data and predictions by an iForest}
    \label{fig:webapp}
\end{figure}

This year the webapp was modified to support the new dataset (figure \ref{fig:webapp}). Another reason for this change was the LBDS logbook which is quite different to the logbook for the MKI system. Now only anomalies are logged instead of other issues and the data for each entry in the logbook itself has also changed to the point where almost all entries in the logbook look entirely different. This means that the information shown when moving the cursor over an anomaly is different and how the logbook has to be interpreted has also changed. Only one kind of label now exists which is simply named 'anomaly'.



