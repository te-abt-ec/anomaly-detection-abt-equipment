#!/usr/bin/env bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 [lbds|mki]"
    exit 1
fi

MACHINE="$1"
echo "Downloading Spark preprocessed May 2017 feature-set, possibly obsolete, from cernbox pvantrap .."
if [ "$MACHINE" == "mki" ]; then
    curl -sS https://cernbox.cern.ch/index.php/s/j4f2sRdrYXVAO7M/download --create-dirs -o data-cern/ml-data/raw/mki/2017/data_mki_raw.csv
    curl -sS https://cernbox.cern.ch/index.php/s/HmJPvWcbJ9KRvVM/download --create-dirs -o data-cern/state_mode_B1_data.csv
    echo "Done!"
elif [ "$MACHINE" == "lbds" ]; then
    curl -sS https://cernbox.cern.ch/index.php/s/sSKoB1HQsnhd297/download --create-dirs -o data-cern/ml-data/raw/lbds/2017/data_lbds_raw_B2.csv
    echo "Done!"
else
    echo "Error, $MACHINE is not a valid machine"
fi
