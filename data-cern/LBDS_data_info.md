# LBDS information for anomaly detection research

## Data filtering
As part of the preprocessing, the following filters should be applied. If one of the below conditions exist, the full row should be dropped.

| Variable     	| Filter      	  	| Reason |
| ---        	|  ---			| ---    |
| SCSS %control | <> 6 (i.e. <> LOCAL)	| only consider data from nominal LHC operation |
| IPOC %CTC1A.\_B\_:I_MAX   | > 0.2 kA | drop all false measurements, easily identified by the CTC1 sensor |

