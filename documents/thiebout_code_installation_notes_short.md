Short version of Guide to run Thiebouts MKI code on a new Ubuntu 20.04 machine.

Links to sections:

- [PYTHON and PIPENV installation](#python-and-pipenv-installation)
- [MongoDB Installation](#mongodb-installation)
- [Running Thiebouts code](#running-thiebouts-code)
    - [Prerequisites](#prerequisites)
    - [Database Building](#database-building)
    - [Feature Creation and Grid Search](#feature-creation-and-grid-search)


### PYTHON and PIPENV installation
```
sudo apt install -y python3-pip
```

BEFORE YOU GO FURTHER, **change the python version in the (existing) Pipfile to your machine's python version!**

Install Pipenv:
```
sudo pip3 install pipenv
```

Add alias for python in ```~/.bashrc```
```
gedit ~/.bashrc
```
and add ```alias python=python3``` at the end of this file.

cd into the repo root directory and install the dependencies in Pipfile:
```
pipenv install
```

### MongoDB Installation

1) Follow the instructions on: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

2) After installation: these are some useful mongodb commands:

Start the mongod process on localhost with default port 27017)
```
sudo systemctl start mongod
```
Verify that MongoDB has started successfully
```
sudo systemctl status mongod
```
or
Check running mongod processes:
```
ps -aux | grep mongo
```

### Running Thiebouts code
#### Prerequisites
Install:
```
sudo apt install python3-gdbm
sudo apt install python3-tk
sudo apt install libpng-dev
sudo apt install libatlas-base-dev
sudo apt install gfortran
```


#### Database Building
Go into the environment:
```
pipenv shell
```

Change 
```'waiting for connections on port'``` to ```'Waiting for connections'``` 
in ```/src/scripts/setup_db.sh```

1) Delete ```mongodb.log``` before running this:
    ```
    sudo ./src/scripts/setup_db.sh
    ```

2) Comment out the call to ```reindex_mongodb.py``` in ```/src/scripts/load_data.sh```

    If this command gives problems, it is probably because of corrupted .gz files. Then download them manually from the repo
    ```
    sudo ./src/scripts/load_data.sh localhost
    ```
   
    Running the Python script directly instead seems to work:
    ```
    python ./src/scripts/reindex_mongodb.py localhost
    ```

#### Feature Creation and Grid Search

1)  Change line 136 in ```/database/CERNMongoClient.py```
    
    from
    ```entries = entries.ix[start:end]```
    to
    ```entries = entries.loc[start:end]```
    
    (ix is deprecated)

    Build the features, choose beam 1 or 2 as arg:
    ```
    python src/scripts/build_features.py -b [beam]
    ```

2) Choose beam 1 or 2 and extended for the grid search:
    ```
    python src/scripts/grid_search_gmm.py -b [beam] [--extended]
    ```
   
3) Next, change the value of the "behaviour" parameter on line 10 in 
    ```/anomaly_detection/isolationforest.py``` from 
    ```'old'``` to ```'deprecated'``` and run:
    ```
    python src/scripts/grid_search_iforest.py -b [beam] [--extended]
    ```




