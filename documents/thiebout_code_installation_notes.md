Guide to run [Thiebouts MKI code](https://gitlab.cern.ch/te-abt-ec/anomaly-detection-mki-2019) on a new Ubuntu 20.04 machine.

A shorter version without extra explanation can be found [here](thiebout_code_installation_notes_short.md)

Links to sections:

- [PYTHON and PIPENV installation](#python-and-pipenv-installation)
- [MongoDB Installation](#mongodb-installation)
- [Running Thiebouts code](#running-thiebouts-code)
    - [Prerequisites](#prerequisites)
    - [Database Building](#database-building)
    - [Feature Creation and Grid Search](#feature-creation-and-grid-search)
    - [Conclusion](#conclusion)


### PYTHON and PIPENV installation
```
sudo apt install -y python3-pip
```

to see the packages installed on your system:
```
sudo pip3 list
```
BEFORE YOU GO FURTHER, **change the python version in the (existing) Pipfile to your machine's python version!**

Install Pipenv:
```
sudo pip3 install pipenv
```

Clone https://gitlab.cern.ch/te-abt-ec/anomaly-detection-mki-2019.git  into your preferred directory

cd to this directory and:

install the dependencies in Pipfile:

with this command if you didn't change the Pipfile Python requirement to your machines Python version
```
pipenv install --python 3.8
```
OR if you did change it, just install it the normal way:
```
pipenv install
```

=> Gives error (bottom of stacktrace): 

```
pexpect.exceptions.TIMEOUT: <pexpect.popen_spawn.PopenSpawn object at 0x7fb1acf6f880>
searcher: searcher_re:
    0: re.compile('\n')
<pexpect.popen_spawn.PopenSpawn object at 0x7fb1acf6f880>
searcher: searcher_re:
    0: re.compile('\n')
```

helpful command: ``` pipenv --support ```

```which python``` does not return anything and ```python``` wasn't recognized, this was the problem (python3 does work (3.8))

SOLUTION
Add alias for python in ```~/.bashrc```
```
gedit ~/.bashrc
```
and add ```alias python=python3``` at the end of this file.

Now run ```pipenv install``` (or ```pipenv install --python 3.8```) again and it should work


### MongoDB Installation

1) Follow the instructions on: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

2) After installation: these are some useful mongodb commands:

Start the mongod process on localhost with default port 27017)
```
sudo systemctl start mongod
```
Verify that MongoDB has started successfully
```
sudo systemctl status mongod
```
Stop the mongod process
```
sudo systemctl stop mongod
```

Restart the mongod process
```
sudo systemctl restart mongod
```
Start a mongo shell
```
mongo
```

Check running mongod processes:
```
ps -aux | grep mongo
```

### Running Thiebouts code
#### Prerequisites
Prerequisites from Thiebout's README.md (I think python versions aren't necessary and mongodb is already installed)
```
sudo apt install python3-gdbm
sudo apt install python3-tk
sudo apt install libpng-dev
sudo apt install libatlas-base-dev
sudo apt install gfortran
```


#### Database Building
Go into the environment:
```
pipenv shell
```

=> change 
```'waiting for connections on port'``` to ```'Waiting for connections'``` 

in ```/src/scripts/setup_db.sh```, because this was also changed on the last line in ```anomaly-detection-mki-2019/mongodb.log```, probably because a new mongo version (currently 4.4.1, Thiebout used >=3.6.0 )changed the logging text.

1) Delete ```mongodb.log``` before running this (the remove command in the script doesn't seem to remove it):
    ```
    sudo ./src/scripts/setup_db.sh
    ```

2) A problem was that my GitKraken corrupted the .gz files in data-cern while cloning the repo, so I manually downloaded them again through Firefox and the MongoDB restore worked :)
    
    If this command gives problems, it is probably because of corrupted .gz files.
    ```
    sudo ./src/scripts/load_data.sh localhost
    ```
    This script also runs ```src/scripts/reindex_mongodb.py``` using pipenv:

    Here I got a ```ModuleNotFound``` error for package pytz (required by an import)
    I tried to solve it by removing the pipenv and installing it again with Python 3.8 as requirement in Pipfile, but still got the ModuleNotFound when the .sh script tries to run the reindexing Pyhton script.

    Running the Python script directly instead seems to work, so I commented out the call in the .sh script and runned this:
    ```
    python ./src/scripts/reindex_mongodb.py localhost
    ```

#### Feature Creation and Grid Search

Check training interval:
```
grep TRAINTIME src/util.py
```
1) Build the features, choose beam 1 or 2 as arg:
    ```
    python src/scripts/build_features.py -b [beam]
    ```
    => Gives ```AttributeError```: 'DataFrame' object has no attribute 'ix' on line 51
    
    SOLUTION: change line 136 in ```/database/CERNMongoClient.py```
    
    from
    ```entries = entries.ix[start:end]```
    
    to
    ```entries = entries.loc[start:end]```
    
    (ix is deprecated)
    This solved the error.

2) Choose beam 1 or 2 and extended (adds more combinations to the gridsearch, will take a lot longer; 30 instead of 4 grid points, default is False)
    ```
    python src/scripts/grid_search_gmm.py -b [beam] [--extended]
    ```
   
3) Next, for the grid search, in the README.md is stated to run:
    ```
    python src/scripts/grid_search_isolation_forest.py -b [beam] [--extended]
    ```
  
    but this file doesn't exist, instead run the command below, 
    but first change the value of the "behaviour" parameter on line 10 in 
    ```/anomaly_detection/isolationforest.py``` from 
    ```'old'``` to ```'deprecated'``` because this feature is deprecated in a newer version of Scikit-learn and will give an error if the value is 'old'.
    ```
    python src/scripts/grid_search_iforest.py -b [beam] [--extended]
    ```
4) In the readme is stated that: "### result of the best run is written to src/*-best.csv" this is not true,
    it is written to the root directory of the repo

#### Webapp
To launch webapp:
```
python src/webapp/wsgi.py
```
browse in firefox to http://127.0.0.1:8050/

#### Conclusion
To conclude, the 2 problems I still have with the code (on my machine) are:
1) The ```./src/scripts/setup_db.sh``` script doesn't delete the ```mongo.log``` file.
2) The ```./src/scripts/load_data.sh localhost``` script gives a ```ModuleNotFound``` error when trying to 
    import Pytz in the called Python script ```src/scripts/reindex_mongodb.py```, which runs without problems when
    called directly from the terminal.


