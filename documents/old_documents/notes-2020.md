<<<<<<< HEAD
## Overall follow-up
Priority: 1 > 2 > 3
### General
* (1) ensure that pipeline runs for MKI with the same results as last year
* (1) fix gitlab CI
* (2) add pyspark data retrieval to the CI
* (2) add tests for new pipeline
* ~~(1) LBDS grid search on different years, also on combined beam-1 and beam-2 feature-set~~
* ~~(1) fix LBDS vs MKI discrepancies in code, among others:~~
  * ~~remove preprocess *task* and if/else based on this parameter~~
  * ~~introduce if needed more specific parameters such as logbook-type and segments-type~~

### Spark todo
* (1) parallelize list processing 
* (2) fix: duplicate timestamp dropping does cause data-loss fix with transpose on 'SCSS_device', only for scss data, beil data is constant for each device it seems
* (3) fix: automate pyspark and file copy scripts so a single command can be launched (includes using correct config.yaml)
* (3) fix: fix the remaining 'TODO/FIXME' comments in the code
* (3) add: posibility to provide *hard* filter values in config.yml, e.g. drop rows where 1<Imax<2, drop rows where control != 6
* (3) add: script json retrieval, see also README in the src/spark/ folder
* (3) add: fft feature, taken out recently cause it was commented out and needed pipeline import
* (3) add: more efficient version of dropna(how=all,axis=1) to spark 
* ~~(1) sliding window~~
* ~~(1) use a generic data-processor instead of having one per data-type (IPOC, SCSS, etc.), doesn't scale~~
* ~~(1) retrieve 2015 data~~
* ~~(2) retrieve 2016 data~~
* ~~(1) use a generic data-processor instead of having one per data-type (IPOC, SCSS, etc.), doesn't scale~~
* ~~(1) parallelize forwardfill together with loading new data~~
* ~~(1) fix: ipoc resampling~~
* ~~(2) deal correctly with arrays, should not be thrown away but disassembled~~
* ~~(2) fix: fill_none_vals (evaluate first if still needed), broken~~
* ~~(2) check: after combining data, i.e. transposing wrt device, is it acceptable to drop row with equal acqStamp?~~
* ~~check: TsuProcessor handle_strings, why has it been added? Doesn't work with device names~~
* ~~fix: add docstrings for every function~~
* ~~fix: code efficiency, too much data is kept in memory and passed around, doesn't scale well~~ PVT
* ~~fix: use all BETS data~~ PVT
* ~~add: write-out only features~~ PVT
* ~~(1) add: print number of rows when writing feature vectors~~

### Webapp
* (1): merge webapps
* ~~fix: allow mulitple feature selection for the graphs~~
* ~~fix: remove obsolete (MKI) views/data~~
* ~~(1) fix: wrong csv in repo~~

### ML todo
* (1) run pipeline on MKI data
* (1) run grid searches on 2018 data
* (3) check: which experiment scripts are still present from last year and could possibly be large reused
* (3) add: scripts to run experiments (can already be written since the ML pipeline already works)
* (3) apply feature-selection (method(s) to be decided on (scripts available))
* ~~(1) added HBOS~~
* ~~(1) fix MKI pipeline~~
* ~~(1) support multiple beams in logbook preprocessing~~
* ~~(1) singularity issues in GMM? + check in webapp~~
* ~~(1) run: rerun grid searches on new feature vectors once available (iforest done, gmm is busy)~~
* ~~(1) add: support for variable filtering in the config file (improve training data with this step)~~
* ~~check: are there numerical errors present when using the feature vectors created on SPARK~~
* ~~add: Merge branches of svm and lof into ML_step~~
* ~~(2) too much duplicated code in src/scripts/ files, should be put in util.py functions and reused~~
* ~~(2) document numeric values warning, does it affect the results?~~
* ~~double preprocessing (preprocessing.py and as part of the pipeline), fix~~
* ~~(2) Check in the code if how the predictions are made for the segments when more than 1 feature vector corresponds to 1 timestamp (no errors but prediction method might not be valid anymore)~~
* ~~(1) check: sklearn warning: does keeping these columns affect the performace~~
* ~~(1) add: send warning to Wannes~~
* ~~add: debug messages~~
* ~~(1) check: how ad scores are assigned and how segments are merged etc.~~
* ~~(1) add: timeout to threads in grid search~~

## week 19 (11/05)
Work done for SPARK:
- busy trying to see why dropping duplicate timestamps explodes dataset size
- 'SCSS_device' and 'BEIL_device' cause a lot of extra data, working on fix with transpose (see SPARK todo's)
- almost done with downloading data for 2018

Work done for ML pipeline:
- Modified multiprocessing with timeouts and printing of progress
- Added support for duplicate timestamps
- Fixed webapp bug (missing csv file)
- Checked if sklearn affected performance -> it does
- Ran grid-search for iforest for 2017, currently processing results before sharing
- Refactored loading of LBDS data and removed duplicate code

## week 19 (06/05)

Work done for SPARK:
- Added variance filtering and nan column dropping

Work done for ML pipeline:
- Fixed numerical errors, filtered those columns (mean < ~1e-28), quasi constant columns so less features
- Experimented with feature selection from last year, runs now on new data but takes VERY long
- Wrote feature selection script which can use sklearn metrics to filter possible excess columns
- Wrote feature selector based on iForest feature importance scores, simple selector simply based on importance scores of a single run (throw away features with score <= eps)
- Wrote 2 scripts for experiments: 1 to train on multiple periods + 1 to use test data different from training data
- Ran grid searches:
    + GMM: 1 year and 1 month (May) (month chosen based on amount of anomalies in logbook)
    + iForest: 1 month (May)
- Webapp has been modified to support the new feature set and old MKI views have been removed
- Updated config to contain parameters for grid_search and pipeline runs
- Modified grid searches and pipeline runs to allow for usage of new config file
- Updated config_manual.txt with description of how to use the updated config file
- Added preprocess_features to remove nan-columns and removed its variance filtering (moved to grid search due to timeframe dependency)
- Added preprocess_feature_vectors to filter on variance based on parameter in config + drop non-numerical columns
- Experimented with duplicate timestamps, they do not create any errors in the pipeline

Questions:
- ~~Tried screen and tmux again, still does not work for me~~
- dropna in grid_search does not drop any columns so the ffill does not fill anything, fill_none_vals can therefore probably be dropped in SPARK?

## week 18 (29/04)
General:
- Added pylint to the project (updated some files using this)
- Created general layout and outline for my thesis text

Work done for SPARK:
- Fixed time-out error for BETS buffers, they can be dropped because they don't contain any new data
- Still get memory errors wen preprocessing BETS_BEI data
- Tried not repartioning BETS data when writing to json but did not help
- created bash script to start spark session and to move get_data from local project to correct folder on LXPLUS\

Work done for ML pipeline:
- fixed numerical errors in ML_step
- fixed data files and paths in ML_step branch
- finished the preprocessing in pandas by updating some functions based on knowledge gained from implementing it in SPARK
- BEM data gets filtered away fulle? -> fixed, I was resampling to Xpoc istead of IPOC
- created lof-grid-search branch with grid-search using Local Outlier Factor (modified lof.py as well)
- created svm-grid-search branch with grid-search using Support Vector Machine (modified one_class_svm as well)\

Questions:
- How to choose proper treshold? Current is set at 0.98 for contingency tables
- What new AD methods should be used? (LOF, SVM (probably not), Time series with LSTM or something of the sorts?)
- fourier returns a single entry (1 Sxx for 1 timestamp), is that ok?

## Meeting 03/10

- MERCS + AD -> others still have to figure this out
- start with writing boiler plate code and maybe look at the possibilities of visualising the data using libraries
- GMM + IForrest + LOF
- Only use supervised learning
- Time series have many varied interval times, algorithms for these data structures not useful
- Libraries: Matplotlib, Plotly, dash, bokeh,... (which to use/ which are useful?)

### Work

- Read up on the bokeh library
- Read up on GMM's
- Read up on isolation forests
- Read up on spark, NXCALS, Magnetic Quenching
- Read up on noSQL, MongoDB?
- Read up on how the LBDS works
- Read up on grid search
- Read part of a presentation of anomaly detection algorithms in high dimensional spaces

## Meeting 22/10

- make CERN account + try to access the data
- subspace pruning, something alike PCA via sk-learn?
- Learn how to work with MongoDB
- Pickle for serialization? (for writing data to file)
- How to save Spark data efficiëntly?
- How much preprocessing can be reused?
- Start work on forked repo
- Load all old libraries to check if the new versions still work with the old code

### Work

- Read the code on the repository -> clear and organised
- Write data to csv or pytables (read (past tense) up on pytables)
- MongoDB installed
- pipenv installed
- MongoDB is giving annoying errors due to the root folder on MacOS being read-only
- Read presentation on clustering and anomaly detection in high dimensional spaces
- Read up on PyTables
- Ask questions written on paper of the previous year

## Meeting 29/10

- SWAN (CERN account nodig) -> share -> projects shared with me
- sk-learn with SPARK session in background? -> which library is it?
- Start with data from 1 generator
- Try to load data into MongoDB and clean it up before starting with the other steps
- Try to run as many queries as possible on Spark
- Partially preprocess on SWAN
- Request access to NXCALS cluster -> ask via CERN email address
- Use a VM (Linux)
- gitlab.ci.yaml -> run pipeline on current code -> check if everythin from the MKI project still works

### Work

- Created CERN account
- finished the quizzes
- Spark+sk-learn -> databricks: 
                        - seems to be useful for small amounts of data (what is small?)
                        - focusses on parallelization
- Gained NXCALS access
- Setup Linux VM
- How to access the links on the SWAN notebook
- What is the syntax of the DevicePropertyQuery?
- Time series useful due to more constant measuring frequency?

## Meeting 5/11

- try Vidyo on Firefox next time
- Use JSON as data storage instead of MongoDB
- load HDFS data locally
- run old code
- (magnets are on constantly)
- Excel file (all possible anomalies) -> csv -> JSON
- Setup remote desktop to access sites on intranet

### Work

- Able to fully run the notebook
- Remote desktop works
- Remove excess columns? (there are a lot of them in the data)
- IPOC: acqStamp is duplicated many times
- Linux VM: problems with mongoDB repository for installation of mongoDB
- PYTHONPATH problems -> fixed by appending new paths
- replaced deprecated --db and -d with --nsInclude
- How to read files from HDFS? (SWAN)
- Removing excess data (columns) works
- How to load file into SWAN script from HDFS?
- Manually unzipped gzip files to retrieve data -> build_features works now
- HMM's are maybe interesting?
- Loading new data into MongoDB works

## Meeting 12/11

- Try mongoDB 3.6 instead of current version because it might be incompatible
- Try loading data with LFS
- Create Git repository
- Work with virtualenv -> use pipenv
- Plot the distributions of the data to learn about how it is distributed (pandas can do this)
- Preprocess as much as possible in Spark
- Try to completely remove MongoDB from code (future work)

### Work

- fixed MongoDB
- Data exploration (new notebook):
    - many entries with exactly the same values
    - played around with Spark API
    - read up on RDD and used it in data exploration
    - BTS data consists of buffers and booleans (what to do with the buffers?)
    - Many None values
- Debugged builder.py to see how it works and what it does
- pipenv works in Linux VM within a pyenv (3.7)

## Meeting 19/11

- Always push all code to the repository even if it is not working code
- Make code more generic -> less dependent of the exact data
- state_mode and IPOC timestamps necessary
- JSON/YAML config file + use mongoDB or not?
- Put questions on repo
- Put preprocessing in sepearate function
- Filter excess columns via config file parameters
- Each week in file: TODO + done + questions

### Work

- YAML config files + text file for manual
- Generic type handling: written generic functions to handle these
- Config file used to replace the build_features.py script
- DataProcessor: subclass for each datatype (IPOC, SCSS,...)
- Config.yaml -> JSON works
- Subclassing of DataProcessors DONE
- Writing to CSV DONE
- Filtering columns through the config DONE
- handle_int case implementation tested
- DataProcessor Class structure cleaned up (created more static methods
- Change data manipulation methods (Dictionary (old) -> Pandas (New))
- Conversion to using Pandas DONE

## Meeting 26/11

- Fix issues addressed by Pieters comments
- Frequently check if MKI project still works via the pipeline checks
- Move old code to DataProcessor objects -> learn how last years preprocessing worked
- add date filter to config file
- replace argument parser in build_features.py with config parameters in config file
- (type: list) for column values: take mean or first n
- Add notes of previous meetings in a file and put it on the repo

### Work

- Adressed comments of Pieter in repo
- uploaded old notes
- added date filtering in config
- added date conversion
- running old code via config seems to initially work
- TODO: filter andomalous data
- Where is all the numerical data?
- Averaging and slicing arrays in df works (processing arrays of BETS)
- Filtering extreme values from data works
- Ask how IPOC resampling works
- Sliding window works
- Ask for explenation on IPOC features
- column normalization works
- initial fft-ft done
- load DataProcessor data from .csv file
- updated documentation + translated it to english

## Meeting 3/12

- Run pipeline on old data
- Send mail to ask which data is missing
- NaN's when values don't change
- build JSON from folder, not file (SPARK)
- let builder be for now
- test new preprocessing on data from last year
- future => full preprocessing on spark
- Read link Pieter sent
- test ML with old MKI data

### Work

- Replace None with 0 -> write works correctly
- try to get remote mounting working
- resampling to IPOC timestamps works
- fixed pipeline error
- CERNBOX integration? ask about this
- problems with writing to json, structure is not kept etc.
- IPOC and state data debugged in builder.py
- which data is the state data?
- partially able to pass features along to dataprocessors for appending

## Meeting 10/12

- SPARK erad dos it add None's? -> maybe not JSON? => read documentation to see how JSON files are written to HDFS
- lock library versions at the end of the project
- mount CERNBOX + spark -> (HDFS -> EOS) -> lcoal mounting
- only filter in ipoc data
- add automisation as final step
- nieuwe preprocessing in de CI zetten om het te testen

### Work

- JSON files fixed by appending each of them
- fixed duplicate timestamps
- used .repartition(1) to correctly write JSON files in 1 piece
- No pressure columns in IPOC data anymore?
- all TSU data gone after resampling?
