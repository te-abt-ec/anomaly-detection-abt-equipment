# How to get a CERN VM ready for the pipeline

## Setup of a OpenStack VM:
* Go to [Cern Openstack](https://openstack.cern.ch)
* Launch instance
  * name: <name_of_vm>
  * image: CC7 - x86_64
  * flavour: the larger the better => more cores for grid search and feature selection
  * keypair: lxplus keypair (automatically generated)
* Wait until instance is running

## Connect to LXPLUS
```sh
ssh <user>@lxplus.cern.ch
ssh root@<name_of_vm>
```

## Installation
### MongoDB
See: <https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/>.

### Python 3
```sh
yum install wget sqlite-devel gcc zlib-devel libffi-devel openssl-devel
cd /usr/src
wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
tar xzf Python-3.7.0.tgz
cd Python-3.7.0
./configure --enable-loadable-sqlite-extensions && make && sudo make install
rm -f /usr/src/Python-3.7.0.tgz
```

Test the installation using 

```
python3.7 -V
```

It is possible to add aliases: 

```sh
echo "alias pip=pip3.7" >> ~/.bashrc
echo "alias python=python3.7" >> ~/.bashrc
source ~/.bashrc
```

Upgrade pip

```sh
pip install --upgrade pip
pip install --upgrade setuptools
```