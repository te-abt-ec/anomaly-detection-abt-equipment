Links to sections:
- [TODO LIST](#todo-list)
- Big picture progress in [Overleaf](https://www.overleaf.com/read/mmqxjzgfszbh) (outdated)
- 2020
	- [Week 40: 28/09-4/10](#week-2809-410)
	- [Week 41: 5/10-11/10](#week-510-1110)
	- [Week 42: 13/10-19/10](#week-1310-1910)
	- [Week 43: 20/10-26/10](#week-2010-2610)
	- [Week 44: 27/10-2/11](#week-2710-211)
	- [Week 45: 3/11-9/11](#week-311-911)
	- [Week 46: 10/11-16/11](#week-1011-1611)
	- [Week 47: 17/11-23/11](#week-1711-2311)
	- [Week 48: 24/11-30/11](#week-2411-3011)
	- [Week 49: 1/12-7/12](#week-112-712)
	- [Week 50: 8/12-14/12](#week-812-1412)
	- [Week 51: 15/12-19/12](#week-1512-1912)
- 2021
	- [Week 6: 9/02-15/02](#week-902-1502)
	- [Week 7: 16/02-22/02](#week-1602-2202)
	- [Week 8: 23/02-01/03](#week-2302-0103)
	- [Week 9: 02/03-08/03](#week-0203-0803)
	- [Week 10: 09/03-15/03](#week-0903-1503)
	- [Week 11: 16/03-22/03](#week-1603-2203)
	- [Week 12: 23/03-29/03](#week-2303-2903)
	- [Week 13: 30/03-05/04](#week-3003-0504)
	- [Week 14: 06/04-12/04](#week-0604-1204)
	- [Week 15: 13/04-19/04](#week-1304-1904)
	- [Week 16: 20/04-26/04](#week-2004-2604)
	- [Week 17: 27/04-03/05](#week-2704-0305)
	- [Week 18: 04/05-10/05](#week-0405-1005)
	- [Week 19: 11/05-17/05](#week-1105-1705)
	- Exam Break
	- [Week 27: 06/07-12/07](#week-0607-1207)
	- [Week 28: 13/07-20/07](#week-1307-2007)
	



# Week 28/09-4/10:

## Work:
- Slides Big Data Analytics bekeken (vooral over parallel databases en MapReduce/Spark)
- Read/watched videos about Spark/RDD's and Hadoop
- Read and tried to decode the README's from previous year
- Read about yaml/kerberos
- installed Ubuntu20.04 on WSL2 (if this works no linux VM needed)

## Questions:
- Can I already commit on GitLab. How is this going to work, own branch, check when merge? => CI?
- Make GitLab account with Cern mail?
    => automatically with Cern Account!

- notes on Git like previous years?
- linux or windows? linux VM or dual boot?
- How many space needed for Cern data?

- In data-cern/LBDS_data_info.md is stated that SCSS data has a "<>6" filter, what does this mean?

- Use MongoDB to test pipeline locally from ML? => Use Mongo to test Thiebouts code, not sure for Kyle.
- "Use JSON as data storage instead of MongoDB"?? -notes-2020.md

interesting from notes2020:
- "Plot the distributions of the data to learn about how it is distributed (pandas can do this)"
- "build JSON from folder, not file (SPARK)"

Still correct?





# Week 5/10-11/10:

## Work:
- Tried to run current codebase (followed instructions in README.md)
	- setup pyenv: switch between python versions
	- setup pipenv: installed dependancies and locked pipenv
- Watched keynote about NXCALS (Spark summit)
- Read NXCALS article "The Architecture of the Next CERN Accelerator Logging Service"
- Drew preprocessing overview

## Questions:
- How can I ask questions, through GitLab notes or mail? => Mail best option & document problem+solution on GitLab afterwards
- First small presentation in 2 weeks?


- .python-version says 3.7-dev but Pipfile requires python 3.6?
- it sometimes said that a certain package wasn't installed after running pipenv install (succesfull lock)
    => Works now, problem was pyenv + pipenv (only install pipenv!!)

- Everyone can download from the CernBOX link of Kyle?
- (Apache) YARN cluster manager of Spark <-> yarn package manager? not the same thing but same name

- What part of the preprocessing is done locally? The unparallelizable filters (forwardfill)? or is this also done remotely => see code

- is mongoDB still used for local preprocessing?






# Week 13/10-19/10:
(From here on, weeks are from tuesday (meeting day) to monday)
## Work:
- Made literature overview
- Tried Thiebout his code in WSL (some "\n" error occured, so installed Ubuntu VM) (wasn't because of WSL, because it also occurred in Ubuntu VM, was probably because of Pyenv/Pipenv combination)
- Installed Ubuntu20.04 VM
- Tried Thiebout code on (clean) VM but a lot of errors with installation of packages  which i didn't have in WSL
	- pipenv install gives packages installation errors
	- mongodb won't start
	- No permission to stop a mongodb process (first script from Thiebout)
	- ...

=> was all because of pyenv/pipenv combination (follow [```thiebout_code_installation_notes.md```](documents/thiebout_code_installation_notes.md) for solution)

- Tried to make code dependency graph (imports) with snakeviz and snakefood (didn't work)

- Came across the Luigi package (visualizing batch jobs): 
    - https://github.com/spotify/luigi
    - https://luigi.readthedocs.io/en/stable/configuration.html#spark

## Questions
#### Questions for Elia/Pieter:
- Ask to write thesis text in English instead of Dutch. Mail to promotor, which of the 2? Both? (31 okt deadline) => both
- Restructure the Gitlab such that Kyle and my notes are seperated or make new gitlab?
- Ik kan precies al op CernBox maar in mijn Cern account staat van niet


#### Questions general:
- LBDS is constantly online/working/generating data, but when is this data useful?/When is a dump going to start/occurring? button to dump in controle room/automatically? parameter/signal that signals the start of the dump?






# Week 20/10-26/10:
## Work:
- Runned Thiebout his code (starting from a clean Ubuntu20.04 VM) => [```thiebout_code_installation_notes.md```](documents/thiebout_code_installation_notes.md)
	- These are notes with every command and change that was necessary to run the code properly
	- For commands and instructions only => [```thiebout_code_installation_notes_short.md```](documents/thiebout_code_installation_notes_short.md)
- Installed dual boot on pc and setup the whole thing again (~30mins with guide)
- Tried to run Kyle his code, works but you have to pass the right (lbds) config file, but gives NaN's at the end?
- Made literature overview of repo literature folder (read/not read)
- Wrote [```notes-2021.md```](notes-2021.md), [```thiebout_code_installation_notes.md```](documents/thiebout_code_installation_notes.md) and [```thiebout_code_installation_notes_short.md```](documents/thiebout_code_installation_notes_short.md)
- Tried to run Thiebout's code on 19-20 repo, didn't work fully because build_features was missing
- Connected with LXPLUS and tried the keytab authentication. When executing ```klist```, I didn't get a correct output as seen here:
    - http://nxcals-docs.web.cern.ch/current/user-guide/data-access/configuration/#kerberos-keytab-file-generation
    - I also followed these instructions: http://nxcals-docs.web.cern.ch/current/user-guide/data-access/access-methods/#pyspark-tool
- First intermediate presentation slides

## Questions:
#### Questions for Elia/Pieter:
- Heeft Guillaume zijn report al doorgestuurd?
- Weten jullie toevallig een goede RDP (remote desktop) app voor Ubuntu/Linux?
- When connecting to LXPLUS, following the REAME of Kyle, my pipenv install doesn't install anything because there was no Pipfile, clone git first?
- Why virtualenv + pipenv? Why not pipenv shell?
- Keytab works but is it necessary?


- How to do ```source ./source-me.sh``` on LXPLUS?
- Can't find ./bin/spark on LXPLUS


#### Questions general:
- Does there exist a better tool to view/visualize the features/data (.csv) then LibreOffice? For example the features of Kyle (features_2017.csv) from CernBox have too much columns to open properly in LibreOffice.


# Week 27/10-2/11:
## Work:
- Finished fixing the CI by copying the yml file from fix-ci branch to master and small changes (fix-ci-on-master branch).
- Installed the Spark bundle on /eos/ and tried querying by following this guide: 
    - http://nxcals-docs.web.cern.ch/current/user-guide/data-access/configuration/ (to generate a keytab if necessary)
    - http://nxcals-docs.web.cern.ch/current/user-guide/data-access/access-methods/#nxcals-spark-bundle (spark bundle, Python 3 environment and using Spark)
    - Also follow the [Spark README](src/spark/README.md)
- Read Guillaume's report
- Tried basic tmux commands
- Runned the [```get_data.py```](src/spark/get_data.py) several times with different timestamps, imported results in MongoDB and tried to run 18-19 code:
  not working because data is not the same => should make spark query such that received csv file is same as Thiebout his data
- Explored the [```get_data.py```](src/spark/get_data.py) file
- Read about the spark configuration, NXCALS query parameters
	- http://nxcals-docs.web.cern.ch/current/user-guide/extraction-api/#data-extraction-api
	- https://gitlab.cern.ch/acc-logging-team/nxcals/blob/18f23219e3695940100d2c39e32dd4abfce370c5/docs/pages/user-guide/library-reference.md
- Visualised some MKI data in a SWAN notebook using the mki_test_B1.yaml file. Also simulated part of the [```get_data.py```](src/spark/get_data.py) code on Swan
- Made overview of the columns that are currently queried for MKI.
- Made overview of the [```get_data.py```](src/spark/get_data.py) file (WIP)
- Made overview of the latest [```mki_test_B1.yaml```](src/config_files/mki_test_B1.yaml) file (WIP)

## Questions:
#### Questions for Elia/Pieter:
- Bij kopieren van features van HDFS naar EOS: mijn features csv bestand heeft een heel lange naam/hash-achtig, is dit normaal?
- Kan ik ergens informatie vinden over device en property parameters van in de CMW query? Ergens een overzicht? Zoals:
    - "MKI.UA23.IPOC.AB1" (device)
    - AnalyserResults (property)
- Waarom zijn sommige property values bij bvb features > systems in de config met quotes en andere niet? Heeft dit een reden?
Wordt denk ik automatisch omgezet naar strings door python
- De data die Thiebout had (.gz bestanden in zijn repo), is daar al een preprocessing operatie op uitgevoerd?
Zijn de query's van die data beschikbaar? of is dit letterlijk gewoon alle data van de 6 systemen? Want we moeten proberen om een vergelijkbare/exact dezelfde dataset uit Spark te halen.
- Ask Guillaume for permission to upload report and/or slides to repo?
- What is the difference between the timestamps acqStamp and the IPOC acqStamp? A comparison of the minimum between these timestamps occurs in the resampling function so we don't have to backfill.
	- timestamps are gathered by querying with: "MKI.UA23.IPOC.AB1" and AnalyserResults
	- IPOC data is gathered by querying with:   "MKI.UA%.IPOC.%B1" and "AnalyserResults"
	- both have a acqStamp column
	


#### Questions general:
- get_data.py: get_segment_timestamps: timestamp_query arg: is this the device and property from which the final featurevector timestamps are gathered
or is this for the segments (like the function says)? Same thing?

# Week 3/11-9/11:
## Work:
- Finished studying [```get_data.py```](src/spark/get_data.py) using SWAN (to see the operations effect on the data)
and made overview.
- Downloaded for an example IPOC data from timber.cern.ch between 2017-05-01 and 2017-05-08 and also via the get_data file to compare (was the same).
- Tested the extreme value filtering called in the local preprocessing (now called in apply_ipoc_filters() in [```preprocess_modules.py```](src/preprocessing/preprocess_modules.py)).
I think the regexes of Spark queries need to be adapted to include the magnet data on which the extreme value filters are executed. (see mail)
- Started going through pipeline/anomaly detection code to fix segmentation


## Questions:
#### Questions for Elia/Pieter:
- in data-cern/ is een logbook_B1.csv. Is er ook een voor beam 2? of verwijst dit niet naar beam?
- Thiebout his extremes filters are in seperate file, Kyle also had 2 extremes filters (>0.2kA and control <>6)
  Are some of these the same filters? Put in same file? MKI <-> LBDS?
- Regexes of extremes values in config are weird (MKD?) Are these from LDBS?
- Bij current regex van extreme values, welke filteren?
- What are the "B1" columns? looks like they all get dropped except wholeBeamIntensity.
- Some temporal features seem useless? for example of the control data.
- What is the STATE:MODE column of 18-19 thesis? state column disappears after preprocessing? (chages to x and y features)
    - SCSS_MKI.UA23.GEN_first(state,false)
    - SCSS_MKI.UA23.GEN_first(mode,false)
 - Somewhere in local preprocessing, "acqStamp" column gets renamed to "timestamps" column, reason?
 


# Week 10/11-16/11:

## Work:
- Finished exterme value filtering, regexes are now correct
- [```get_data.py```](src/spark/get_data.py) now writes STATE:MODE data to HDFS after a ffill
- [```get_data.py```](src/spark/get_data.py) scripts sometimes takes long, added prints for ordering, repartitioning and writing the data
- Restructured the [```get_data.py```](src/spark/get_data.py) file such that it can now do multiple jobs, currently
1 job is to write the raw STATE:MODE data (after a ffill) and the second job is to write the features (after Spark preprocessing and resampling)
- config file now has a section for the segmentation with type, statemode_query_data, columns_to_write_regex and local_file which are used
to know what segmentation type to use and, if statemode data has to be retrieved, which data has to be queried that contains the statemode data,
which columns to write to the statemode file and the file name (see below for details)
- Added [```get_data.sh```](src/spark/get_data.sh) to run get_data.py with all Spark config params and copy the whole /ml-data/ folder 
(= get_data.py output) from HDFS to EOS automatically (TODO: better map structures and csv file renaming)
- Updated the code run by [```preprocess_features.py```](src/scripts/preprocess_features.py) such that the dumped state mode data from Spark gets read in and forward/backfilled
- Updated the code run by [```pipeline_ad.py```](src/scripts/pipeline_ad.py) such that the locally preprocessed state mode file gets read in and segmented through 18-19 code ([```segmentation.py```](src/postprocessing/segmentation.py))
- Overview of current segmentation, type in config can be either:
	- 'statemode_based': 
		1. Write the raw statemode data to hdfs (currently after a ffill, which is done on each Spark node seperately so not a full ffill => done locally)
		   Should this also be split up in intervals and/or months?, unnecessary columns get dropped before ffill so I don't think this will ask lots of processing
		   This output then gets copied to EOS/CernBox where it should be manually downloaded to /data-cern/state_mode_data.csv (can also be scripted?)
		2. state_mode_data.csv gets locally preprocessed by preprocess_features.py (ordered/ffill/backfill)
		3. pipeline_ad.py uses state_mode_data.csv to construct segments based on on/off switching (on=1)
	- 'timestamp_based':
		1. No statemode data gets dumped by Spark
		2. preprocess_features.py outputs STATE:MODE column (using regex), but this file isn't used in the next step (disable?)
		   = timestamps and mode data after Spark resampling and full local preprocessing (old code from Thiebout complex CI?)
		3. The segments get constructed from each timestamp of the dataframe 
		   (is this correct?, these are timestamps after resampling of Spark, change to timestamps of state_mode_data.csv file?)
		   
## Questions:
#### Questions for Elia/Pieter:
- What x_test to use? Currently no x_test gets passed so x_test=x_train.
- 10-fold cross-validation?

- Tmux sessions run slower and slower, why is this/is there some way to terminate all running processes by 1 user?
- STATE_MODE data forwardfillen?
- STATE_MODE data backfillen?
- Last meeting we said the SCSS 'mode' column was the STATE:MODE column, but on line 102 in the config, a regex with SCSS 'state' is used as state data column
and used in def ipoc_and_state_data() to add to the ipoc data (line 183 in prep modules). Why is this one added, which one to use? I think the 'mode' is the correct one.
- Statemode data has more rows, maybe this is because of dropping rows with all missing values? or features rowcount gets reduced during resampling
- (extreme value filtering) Is "T_DELAY" regex correct?
- (pipeline) What does XPoc stand for? or is this a type, i've seen it twice now (eg segmentation.py).




# Week 17/11-23/11:

## Work:
- To get the state_mode data, only the 'Status' property gets queried now (see config). This reduces a lot of unnecessary columns
- Added check in local preprocessing if certain months are not containing any data, these must be skipped in the preprocessing and the concatenation of the different months in the end. This should now work correctly with any data/config dates input

- Output data saved to HDFS now is structured in folders with for example month3-4 for features of March (month3-5 = March and April, ...). An example output with 2 intervals in the config from 2017-03-01 to 2017-04-01 and 2017-04-01 to 2017-05-01 looks like this:
	- features_2017_part_0
		- month3-4
			- _SUCCESS
			- part-XXX.csv
	- features_2017_part_1
		- month4-5
			- _SUCCESS
			- part-XXX.csv
	- STATE_MODE
		- _SUCCESS
		- part-XXX.csv (statemode data from start of first interval until end of last interval)
- The 'monthX-Y' folders must then be copied manually from CernBox to /data-cern/ml-data/
- If the preprocess:preprocess_params:multi_input in config is set to True, the local preprocessing (preprocess_features.py), will concatenate all csv files in these month folders and write the output to /data-cern/ under the specified filename (MKI_test.csv) in config to be used later in the local preprocessing

- Made diagram of current full pipeline and local column transformations [```anomaly_detection_app_diagram.svg```](documents/diagrams/anomaly_detection_app_diagram.svg)
- Moved MKI extreme filters to config and generalised local preprocessing filtering, deleted ```filter_extremes.py```
- Runned old pipeline of Thiebout again and compared columns

## Questions:
#### Questions for Elia/Pieter:
- Remove KITS columns with "__" like in Spark column filtering?
- Segment tuple row lengths are 67 a lot of times, is this the repeating pattern when a bunch is injected?
- What data to use? ([LHC_Schedule_2017.pdf](data-cern/LHC_Schedule_2017.pdf))
- Extreme value filter also on vacuumIntegral columns? Because they always seem to be dropped whole (all values outside bounds)
- Bij extreme values wordt soms vervangen door NaN en soms rij gedelete, waarom welke optie?
- state_mode data suddenly only gives entrys where mode column changes value (most of the time) and not every second (nans rows dropped before ffill? Can't find where this happens. Is the result the same?)





# Week 24/11-30/11:
## Work:
- Uploaded the column "analysis" textfiles to [documents/feature_analysis](documents/feature_analysis) folder
- added fix-spark-25h-bug branch. This is now fixed so branch can be removed
- rebased fix-bottleneck
- Added "type" option in config at extreme value filters to 'drop' or 'nan' an entry that contains an extreme value
- tried to get data from 2018, gave a type error (maybe way of saving data changed?), didnt look into it yet because fix bottleneck first
- ran script for a whole year (2x 6months) with suggestions of Luca (2 conf parameters extra in spark-submit). This took ~92h which is a lot longer than expected. The bottleneck seems to be the window operation in the resampling. Luca gave a few suggestions:
1) write and reread the dataframe inbetween big chunks of code to split up the spark job to detect the location of the bottleneck

2) write and reread dataframe after the window operation to restore parallelisation (he said there was almost no parallelisation after window operation) "the key is to address parallelism after the window operation and UDF perfomance."

3) change the python udf to a pandas udf to improve parallelisation (I think this is the data_range_udf in resample)

4) Message from Luca: "Another topic that I want to mention is memory footprint on the driver. I understand you need a lot of memory on the driver. I believe this again comes from the window operations.
In a previous exercice I had fixed this moving from the Python declarative API to SQL, something like this:

Original code to tune
```
out = out.withColumn("groupid", lit("groupid1"))   #Adding new column with constant value for groupby
window_ff = Window.partitionBy('groupid')\
               .orderBy('acqStamp')\
               .rowsBetween(-sys.maxsize, 0)
out = out.withColumn("currentLinacSingle", func.last(out['currentLinacSingle'], ignorenulls=True).over(window_ff))\
                .withColumn("biasDiscAqnI", func.last(out['biasDiscAqnI'], ignorenulls=True).over(window_ff))\
                .withColumn("biasDiscAqnV", func.last(out['biasDiscAqnV'], ignorenulls=True).over(window_ff))\
                .withColumn('gas2Aqn', func.last(out['gas2Aqn'], ignorenulls=True).over(window_ff))\
                .withColumn('gas3Aqn', func.last(out['gas3Aqn'], ignorenulls=True).over(window_ff))\
                .withColumn("gasAqn", func.last(out['gasAqn'], ignorenulls=True).over(window_ff))\
                .withColumn("gasExtractionAqn", func.last(out['gasExtractionAqn'], ignorenulls=True).over(window_ff))\
                .withColumn("oven1AqnI", func.last(out['oven1AqnI'], ignorenulls=True).over(window_ff))\
               ..etc
```

Modified into SQL:
```
df2 = spark.sql("""
select acqStamp,
last(currentLinacSingle, true) over(order by acqStamp) as currentLinacSingle,
last(biasDiscAqnI, true) over(order by acqStamp) as biasDiscAqnI,
last(biasDiscAqnV, true) over(order by acqStamp) as biasDiscAqnV,
last(gas2Aqn, true) over(order by acqStamp) as gas2Aqn,
last(gas3Aqn, true) over(order by acqStamp) as gas3Aqn,
last(gasAqn, true) over(order by acqStamp) as gasAqn,
last(gasExtractionAqn, true) over(order by acqStamp) as gasExtractionAqn,
last(oven1AqnI, true) over(order by acqStamp) as oven1AqnI,
..etc
```
"

- pandas udf geprobeerd ipv normale udf => parallelisatie met groupby(). Soms error door kolomnamen die "." of "," bevatten.
- I suggest to change the resampling:

Currently
1) a new column next_ts is made with value the acqStamp of the next entry
2) a udf is launched on the acqStamp and next_ts columns which, for every entry constructs a list (with step=1s) of all timestamps between the value of acqStamp and next_ts, then filters this list such that only values of the IPOC "timstamps" list are kept and then does some edge cases
This list is then exploded?
And set as the new acqStamp list


I suggest to:
1) make a scalar pandas udf which adds a column "IPOC_ts" to the dataframe which contains as value the IPOC timestamp from "timestamps" list which is smaller or equal to the acqStamp ("timestamps" is sorted ascending)
2) use a group pandas udf which groupby's the dataframe on the new "IPOC_ts" column. The udf returns a pandas dataframe which only keeps the column for which acqStamp=="IPOC_ts" and also accounts for the edge cases

This way the resampling operation is split up per "IPOC_ts" group and can be parallelized?

## Questions:
#### Questions for Elia/Pieter:
- Is niewe resampling manier goed?
- Wat gebeurt er juist in data_range()?
- Vragen van vorige week voor Pieter (extreme vals, vacuumIntegral)


#### TODOs from this week:
- Komma in kolomnamen wegdoen, gaf problemen bij pandas udf (error zei iets van split(","))
- Finish pandas udfs to resample
- get MKI data from 2015,2016, 2018,2019,2020?
- get some LBDS data to start getting results




# Week 1/12-7/12:
## Work:
- Changed the second rounding udfs to more efficient pyspark dataframe operations
- Tried write/reread after ffill window operation, but no speedup
- Removed write/reread again because it caused some errors with getting data from 2018 (some files could sometimes not be found/accessed/read on hdfs)
- Tried to multithread the get_data.py, currently multithreading the intervals works and seems to speedup the code
 (still tests needed for longer date intervals)
- Made systems (feature sets/columns) also multithreaded via a queue, the only part that has to be locked is the joining of the dataframes

- grid search is runnable again
- Made the local preprocessing code compatible for multiple years
- Got results for 2016,2018 (2019 and 2020 need different processing)
- Made overview of columns of 2018-2020 (same), different from 2016-2017 columns
- Made overview of lbds columns for 2017, started fixing get_data for lbds

## Questions:
#### Questions for Elia/Pieter:
- Multiple intervals in get_data are currently multithreaded (seems like a speedup), but can rows be multithreaded if you do a ffill?
- SCSS mode data from 2019 only contains "null" or "4" values. Where is this data located or how to create segments with this data?
- From 2019, a lot more columns are stored, are there some obvious ones to keep/remove?


#### TODOs from this week:
- Remove columns that aren't in Thiebout his data (fft from statemode etc)
- get LBDS data (nieuwe branch)
- Pandas udf wilt niet werken (Luca zegt dat het beter is, maar resultaten zeggen van niet)
- Make automatic documentation of columns/operations from code instead of printing in console/copypasting.
Not needed in end result but is going to make ML and analysing lbds faster




# Week 8/12-14/12:
## Work:
- Got the results from multithreaded get_data.py. Made log-log plot to see from what point it scales non-linearly. (2-3 months)
- Read HBOS paper
- Experimented with HBOS parameters, amount of bins is theoretically rule of thumb sqrt(nb instances) but 64 works well aswell on 2017 data
- Tried to visualize good HBOS results in altair
- Implemented visualization for HBOS results for each anomaly in plotly to see if HBOS finds global anomalies in Plotly


## Questions:
#### Questions for Elia/Pieter:
- Analyse individual histograms of highest ranked HBOS values/segments?
- Tomorrow presentation, show comparison of results from Thiebout thesis and current (Spark) pipeline
- Show get_data non-linear time problem in presentation as result?

- It's possible to analyse each feature individually for MKI, but this is not feasable for larger datasets. There should be a tool that loops over all results and shows interesting graphs/results as feedback one by one such that the anomaly detection model can be made more accurate.




# Week 15/12-19/12:
## Work:
- Got the results from multithreaded get_data.py. Made log-log plot to see from what point it scales non-linearly. (2-3 months)
- Read HBOS paper
- Experimented with HBOS parameters, amount of bins is theoretically rule of thumb sqrt(nb instances) but 64 works well aswell on 2017 data
- Tried to visualize good HBOS results in altair
- Implemented visualization for HBOS results for each anomaly in plotly to see if HBOS finds global anomalies
- Made slides second intermediate presentation


## Questions:
#### Questions for Elia/Pieter:
- Analyse individual histograms of highest ranked HBOS values/segments?
- Tomorrow presentation, show comparison of results from Thiebout thesis and current (Spark) pipeline
- Show get_data non-linear time problem in presentation as result?


#### Questions general:
- It's possible to analyse each feature individually for MKI, but this is not feasable for larger datasets. There should be a tool that loops over all results and shows interesting graphs/results as feedback one by one such that the anomaly detection model can be made more accurate.


# Semester 2
# Week 09/02-15/02:
## Work:
- Deleted Pytimber, deprecated package from CALS (caused CI problems)
- write output to files with parameters in filename
- made generalised anomalies vs AD detectors TP plot [```plot_evaluation.py```](src/plot/plot_evaluation.py)
- made generalised parameter vs confusion matrix parameters plot [```plot_evaluation.py```](src/plot/plot_evaluation.py)
	- These are plottable once you have run a pipeline/grid search which saves the results in [/data-cern/<AD-type>_results](data-cern/)
- table anomalies detected for different detectors
- Removed fst from handle_arrays function call in config_lbds.yaml (the function doesn't take an argument)


## Questions:
#### Questions for Elia/Pieter:
- Message "WARN DFSUtil: Namenode for hdpadev remains unresolved for ID ithdpadev01.cern.ch.  Check your hdfs-site.xml file to ensure namenodes are configured properly." spam in lxplus machine when running spark script
	- solved by reinstalling CERN Spark environment in a new folder
- Goede/beste thresholds en parameters hangen af van jaar tot jaar/periode dat je kiest = minder belangrijk (enkel op einde)
=> moeilijk om algemene parameters/thresholds te nemen met zo weinig data
=> verderwerken aan lbds en meer feature engineering ipv parameters optimizen
  

# Week 16/02-22/02:
## Work:
- Solved some LBDS Spark errors:
	- --conf spark.driver.maxResultSize=2G seems to work, don't really know what the effect of this is on performance. 
	  Code should be inspected for performance improvements beacuse 2h50 for a week is not really normal I think 
	  (3h54 for a month so prob overhead of Spark). Can also be because LBDS has a lot more columns than MKI.
	- Sometimes getting process killed at a random time interval (of get_data.sh), no idea of the cause:
  		- anomaly-detection-abt-equipment/src/spark/get_data.sh: line 25: 18025 Killed                  ./bin/spark-submit --master yarn --num-executors 10 --executor-cores 10 --executor-memory 20G --conf spark.driver.memory=100G --conf spark.hadoop.fs.hdfs.impl.disable.cache=true --conf spark.security.credentials.renewalRatio=0.1 --conf spark.driver.maxResultSize=2G anomaly-detection-abt-equipment/src/spark/get_data.py -c $CONFIG
- Got data for 1day, 1week, 1month, 3months and 1year for LBDS and made time plot (similar scaling to MKI, but large initial overhead).
  The 1year script crashed with above "Killed" error. Don't know the cause, needs to be inspected in the Spark logs.
- Made current progress Latex document on Overleaf + overview of all high level concepts and ideas we are considering
- LBDS local preprocessing is working
- Started merge with MKI
- LBDS webapp data input works

## Questions:
#### Questions for Elia/Pieter:
(Pieter)
- state mode LBDS is all '1's in may 2017, is this normal? or are there no 3/4 values like with MKI?
- state mode LBDS, there are multiple mode columns, are these all the same?
- => LBDS should consider each entry as a potential anomaly, so a dump of all timestamps as state_mode is correct.



# Week 23/02-01/03:
## Work:
- Moved threshold to config
- LBDS local anomaly detection pipeline is working
- LBDS multi input local prep
- generalised pipeline result output destination folder
- added plot_confusion_matrix_vs_parameter and plot_anomalies_vs_detectors support for LBDS
- LBDS gridsearch for 2017 05-06/05-08/05-11/whole year
- Made LBDS anomaly plots to compare detectors and with MKI
- Wrote draft introduction (main topics are explainability, generic pipeline and new AD-MERCS detector)
- The origin of the zeros in the Spark output data is at the resampling: 
Just after the ffill and before the resample, about 10% of the entries are zeros (for some columns) most IPOC timestamps contain zeros, 
so in the end result, a lot of zeros get "selected" (about 95%). Examples are the BUNCH columns in MKI (like BQMLHC1_first(bunchPeakMax, false)).


## Questions:
#### Questions for Elia/Pieter:
(Pieter)
- zeros in raw data columns are also in MKI data (for example 2017-05/06) but less frequently 
BUNCH_BQMLHC1_first(bunchIntensityMax,false), 
BUNCH_BQMLHC1_first(bunchIntensityMean,false),
...
this is also in the locally prepped data
- What is the reason that sometimes a zero is measured for these systems? (~10%) and most IPOC timestamps occur at these zero measurements?
Is this normal?
  => solved: Origin is in resampling (see above). 
  Reason is because sampling is not in sync with IPOC timestamps. These columns can be dropped and results
  on AD should be compared.

- GMM sometimes gets a timeout (configured in config to 10000). Takes a long time for LBDS (high dimensionality)
- Draft introduction is on Toledo wiki, still needs some additions (later)




# Week 02/03-08/03:
## Work:
- Fixed the sorting issue on Spark, coalesce(1) is used instead of repartition(1) to accumulate the executor parts into 1 csv file.
  This removes the random shuffle before repartitioning.
- Basic pandas statistics plot for each feature ([```plot_evaluation.py```](src/plot/plot_evaluation.py)).
The 3 plots Should be made into a script and later added to the webapp in seperate tabs.
- Started to look into the webapp code


## Questions:
#### Questions for Elia/Pieter:
- errors get_data
- column statistics plot for LBDS and MKI, which features to drop? => Variance threshold experiment





# Week 09/03-15/03:
## Work:
- The Spark script is now called using a [wrapper](src/spark/get_data_wrapper) with a ```--write``` option (Pieter)
- Updated the folder structure of the [```get_data_wrapper```](src/spark/get_data_wrapper) output (both features and state mode)
- Cleaned up the configs such that they have the same skeleton and one per machine: 
  - [```config_mki.yaml```](src/config_files/config_mki.yaml)
  -	[```config_lbds.yaml```](src/config_files/config_lbds.yaml)
- The CI now uses correctly queried/preprocessed features from Spark using download links to the features (and state_mode data for MKI) in [```download_features.sh```](data-cern/download_features.sh)
- The CI now tests all 3 parts of the local code for LBDS and MKI
  1) Local preprocessing on the output of the Spark [```get_data_wrapper```](src/spark/get_data_wrapper) script
  2) [```pipeline_ad.py```](src/scripts/pipeline_ad.py)
  3) [```grid_search_ad.py```](src/scripts/grid_search_ad.py)
- Got LBDS data for 2016 and 2018
- Removed the ```one_bottleneck_pass``` and ```write_and_reread``` features used to test the Spark bottleneck, which wasn't solved (window operation)
- Restructured the configuration files such that it is chronological and the 4 distinct steps are seperated:
	1) Spark
		1) Querying
		2) Preprocessing
	2) Local preprocessing
	3) Grid search
	4) Pipeline
- More local preprocessing merging (MKI/LBDS)	
- Webapp [```webapp.py```](src/webapp/webapp.py) fixing:
    - Merged the 2 webapps (mki/lbds), still todo is the state mode graph for mki if this is necessary
    - Added a card with some extra dataset information
    - lines/markers toggle
    - Grid search writes best results (AUC) to ```/data-cern/[AD_type]_results/MACHINE/YEAR/best/```
    - fourier/sw features filter
    - Evaluation calculation caching for better UX
- Set random seed for less randomisation noise while gridsearching

## Questions:
#### Questions for Elia/Pieter:
- mki logbook for beam 2? Thiebout has beam seperated code
- tab in webapp to add/remove columns for filters in the config (drop/fft/sw/...), updates the config file



# Week 16/03-22/03:
## Work:
- cleanup the code: before-cleanup branch is legacy, after the cleanup there is a /legacy/ folder with the src and results from that moment. 
All the unused code was removed, except the cobras code
- changed the results destination from /data-cern/ to /results/
- Tried global tabs in webapp, gave error because some callbacks with non-existing id's were called => made a seperate card for the tabs
- webapp modularity and defaults
- webapp histograms for whole selected timeframe (global) and locally selected timeframe (around the prediction)
=> select tp/fp/tn/fn/all predictions to plot to locate the value of these on the histogram
- added feature ranking dependant on the histogram bar height (bar where the prediction is classified)
=> also for 1 prediction or all predictions. Bar heights are divided by the amount of timestapms/entries in the local timeframe to make predictions comparable. 
Features are automatically comparable because the ranking is depedant on the number of entries that are binned in the prediction bin (not the feature values itself).
 When all predictions are selected, rankings per feature are summed over all predictions
- Made IO class to handle the pipeline prediction results df's storing and reading. Future IO operations should be added to this class
- Pipeline output now generates a metadata to keep track of which output csv links to which anomaly detector variables (metadata)
- Tried out the SHAP values API, really slow at the moment, but doable, preprocess scaling function introduces nans in the SHAP library code
- Anomaly detector and training features dump after training to /results/temp_model_dump/ as input for SHAP
- SHAP values per feature ranking in webapp (sorted on absolute value, so feature with most impact + or - is listed on top)

- added the anomalies versus detectors plot to the webapp, still bad layout (just linked the webapp to the current plotly code in plot_evaluation.py)
Threshold parameters can be tuned.
- Dumping columns list is possible with ```dump_colnames``` in config. 
  They get written to txt files inbetween the different local preprocessing/pipeline steps.
  Made LBDS column overview files

## Questions:
#### Questions for Elia/Pieter:
- SHAP values take a long time to calculate, but it is doable for only TP, FN, (FP)
- Are histograms comparable between features if the sum of all bin heights is 1? Change to surface area?
- Histogram ranking comparable between features? => divided by the # timestamps because # timestamps differs for same time interval around a prediction to make predictions comparable (when summing over them in 'all')
- Which prediction function as input to the SHAP API? Right now it's just the anomaly scores straight from scikit-learn
- In the postprocessing step, the output anomaly scores get flipped, does this mean that the output of the SHAP also needs to be flipped?
- Combine the data of different magnets and then run SHAP/histogram experiment to locate the most important/less important columns
Or does this destroy information for when for example 1 magnet fails and the others function correctly?
  



# Week 23/03-29/03:
## Work:
- More webapp functionalities
    - AD dropdown
	- automatic prediction file selection
	- splitted rankings and histogram card to have a better overview for a single prediction  
	- 2nd histogram
	- Feature selection tab to select the features that have to be kept in dataset. this creates a clone of the config named:
	```[selected_config]_analysis.yaml``` with the non-selected features in the ```local_preprocessing<drop_columns``` setting.
- Columns can be dropped locally during preprocessing (and temporarily during pipeline/grid search too for quick analysis).
  This is based on the column name and feature set. (TODO: regexes)
- Automatic pipeline/best grid_search results/ids bookkeeping
- Fixed shap, train/predict data is now correctly passed => new bug after calculating values (TODO)
- Checked the inner workings of segmentation for LBDS to see if correct. Every segment that has no anomalous ground truth (FP, TN)
  has a size of 1 because 1 row is assigned to this segment. 
  For a segment that has an anomalous ground truth (TP, FN), the rows of the dataset around this anomaly timestamp are aggregated
  and put into the segment. Currently the maximum anomaly score of the rows is selected as the anomaly score of the segment.
  This convention has to be the same everywhere!


## Questions:
#### Questions for Elia/Pieter:




# Week 30/03-05/04:
## Work:
- Extra meeting 29/03
- Presentation slides
- 3rd intermediate presentation 30/03
- Webapp manual: [```src/webapp/README.md```](src/webapp/README.md)
- Webapp bugs solved and launchable on a clean repo clone
- 2017 time frame with useful data (based on triggertimestamp):
    - 2017-03-22 12:47:42
    - 2017-12-04 03:00:13
  => automate this if this is always the case for LBDS datasets? (for other years)
- Overview of the content for the thesis
- Looked with help of statistics plot if there is any unrealistic data in the dataset, which can be sliced off (extreme value filtering)
- Columns can now be dropped locally using regexes ```local_preprocessing<drop_columns_regex```.
- Webapp aggregated rankings
- Webapp rankings are now written to files automatically. Toggles in config: 
  [```write_rankings_aggregated```](src/config_files/config_lbds.yaml) for 'all', 'all [prediction type]' and 
  [```write_rankings_single```](src/config_files/config_lbds.yaml) for '[prediction type]' with prediction type: TP/FP/TN/FN.
- Fixed SHAP values calculation, but too much computing power needed for 1000+ features, 300 works fine.

## Questions:
#### Questions for Elia/Pieter:
- Interesting to look at in webapp:
	- The lowest histogram ranked features for 'all TP', are these important features?

- Feature questions:
    - SCSS Mode features weghalen? [0, 3]
    - BEM triggertimestamp (is gewoon een rechte en geeft verwarring met de nullen => cappen op rechte (nuttige data))
    - bagkTriggerDelay manual deleten want er blijft er 1tje over met variance > 0.25 (BB1) of variance filter hoger
- Feature ideas:
    - de features van de magneten aggregeren (average/sum/max/min) om te detecteren of alle magneten tegelijk anomalous behaviour hebben of
      dat het aan 1...n-1 magneten ligt
- Spark
	- Script ends with error code for 2015-07-01

- Ideas:
    - features met lage variance en hoge histogram ranking voor 'all TP' en misschien 'all FN' weggooien (alle anomalies vallen op de
    bin met zeer hoge frequentie = weinig informatie) => dit gaat wel leiden tot overfitting als deze features wel effectief anomalous
    behaviour kunnen vertonen in test data. (komt terug op "drop unuseful features by expert feedback", dus weer een lijstje van deze
    features ter beschikking stellen)
        - voorbeeld = BB1 switchARatio
    - Features subset that is the least "outlier" based on histogram ranking
    => Keep features that are important for the anomalies and keep features that are important
    for learning the normal behaviour of the system, drop the rest
    Method:
    - minimale set vinden van (important) features die niet gedropped mag worden (voor anomalous behaviour)
    - minimale set vinden van (important) features die niet gedropped mag worden (voor normal behaviour)
    - => alle "onnodige" features ertussen die het model enkel kunnen verwarren droppen/tonen aan CERN expert die de beslissing neemt
    - Feature operations:
        - drop
        - lower the y-axis resolution (make bins and take mean/max/min)
    - Model operations:
        - Give a weight to each feature. The weights are then taken into account when calculating the anomaly score
        (e.g. an isolation tree has a weight dependant on the weights of the features that were selected. 
        The anomaly score is then calculated with these isolation tree weights taken into account)
    - Dit moet gebaseerd zijn op expert feedback, want anders kan je nooit weten of een feature in de toekomst
    misschien wel anomalous/normal behaviour gaat vertonen
    - Komt erop neer om features die niets met anomalies te maken hebben, maar toch soms anomalous gedrag vertonen te droppen
    => manier vinden zoals de histogram ranking om deze te selecteren en een kort lijstje te tonen aan een CERN expert
    - goede features met lage variance (manueel bijhouden?):
      - bispulsevoltage
      - bispulsetime
    - slechte features die toch overblijven na variance filter droppen
    - Checken of er van een bepaalde set van features (voor de verschillende magneten) 1 wordt weggefiltert met variance filtering.
    Dan kunnen de andere van de andere magneten misschien ook weggefiltert worden als de variance ook redelijk laag is.
    (hangt er vanaf natuurlijk voor als er 1 magneet faalt)
    - Opletten dat er geen goede features worden weggefiltert omdat die toevallig op de TP's en FN's hoogfrequente waardes hebben
    bij aggregated TP en FN (overfitting, CERN advies nodig of dit effectief geen nuttige feature is voor anomalies)
    
	  
# Week 06/04-12/04:
## Work:
- Checked the dec-mar period on rows where control=6 and null values (on SWAN). For 2017, only ~100 of ~1500 rows have control=6,
  so most of the IPOC timestamps have control!=6. A lot of them get dropped though, but not because of the control=6 filter.
- Ran pipeline with dropped features from Pieter, results (AUC/rank/TP/FN) seem worse at first hand. 
  This is not necessarily the cause of this feature dropping. The model could be really bad too. (A better model would
  then should give better results for this feature set dropping)
	- Mean value over a grid search (runs = 192 configurations)
	- Mean value over multiple pipeline with same AD parameters (runs = 100)
- Spark support for beam filter
- Beam identification in config
- Beam wildcards for MKI querys (no B2 logbook though)  
- 2016 beam2
- Tried Spark 3 on LXPLUS, get_data_wrapper error
- Variance threshold 1D grid search experiment: new best variance is 0.1, but only slightly better and higher variance
  threshold gives a better dimensionality reduction, so sticking with 0.25, maybe even higher.
- Dimensionality needs to reduce for SHAP value calculation  
- Tried to change features to std rolling features (can be helpful for the live evaluation) 
  => subtract the std up to that point in time from the datapoint.
- restructured /data-cern/ml-data/ folder into subfolders with machine/year/...
- basic live evaluation, results are the same as predicting on train dataset. ```live_evaluation``` tag in config.
    

## Questions:
#### Questions for Elia/Pieter:
- BEIL query returnt geen data
- BEM query enkel voor magneet "I"

- looked at 2016 beam2 dataset for the triggertimestamp feature
and here, the FP's are much less compared to 2017 in this period, but the FN's are higher,
  so probably too high threshold.
- So the results that were "good" may actually not be that good and the model has not learned any system-specific anomaly information (many FP's).
  But there are a lot of TN's so the model does learn that normal behaviour occur at frequent datapoints.
- Dimensionality reduction?

(feature selection)
- variance threshold
- wrapper method

(feature engineering)  
- Some features are very similar:
 	- Same feature for different magnets
 	- exact same histogram rankings for 'all TP/FN/FP' (different feature for same magnet)
	- Make correlation 1212x1212 matrix and create "aggregated anomaly features"
Use this correlation: Aggregate them in a way such that outliers are detected more easily: (difference from the mean of all features squared => transformation not on data, but on a metric that measures outlier behaviour) 
or difference from the mean divided by std. For the explainability: take the feature from this set with highest deviation as explanation for the anomaly at that timestamp
= I think this works because normal behaviour will be almost always zero and a deviation will explode with the square = more easily seperated datapoints that have more chance to be anomalous.
	  = manual deciding what is anomalous and what is not.
Transformation on the dataset itself would not work because points that are close to "1" can too be anomalous.  
	  
- Basic live evaluation results are the same "good results" as predicting on train data. 
  I think this is a bad sign. The model may not be learning anything context specific (only generic anomaly detection)
  and the observed results for both train and test are thus the same?
- Predicting with a model that has learned on 1 day data, does a lot worse (double FP's/10% AUC), so it does learn something
  

# Week 13/04-19/04:
## Work:
   

## Questions:
#### Questions for Elia/Pieter:



# Week 20/04-26/04:
## Work:

- Implemented sliding window features for LBDS; this gives a non expected result.
Most sliding window features have 0 everywhere so are filtered away by the variance filter. 
The reason for this could be that there are not enough datapoints (rows) (or a bug/wrong time window size).'
- Found wierd global behaviour for Error features around april 2016 (unusual large spike)
    - maybe introduce a "super" outlier filter, that filters on non-realistic values?


## Questions:
#### Questions for Elia/Pieter:
- See meeting slides


# Week 27/04-03/05:
## Work:
- made poster for next week poster session

(feature selection)
- When selecting a feature of a certain magnet, all other features of the same type from the other magnets are selected too (toggle)
    - Did all below experiments for both settings to compare, no other magnet selection seems to overfit for SHAP
- experiments:
    - number of features vs AUC/rank
        - SHAP
            - TP FN FP (delete positive % but always select all negative FP SHAP)
            - FP (delete positive %)
        - Histogram
            - TP FN FP (delete top %)
            - FP (delete top %)
- results:
    - nb_features vs best and average AUC/rank graphs for above experiments
    - comparison between SHAP values and Histogram score (SHAP better)
- Wrote part of the feature selection chapter text
    - explanation about Histogram score and SHAP values usage
    - Results of the experiments



## Questions:
#### Questions for Elia/Pieter:
(wrong measurement)
- All BEI MKCBI energyErrorAbs and EnergyErrorRel features have 1 single "super" outlier/extreme value in april 2016 (B2) and 2017 (B1) but not in 2018 (B1)
=> solved with new SCSS filter on new SCSS features

- Histogram score feature selection does not seem to have an effect, the AUC and rank just
become worse when selecting less features. This could be an indication that the biggest part of the
anomalous behaviour of the system is non-global.
    - Local histogram ranking. Time frame?
    - Sliding window features
- SHAP values work better than Histogram ranking, because
    1) The average and best AUC/rank is higher then Histogram results
    2) The AUC/rank stays constant when decreasing the number of selected features 
    (until a critical point of ~100-150 features where the system just doesn't have enough information to do anomaly detection)
- Future work:
    - Inspect other types of anomalies: The result of the Histogram score feature selection suggests that there
    are almost no global outliers, this means that most of the anomalies are either 
        - local (in its context/neighbourhood): Approaches could be an analysis/score with local histograms or sliding window features
        to include this "time"/"context" information
        - local (in a certain subset of features): The anomaly has a perfectly "normal" value globally, but is an outlier in a certain subset of features
        Approach: AD-MERCS, greedy feature selection search algorithm (not feasible for 1000+ features, so possible on a subset)
        - "accidental" inlier: The anomaly has a perfectly normal value in the observed feature, but is for example contextually anomalous in another feature.
        Approaches: AD-MERCS
- maybe introduce a "super" outlier filter, that filters on non-realistic values?
- See meeting slides

----
# Week 27/04-03/05:
## Work:
- Poster session

(feature selection)
- Feature selection experiments and 4 different pipeline runs with AD parameters of the best AUC and best rank (to take average)
and write the means to csv for more representative results and to be able to draw conclusions from the plots.
- Percentage and absolute values to choose when adding features of all magnets (when 1 magnet is selected) are now modified
 such that selected nb_features don't cluster at the end for high chosen number of features. e.g. The maximum number
of chosen values is 1000, then with 15 magnets we are already almost approaching 1000 at a absolute value of 1000/15=67.
- Feature selection experiments for new 2016 data (with extra SCSS filter): Beam 1 and Beam 2
    - SHAP feature selection
    - Histogram feature selection
    - Random feature selection
- [feature_selection.py](src/scripts/feature_selection.py) script
- [scripts README](src/scripts/README.md) (WIP), sHAP feature selection explanation done
- Spark 2 working again with old nxcals bundle

(thesis text)
- Overview of the chapters
- Feature selection chapter
- Feature selection results for FP

## Questions:
#### Questions for Elia/Pieter:
(Elia)
- If results are not what we expected from hypothesis, should you explain
why you think the results are not as previously thought or be very objective and only "accept" the given results
and don't speculate on why this strategy didn't work, ...
- how far should you go in "trying" to explain why the observed result is not what we expected?
- Put all the "speculation" and hypothetical explanations in conclusion or translate to suggestions for future work chapter? or not at all?

(Pieter)
- Histograms and SHAP values all explain the effect of 1 feature at a time. 
Are there known correlations between certain features concerning anomalous behaviour that we can exploit? (AD-MERCS should do this)
- For the variance filter, do same features for other magnets need to be kept too?
This would be consistent with the feature selection, but will probably add thousands of features.

# Week 04/05-10/05:
## Work:
- 1 script for SHAP/Histogram/random feature selection experiments
- Feature selection experiment with more runs (100 for best, 25 for random) => more datapoints to take an average from.


## Questions:
#### Questions for Elia/Pieter:
- How much/how detailed explanation of potential causes for the results we observe in thesis text?
=> put this into a discussion segment





# Week 11/05-17/05:
## Work:
- added max_nb_features attribute to select the maximum possible number of features automatically
- TP feature selection experiments on new 2016 beam 1 and 2 data on LXPLUS machines
- Train/test data split experiment:
	- Test data 2nd half of 2016, train data incremental per week of 1st half of 2016. 
	Results are similar (constant AUC for all number of weeks) => randomisation problem



(TODO)
- Test with dummy detector
- Test with noise data
- Test each feature independently


## Questions:
#### Questions for Elia/Pieter:
- Voor SHAP values in webapp moet je ook de juiste prediction file hebben (want die bepaalt de TP/FP/FN/TN waarop de shap values zijn berekend)
- Live evaluation experiments nuttig als random feature selection al zo hard fluctueert?
- hoe gedetailleerd in thesis over spark deel? Niet echt anomaly detection, enkel data processing



# Week 06/07-12/07:
(july-august period, refresher needed)
## Work:
- Overview of remaining work/plan
- AUPRG is now an option in the scoring method instead of the (now removed) auc_to_use feature  
- More AUPRG experiments in notebooks ([1](notebooks/AUPRG%20vs%20AUPR.ipynb), [2](notebooks/first%20detected%20instance%20location%20vs%20AUPRG%20value.ipynb), [3](notebooks/first%20detected%20instance%20location%20vs%20AUPR%20value.ipynb), [4](notebooks/first%20detected%20instance%20location%20vs%20rank%20value.ipynb))
	- Result: The AUPRG also has a problem: it is highly correlated with the position of the first TP in the predictions list (sortind predictions descending on anomaly score)
	- I think AUPR is a better choice in this thesis (and rank of course). To solve the AUPR issue, we can maybe split up the instances where the first (or/and second) prediction is TP from those where the first TP occurs later.
	Or maybe have a well chosen decreasing function dependant on the position of the first TP such that the AUC values become comparable.
	=> consequence is that the best run of the grid search will almost always be one which has a TP on number 1. 
	  But this order does not really matter that much in our application.
- Fixed horizontal bar plot issues in webapp


## Questions:
#### Questions for Elia/Pieter:
Waar waren we gebleven:

(high level analyse, aggregated number as evaluation)
- AUPRG als alternatief voor AUC (AUPR), rank werkte beter dan AUC

(low level analyse, anomaly per anomaly)
- Een plot om gedetecteerde anomalies te vergelijken tussen verschillende feature sets (SHAP/Histogram/normaal)

Voor in de thesistekst:
- AUPRG niet super bruikbaar, ook in thesis? Niet echt informatief voor de lezer als iets niet veel bijdraagt (vergeleken met AUC).
- Features uitleg die niet in Kyle zijn thesis zijn uitgelegd (via mail best zodat we een overzicht behouden)
- Filters uitleg en in hoeverre gedetaileerd de reden 
- Welke soort data zijn deze features? (continu, pulse, discrete values, ... )
- 1 voorbeeld van een anomaly in LBDS in de thesistekst gelinked aan het verwachte anomalous behaviour in de feature values en (hopelijk) gedetecteerd/zichtbaar in data.


Mijn voorstel:
- AUC en AUPRG laten liggen voor wat het is, de problemen hiermee zijn artefacten van het weinig aantal anomalies en zullen met meer anomaly voorbeelden verdwijnen.
- Enkel nog de low level analyse per anomaly doen zodat we een voorbeeld kunnen vinden van een gedetecteerde en niet gedetecteerde anomaly.
- High level analyses zoals de SHAP/Histogram/random, zijn momenteel onevalueerbaar door de variatie in het begin van de PR curve (als de eerste prediction een TP is of niet). Hetzelfde geld voor PR-Gain. 
De rank had ook hoge variantie bij de resultaten, maar nog niet bekend waarom dit is.
- We kunnen wel een vergelijking maken van SHAP/Histogram/Random op lower level bij 1 geselecteerde thresholdwaarde (TP/FP/FN/TN).
Deze kan geselecteerd worden adhv het dichtste punt op de PR-curve bij (1,1).




-----------------------------------------
# TODO LIST
## Short term TODOs:
(evaluation)
- Set the threshold very low for low amount of FP and (almost) all actual anomalies FN and select features
 with positive SHAP value of aggregated FN SHAP ranking. (and maybe histogram ranking)

~~CLEANUP OF FILES THAT AREN'T CALLED => before-cleanup branch to revert~~

(live eval)
- Handle different number of features for different years => also in same year because of variance filter

(LBDS postprocessing)
- ~~Check if LBDS postprocessing with segmentation works correctly, otherwise replace by simpler/different code~~

(webapp)
- Show state_mode graph for mki like in wsgi_old.py
- check statemode handling in webapp
- statemode retrieval (import seperate file!)

- ~~Add plots from plot_evaluation.py in seperate tabs (after IO class)~~
- ~~Anomalies vs detectors plot~~
- Confusion matrix metrics versus parameters (with parameter checkbox/ choose anomaly detector)
- Feature statistics plot

(IO)
- ~~Add IO module to compact and generalise filenames/IO paths/reading,writing csv's~~
- ~~Pipeline output should be a csv with the predictions and a yaml file in the same folder containing the metadata 
(anomaly detector/ad_params/threshold/...). These should be linked by an unique identifier (uuiv4?) in the filename~~
~~=> Querys on this pipeline output then read in all the yaml files and depending on the wanted metadata, can open the right corresponding csv predictions file~~
(run all grid_searches again after this to generate these files)
~~(easy to import data to the plots in this way)~~

#### Codebase:
(config)
- ~~Add a section for the webapp with all webapp configuration/defaults~~
- Add a section for the IO with all filenames/pa
- Make lower time_intervals implicit on Spark time_intervals [0] and [-1]

(code general)
- ~~Make overview of the differences between files in repo2019/repo2020~~
- ~~Tekstje MKI Thiebout code aanpassen (korte versie maken met enkel commands) => .md formaat en doorsturen/op mki repo zetten?~~
- ~~Thesisnotas opkuisen en in md formaat en short term, long term todos en pushen naar repo~~
- ~~Try out connecting to NXCALS and Spark querying + retrieving results (spark notes Kyle repo)~~

This was tested on Spark 2, with the upgrade to Spark 3 it is possible that pandas UDF divides the work better over executors:
- ~~Try to reproduce Thiebout his results to get an understanding of the pipeline steps (Thiebout used SW but not FFT features)~~
- ~~Try to see if ```thiebout_code_installation_notes_short.md``` works on the new repo (backwards compatible or not?)~~ => no
- Solve Spark window function bottleneck:
    - ~~Write reread dataframe before/after window operatio~~
    - ~~Convert Python udf to pandas udf~~
    - Try to do resampling different
    - memory footprint on the driver (see Luca message)

(config)
- Reduce the redundancy of grid_search -> pipeline
- Make the time_intervals (and maybe other variables too) override eachother with Spark being the most important
then local_prep<grid_search<pipeline (such that one can easily configure 1 fixed time_interval by only filling it in once at Spark config
  and leave the other ones empty)
- update config manual

(config during local preprocessor merge)  
- Remove irrelevant parameters such as 'host'
- configure the MKI fft and SW features
- Use the fft and SW toggles in the code
- Use the beam filter toggle in the code
- Should be able to filter on beam at each step (if data and logbook allows this)
- make some things config dependant, like timestamps, acqStamp in Spark instead of variable name in code

(local)
- lbds preprocessor (preprocess features) data cutting op time_interval!! (seems like this doesn't happen)
- investigate lbds preprocessing multi input warning when reading in features from 2017 01-05


- ~~Test new code with data of longer periods so there are some anomalies~~
- ~~enable (and test) the variance threshold filtering (line 276 in util)~~
- ~~Move 18-19 extreme value filters to 19-20 extreme value filters using config~~
- ~~Script to automatically download feature and statemode data from CernBox if this is possible (after just generating it)
=> done, but manual download links are still an issue~~

~~- See what temporal features are not useful/can be removed (like status data)~~
~~=> Turn on variance filtering to see if they get filtered out~~

(Spark)
- Remove columns without any values (all missing) (done in local preprocessing atm)
- See difference between 'left' and 'outer' join, left is used in Spark preprocessing and to generate statemode data and outer is used in local preprocessing (see def ipoc_and_state_data() in prep modules) 
- Let the script rename the csv files that are in the folders (read in config file and rename/move according to dates)

(global)
- ~~Make scheme of how the code currently works (all different preprocessing steps and files that must be run)~~
- Add Spark preprocessing to scheme
- Update config file guide
- ~~Run old get_data.py from 2017-05-01 until 2017-06-01 to see if disconnect error also occurs there (before changes to code). => didnt occur anymore, was probably a Kerberos token authentication error. Worked again after keytab refresh.~~

- ~~Fix webapp to see data of anomalys (feedback loop)~~
- LBDS Feature engineering:
    - Time information (sliding window/Fourier) for relevant features (temp/pressure/current)
- Second layer that "learns" on the output of current pipeline/meta-analysis

- Make postprocessing more efficient by only checking the anomalys between the given time period
- Feature input data caching after pipeline_ad.py preprocessing (variance filter) for grid search because the same input file should be used for each grid search run (same time interval).
- split up get_training_data_from_file() in util.py and move to preprocessor.py or a seperate file with preprocessor class

- Less important: Replace Segmentation structure for LBDS by something else (not necessary because every segment is 1 entry) = more efficient

Restructure preprocess: make a preprocesser Object
1)  p.read_input_features()
2)  p.preprocess()
3)  p.write_output()

- ~~Test GMM on LBDS (make code more efficient?)~~
- Merge LBDS and MKI local preprocessing
- Move all preprocessing operations from ad_pipeline to local preprocessing (except maybe variance column removal? because it is time interval dependent (make cache))
- ~~Do grid_searches for LBDS and get best results~~
- ~~Do LBDS grid_searches on larger dataset~~ => good results
- More data on LBDS! 2015, ~~2016, 2018~~
- ~~Beam 2 for LBDS~~
- ~~LBDS multi input~~
- ~~Make skeleton of config files same~~
- ~~Spark: Check if columns get dropped that aren't needed~~ Yes they get dropped (from config)
   - see if BUNCH columns get dropped correctly (from MKI config)
- ~~Why does Spark get data for beam 1 and 2 for LBDS but only for one certain beam for MKI?~~ Fixed with beam filter
- ~~Rename "raw" and "prepped" input feature csvs of MKI~~
- Put get_data time plot in repo
- ~~Add overleaf link in repo~~

- ~~make new_installation2 on Spark to get data quicker~~
- Generalise MKI local state_mode folder getter (for each year)

- ~~lbds spark data 2017 11-01 doesn't get ffilled all the way through (maybe because of spark memory error?) => is correct now with coalesce instead of repartition (no shuffle)~~
This was because certain features are not really synchronized with the IPOC timestamps. 
The resampling process on IPOC timestamps selects almost always a zero for these features instead of a useful value (which occur at different timestamps).


- ~~See differences between current Spark mki data and Thiebout MongoDB data~~

- ~~See what happens to the "IPOC_MKI.UA23.IPOC.B1_first(delay_IM_TMR_A,false)" column for each magnet~~
~~(they disappear during preprocessing). => it doesn't get selected by the regexes in config that are used in build_features_multiprocessed() in~~ [```preprocessing_modules.py```](src/preprocessing/preprocess_modules.py)

- ~~Link to progress doc in GitLab~~
- ~~Some columns get ffilled with 0's instead of a regular ffill, check this (also in MKI) => async data with IPOC timestamps, so a lot of zeros during resampling on IPOC timestamps~~

- ~~check extreme filtering for LBDS~~
- drop cols for LBDS in beginning of local prep or on Spark? like \_\_record_version__
- ~~LBDS acqStamp column name is not set to timestamps~~
- ~~raw LBDS input features aren't sorted on timestamp! Try to fix this in Spark => coalesce instead of repartition~~

- merge mki and lbds preprocessing
	- ~~add concat to LBDS~~
	- remove hardcodings
	- ...
- dropping unneccesary columns should be done on Spark

(anomaly detection)
- ~~Make evaluation compatible with LBDS (segment/statemode mode and entry/timestamp mode)~~


(MKI webapp)
- change data input from MongoDB to csv


#### General:
- ~~Mail naar promotors voor engelse thesistekst~~
- ~~Problem Statement?~~
- ~~Make slides first presentation~~
- ~~Read Guillaume's report and code changes~~



## Long term TODOs:
#### Codebase:
- Timestamp based segmentation should not be done via Thiebout his code because it takes too long to build a whole segment class instance for each timestamp (for LBDS)
~~This is also wrongly implemented atm (for LBDS) because 2 tuples get assigned for each segment (should be 1, the row itself)~~

- Move for example 2018 "edge case" timestamps also into config, and in long term, merge whole CERNCsvClient (whole database) folder
with all the other "matrix operations" that happen around it.
=> Code should provide functions to do matrix operations, and a function per type of matrix operation (to subdivide in config) that calls these matrix operations.
The control is done by specifying what operations to do in config, so the config becomes a "pseudo-language" for data manipulations.

- After merging preprocessing of MKI and LBDS locally, move this to Spark

- ~~CI begrijpen (en fixen)~~
- PyLint?
- ~~Restructure the get_data.py and mki_test_B1.yaml config file such that it outputs the same/almost the same data as Thiebout his data~~
- ~~Implement the extreme filters from Thiebout (see Guillaumes report), branch fix-filter-extremes-ipoc-segments~~
- Restructure the get_data.py and config files such that it automatically splits the time intervals up into smaller pieces
because currently the query time doesn't scale linearly with the time interval. Or for example when a change in data storage
occurred => different kind of query/processing (see last paragraph in [spark readme](src/spark/README.md)), ...
- ~~Check if the ipoc segmentation in postprocessing still works~~

- Detach the "machine learning" (research) as much as possible from the code itself.
Ultimately you should be able to do machine learning by only interacting with the config file (drop column/timestamps/filters/...)

- Restructure [```preprocessor.py```](src/preprocessing/preprocessor.py) such that it is abstracted into 1 preprocessor, and all the differences between machine (MKI/LBDS) or year (16/17/18) are dependant on the passed config file 


Less important (first make code work, then refactor):
- add errormss if config load fails in util.py (catch errors/exceptions)?: https://stackoverflow.com/questions/1773805/how-can-i-parse-a-yaml-file-in-python
- Change preprocessing/prep_factory.py to polymorphism instead of if/else checking on config string preprocessor type?
- Refactor large functions in database
- Make 1 script to run everything (if possible) and ask interactive questions inbetween to continue with the pipeline or to stop (less copypasting from readme to terminal)
- ~~Test the config files (different values)~~
- Multithread local preprocessing? (parallel parameter)

Webapp:
- Can't close errors popup

#### Anomaly detection:
- ~~Check if training/test is using cross validation? or is this not useful here~~
=> ~~Anomaly detection can just use the training dataset as a testing dataset, so no cross validation needed.~~

#### General:
- Give an urgency/importance to TODOs
- Own Spark project to test out locally/try out queries on a small dataset?

- Make scheme of codebase/pipeline
- Make "import" diagram (dependancy diagram)
- Sequence diagram from data request call until the anomaly scores output with subdiagrammas:
	- Sequence diagram of remote data collection, processing and download
	- Sequence diagram of local data processing
	- Sequence diagram of local ML, evaluation(, visualization)

##### Reading:
- ~~Search about tmux~~
- Understand GMM in detail
- Understand IForest in detail
- ~~Understand HBOS in detail~~
- Understand LOF in detail
- Read about the MKI and LBDS machines
- Read remaining chapters of Aggarwal
- Read up in general the literature on gitlab (make overview on notion.so)
- ~~Read about spark structure~~
- ~~Read links in interessante bronnen.txt => move to notion.so~~

